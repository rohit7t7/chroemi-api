<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'login/index';

$route['dashboard'] = 'dashboard/index';
$route['dashboard/logout'] = 'dashboard/logout';

$route['eventcategory/index'] = 'eventcategory/index';
$route['alleventcategory/create'] = 'eventcategory/create';
$route['eventcategory/create'] = 'eventcategory/create';
$route['eventcategory/edit/(:any)'] = 'eventcategory/edit/$1';
$route['eventcategory/details/(:any)'] = 'eventcategory/details/$1';
$route['eventcategory/update/(:any)'] = 'eventcategory/update/$1';
$route['eventcategory/delete/(:any)'] = 'eventcategory/delete/$1';

$route['events/index'] = 'events/index';
$route['allevents/create'] = 'events/create';
$route['events/create'] = 'events/create';
$route['events/edit/(:any)'] = 'events/edit/$1';
$route['events/details/(:any)'] = 'events/details/$1';
$route['events/update/(:any)'] = 'events/update/$1';
$route['events/delete/(:any)'] = 'events/delete/$1';

$route['attendee'] = 'attendee/index';
$route['allattendee/create'] = 'attendee/create';
$route['attendee/create'] = 'attendee/create';
$route['attendee/edit/(:any)'] = 'attendee/edit/$1';
$route['attendee/details/(:any)'] = 'attendee/details/$1';
$route['attendee/update/(:any)'] = 'attendee/update/$1';
$route['attendee/delete/(:any)'] = 'attendee/delete/$1';


$route['organizers'] = 'organizers/index';
$route['allorganizers/create'] = 'organizers/create';
//$route['organizers/create'] = 'organizers/create';
$route['organizers/edit/(:any)'] = 'organizers/edit/$1';
$route['organizers/details/(:any)'] = 'organizers/details/$1';
$route['organizers/update/(:any)'] = 'organizers/update/$1';
$route['organizers/delete/(:any)'] = 'organizers/delete/$1';

$route['eventorder/index'] = 'eventorder/index';
$route['eventorder/confirm'] = 'eventorder/confirm';
$route['eventorder/cancel'] = 'eventorder/cancel';
$route['eventorder/latestorder']='eventorder/latestorder';
$route['eventorder/allorder']='eventorder/allorder';
$route['eventorder/search']='eventorder/search';

$route['eventfeedback/index'] = 'eventfeedback/index';
$route['eventfeedback/edit/(:any)'] = 'eventfeedback/edit/$1';
$route['eventfeedback/details/(:any)'] = 'eventfeedback/details/$1';
$route['eventfeedback/update/(:any)'] = 'eventfeedback/update/$1';
$route['eventfeedback/delete/(:any)'] = 'eventfeedback/delete/$1';

$route['content'] = 'content/index';
$route['allcontent/create'] = 'content/create';
$route['content/edit/(:any)'] = 'content/edit/$1';
$route['content/details/(:any)'] = 'content/details/$1';
$route['content/update/(:any)'] = 'content/update/$1';
$route['content/delete/(:any)'] = 'content/delete/$1';

$route['paymenthistory/index'] = 'paymenthistory/index';

$route['commission/index'] = 'commission/index';
$route['commission/edit/(:any)'] = 'commission/edit/$1';
$route['commission/update/(:any)'] = 'commission/update/$1';

$route['permission/index'] = 'permission/index';
$route['permission/create'] = 'permission/create';
$route['permission/edit/(:any)'] = 'permission/edit/$1';
$route['permission/update/(:any)'] = 'permission/update/$1';
$route['permission/delete/(:any)'] = 'permission/delete/$1';

$route['paymentgateway/index'] = 'paymentgateway/index';
$route['paymentgateway/create'] = 'paymentgateway/create';
$route['paymentgateway/edit/(:any)'] = 'paymentgateway/edit/$1';
$route['paymentgateway/update/(:any)'] = 'paymentgateway/update/$1';
$route['paymentgateway/delete/(:any)'] = 'paymentgateway/delete/$1';

$route['paymentcredentials/index'] = 'paymentcredentials/index';
$route['paymentcredentials/create'] = 'paymentcredentials/create';
$route['paymentcredentials/edit/(:any)'] = 'paymentcredentials/edit/$1';
$route['paymentcredentials/update/(:any)'] = 'paymentcredentials/update/$1';
$route['paymentcredentials/delete/(:any)'] = 'paymentcredentials/delete/$1';

$route['globalsearch'] = 'globalsearch/index';
$route['globalsearch/gensearch'] = 'globalsearch/gensearch';
$route['globalsearch/supervisorsearch'] = 'globalsearch/supervisorsearch';



/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/v1/api/(:any)'] = 'api/(:any)';

$route['api/v1/auth/index/(:num)'] = 'api/v1/auth/index/id/$1';

$route['api/v1/attendee/index/(:num)'] = 'api/v1/attendee/index/id/$1'; // Example 4
// $route['api/v1/attendee/index/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/v1/attendee/index/id/$1/format/$3$4'; // Example 8
// $route['api/v1/attendee/index/(:num)'] = 'api/v1/attendee/index/id/$1'; // Example 4
// $route['api/v1/twilio/index/(:num)'] = 'api/v1/twiliodemo/index/id/$1';
