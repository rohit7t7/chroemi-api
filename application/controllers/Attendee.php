<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendee extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('attendeemodel');
		$this->load->model('languagemodel');
		$this->load->library('upload');
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}

			if($this->session->userdata('site_lang') == 'english') 
				$this->langid = '1';
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->langid = '2';
	}
	
	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
			$data = $this->attendeemodel->get_users('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data = $this->attendeemodel->get_users('2');

		$this->template->show("attendee", "index", $data);
	}


	public function create()
	{
		
		$data['languageData']=$this->languagemodel->get_languages();
			
		$is_unique ="";
		if($this->input->post('email'))
		{
			$getstatus=$this->basemodel->getDeleteStatus('attendee','email',$this->input->post('email'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_attendee.email]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_attendee.email]';
			}
		}


		$this->form_validation->set_rules('name', $this->lang->line('name'), "trim|required");
		$this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|valid_email'.$is_unique);
		$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|min_length[10]|max_length[10]|regex_match[/^[0-9().-]+$/]');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required');
		//$this->form_validation->set_rules('professionaldetails', $this->lang->line('professional') .' '.$this->lang->line('details'), 'trim|required');
		 $this->form_validation->set_rules('image', $this->lang->line('image'), 'callback_handle_upload');

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("attendee", "create", $data);
          else
          {
          	
		   $cdata['name'] = $this->input->post('name');
		   $cdata['email'] = $this->input->post('email');
		   $cdata['phone'] = $this->input->post('phone');
		   $cdata['password'] = md5($this->input->post('password'));
		   $cdata['gender'] = $this->input->post('gender');
		   $cdata['dob'] = $this->input->post('dob');
		    $cdata['address'] = $this->input->post('address');
		    //$cdata['personaldetails'] = $this->input->post('personaldetails');
		  // $cdata['professionaldetails'] = $this->input->post('professionaldetails');
		   $cdata['createddate']= date("Y-m-d H:i:s");
		  
		   $cdata['language_id']= $this->langid ; 
		   $cdata['status']= $this->input->post('status'); 

			   if($_FILES['image']['name']!='')
			   {
                               //echo $_FILES['image']['name']; die;
			   		 $uploadedfilename = $this->doImageUpload();	
			   		 $cdata['image'] = $uploadedfilename;

			   }
		   
			
			$res = $this->attendeemodel->savedata($cdata);

			if($res)
			{
				if($this->session->userdata('site_lang') == 'english')
				$this->session->set_flashdata('msg', '<div class="alert alert-success">User added successfully</div>');
				elseif($this->session->userdata('site_lang') == 'indonesia')
				$this->session->set_flashdata('msg', '<div class="alert alert-success">pemakai berhasil ditambahkan</div>');
	        	


	        	redirect('attendee');
			}
          }

              	
	}

	public function details($id="")
	{
		
		 $data['modelData'] = $this->attendeemodel->getById($id);
		 $data['languageData']=$this->languagemodel->get_languages();
		$this->template->show("attendee", "details", $data);
		      	
	}

	public function edit($id="")
	{
		
		$data['modelData'] = $this->attendeemodel->getById($id);
  		$data['languageData']=$this->languagemodel->get_languages();
                $this->template->show("attendee", "edit", $data); 	
	      	
	}

	public function update()
	{

		$data['modelData'] = $this->attendeemodel->getById($this->input->post('id'));
		 $data['languageData']=$this->languagemodel->get_languages();
		
		 $is_unique ="";
		if($this->input->post('email') != $data['modelData']['email'])
		{
			$getstatus=$this->basemodel->getDeleteStatus('attendee','email',$this->input->post('email'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_attendee.email]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_attendee.email]';
			}
		}
		

		$this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required');
		$this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|valid_email'.$is_unique);
		$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|min_length[10]|max_length[10]|regex_match[/^[0-9().-]+$/]');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required');
		//$this->form_validation->set_rules('professionaldetails', $this->lang->line('professional') .' '.$this->lang->line('details'), 'trim|required');
		 $this->form_validation->set_rules('image', 'Image', 'callback_handle_upload');
	
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("attendee", "edit", $data);
          else
          {
		 	 $cdata['name'] = $this->input->post('name');
		   $cdata['email'] = $this->input->post('email');
		   $cdata['phone'] = $this->input->post('phone');

		    if($data['modelData']['password'] == $this->input->post('password'))
		   		$cdata['password'] = $this->input->post('password');
		   	else
		   		$cdata['password'] = md5($this->input->post('password'));


		   $cdata['gender'] = $this->input->post('gender');
		   $cdata['dob'] = $this->input->post('dob');
		    $cdata['address'] = $this->input->post('address');
		  //  $cdata['personaldetails'] = $this->input->post('personaldetails');
		  // $cdata['professionaldetails'] = $this->input->post('professionaldetails');
		  // $cdata['createddate']= date("Y-m-d H:i:s");
		  
		   $cdata['language_id']= $this->langid ; 
		   $cdata['status']= $this->input->post('status'); 

			   if($_FILES['image']['name']!='')
			   {
                               
			   		 $uploadedfilename = $this->doImageUpload();	
			   		 $cdata['image'] = $uploadedfilename;

			   }
		   
			
			$res=$this->attendeemodel->update_info($cdata, $this->input->post('id'));


		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">User updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">pemakai berhasil diperbarui</div>');
		        


		        redirect('attendee');     	
	         }
         }

         
	}

	public function chkuserstatus($userid)
	{
		//current date & time
		$currdt = date('Y-m-d');

		$qrord =  $this->db->select('o.*');
		$qrord = $this->db->where('o.attendee_id', $userid);
		$qrord = $this->db->where('o.deletestatus', '0');
		$qrord = $this->db->where('MID(o.order_date,1,10) >=', $currdt);
		$qrord = $this->db->get($this->db->dbprefix.'_orders as o');

		//echo $this->db->last_query();
		
		$num = $qrord->num_rows();

		

		return $num;
	}

	public function delete()
	{

		$id = $_POST['userid'];

		
		$cntusr= $this->chkuserstatus($id);

		if($cntusr == '0')
		{
			$res=$this->attendeemodel->delete_info($id);

			if($this->session->userdata('site_lang') == 'english')
	 			echo "0";
	 		elseif($this->session->userdata('site_lang') == 'indonesia')
	 			echo "1";
		}
		else
		{
			if($this->session->userdata('site_lang') == 'english')
	 			echo "2";
	 		elseif($this->session->userdata('site_lang') == 'indonesia')
	 			echo "3";
		}

	 	

	 	/*if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">User deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">pemakai dihapus diperbarui</div>');
			


			redirect('users');  
		}*/

	}

	function handle_upload()
  {

  	$imgfiletype = array("image/gif", "image/jpg", "image/png", "image/bmp","image/jpeg");
  			
  		
	if ($_FILES['image']['name']!='')
      {
      	if(!in_array($_FILES['image']['type'],$imgfiletype))
      	{
      		 // throw an error because nothing was uploaded
		      $this->form_validation->set_message('handle_upload', "You must upload an image!");
		      return false;
      	}
      	 else
	    {
	     	return true;
	    }
	     
    }
		   
  }

	public function doImageUpload()
	{
		
	      $config['upload_path'] = './upload/attendee'; 
	      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	      $config['overwrite'] = TRUE;
	      //$config['max_width']  = '2048';
	      //$config['max_height']  = '2048';
	      
	        //store image info once uploaded
          $image_data = array();

	       if (!$_FILES) 
	       {
	       		$this->template->show("attendee", "create", $data);
	       		return false;

            }
            else
            {
       
            	 /* Load the image library */
			      $this->load->library('upload',$config);

			       $this->upload->initialize($config);

			      	if ($this->upload->do_upload('image')) 
			      	{

			      		$imagedata = $this->upload->data();
                		$this->load->library('image_lib');
                		$target_path = './upload/thumbnail/attendee';

                
                		$config_manip['image_library'] = 'gd2';
	                    $config_manip['source_image'] = $imagedata['full_path']; //get original image
	                    $config_manip['new_image'] = $target_path;
	                    //$config_manip['create_thumb'] = TRUE;
	                    $config_manip['maintain_ratio'] = TRUE;
	                    $config_manip['width'] = 75;
	                    $config_manip['height'] = 50;

		                    $this->load->library('image_lib', $config_manip);

		                    $this->image_lib->clear();
							$this->image_lib->initialize($config_manip);
							$this->image_lib->resize();

							return $imagedata['file_name'];
                	
		            } 
		            else 
		            {
		     
		     			$this->template->show("attendee", "create", $data);
		     			return false;
 							
		            }

	                 
            }

          	           
		}


}