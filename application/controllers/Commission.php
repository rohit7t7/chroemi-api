<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commission extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');
		$this->load->model('commissionmodel');
		$this->load->model('currencymodel');
		$this->load->model('languagemodel');
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}
		if($this->session->userdata('site_lang') == 'english') 
				$this->langid = '1';
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->langid = '2';
		

	}
	public function index()
	{
		$data = $this->commissionmodel->get_commissions();
    	$this->template->show("commission", "index", $data);
	}


	
	public function edit($id="")
	{
		 $data['modelData'] = $this->commissionmodel->getById($id);
		 $data['currencyData'] = $this->currencymodel->getdefaultcurrency();
		 $this->template->show("commission", "edit", $data);      	
	}

	

	public function update()
	{

		$data['modelData'] = $this->commissionmodel->getById($this->input->post('id'));
		
		 $this->form_validation->set_rules('commission', 'Commission', 'required|regex_match[/^[0-9]+(\.[0-9]{0,2})?$/]');
		

		if ($this->form_validation->run() == FALSE)
		   $this->template->show("commission", "edit", $data);
          else
          {
          	 $cdata['type'] = $this->input->post('type');
		 	 $cdata['commission'] = $this->input->post('commission');
		  
			  $res=$this->commissionmodel->update_info($cdata, $this->input->post('id'));
		  		 	 
			 	if($res)
		         {
		         	//$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Commission updated successfully</div>');
		         	if($this->session->userdata('site_lang') == 'english')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Commission updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Komisi berhasil diperbarui</div>');
		        
			        redirect('commission');     	
		         }
         }
         
	}

 		
}
