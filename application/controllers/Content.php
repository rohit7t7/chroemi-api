<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('contentmodel');	
		$this->load->model('languagemodel');

		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}

				if($this->session->userdata('site_lang') == 'english') 
				$this->sellang =1;
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->sellang =2;



	}
	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
			$data = $this->contentmodel->getContent('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data = $this->contentmodel->getContent('2');
		

		$this->template->show("content", "index", $data);

	}

	

	public function create()
	{
		
		
		$data['languageData']=$this->languagemodel->get_languages();

		//$this->form_validation->set_rules('page_name', $this->lang->line('page').' '.$this->lang->line('name'), 'required|is_unique[gm_cms.page_name]|regex_match[/^[A-Za-z \\&-]+$/]');
		$this->form_validation->set_rules('page_name', $this->lang->line('page').' '.$this->lang->line('name'), 'required|regex_match[/^[A-Za-z \\&-]+$/]');
		$this->form_validation->set_rules('content', $this->lang->line('content'), 'required');
		
		

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("content", "create", $data);
          else
          {
          	
		   $cdata['page_name'] = $this->input->post('page_name');
		   $cdata['meta_title'] = $this->input->post('meta_title');
		   $cdata['meta_keywords'] = $this->input->post('meta_keywords');
		   $cdata['meta_description'] = $this->input->post('meta_description');
		   $cdata['content']= $this->input->post('content');
		   $cdata['language_id']= $this->sellang; 
		   $cdata['status']= $this->input->post('status');

			$res = $this->contentmodel->savedata($cdata);

			if($res)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Content added successfully</div>');
	        	redirect('content');
			}
          }

              	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->contentmodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();
         
         $this->template->show("content", "details", $data);      	
	}

	public function edit($id="")
	{
		$data['id'] = $id;

		 $data['modelData'] = $this->contentmodel->getById($id);
		 $data['languageData']=$this->languagemodel->get_languages();
		 

         if($this->input->post())
         {
         	//echo  html_entity_decode($this->input->post('content'));die;

			$this->form_validation->set_rules('page_name', $this->lang->line('page').' '.$this->lang->line('name'), 'required|regex_match[/^[A-Za-z \\&-]+$/]');
			$this->form_validation->set_rules('content', $this->lang->line('content'), 'required');
		
	
			if ($this->form_validation->run() == TRUE)
	          {

		 		$cdata['page_name'] = $this->input->post('page_name');
			   $cdata['meta_title'] = $this->input->post('meta_title');
			   $cdata['meta_keywords'] = $this->input->post('meta_keywords');
			   $cdata['meta_description'] = $this->input->post('meta_description');
			   $cdata['content']= $this->input->post('content');
			   $cdata['status']= $this->input->post('status');
			    $cdata['language_id']= $this->sellang; 
				$res=$this->contentmodel->update_info($cdata, $this->input->post('id'));


			 	if($res)
		         {
		         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Content updated successfully</div>');
			        redirect('content');     	
		         }
	         }
         }

         
         $this->template->show("content", "edit", $data);      	
	}

	
	public function delete($id)
	{

	 	$res=$this->contentmodel->delete_info($id);

	 	if($res)
	    {
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Content deleted successfully</div>');
			redirect('content');  
		}

	}

	
}