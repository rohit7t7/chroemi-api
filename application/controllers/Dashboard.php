<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

				
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}

			if($this->session->userdata('site_lang') == 'english') 
				$this->sellang =1;
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->sellang =2;



	}
	
	public function index()
	{
		
		$users = $this->db->query("select count(`id`) as cntid from ch_attendee where deletestatus ='0' and language_id=".$this->sellang);
		$usrdet= $users->row();

		if(isset($usrdet))
			$usercount = $usrdet->cntid;

		$services = $this->db->query("select count(`id`) as cntid from ch_events where deletestatus ='0' and language_id=".$this->sellang);
		$servicedet= $services->row();

		if(isset($servicedet))
			$servicecount = $servicedet->cntid;

		$categories = $this->db->query("select count(`id`) as cntid from ch_category where deletestatus ='0' and language_id=".$this->sellang);
		$catdet= $categories->row();
		
		if(isset($catdet))
			$catcount = $catdet->cntid;

		$feedback = $this->db->query("select count(`id`) as cntid from ch_feedback where deletestatus ='0'");
		$feedbackdet= $feedback->row();
		
		if(isset($feedbackdet))
			$feedbackdetcount = $feedbackdet->cntid;

		$supervisor = $this->db->query("select count(`id`) as cntid from ch_organizers where deletestatus ='0' and type = 2 and language_id=".$this->sellang);
		$supervisordet= $supervisor->row();
		
		if(isset($supervisordet))
			$supervisordetcount = $supervisordet->cntid;

		//$serviceorder = $this->db->query("select count(`id`) as cntid from ch_orders where deletestatus ='0'");
		//$serviceorderdet= $serviceorder->row();
		
		//if(isset($serviceorderdet))
			//$serviceorderdetcount = $serviceorderdet->cntid;

		//$conserviceorder = $this->db->query("select count(`id`) as cntid from ch_orders where deletestatus ='0' and order_status='1'");
		//$conserviceorderdet= $conserviceorder->row();
		
		//if(isset($conserviceorderdet))
			//$conserviceorderdetcount = $conserviceorderdet->cntid;

		//$cancelserviceorder = $this->db->query("select count(`id`) as cntid from ch_orders where deletestatus ='0' and order_status='2'");
		//$cancelserviceorderdet= $cancelserviceorder->row();
		
		//if(isset($cancelserviceorderdet))
			//$cancelserviceorderdetcount = $cancelserviceorderdet->cntid;

		//$pendingserviceorder = $this->db->query("select count(`id`) as cntid from ch_orders where deletestatus ='0' and order_status='0'");
		//$pendingserviceorderdet= $pendingserviceorder->row();
		
		//if(isset($pendingserviceorderdet))
			//$pendingserviceorderdetcount = $pendingserviceorderdet->cntid;

		$data['usercount'] = $usercount;
		$data['servicecount'] = $servicecount;
		
		$data['categorycount'] = $catcount;
		
		$data['supervisordetcount'] = $supervisordetcount;
		

		$this->template->show("dashboard", "dashboard", $data);
              

	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		$data['message_display'] = 'Successfully Logout';
		
		redirect(base_url());
	}




}
