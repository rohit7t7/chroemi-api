<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailcredentials extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('emailcredentialsmodel');
			
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}

	public function index()
	{
		 
		$data = $this->emailcredentialsmodel->get_emailcredential();
			
		$this->template->show("emailcredentials", "index", $data);
	}


	public function create()
	{

		$data = array();
		
		$this->form_validation->set_rules('host', $this->lang->line('host'), 'required');
		$this->form_validation->set_rules('port', $this->lang->line('port'), 'required');
		$this->form_validation->set_rules('username', $this->lang->line('user').' '. $this->lang->line('name'), 'required');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'required');
		$this->form_validation->set_rules('smtpsecure', $this->lang->line('smtpsecure'), 'required');
		$this->form_validation->set_rules('fromuser', $this->lang->line('from').' '.$this->lang->line('user'), 'required');
		$this->form_validation->set_rules('fromname', $this->lang->line('from').' '.$this->lang->line('name'), 'required');


		      if ($this->form_validation->run() == FALSE)
		      {
		      	 $this->template->show("emailcredentials", "create", $data);


		      }
		      else
		      {
		      	 	$cdata['host'] = $this->input->post('host');
				   $cdata['port']= $this->input->post('port'); 
				   $cdata['username']= $this->input->post('username'); 
				   $cdata['password']= $this->input->post('password'); 
				   $cdata['smtpsecure']= $this->input->post('smtpsecure'); 
				   $cdata['fromuser']= $this->input->post('fromuser');
				   $cdata['fromname']= $this->input->post('fromname');
				   $cdata['status']= $this->input->post('status');  

				 
				  	 $res = $this->emailcredentialsmodel->savedata($cdata);

					  	 if($res)
						{
							if($this->session->userdata('site_lang') == 'english')
							$this->session->set_flashdata('msg', '<div class="alert alert-success">Email Credentials added successfully</div>');
							elseif($this->session->userdata('site_lang') == 'indonesia')
							$this->session->set_flashdata('msg', '<div class="alert alert-success">Email Kredensial berhasil ditambahkan</div>');
				        	

				        	redirect('emailcredentials');
						}
				

					
		      }
          	
          		  
  	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->emailcredentialsmodel->getById($id);
		
		$this->template->show("emailcredentials", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->emailcredentialsmodel->getById($id);
         
         $this->template->show("emailcredentials", "details", $data);      	
	}

	public function update()
	{

		
		$data['modelData'] = $this->emailcredentialsmodel->getById($this->input->post('id'));
		
		$this->form_validation->set_rules('host', $this->lang->line('host'), 'required');
		$this->form_validation->set_rules('port', $this->lang->line('port'), 'required');
		$this->form_validation->set_rules('username', $this->lang->line('user').' '. $this->lang->line('name'), 'required');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'required');
		$this->form_validation->set_rules('smtpsecure', $this->lang->line('smtpsecure'), 'required');
		$this->form_validation->set_rules('fromuser', $this->lang->line('from').' '.$this->lang->line('user'), 'required');
		$this->form_validation->set_rules('fromname', $this->lang->line('from').' '.$this->lang->line('name'), 'required');


		  if ($this->form_validation->run() == FALSE)
		      {
		      	 $this->template->show("emailcredentials", "edit", $data);	
		      }
		      else
		      {
		      	 $cdata['host'] = $this->input->post('host');
				   $cdata['port']= $this->input->post('port'); 
				   $cdata['username']= $this->input->post('username');

				   //$cdata['password']= $this->input->post('password'); 
				//if($data['modelData']['password'] == $this->input->post('password'))
		   			$cdata['password'] = $this->input->post('password');
		   		//else
		   			//$cdata['password'] = md5($this->input->post('password'));	

				   $cdata['smtpsecure']= $this->input->post('smtpsecure'); 
				   $cdata['fromuser']= $this->input->post('fromuser');
				   $cdata['fromname']= $this->input->post('fromname');
				   $cdata['status']= $this->input->post('status');  

				    $res = $this->emailcredentialsmodel->update_info($cdata,$this->input->post('id'));
				    
					if($res)
					{
						if($this->session->userdata('site_lang') == 'english')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Email Credentials updated successfully</div>');
						elseif($this->session->userdata('site_lang') == 'indonesia')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Email Bukti Updated Berhasil</div>');
			        	

			        	redirect('emailcredentials');
					}
		      }
          		
		         
	}

	public function delete($id)
	{

	 	$res=$this->emailcredentialsmodel->delete_info($id);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Email credentials deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">kredensial Email berhasil dihapus</div>');
			

			redirect('emailcredentials');  
		}

	}

 		
}
