<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailtemplate extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');
		
		$this->load->model('emailtemplatemodel');
		$this->load->model('languagemodel');
		
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}

			if($this->session->userdata('site_lang') == 'english') 
				$this->langid = '1';
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->langid = '2';
	}
	
	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
			$data = $this->emailtemplatemodel->get_emailtemplate('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data = $this->emailtemplatemodel->get_emailtemplate('2');
		$this->template->show("emailtemplate", "index", $data);
	}

	public function edit($id="")
	{
		
		$data['modelData'] = $this->emailtemplatemodel->getById($id);
  		$data['languageData']=$this->languagemodel->get_languages();
        $this->template->show("emailtemplate", "edit", $data); 	
	      	
	}

	public function update()
	{

		$data['modelData'] = $this->emailtemplatemodel->getById($this->input->post('id'));
		 $data['languageData']=$this->languagemodel->get_languages();
		
		 
		

		$this->form_validation->set_rules('subject', $this->lang->line('subject'), 'trim|required');
		$this->form_validation->set_rules('body', $this->lang->line('body'), 'required');
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("emailtemplate", "edit", $data);
          else
          {
		 	 $cdata['subject'] = $this->input->post('subject');
		 	 $cdata['body']= $this->input->post('body');
		  
		   $cdata['languageid']= $this->langid ; 
		   
			$res=$this->emailtemplatemodel->update_info($cdata, $this->input->post('id'));


		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Template updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Template berhasil diperbarui</div>');
		        


		        redirect('emailtemplate');     	
	         }
         }

         
	}


}