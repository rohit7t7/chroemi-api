<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventcategory extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');


		$this->load->model('eventcategorymodel');
		$this->load->model('languagemodel');
		$this->load->model('basemodel');
		$this->load->library('ImageThumb');

		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}

			if($this->session->userdata('site_lang') == 'english') 
				$this->langid = '1';
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->langid = '2';

	}

	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
			$data = $this->eventcategorymodel->get_categories('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data = $this->eventcategorymodel->get_categories('2');
		//$data['servicecategoryData']=$this->servicecategorymodel->getcategoryoption();
		
		$this->template->show("eventcategory", "index", $data);
	}


	public function create()
	{
		if($this->session->userdata('site_lang') == 'english') 
		 $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption(1,1);
		elseif($this->session->userdata('site_lang') == 'indonesia') 
		 $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption(2,1);
		

		$data['languageData']=$this->languagemodel->get_languages();
		$is_unique ="";
		if($this->input->post('category'))
		{
			$getstatus=$this->basemodel->getDeleteStatus('category','category',$this->input->post('category'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_category.category]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_category.category]';
			}
		}

		$this->form_validation->set_rules('category', $this->lang->line('category').' '.$this->lang->line('name'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("eventcategory", "create", $data);
          else
          {
          	
		   $cdata['category'] = $this->input->post('category');

		   //if($this->input->post('parent_id') ==0)
		    	//$cdata['parent_id']= null; 
		   // else
		    	//$cdata['parent_id'] = $this->input->post('parent_id');


		    $config['upload_path'] = './upload/eventcategory';

           $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $imgfilename = $_FILES['imageavatar']['name'];
           $new_name = str_replace(' ', '_',$imgfilename);


			$config['file_name'] = $new_name;
			 //move_uploaded_file(getcwd().'/upload/'.$new_name, "http://navsoft.co.in/logintest/img/".$new_name);
            
		 	$this->load->library('upload', $config);

		 

		 	if ( ! $this->upload->do_upload('imageavatar'))
            {

            	$error = array('error' => $this->upload->display_errors());

                //$this->load->view('upload_form', $error);
            }
            else
            {
                
            	$cdata['imageavatar'] = $new_name;

            	$srcPath= getcwd().'/upload/eventcategory/'.$new_name;
	       		 $destPath1 = getcwd().'/upload/thumbnail/eventcategory/'.$new_name;
            	 $destWidth1=100;
                $destHeight1=100;
                $this->imagethumb->thumbnail_new($destPath1, $srcPath, $destWidth1, $destHeight1);
                  
            }

		   $cdata['language_id']= $this->langid; 
		   $cdata['status']= $this->input->post('status'); 
			
			$res = $this->eventcategorymodel->savedata($cdata);

			if($res)
			{
				if($this->session->userdata('site_lang') == 'english') 
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Service Category added successfully</div>');
				elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->session->set_flashdata('msg', '<div class="alert alert-success">layanan Kategori berhasil ditambahkan</div>');
	        	
	        	redirect('eventcategory');
			}
          }

              	
	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->eventcategorymodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();

         if($this->session->userdata('site_lang') == 'english') 
          $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption(1,1);
       	elseif($this->session->userdata('site_lang') == 'indonesia') 
          $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption(2,1);
         

         $this->template->show("eventcategory", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->eventcategorymodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();
         
         $this->template->show("eventcategory", "details", $data);      	
	}

	public function update()
	{

		$data['modelData'] = $this->eventcategorymodel->getById($this->input->post('id'));
		 $data['languageData']=$this->languagemodel->get_languages();



		
		 if($this->input->post('category') != $data['modelData']['category']) 
		{
			$getstatus=$this->basemodel->getDeleteStatus('category','category',$this->input->post('category'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_category.category]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_category.category]';
			}
		}

		$this->form_validation->set_rules('category', $this->lang->line('category').' '.$this->lang->line('name'), 'required[ch_category.category]|regex_match[/^[A-Za-z -]+$/]');
		 
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("eventcategory", "edit", $data);
          else
          {
		 	$cdata['category'] = $this->input->post('category');

		 	 if($this->input->post('parent_id') ==0)
		 		$cdata['parent_id']= null;
		 	else
		 		$cdata['parent_id']= $this->input->post('parent_id');

            	 	
		 	if($_FILES['imageavatar']['name']!='')
			   {
			    @unlink('upload/eventcategory/'.$data['modelData']['imageavatar']);
			    @unlink('upload/thumbnail/eventcategory/'.$data['modelData']['imageavatar']);

			   	$imgfilename = $_FILES['imageavatar']['name'];
           		$new_name = str_replace(' ', '_',$imgfilename);
				$config['file_name'] = $new_name;
				$config['upload_path'] = './upload/eventcategory/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
            	//move_uploaded_file(getcwd().'/upload/'.$new_name, "http://navsoft.co.in/logintest/img/".$new_name);	

            	$this->load->library('upload', $config);

			   

			   	if ( ! $this->upload->do_upload('imageavatar'))
	            {
	            	$error = array('error' => $this->upload->display_errors());

	            }
	            else
	            {
	                
	            	$cdata['imageavatar'] = $new_name;
	            	$srcPath= getcwd().'/upload/eventcategory/'.$new_name;
	       			 $destPath1 = getcwd().'/upload/thumbnail/eventcategory/'.$new_name;
            	 	$destWidth1=100;
               		 $destHeight1=100;
               		 $this->imagethumb->thumbnail_new($destPath1, $srcPath, $destWidth1, $destHeight1);
	                  
	            }
			  }


		  	$cdata['language_id']= $this->langid; 
		  	$cdata['status']= $this->input->post('status'); 

		 	$res=$this->eventcategorymodel->update_info($cdata, $this->input->post('id'));

		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english') 
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Event Category updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia') 
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">layanan Kategori diperbarui berhasil</div>');
		        

		        redirect('eventcategory');     	
	         }
         }

         
	}

	public function delete($id)
	{

	 	$res=$this->eventcategorymodel->delete_info($id);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english') 
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Event Category deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia') 
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">layanan Kategori dihapus berhasil</div>');
			

			redirect('eventcategory');  
		}

	}


}
