<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicefeedback extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('servicefeedbackmodel');
		$this->load->model('servicesmodel');
		$this->load->model('usersmodel');
					
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}
	public function index()
	{
		
		$data = $this->servicefeedbackmodel->get_feedback();
		
		$this->template->show("servicefeedback", "index", $data);
	}


	public function edit($id="")
	{
		 $data['modelData'] = $this->servicefeedbackmodel->getById($id);
         $data['serviceData']=$this->servicesmodel->getserviceoption();
         $data['userData']=$this->usersmodel->getuseroption();
		$this->template->show("servicefeedback", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->servicefeedbackmodel->getById($id);
        
        $this->template->show("servicefeedback", "details", $data);       	
	}

	public function update()
	{

		$data['modelData'] = $this->servicefeedbackmodel->getById($this->input->post('id'));
		 $data['serviceData']=$this->servicesmodel->getserviceoption();
		 $data['userData']=$this->usersmodel->getuseroption();
		

		$this->form_validation->set_rules('user_id', 'Name', 'required');
		$this->form_validation->set_rules('service_id', 'Service Name', 'required');
		$this->form_validation->set_rules('rating', 'Rating', 'trim|integer|less_than_equal_to[5]|is_natural_no_zero|required');
		$this->form_validation->set_rules('Feedback', 'Feedback', 'regex_match[/^[A-Za-z -]+$/]');
		 
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("servicefeedback", "edit", $data);
          else
          {
		 	
		   $cdata['user_id'] = $this->input->post('user_id');
		   $cdata['service_id'] = $this->input->post('service_id');
		   $cdata['rating'] = $this->input->post('rating');
		   $cdata['feedback'] = $this->input->post('feedback');
		  

		 	$res=$this->servicefeedbackmodel->update_info($cdata, $this->input->post('id'));

		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english') 
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Service Feedback updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia') 
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Layanan Masukan berhasil diperbarui</div>');
		    
		        redirect('servicefeedback');     	
	         }
         }

         
	}

	public function delete($id)
	{

	 	$res=$this->servicefeedbackmodel->delete_info($id);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english') 
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Feedback deleted successfully</div>');
			elseif($this->session->userdata('site_lang') == 'indonesia') 
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Umpan balik berhasil dihapus</div>');

			redirect('servicefeedback');  
		}

	}

	

}
