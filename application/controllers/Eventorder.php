<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serviceorder extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('servicecategorymodel');
		$this->load->model('servicetypemodel');
		$this->load->model('serviceordermodel');
		$this->load->model('usersmodel');
		$this->load->model('servicesmodel');

		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}
	

	public function latestorder()
	{
		$data['serviceorder'] = $this->serviceordermodel->get_servicerequest('latest');
		
		if($this->session->userdata('site_lang') == 'english') 
			$data['servicecategoryData']=$this->servicecategorymodel->getcategoryoption(1,2);
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data['servicecategoryData']=$this->servicecategorymodel->getcategoryoption(2,2);

		if($this->session->userdata('site_lang') == 'english') 
		$data['servicetypeData']=$this->servicetypemodel->gettypeoption(1);
		elseif($this->session->userdata('site_lang') == 'indonesia') 
		$data['servicetypeData']=$this->servicetypemodel->gettypeoption(2);

		if($_POST)
			$data['serviceorder'] = $this->serviceordermodel->searchdata($_POST);

		
		$this->template->show("serviceorder", "index", $data);
	}

	public function allorder($stat="")
	{

		
		if($stat == "")
			$data['serviceorder'] = $this->serviceordermodel->get_servicerequest("","");
		elseif($stat == "pending")
			$data['serviceorder'] = $this->serviceordermodel->get_servicerequest("","pending");
		elseif($stat == "confirm")
			$data['serviceorder'] = $this->serviceordermodel->get_servicerequest("","confirm");
		elseif($stat == "cancel")
			$data['serviceorder'] = $this->serviceordermodel->get_servicerequest("","cancel");

		if($this->session->userdata('site_lang') == 'english') 
			$data['servicecategoryData']=$this->servicecategorymodel->getcategoryoption(1,2);
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data['servicecategoryData']=$this->servicecategorymodel->getcategoryoption(2,2);

		if($this->session->userdata('site_lang') == 'english')
		$data['servicetypeData']=$this->servicetypemodel->gettypeoption(1);
		elseif($this->session->userdata('site_lang') == 'indonesia')
		$data['servicetypeData']=$this->servicetypemodel->gettypeoption(2);

		if($_POST)
			$data['serviceorder'] = $this->serviceordermodel->searchdata($_POST);

		$this->template->show("serviceorder", "index", $data);
	}


	public function confirm($id)
	{

	 	$res=$this->serviceordermodel->confirm_info($id);

	 	if($res)
	    {
	    	$this->session->set_flashdata('confirmmsg', '<div class="alert alert-success">Order has been confirmed successfully</div>');
			
	    	redirect('serviceorder/allorder');  
		}

	}

	public function cancel($id)
	{

	 	$res=$this->serviceordermodel->cancel_info($id);

	 	if($res)
	    {
	    	$this->session->set_flashdata('cancelmsg', '<div class="alert alert-success">Order has been cancelled successfully</div>');
			redirect('serviceorder/allorder');  
		}

	}


}
