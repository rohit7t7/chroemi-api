<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('eventcategorymodel');
		
		$this->load->model('eventsmodel');
                $this->load->model('currencymodel');
                $this->load->model('eventfundsmodel');
		//$this->load->model('currencymodel');
		$this->load->model('languagemodel');
		//$this->load->library('image_lib');
		$this->load->library('ImageThumb');
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}


			if($this->session->userdata('site_lang') == 'english') 
				$this->sellang =1;
			elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->sellang =2;

		
		

	}


	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
		{
			$data['services'] = $this->eventsmodel->get_services('1');
			
		}
		elseif($this->session->userdata('site_lang') == 'indonesia') 
		{
			$data['services'] = $this->eventsmodel->get_services('2');
			
		}


		$data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption($this->sellang);
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
		
			

		if($_POST)
			$data['services'] = $this->eventsmodel->searchdata($_POST);


		$this->template->show("events", "index", $data);
	}


	public function create()
	{

		$data['languageData']=$this->languagemodel->get_languages();

        $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption($this->sellang,2);
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
		

		//$data['currencyData'] = $this->currencymodel->getcurrencyoption();
		//$data['defaultcurrency'] = $this->currencymodel->getdefaultcurrency();


		$is_unique ="";
		if($this->input->post('servicename'))
		{
			$getstatus=$this->basemodel->getDeleteStatus('events','name',$this->input->post('servicename'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_events.name]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_events.name]';
			}
		}

		$this->form_validation->set_rules('categoryid', $this->lang->line('event').' '.$this->lang->line('category'), 'required');
		//$this->form_validation->set_rules('typeid', $this->lang->line('service').' '.$this->lang->line('type'), 'required');
		$this->form_validation->set_rules('servicename', $this->lang->line('event').' '.$this->lang->line('name'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);
		//$this->form_validation->set_rules('servicename_in_indonesian', $this->lang->line('service').' '.$this->lang->line('name').' '.$this->lang->line('in').' '.$this->lang->line('indonesian'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);
		//$this->form_validation->set_rules('cost', $this->lang->line('cost'), 'regex_match[/^[0-9]+(\.[0-9]{0,2})?$/]');
		$this->form_validation->set_rules('features', $this->lang->line('features'), 'required|regex_match[/^[A-Za-z -]+$/]');
		//$this->form_validation->set_rules('discount', $this->lang->line('discount'), 'regex_match[/^[0-9]+(\.[0-9]{0,2})?$/]');
		//$this->form_validation->set_rules('noofservices', $this->lang->line('no').' '.$this->lang->line('of').' '.$this->lang->line('services'), 'is_natural_no_zero');
		$this->form_validation->set_rules('availabilitystartdate', $this->lang->line('availability').' '.$this->lang->line('start').' '.$this->lang->line('date'), 'required');
		$this->form_validation->set_rules('availabilityenddate', $this->lang->line('availability').' '.$this->lang->line('end').' '.$this->lang->line('date'), 'required');
		//$this->form_validation->set_rules('imageavatar', 'Image', 'callback_handle_upload');

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("events", "create", $data);
          else
          {
          	$loggeduserinfo = $this->session->userdata('logged_in');
          	
          	
		   $cdata['categoryid'] = $this->input->post('categoryid');
		   //$cdata['typeid'] = $this->input->post('typeid');
		   $cdata['name'] = $this->input->post('servicename');
		   // $cdata['servicename_in_indonesian'] = $this->input->post('servicename_in_indonesian');
		   $cdata['description'] = $this->input->post('description');
		  // $cdata['cost'] = $this->input->post('cost');
		  // $cdata['currency_id'] = $this->input->post('currency_id');
		   $cdata['availabilitystartdate'] = $this->input->post('availabilitystartdate');
		   $cdata['availabilityenddate'] = $this->input->post('availabilityenddate');
		   $cdata['features'] = $this->input->post('features');
		   // $cdata['noofservices'] = $this->input->post('noofservices');
		  // $cdata['discount'] = $this->input->post('discount');
		  //  $cdata['discountex_date'] = $this->input->post('discountex_date');
		    $cdata['address'] = $this->input->post('address');
		   $cdata['createddate']= date("Y-m-d H:i:s");


		  
		   $cdata['language_id']= $this->sellang; 


		   $cdata['status']= $this->input->post('status'); 
		   $config['upload_path'] = './upload/event/';
           $config['allowed_types'] = 'gif|jpg|png|jpeg';
           $new_name = $_FILES['imageavatar']['name'];
			$config['file_name'] = $new_name;
            
		 	$this->load->library('upload', $config);



		 	if ( ! $this->upload->do_upload('imageavatar'))
            {

            	 $error = array('error' => $this->upload->display_errors());

                //$this->load->view('upload_form', $error);
            }
            else
            {
            	   
            	$cdata['imageavatar'] = $new_name;
                
                
	        	$srcPath= getcwd().'/upload/event/'.$new_name;
	        	$destPath1 = getcwd().'/upload/thumbnail/event/'.$new_name;
             	$destWidth1=100;
                $destHeight1=100;
                $this->imagethumb->thumbnail_new($destPath1, $srcPath, $destWidth1, $destHeight1);
            }
 

		   /*if($_FILES['imageavatar']['name']!='')
		   {
		   		$uploadedfilename = $this->doImageUpload('upload/serviceicon','');
		   		$cdata['imageavatar'] = $uploadedfilename[0]['file_name'];

		   }*/


	  	
			$res = $this->eventsmodel->savedata($cdata);

			/*if(count($_FILES['image']['name']) > 0)
		   {

		   	 $responseimgdata = $this->doImageUpload('upload/service',$res);

		   }*/
		   
			if($res)
			{
				if($this->session->userdata('site_lang') == 'english') 
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Event added successfully</div>');
				elseif($this->session->userdata('site_lang') == 'indonesia') 
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Layanan berhasil ditambahkan</div>');
	        	
	        	

	        	redirect('events');
			}
          }

              	
	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->eventsmodel->getById($id);

		 $data['modelimageData'] = $this->eventsmodel->getimageinfo($id);


         $data['languageData']=$this->languagemodel->get_languages();
        $data['eventcategoryData']=$this->eventcategorymodel->getcategoryoption($this->sellang,2);

		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
		//$data['currencyData'] = $this->currencymodel->getcurrencyoption();
  	
         $this->template->show("events", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->eventsmodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();
        $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption($this->sellang);
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
  
         $this->template->show("events", "details", $data);      	
	}
        public function fund($id="")
	{
            $data['id'] = $id;
		 $data['funds'] = $this->eventfundsmodel->get_funds($id);
         $data['languageData']=$this->languagemodel->get_languages();
        
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
  
         $this->template->show("events", "funds", $data);      	
	}
         public function addfund($id="")
	{
             if($this->input->post('fundadd')==1)
		{ 
                 $this->form_validation->set_rules('fund_amount', $this->lang->line('fund'), 'required|numeric|greater_than[0.99]');
                 if ($this->form_validation->run() == FALSE)
                 {
                      $data['currency'] = $this->currencymodel->getcurrencyoption();
                $data['id'] = $id;
                $data['funds'] = $this->eventfundsmodel->get_funds($id);
                $data['languageData']=$this->languagemodel->get_languages();
		  	   $this->template->show("events", "addfunds", $data);
                }
          else
          {
              $cdata['event_id'] = $id;
              $cdata['date'] = @date("Y-m-d");
              $cdata['fund_amount'] = $this->input->post('fund_amount');
              $cdata['currency_id'] = $this->input->post('currency_id');
                 $res = $this->eventfundsmodel->savedata($cdata);
                 redirect('events/fund/'.$id);
             }
                }
                $data['currency'] = $this->currencymodel->getcurrencyoption();
                $data['id'] = $id;
                $data['funds'] = $this->eventfundsmodel->get_funds($id);
                $data['languageData']=$this->languagemodel->get_languages();
                    $this->template->show("events", "addfunds", $data);     
               
		 
        
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
  
       
	}
	public function update()
	{

		$data['modelData'] = $this->eventsmodel->getById($this->input->post('id'));
		$data['languageData']=$this->languagemodel->get_languages();
                $data['servicecategoryData']=$this->eventcategorymodel->getcategoryoption($this->sellang);
		//$data['servicetypeData']=$this->servicetypemodel->gettypeoption($this->sellang);
		//$data['currencyData'] = $this->currencymodel->getcurrencyoption();
		


		/*if($this->input->post('servicename') != $data['modelData']['servicename']) {
		     $is_unique =  '|is_unique[gm_services.servicename]';
		} else {
			
			$is_unique =  '';
		}*/
		$is_unique ="";
		if($this->input->post('servicename') != $data['modelData']['servicename']) 
		{
			$getstatus=$this->basemodel->getDeleteStatus('events','name',$this->input->post('servicename'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_events.name]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_evants.name]';
			}
		}

		$this->form_validation->set_rules('categoryid', $this->lang->line('event').' '.$this->lang->line('category'), 'required');
		//$this->form_validation->set_rules('typeid', $this->lang->line('service').' '.$this->lang->line('type'), 'required');
		$this->form_validation->set_rules('servicename', $this->lang->line('event').' '.$this->lang->line('name'), 'required[ch_events.name]|regex_match[/^[A-Za-z -]+$/]'.$is_unique);
		//$this->form_validation->set_rules('servicename_in_indonesian', $this->lang->line('service').' '.$this->lang->line('name').' '.$this->lang->line('in').' '.$this->lang->line('indonesian'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);
		//$this->form_validation->set_rules('cost', $this->lang->line('cost'), 'regex_match[/^[0-9]+(\.[0-9]{0,2})?$/]');
		$this->form_validation->set_rules('features', $this->lang->line('features'), 'required|regex_match[/^[A-Za-z -]+$/]');
		//$this->form_validation->set_rules('noofservices', $this->lang->line('no').' '.$this->lang->line('of').' '.$this->lang->line('services'), 'is_natural_no_zero');
		//$this->form_validation->set_rules('discount', $this->lang->line('discount'), 'regex_match[/^[0-9]+(\.[0-9]{0,2})?$/]');
		$this->form_validation->set_rules('availabilitystartdate', $this->lang->line('availability').' '.$this->lang->line('start').' '.$this->lang->line('date'), 'required');
		$this->form_validation->set_rules('availabilityenddate', $this->lang->line('availability').' '.$this->lang->line('end').' '.$this->lang->line('date'), 'required');
		// $this->form_validation->set_rules('imageavatar', 'Image', 'callback_handle_upload');

		if ($this->form_validation->run() == FALSE)
		   $this->template->show("events", "edit", $data);
          else
          {
          	 $loggeduserinfo = $this->session->userdata('logged_in');
          	
		 	$cdata['categoryid'] = $this->input->post('categoryid');
		  // $cdata['typeid'] = $this->input->post('typeid');
		   $cdata['name'] = $this->input->post('servicename');
		   $cdata['description'] = $this->input->post('description');
		 //  $cdata['cost'] = $this->input->post('cost');
		 //  $cdata['currency_id'] = $this->input->post('currency_id');
		   $cdata['availabilitystartdate'] = $this->input->post('availabilitystartdate');
		    $cdata['availabilityenddate'] = $this->input->post('availabilityenddate');
		    $cdata['features'] = $this->input->post('features');
		 //   $cdata['noofservices'] = $this->input->post('noofservices');
		 //  $cdata['discount'] = $this->input->post('discount');
		  //  $cdata['discountex_date'] = $this->input->post('discountex_date');
		   $cdata['address'] = $this->input->post('address');
		   $cdata['language_id']= $this->sellang; 
		   $cdata['status']= $this->input->post('status');
		   $config['upload_path'] = './upload/event/';
           $config['allowed_types'] = 'gif|jpg|png|jpeg';
           $new_name = $_FILES['imageavatar']['name'];
			$config['file_name'] = $new_name;
            
		 	$this->load->library('upload', $config);

			  if($_FILES['imageavatar']['name']!='')
			   {
			   		//unlink('upload/serviceicon/'.$data['modelData']['imageavatar']);
			 		//unlink('upload/serviceicon/thumbnail/'.$data['modelData']['imageavatar']);
			   		unlink('upload/event/'.$data['modelData']['imageavatar']);
			 		unlink('upload/thumbnail/event/'.$data['modelData']['imageavatar']);
			   		
			   		//$uploadedfilename = $this->doImageUpload('upload/serviceicon', $this->input->post('id'));
			   		//$cdata['imageavatar'] = $uploadedfilename[0]['file_name'];
			   		if ( ! $this->upload->do_upload('imageavatar'))
		            {
		                $error = array('error' => $this->upload->display_errors());

		                //$this->load->view('upload_form', $error);
		            }
		            else
		            {
		                
		            	$cdata['imageavatar'] = $new_name;
		                
		                
			        $srcPath= getcwd().'/upload/event/'.$new_name;
			        $destPath1 = getcwd().'/upload/thumbnail/event/'.$new_name;
		             $destWidth1=100;
		                $destHeight1=100;
		                $this->imagethumb->thumbnail_new($destPath1, $srcPath, $destWidth1, $destHeight1);
		            }

			   } 


			   $res=$this->servicesmodel->update_info($cdata, $this->input->post('id'));

			    /*if(count($_FILES['image']['name']) > 0)
			   {

			   		$responseimgdata = $this->doImageUpload('upload/service',$this->input->post('id'));

			   		if(!empty($responseimgdata))
			   		{
			   			//$this->servicesmodel->saveimgdata($responseimgdata,$data['modelData']['id']);
			   		}
			   		
			   }*/

		 	 
		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Event updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Layanan  berhasil diperbarui</div>');
		        
		        redirect('events');     	
	         }
         }

         
	}

	public function chkservicestatus($serviceid)
	{
		//current date & time
		$currdt = date('Y-m-d');

		$qrsrv =  $this->db->select('a.*');
		$qrsrv = $this->db->where('a.serviceid', $serviceid);
		$qrsrv = $this->db->where('a.deletestatus', '0');
		$qrsrv = $this->db->where('MID(a.bookingdate,1,10) >=', $currdt);
		$qrsrv = $this->db->get($this->db->dbprefix.'_appointment as a');

		//echo $this->db->last_query();
		
		$num = $qrsrv->num_rows();

		return $num;
	}

	public function delete()
	{

		$id = $_POST['srvid'];

		
		$cntsrv= $this->chkservicestatus($id);

		if($cntsrv == '0')
		{
	 		$res=$this->servicesmodel->delete_info($id);

	 		if($this->session->userdata('site_lang') == 'english')
	 			echo "0";
	 		elseif($this->session->userdata('site_lang') == 'indonesia')
	 			echo "1";
	 		

	 		if($res)
		    {
		    	if($this->session->userdata('site_lang') == 'english')
		    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Service deleted successfully</div>');
		    	elseif($this->session->userdata('site_lang') == 'indonesia')
		    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Layanan dihapus diperbarui</div>');
				

				redirect('services');  
			}
		}
	 	else
	 	{
	 		if($this->session->userdata('site_lang') == 'english')
	 			$this->session->set_flashdata('errmsg', '<div class="alert alert-success">Event should not be deleted.</div>');
	 		elseif($this->session->userdata('site_lang') == 'indonesia')
	 			$this->session->set_flashdata('errmsg', '<div class="alert alert-success">Layanan seharusnya tidak dihapus.</div>');

	 		if($this->session->userdata('site_lang') == 'english')
	 			echo "2";
	 		elseif($this->session->userdata('site_lang') == 'indonesia')
	 			echo "3";
	 	}

	}

  function handle_upload()
  {

  	if ($_FILES['imageavatar']['name']!='')
      {
      	
      	$filetype = mime_content_type($_FILES['imageavatar']['name']);

      
      	if(strpos($filetype, 'image') !== false)
       		return true;
      	 else
	    {
	     	  $this->form_validation->set_message('handle_upload', "You must upload an image!");
		      return false;
	    }
	     
    }
		   
  }

	public function doImageUpload($imgpath,$serviceid='')
	{

			//store image info once uploaded
          $data = array();

	       if (!$_FILES) 
	       {
	       		$this->template->show("events", "create", $data);
	       		return false;

            }
            else
            {
            	
            	if($imgpath =='upload')
            	{
            		$files = $_FILES['imageavatar'];

            		if($_FILES['imageavatar']['name'][0]!='')
       					$filesCount = 1;
       				else
       					$filesCount = 0;

       				
            	}
       			elseif($imgpath =='upload/event')
       			{
       				$files = $_FILES['image'];

       				if($_FILES['image']['name'][0]!='')
       					$filesCount = count($_FILES['image']['name']);	
       				else
       					$filesCount = 0;

       			}

       				for ($i = 0; $i < $filesCount; $i++) 
       				{

       					if($filesCount > 1)
       					{
       						
       							 $_FILES ['uploadedimage'] ['name'] = $files ['name'] [$i];
						         $_FILES ['uploadedimage'] ['type'] = $files  ['type'] [$i];
						         $_FILES ['uploadedimage'] ['tmp_name'] = $files  ['tmp_name'] [$i];
						         $_FILES ['uploadedimage'] ['error'] = $files  ['error'] [$i];
						         $_FILES ['uploadedimage'] ['size'] = $files ['size'] [$i];
       					}
       					elseif($filesCount ==1)
       					{
       						
       						 $_FILES ['uploadedimage'] ['name'] = $files ['name'];
					         $_FILES ['uploadedimage'] ['type'] = $files  ['type'];
					         $_FILES ['uploadedimage'] ['tmp_name'] = $files  ['tmp_name'];
					         $_FILES ['uploadedimage'] ['error'] = $files  ['error'];
					         $_FILES ['uploadedimage'] ['size'] = $files ['size'];
       					}
       				
	       			
	            	 	 $config['upload_path'] = $imgpath; 
					      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
					      $config['overwrite'] = TRUE;
					      	//$config['max_width']  = '2048';
					      	//$config['max_height']  = '2048';


				      /* Load the image library */
				      	$this->load->library('upload',$config);
	 			        $this->upload->initialize($config);

	 			        
	 			        if ($this->upload->do_upload('uploadedimage'))
	 			        {
	 			        	 $imagedata = $this->upload->data();
				       		$uploadimgData[$i]['file_name'] = $imagedata['file_name'];
		                    $uploadimgData[$i]['created'] = date("Y-m-d H:i:s");

		                    if($imgpath =='upload/service')
		                    	$this->servicesmodel->saveimgdata($imagedata['file_name'],$serviceid);


		                    $this->load->library('image_lib');
                			$target_path = $imgpath.'/thumbnail';

                
	                		$config_manip['image_library'] = 'gd2';
		                    $config_manip['source_image'] = $imagedata['full_path']; //get original image
		                    $config_manip['new_image'] = $target_path;
		                    //$config_manip['create_thumb'] = TRUE;
		                    $config_manip['maintain_ratio'] = TRUE;
		                    $config_manip['width'] = 75;
		                    $config_manip['height'] = 50;

			                    $this->load->library('image_lib', $config_manip);

			                    $this->image_lib->clear();
								$this->image_lib->initialize($config_manip);
								$this->image_lib->resize();

								return $uploadimgData;
	 			        }
	 			         else 
			            {

			            			     
			     			$this->template->show("events", "create", $data);
			     			return false;
	 							
			            }
      

       				}

              
            }

          	           
		}

		
}
