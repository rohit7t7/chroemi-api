<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Globalsearch extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();  
        // Load form helper library
        $this->load->helper('form');

        $this->load->model('searchmodel');
        //$this->load->model('permissionmodel');
        $this->load->model('languagemodel');
        $this->load->model('basemodel');
        
        if(!isset($_SESSION['logged_in']))
          {
            redirect('login');
          }
   
    }

    function index()
    {
      $data = array();
      $this->template->show("globalsearch", "index", $data);
    }
 
    function gensearch() 
    {
        $textval = $_POST['searchdata'];
        $tabval = $_POST['tabvalue'];

        if($tabval =='supervisor')
            $data = $this->searchmodel->getsupervisorsearchresult($textval);
        elseif($tabval =='user')
          $data = $this->searchmodel->getusersearchresult($textval);
       elseif($tabval =='category')
        $data = $this->searchmodel->getcategorysearchresult($textval);
       elseif($tabval =='service')
       $data = $this->searchmodel->getservicesearchresult($textval);
      elseif($tabval =='order')
       $data = $this->searchmodel->getordersearchresult($textval);
       //$data['paymentslt'] = $this->searchmodel->getpaymentsearchresult($textval);
       
       echo json_encode($data);
     
              
    }

    function supervisorsearch($textval="") 
    {
      
         $this->session->set_userdata('search_data', $textval);

        if($textval !="")
        {
           $data = $this->searchmodel->getsupervisorsearchresult($textval);
           //$resultdata = json_encode($data);
        }
        else
          $data = array();

        $this->template->show("globalsearch", "index", $data);
           
     
   }

    
}

?>