<?php

//session_start(); //we need to start session in order to access it through CI

Class Login extends CI_Controller {

public function __construct() {
parent::__construct();

// Load form helper library
$this->load->helper('form');


// Load database
$this->load->model('loginmodel');

	if(isset($_SESSION['logged_in']))
	{
		redirect('dashboard');
	}

}

// Show login page
public function index()
{
	
	if($this->input->post('submit')==1) {
	$this->form_validation->set_rules('email', 'Email', 'required');
$this->form_validation->set_rules('password','Password', 'required');

if ($this->form_validation->run() == FALSE)
{
	if(isset($this->session->userdata['logged_in']))
	{
		redirect('dashboard');
	}
	else
	{
		$this->load->view("login/login.php");
	}
} 
else
 {
	$data = array(
	'email' => $this->input->post('email'),
	'password' => $this->input->post('password')
	);



$result = $this->loginmodel->login($data);
if ($result == TRUE)
 {

	$username = $this->input->post('email');
	$result = $this->loginmodel->read_user_information($username);

	if($result=='false')
	{
		$data = array(
		'error_message' => 'Invalid Username or Password or you are not authorized to access'
		);
		$this->load->view("login/login.php",$data);
	}
	else
	{
		$session_data = array(
		'id'=>	 $result[0]->id,
		'name'=>	 $result[0]->name,
		'username' => $result[0]->email,
		'phone' => $result[0]->phone,
		'password' => $result[0]->password,
		'createddate' => $result[0]->createddate,
		'status' => $result[0]->status,
		'type' => $result[0]->type
		);
		// Add user data in session
		$this->session->set_userdata('logged_in', $session_data);
		$this->session->set_userdata('site_lang', 'indonesia');
	    redirect('dashboard');
	}
	/*if ($result != 'false') {
	$session_data = array(
	'id'=>	 $result[0]->id,
	'name'=>	 $result[0]->name,
	'username' => $result[0]->email,
	'phone' => $result[0]->phone,
	'password' => $result[0]->password,
	'createddate' => $result[0]->createddate,
	'status' => $result[0]->status,
	'type' => $result[0]->type
	);
		// Add user data in session
		$this->session->set_userdata('logged_in', $session_data);
	    redirect('dashboard');
		
	}
	else {
	$data = array(
	'error_message' => 'Invalid Username or Password or you are not authorized to access'
	);
		$this->load->view("login/login.php",$data);
	}*/
}
 else {
$data = array(
'error_message' => 'Invalid Username or Password or you are not authorized to access'
);
	$this->load->view("login/login.php",$data);
}
}
}
else
{
	$this->load->view("login/login.php");
}
}



}

?>

