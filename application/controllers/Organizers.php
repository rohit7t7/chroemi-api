<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organizers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('organizersmodel');
		$this->load->model('languagemodel');
		$this->load->model('basemodel');
		$this->load->library('ImageThumb');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}


	}
	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
			$data = $this->organizersmodel->getSupervisors('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
			$data = $this->organizersmodel->getSupervisors('2');


		$this->template->show("organizers", "index", $data);
	}

	

	public function create()
	{
		
		$data['languageData']=$this->languagemodel->get_languages();
		$is_unique ="";
		if($this->input->post('email'))
		{
			$getstatus=$this->basemodel->getDeleteStatus('organizers','email',$this->input->post('email'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_organizers.email]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_organizers.email]';
			}
		}
		
		
		$this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required');
		$this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|valid_email'.$is_unique);
		$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|min_length[10]|max_length[10]|regex_match[/^[0-9().-]+$/]');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required');

		

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("organizers", "create", $data);
          else
          {
          	
		   $cdata['name'] = $this->input->post('name');
		   $cdata['email'] = $this->input->post('email');
		   $cdata['phone'] = $this->input->post('phone');
		   $cdata['password'] = md5($this->input->post('password'));
		   $cdata['createddate']= date("Y-m-d H:m:s");
		   $cdata['status']= $this->input->post('status');
		   if($this->input->post('type')!='') {
		   $cdata['type']= $this->input->post('type'); }

		  echo $this->sendmail(); 

			$res = $this->organizersmodel->savedata($cdata);
			


			if($res)
			{
				if($this->input->post('type') ==1)
				{
					if($this->session->userdata('site_lang') == 'english')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Admin added successfully</div>');
					elseif($this->session->userdata('site_lang') == 'indonesia')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Admin berhasil ditambahkan</div>');
				}
				
	        	elseif($this->input->post('type') ==2)
	        	{
	        		if($this->session->userdata('site_lang') == 'english')
	        			$this->session->set_flashdata('msg', '<div class="alert alert-success">Supervisor added successfully</div>');
	        		elseif($this->session->userdata('site_lang') == 'indonesia')
	        			$this->session->set_flashdata('msg', '<div class="alert alert-success">Pengawas berhasil ditambahkan</div>');
	        	}
				

	        	redirect('organizers');
			}
          }

              	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->organizersmodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();
         
         $this->template->show("organizers", "details", $data);      	
	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->organizersmodel->getById($id);

		 $data['languageData']=$this->languagemodel->get_languages();
         
         $this->template->show("organizers", "edit", $data);      	
	}

	public function update()
	{

		$data['modelData'] = $this->organizersmodel->getById($this->input->post('id'));
		 $data['languageData']=$this->languagemodel->get_languages();
		

		$is_unique ="";
		if($this->input->post('email') != $data['modelData']['email'])
		{
			$getstatus=$this->basemodel->getDeleteStatus('organizers','email',$this->input->post('email'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[ch_organizers.email]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[ch_organizers.email]';
			}
		}

		$this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required');
		$this->form_validation->set_rules('email', $this->lang->line('email'), 'trim|required|valid_email'.$is_unique);
		$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|min_length[10]|max_length[10]|regex_match[/^[0-9().-]+$/]');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required');
		
	
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("organizers", "edit", $data);
          else
          {

		   $cdata['name'] = $this->input->post('name');
		   $cdata['email'] = $this->input->post('email');
		   $cdata['phone'] = $this->input->post('phone');

		   if($data['modelData']['password'] == $this->input->post('password'))
		   		$cdata['password'] = $this->input->post('password');
		   	else
		   		$cdata['password'] = md5($this->input->post('password'));
		   
		   //$cdata['createddate']= date("Y-m-d H:m:s");
		  	   
		   $cdata['status']= $this->input->post('status'); 
		   $cdata['type']= $this->input->post('type'); 

			$res=$this->organizersmodel->update_info($cdata, $this->input->post('id'));


		 	if($res)
	         {
	         	if($this->input->post('type') ==1)
	         	{
	         		if($this->session->userdata('site_lang') == 'english')
	         			$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Admin updated successfully</div>');
	         		elseif($this->session->userdata('site_lang') == 'indonesia')
	         			$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Admin berhasil diperbarui</div>');
	         	}	
	         	elseif($this->input->post('type') ==2)
	         	{
	         		if($this->session->userdata('site_lang') == 'english')
	         			$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Supervisor updated successfully</div>');
	         		elseif($this->session->userdata('site_lang') == 'indonesia')
	         			$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Pengawas berhasil diperbarui</div>');
	         	}
		        

		        redirect('organizers');     	
	         }
         }

         
	}

	public function delete($id)
	{

	 	$res=$this->organizersmodel->delete_info($id);

	 	 $data = $this->organizersmodel->getallsupId($id);


	 	if($res)
	    {

	    	if($data['type'] == '1')
	    	{
	    		if($this->session->userdata('site_lang') == 'english')
	    			$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Admin deleted successfully</div>');
	    		elseif($this->session->userdata('site_lang') == 'indonesia')
	    			$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Admin dihapus berhasil</div>');
	    	}
	    	elseif($data['type'] == '2')
	    	{
	    		if($this->session->userdata('site_lang') == 'english')
	    			$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Supervisor deleted successfully</div>');
	    		elseif($this->session->userdata('site_lang') == 'indonesia')
	    			$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Pengawas berhasil dihapus</div>');
	    	}
	
			redirect('organizers');  
		}

	}

	//send mail
	function sendmail()
	{

		// Configure email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = 'antarapal81@gmail.com';
        $config['smtp_pass'] = 'ant123456';
        $config['mailtype'] = 'html';
    	$config['charset'] = 'iso-8859-1';
    	$config['wordwrap'] = TRUE;
    	$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

    	$templatedet = $this->organizersmodel->get_emailtemplate();
    	//$body = file_get_contents('email-template.php');
    	
    	$emailusername = str_replace('%USERNAME%', $this->input->post('email'), $templatedet[0]['body']);
    	$emailpwd = str_replace('%PASSWORD%', $this->input->post('password'), $emailusername);
    	$emailbody = $emailpwd;

		$body = '<!DOCTYPE HTML>'.
		'<head><meta http-equiv="content-type" content="text/html">'.
		'<title>Email notification</title></head>'.
		'<body>'.$emailbody.
		'<div id="footer" style="width: 80%;height: 40px;margin: 0 auto;text-align: center;padding: 10px;font-family: Verdena;background-color: #E2E2E2;">'.
		   'All rights reserved @ mysite.html 2014'.
		'</div>
		</body>'; 

	    $this->load->library('email',$config); // load email library
	    $this->email->from('acharya.666@gmail.com', 'Debraj Acharya');
	    $this->email->to('sales@digitexglobal.com');
	    //$this->email->cc(''); 
	    $this->email->subject($templatedet[0]['subject']);
	    $this->email->message($body);
	    //$this->email->attach('/path/to/file1.png'); // attach file
	    //$this->email->attach('/path/to/file2.pdf');
	    if ($this->email->send())
	        echo "Mail Sent!";
	    else
	        echo "There is error in sending mail!";
	}

}