<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentcredentials extends CI_Controller {

	/**
	 
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('paymentgatewaymodel');
		$this->load->model('paymentcredentialsmodel');
		
		$this->load->model('languagemodel');
		
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}

	public function index()
	{
		 
		$data = $this->paymentcredentialsmodel->get_paymentgatewaycredential();
			
		$this->template->show("paymentcredentials", "index", $data);
	}


	public function create()
	{

		$data['paymenttypeData']=$this->paymentgatewaymodel->gettypeoption();

		$this->form_validation->set_rules('paymenttype', $this->lang->line('payment').' '. $this->lang->line('type'), 'required');
		//$this->form_validation->set_rules('serverkey', $this->lang->line('serverkey'), 'required');
		//$this->form_validation->set_rules('clientkey', $this->lang->line('clientkey'), 'required');
		//$this->form_validation->set_rules('username', $this->lang->line('user').' '. $this->lang->line('name'), 'required');
		//$this->form_validation->set_rules('password', $this->lang->line('password'), 'required');
		//$this->form_validation->set_rules('signature', $this->lang->line('signature'), 'required');
		        
		      if ($this->form_validation->run() == FALSE)
		      {
		      	 $this->template->show("paymentcredentials", "create", $data);	
		      }
		      else
		      {
		      	 	$cdata['paymenttype'] = $this->input->post('paymenttype');
				   $cdata['serverkey']= $this->input->post('serverkey'); 
				   $cdata['clientkey']= $this->input->post('clientkey'); 
				   $cdata['apiusername']= $this->input->post('apiusername'); 
				   $cdata['apipassword']= $this->input->post('apipassword'); 
				   $cdata['apisignature']= $this->input->post('apisignature');
				   $cdata['status']= $this->input->post('status');  

				   //check credential already set or not
				   $qrresult = $this->paymentcredentialsmodel->getdata($this->input->post('paymenttype'));

				   if($qrresult != 0)
				   {

				   		if($this->session->userdata('site_lang') == 'english')
						$data['error_message'] = 'Payment Gateway Credentials already set';
						elseif($this->session->userdata('site_lang') == 'indonesia')
						$data['error_message'] = 'Gerbang pembayaran surat kepercayaan sudah ditetapkan';


						$this->template->show("paymentcredentials", "create", $data);	
				   }
				  	else
				  	{
				  	 $res = $this->paymentcredentialsmodel->savedata($cdata);

					  	 if($res)
						{
							if($this->session->userdata('site_lang') == 'english')
							$this->session->set_flashdata('msg', '<div class="alert alert-success">Payment Gateway Credentials added successfully</div>');
							elseif($this->session->userdata('site_lang') == 'indonesia')
							$this->session->set_flashdata('msg', '<div class="alert alert-success">Gerbang pembayaran surat kepercayaan berhasil ditambahkan</div>');
				        	

				        	redirect('paymentcredentials');
						}
				  	}

					
		      }
          	
          		  
  	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->paymentcredentialsmodel->getById($id);
		 $data['paymenttypeData']=$this->paymentgatewaymodel->gettypeoption();

		$this->template->show("paymentcredentials", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->paymentcredentialsmodel->getById($id);
         
         $this->template->show("paymentcredentials", "details", $data);      	
	}

	public function update()
	{

		$data['paymenttypeData']=$this->paymentgatewaymodel->gettypeoption();
		$data['modelData'] = $this->paymentcredentialsmodel->getById($this->input->post('id'));
		
		 $this->form_validation->set_rules('paymenttype', $this->lang->line('payment').' '. $this->lang->line('type'), 'required');
		//$this->form_validation->set_rules('serverkey', $this->lang->line('serverkey'), 'required');
		//$this->form_validation->set_rules('clientkey', $this->lang->line('clientkey'), 'required');
		//$this->form_validation->set_rules('username', $this->lang->line('user').' '. $this->lang->line('name'), 'required');
		//$this->form_validation->set_rules('password', $this->lang->line('password'), 'required');
		//$this->form_validation->set_rules('signature', $this->lang->line('signature'), 'required');


		  if ($this->form_validation->run() == FALSE)
		      {
		      	 $this->template->show("paymentcredentials", "edit", $data);	
		      }
		      else
		      {
		      	 $cdata['paymenttype'] = $this->input->post('paymenttype');
				   $cdata['serverkey']= $this->input->post('serverkey'); 
				   $cdata['clientkey']= $this->input->post('clientkey'); 
				   $cdata['apiusername']= $this->input->post('apiusername'); 
				   $cdata['apipassword']= $this->input->post('apipassword'); 
				   $cdata['apisignature']= $this->input->post('apisignature');
				   $cdata['status']= $this->input->post('status');  

				    $res = $this->paymentcredentialsmodel->update_info($cdata,$this->input->post('id'));
				    
					if($res)
					{
						if($this->session->userdata('site_lang') == 'english')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Payment Gateway Credentials updated successfully</div>');
						elseif($this->session->userdata('site_lang') == 'indonesia')
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Gerbang pembayaran surat kepercayaan berhasil ditambahkan</div>');
			        	

			        	redirect('paymentcredentials');
					}
		      }
          		
		         
	}

	public function delete($id)
	{

	 	$res=$this->paymentcredentialsmodel->delete_info($id);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Payment Gateway credentials deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Gerbang pembayaran surat kepercayaan diperbarui</div>');
			

			redirect('paymentcredentials');  
		}

	}

 		
}
