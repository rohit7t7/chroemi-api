<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentgateway extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('paymentgatewaymodel');
		
		$this->load->model('languagemodel');
		
		$this->load->model('basemodel');
		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}

	public function index()
	{
		
		$data = $this->paymentgatewaymodel->get_paymentgateway();
			
		$this->template->show("paymentgateway", "index", $data);
	}


	public function create()
	{

		 $is_unique ="";      
		if($this->input->post('name'))
		{
			$getstatus=$this->basemodel->getDeleteStatus('paymentgateway','name',$this->input->post('name'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[gm_paymentgateway.name]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[gm_paymentgateway.name]';
			}
		}

		$this->form_validation->set_rules('name', $this->lang->line('name'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("paymentgateway", "create", "");
          else
          {
          	
		   $cdata['name'] = $this->input->post('name');
		   $cdata['status']= $this->input->post('status'); 
			
			$res = $this->paymentgatewaymodel->savedata($cdata);

			if($res)
			{
				if($this->session->userdata('site_lang') == 'english')
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Payment Gateway added successfully</div>');
				elseif($this->session->userdata('site_lang') == 'indonesia')
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Gerbang pembayaran berhasil ditambahkan</div>');
	        	

	        	redirect('paymentgateway');
			}
          }

              	
	}

	public function edit($id="")
	{
		 $data['modelData'] = $this->paymentgatewaymodel->getById($id);

		$this->template->show("paymentgateway", "edit", $data);      	
	}

	public function details($id="")
	{
		 $data['modelData'] = $this->paymentgatewaymodel->getById($id);
         
         $this->template->show("paymentgateway", "details", $data);      	
	}

	public function update()
	{

		$data['modelData'] = $this->paymentgatewaymodel->getById($this->input->post('id'));
		
		$is_unique ="";
		if($this->input->post('name') != $data['modelData']['name']) 
		{
			$getstatus=$this->basemodel->getDeleteStatus('paymentgateway','name',$this->input->post('name'));
			if(count($getstatus)>0)
			{
				$delstat = $getstatus[0]['deletestatus'];
				if($delstat  == '0') {
						     $is_unique =  '|is_unique[gm_paymentgateway.name]';
					} else {
						
						$is_unique =  '';
					}
			}
			else
			{
				$is_unique =  '|is_unique[gm_paymentgateway.name]';
			}
		}

		$this->form_validation->set_rules('name', $this->lang->line('name'), 'required|regex_match[/^[A-Za-z -]+$/]'.$is_unique);
		
		if ($this->form_validation->run() == FALSE)
		   $this->template->show("paymentgateway", "edit", $data);
          else
          {
		 	 $cdata['name'] = $this->input->post('name');
		               
		 	$res=$this->paymentgatewaymodel->update_info($cdata, $this->input->post('id'));

			   
		 	 
		 	if($res)
	         {
	         	if($this->session->userdata('site_lang') == 'english')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Payment Gateway  updated successfully</div>');
	         	elseif($this->session->userdata('site_lang') == 'indonesia')
	         	$this->session->set_flashdata('updatemsg', '<div class="alert alert-success">Gerbang pembayaran  berhasil diperbarui</div>');
		        
		        redirect('paymentgateway');     	
	         }
         }

         
	}

	public function delete($id)
	{

	 	$res=$this->paymentgatewaymodel->delete_info($id);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Payment Gateway deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Gerbang pembayaran diperbarui</div>');
			

			redirect('paymentgateway');  
		}

	}

 		
}
