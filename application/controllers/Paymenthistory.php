<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymenthistory extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('serviceordermodel');
		$this->load->model('paymentmodel');
		$this->load->model('currencymodel');

		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}

	public function index()
	{
		
	    $data['paymenthistory'] = $this->serviceordermodel->get_servicerequest();

	    if($_POST)
			$data['paymenthistory'] = $this->paymentmodel->searchdata($_POST);

		$this->template->show("paymenthistory", "index", $data);
	}

	

	


}
