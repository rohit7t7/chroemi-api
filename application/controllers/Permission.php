<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		$this->load->model('permissionmodel');	
		$this->load->model('rolemodel');
		$this->load->model('supervisorsmodel');

		
		if(!isset($_SESSION['logged_in']))
			{
				redirect('login');
			}



	}
	public function index()
	{
		if($this->session->userdata('site_lang') == 'english') 
		$data = $this->permissionmodel->getPermission('1');
		elseif($this->session->userdata('site_lang') == 'indonesia') 
		$data = $this->permissionmodel->getPermission('2');


		$this->template->show("permission", "index", $data);
	}

	

	public function create()
	{
		
		$data['superData']=$this->supervisorsmodel->get_supervisors();

		$data['roleData']=$this->rolemodel->get_role();
		$this->form_validation->set_rules('supervisor_id', $this->lang->line('supervisor'), 'required');
		

		  if ($this->form_validation->run() == FALSE)
		  	   $this->template->show("permission", "create", $data);
          else
          {
          	
		   $cdata['name'] = $this->input->post('name');
		   $cdata['email'] = $this->input->post('email');
		   $cdata['phone'] = $this->input->post('phone');
		   $cdata['password'] = $this->input->post('password');
		   $cdata['createddate']= date("Y-m-d H:m:s");
		  
		   $cdata['language_id']= $this->input->post('language_id'); 
		   $cdata['status']= $this->input->post('status');

		  echo $this->sendmail(); 

			$res = $this->supervisorsmodel->savedata($cdata);

			if($res)
			{
				if($this->session->userdata('site_lang') == 'english')
	        			$this->session->set_flashdata('msg', '<div class="alert alert-success">Supervisor added successfully</div>');
	        		elseif($this->session->userdata('site_lang') == 'indonesia')
	        			$this->session->set_flashdata('msg', '<div class="alert alert-success">Pengawas berhasil ditambahkan</div>');

				//$this->session->set_flashdata('msg', '<div class="alert alert-success">Supervisors added successfully</div>');
	        	redirect('supervisors');
			}
          }

              	
	}

	public function details($id)
	{
		 $data['modelData'] = $this->supervisorsmodel->getById($id);
         $data['languageData']=$this->languagemodel->get_languages();
         
         $this->template->show("supervisors", "details", $data);      	
	}

	public function edit($id="",$supervisoerId="")
	{
		$data['id'] = $id;

		 $data['modelData'] = $this->permissionmodel->getById($id,$supervisoerId);
		 
         $data['roleData']=$this->rolemodel->get_role();
         //echo "<pre>";print_r($data['modelData']);echo "</pre>";die;

         if($this->input->post())
         {
         	
         	$res=$this->permissionmodel->update_info($this->input->post('role_id'),$this->input->post('allpermission'),$supervisoerId);
         }

         
         $this->template->show("permission", "edit", $data);      	
	}

	public function update($supervisoerId)
	{
		$data['id'] = $supervisoerId;
         $data['superData']=$this->supervisorsmodel->get_supervisors();
         $data['supervisorData']=$this->supervisorsmodel->getById($supervisoerId);

		 $data['modelData'] = $this->permissionmodel->getById('0',$supervisoerId);
		 
         $data['roleData']=$this->rolemodel->get_role();
         //echo "<pre>";print_r($data['modelData']);echo "</pre>";die;

         if($this->input->post())
         {
         	$res=$this->permissionmodel->update_info($this->input->post('role_id'),$this->input->post('allpermission'),$supervisoerId);
         }

         
         $this->template->show("permission", "create", $data); 

         
	}

	public function delete($id,$supervisorid)
	{

	 	$res=$this->permissionmodel->delete_info($id,$supervisorid);

	 	if($res)
	    {
	    	if($this->session->userdata('site_lang') == 'english')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Permission deleted successfully</div>');
	    	elseif($this->session->userdata('site_lang') == 'indonesia')
	    	$this->session->set_flashdata('deletemsg', '<div class="alert alert-success">Izin dihapus berhasil</div>');
			

			redirect('permission');  
		}

	}

	//send mail
	function sendmail()
	{

		// Configure email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = 'antarapal81@gmail.com';
        $config['smtp_pass'] = 'ant123456';
        $config['mailtype'] = 'html';
    	$config['charset'] = 'iso-8859-1';
    	$config['wordwrap'] = TRUE;
    	$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

    	$templatedet = $this->supervisorsmodel->get_emailtemplate();
    	//$body = file_get_contents('email-template.php');
    	
    	$emailusername = str_replace('%USERNAME%', $this->input->post('email'), $templatedet[0]['body']);
    	$emailpwd = str_replace('%PASSWORD%', $this->input->post('password'), $emailusername);
    	$emailbody = $emailpwd;

		$body = '<!DOCTYPE HTML>'.
		'<head><meta http-equiv="content-type" content="text/html">'.
		'<title>Email notification</title></head>'.
		'<body>'.$emailbody.
		'<div id="footer" style="width: 80%;height: 40px;margin: 0 auto;text-align: center;padding: 10px;font-family: Verdena;background-color: #E2E2E2;">'.
		   'All rights reserved @ mysite.html 2014'.
		'</div>
		</body>'; 

	    $this->load->library('email',$config); // load email library
	    $this->email->from('antarapal81@gmail.com', 'Antara Ghosh');
	    $this->email->to('debraj@navsoft.in');
	    //$this->email->cc(''); 
	    $this->email->subject($templatedet[0]['subject']);
	    $this->email->message($body);
	    //$this->email->attach('/path/to/file1.png'); // attach file
	    //$this->email->attach('/path/to/file2.pdf');
	    if ($this->email->send())
	        echo "Mail Sent!";
	    else
	        echo "There is error in sending mail!";
	}

}