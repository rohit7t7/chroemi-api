<?php

//defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,apikey,token");
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        //Load models
        $this->load->model('Apimodel');
        $this->load->helper('api');
        $this->load->library('twilio');

        //Authenticate before api call
        $this->Authenticate();
    }

    /* Sign up api
      Created : 9-8-2017
    */
    public function signup_post(){
      $email      = $this->input->post('email');
      $password   = $this->input->post('password');
      $name       = $this->input->post('name');
      $gender     = $this->input->post('gender');
      $dob        = $this->input->post('dob');
      $phone      = $this->input->post('phone');
      if(!$email || !$password || !$name || !$phone){
        $this->response(successErrorCode(201) + array('data' =>'Please provide all credentials'));
      }else{
        $user_data = array( 'email'     => $email,
                            'name'      => $name,
                            'phone'     => $phone,
                            'gender'    => $gender,
                            'password'  => md5($password),
                            'dob'       => (!$dob)?'':$dob,
                            'createddate' => date("Y-m-d")
                            );
        $table_name = 'users';
        //$params     = array('data'=>$userdata,'table'=>$table_name)
        try {
          $user_id  = $this->Apimodel->add($table_name,$user_data);
        } catch (UserException $error){
          //do whatever you want when there is an mysql error
            $this->response(successErrorCode(202) + array('data' =>'Something went wrong . Please try again'));
        }
        if($user_id){
           // Add update access token
           $accessToken = generateToken();
           $table       = "user_tokens";
           $data        = array('token'=>$accessToken,'user_id'=>$user_id);
           $this->Apimodel->add($table,$data);
           // get user detail after sign up
           $data        = array('id','name','email','phone','gender','dob');
           $condiction  = array('id'=>$user_id);
           $userData    = $this->Apimodel->get($table_name,$data,null,$condiction);
           $finalArray  = array('token'=>$accessToken,'id'=>$userData[0]->id,'name'=>$userData[0]->name,'email'=>$userData[0]->email,'phone'=>$userData[0]->phone,'gender'=>$userData[0]->gender,'dob'=>$userData[0]->dob);
           $this->response(successErrorCode(200) + array('data'=>$finalArray));
        }else{
           $this->response(successErrorCode(201) + array('data'=>'Something went wrong. Please try again later'));
        }

      }
    }

    /* Login  api
      Created : 12-8-2017
    */
    public function login_post(){
        $email      = $this->input->post("email");
        $password   = $this->input->post("password");
        $table      = "users";
        $field      = array('id,name,email');
        $condiction = array('email' => $email,'password' => md5($password));
        $userData   = $this->Apimodel->get($table,$field,null,$condiction);
        if(!empty($userData)){
          $accessToken = generateToken();
          $table    = "user_tokens";
          $data     = array('token'=>$accessToken,'user_id'=> $userData[0]->id);
          $resultId = $this->Apimodel->add($table,$data);
          if($resultId){
            $finalArray = array('token'=>$accessToken,'id'=>$userData[0]->id,'name'=>$userData[0]->name,'email'=>$userData[0]->email);
            $this->response(successErrorCode(200) + array('data'=>$finalArray));
          }else{
            $this->response(successErrorCode(201) + array('data'=> 'Something went wrong . Please try again latter'));
          }
        }else{
          $this->response(successErrorCode(201) + array('data'=> 'Invalid login credential'));
        }
      }


      /* Social login and Sign up api
        Created : 9-8-2017
      */
      public function socialSignup_post(){
        $email      = $this->input->post('email');
        $login_type = $this->input->post('login_type');
        $social_token   = $this->input->post('social_token');
        $name       = $this->input->post('name');
        $gender     = $this->input->post('gender');
        $dob        = $this->input->post('dob');
        $phone      = $this->input->post('phone');
        $params     = array('login_type'=>$login_type,'social_token'=>$social_token);
        $returnId   = $this->checkUserBeforeSignup($params);
        if($returnId){
          $table      = "users";
          $field      = array('id,name,email');
          $condiction = array('id' => $returnId);
          $userData   = $this->Apimodel->get($table,$field,null,$condiction);
          if(!empty($userData)){
            $accessToken = generateToken();
            $table    = "user_tokens";
            $data     = array('token'=>$accessToken,'user_id'=>$returnId);
            $resultId = $this->Apimodel->add($table,$data);
            if($resultId){
              $finalArray = array('token'=>$accessToken,'id'=>$userData[0]->id,'name'=>$userData[0]->name,'email'=>$userData[0]->email);
              $this->response(successErrorCode(200) + array('data'=>$finalArray));
            }else{
              $this->response(successErrorCode(201) + array('data'=> 'Something went wrong . Please try again latter'));
            }
          }else{
            $this->response(successErrorCode(201) + array('data'=> 'Invalid login credential'));
          }
        }else{
          $filename   = '';
        //  print_r();
          if(!empty($_FILES) && $_FILES['image']){
            $image       =  $_FILES['image'];
            $uploads_dir = 'upload/users';
            $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
            if(in_array($image['type'], $allowed_mime_type_arr)){
              $filename = time().$image['name'];
              move_uploaded_file($image['tmp_name'], $uploads_dir."/".$name);
            }
          }
          $user_data = array( 'email'     => $email,
                              'name'      => $name,
                              'phone'     => $phone || '',
                              'image'     => $filename,
                              'gender'    => $gender,
                              'createddate' => date("Y-m-d")
                              );
          $table_name = 'users';
          try {
            $user_id  = $this->Apimodel->add($table_name,$user_data);
          } catch (UserException $error){
            $this->response(successErrorCode(202) + array('data' =>'Something went wrong . Please try again hkgjhjg'));
          }
          if($user_id){
             // Add update access token
             $accessToken = generateToken();
             $table       = "user_tokens";
             $data        = array('token'=>$accessToken,'user_id'=>$user_id);
             $this->Apimodel->add($table,$data);

             //Add social info
             $table       = "user_social";
             $data        = array('social_token'=>$social_token,'user_id'=>$user_id,'social_type'=>$login_type);
             $this->Apimodel->add($table,$data);

             // get user detail after sign up
             $data        = array('id','name','email','phone','gender','dob');
             $condiction  = array('id'=>$user_id);
             $userData    = $this->Apimodel->get($table_name,$data,null,$condiction);
             $finalArray  = array('token'=>$accessToken,'id'=>$userData[0]->id,'name'=>$userData[0]->name,'email'=>$userData[0]->email,'phone'=>$userData[0]->phone,'gender'=>$userData[0]->gender,'dob'=>$userData[0]->dob);
             $this->response(successErrorCode(200) + array('data'=>$finalArray));
          }else{
             $this->response(successErrorCode(201) + array('data'=>'Something went wrong. Pleaaaaase try again later'));
          }
        }
      }


      private function checkUserBeforeSignup($param){
        $table      = 'user_social';
        $condiction = array('social_type'=>$param['login_type'],'social_token'=>$param['social_token']);
        $field      = array('id,user_id');
        $socialData = $this->Apimodel->get($table,$field,null,$condiction);
        if(count($socialData) >0 && !empty($socialData)) {
          return $socialData[0]->user_id;
        }else{
          return false;
        }
      }

      /* Email verification  api
        Created : 12-8-2017
      */
    public function verifyEmail_post(){
      $email      = $this->input->post("email");
      $table      = 'users';
      $condiction = array('email'=>$email);
      $otp        =  rand(10000,99999);
      $field      = array('id');
      $userData   = $this->Apimodel->get($table,$field,null,$condiction);
      if(count($userData) >0 && !empty($userData)) {
        $this->response(successErrorCode(201) + array('data'=>'Email already exist'));
      }else{
        sendOtp($email,$otp);
        $this->response(successErrorCode(200) + array('otp'=>$otp,'data'=>'Otp send to your email'));
      }
    }

    /* Phone verification  api
      Created : 12-8-2017
    */
    public function verifyPhone_post(){
      $phone      = $this->input->post("phone");
      $table      = 'users';
      $condiction = array('phone'=>$phone);
      $otp        = rand(10000,99999);
      $field      = array('id');
      $userData   = $this->Apimodel->get($table,$field,null,$condiction);
      if(count($userData) >0 && !empty($userData)) {
        $this->response(successErrorCode(201) + array('data'=>'Phone number already exist'));
      }else{
        $from     = '+15005550006';
        $to       = '+919681062313';
        $message  = 'Your otp to verify phone number is '.$otp;
        $response = $this->twilio->sms($from, $to, $message);
          if (!$response->IsError){
            $this->response(successErrorCode(200) + array('otp' =>$otp));
          }else {
            $this->response(['status' => false,'message' => $response->ErrorMessage], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
          }
      }
    }

    /*Logout api
      Created : 12-08-2017
    */
    public function logout_post(){
     $token        = $this->input->get_request_header('token',TRUE);
     if($token){
       $table      = 'user_tokens';
       $condiction = array('token'=>$token);
       $response   = $this->Apimodel->delete($table,$condiction);
       $this->response(successErrorCode(200) + array('data'=>'Logout successfully'));
     }else{
       $this->response(successErrorCode(201) + array('data'=>'Please provide all valid credential'));
     }
    }

    //Authenticate api calling
    private function Authenticate(){
      if(!$this->input->get_request_header('apikey',TRUE)){
          $this->response(successErrorCode(405));
      }elseif($this->input->get_request_header('apikey',TRUE) != 'uthjhi787ybg88fgh788'){
          $this->response(successErrorCode(406));;
      }
    }

}
