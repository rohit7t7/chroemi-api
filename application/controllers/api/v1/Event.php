<?php

//defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,apikey,token");
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Event extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        //Load models
        $this->load->model('Apimodel');
        $this->load->helper('api');
        //Authenticate before  api call
        $this->Authenticate();
    }

    /* Event list api
      Created : 14-08-2017
    */
    public function list_post(){
      $pageno = $this->input->post('pageno');
      $pagesize = $this->input->post('pagesize');
      $table = "events as e";
      $field = array('e.id,e.user_id,e.name ,e.description,e.features,c.category,esc.name as subevent');
      $join = array('_event_scenes as esc'=>'e.id=esc.event_id','_category as c'=>'e.categoryid=c.id');
      $join_type = array('_event_scenes as esc'=>'LEFT','_category as c'=>'LEFT');
      $offset_size = getOffsetSize($pageno,$pagesize);
      $eventData=$this->Apimodel->get($table,$field,null,null,$join,$join_type,$offset_size['offset'],$offset_size['limit']);
      $this->response(successErrorCode(200) + array('data' =>$eventData));
    }


    /* Event list api
      Created : 14-08-2017
    */
    public function category_post(){
      $pageno = $this->input->post('pageno');
      $pagesize = $this->input->post('pagesize');
      $table = "category";
      $field = array('id,category,imageavatar');
      //$join = array('_event_scenes as esc'=>'e.id=esc.event_id','_category as c'=>'e.categoryid=c.id');
      //$join_type = array('_event_scenes as esc'=>'LEFT','_category as c'=>'LEFT');
      $offset_size = getOffsetSize($pageno,$pagesize);
      $categoryData=$this->Apimodel->get($table,$field,null,null,null,null,$offset_size['offset'],$offset_size['limit']);
      $this->response(successErrorCode(200) + array('data' =>$categoryData));
    }

    public function subevents_get(){
      $table = "events as e";
      $field = array('e.id,e.user_id,e.name ,e.description,e.features,c.category,esc.name as subevent');
      $join = array('_event_scenes as esc'=>'e.id=esc.event_id','_category as c'=>'e.categoryid=c.id');
      $join_type = array('_event_scenes as esc'=>'LEFT','_category as c'=>'LEFT');
      $eventData=$this->Apimodel->get($table,$field,null,null,$join,$join_type);
      $this->response(successErrorCode(200) + array('data' =>$eventData));
    }

    //Authenticate api calling
    private function Authenticate(){
      $apikey = $this->input->get_request_header('apikey',TRUE);
      $token  = $this->input->get_request_header('token',TRUE);
      $table  = "user_tokens";
      $field  = array('id,token');
      $condiction = array('token' => $token);
      $tokenData  = $this->Apimodel->get($table,$field,null,$condiction);
      if(!$apikey){
          $this->response(successErrorCode(405));
      }elseif($apikey != 'uthjhi787ybg88fgh788'){
          $this->response(successErrorCode(406));
      }elseif(!$token || empty($tokenData)){
          $this->response(successErrorCode(407));
      }
    }
}
