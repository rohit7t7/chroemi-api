<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Twiliodemo extends REST_Controller {

	function __construct()
	{
		parent::__construct();
                $this->load->library('twilio');
	}

	function index_get()
	{
		
            
		$from = '+15005550006';
		$to = '+15005550005';
		$message = 'This is a test...';

		$response = $this->twilio->sms($from, $to, $message);

                if (!$response->IsError)
            {
            // Check if the users data store contains users (in case the database result returns NULL)
            
                // Set the response and exit
                $this->response('Sent message to ' . $to, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => $response->ErrorMessage
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
           
       
		
	}

}

/* End of file twilio_demo.php */