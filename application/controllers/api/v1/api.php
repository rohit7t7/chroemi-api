<?php defined('BASEPATH') OR exit('No direct script access allowed');

    // This can be removed if you use __autoload() in config.php OR use Modular Extensions
    require APPPATH.'/libraries/REST_Controller.php';

    class Api extends REST_Controller
    {
            function __construct()
        {
            // Construct our parent class
            parent::__construct();
            $this->load->model('api_model');
            $this->load->model('user');
            $this->load->helper('api');
            $this->load->model('state');
            $this->load->model('project');
            $this->load->model('city');
            $this->load->model('book');
            $this->load->model('book_type');
            $this->load->model('article');
            $this->load->model('contractor');
            $this->load->model('design_trend');
            $this->load->model('doit');
            $this->load->model('interview_Model');
            $this->load->model('interiopediaproduct');
            $this->load->model('designer_Week');
            $this->load->model('product');
            $this->load->model('testimonial');
            $this->load->model('project_posts');
            $this->load->model('forum');
            $this->load->model('mentorship');
            $this->load->model('contractor_budget');
            $this->load->model('home_search');
            $this->load->model('room_area_type');
            $this->load->model('door_window');
            $this->load->model('paneling_partition');
            $this->load->model('floor');
            $this->load->model('ceiling');
            $this->load->model('room_size');
            $this->load->model('unit_type');
            $this->load->model('painting');
            $this->load->model('lighting_concept');
            $this->load->model('colour_theme');
            $this->load->model('furniture_theme_style');
            $this->load->model('exterior_theme_style');
            $this->load->model('interior_theme_style');
            $this->load->model('furniture_theme_style');
            $this->load->model('material');
            $this->load->model('furniture_type');
            $this->load->model('project_budget');
            $this->load->model('colour');
            $this->load->model('type');
            $this->load->model('type', 'project_type');
            $this->load->model('work_type');
            $this->load->model('basic_model');

            if(!$this->input->post('apikey'))
            {
                $this->response(array('responsecode'=> 405,'error'=>'API key is missing.'));

            }elseif($this->input->post('apikey') != 'uthjhi787ybg88fgh788'){

                $this->response(array('responsecode'=> 406,'error' => 'Invalid API key'));
            }
        }

        public $rest_format = 'json';

      /*User Section Apis Start */

     /* Api for Registration */
        function registration_post()
        {
             //Validate User before registration
            $this->validation("Registration");
            $lang_id=$this->get_language_id();
            $name=  explode(' ', $this->post('name'));
            $firstname = !empty($name[0])?$name[0]:'';
            $middlename = !empty($name[1])?$name[1]:'';
            $lastname = $middlename.' '.(!empty($name[2])?$name[2]:'');
            $random_str = random_string('alnum', 16);
            $user_data = array('user_type_id' => $this->post('usertype'),
                                'firstname' => $firstname,
                                'lastname' => $lastname,
                                'email' => base64_decode($this->post('email_id')),
                                'username' => $this->post('name'),
                                'password' => md5(base64_decode($this->post('password'))),
                                'phone' => base64_decode($this->post('mobile')),
                                'area' => $this->post('area'),
                                'address' => $this->post('address'),
                                'postcode' => $this->post('pincode'),
                                'state_id' => $this->post('stateid'),
                                'city_id' => $this->post('cityid'),
                                'language_id' =>$lang_id,
                                'created'=>date("Y-m-d H:i:s"),
                                'modified'=>date("Y-m-d H:i:s"),
                                'last_logged_in'=>date("Y-m-d H:i:s"),
                                'activation_code' => $random_str,
                                'status' => 0
                                );
             $user_id = $this->api_model->post_data($user_data,'users');
             if($user_id){
                 appactivationEmail($user_id);
                 $this->response(array('responsecode'=>200,'responsedetails'=>'Success','user_id'=>$user_id));
             }else{
                 $this->response(array('error'=>'Something went wrong. Please try again later'),500);
             }

        }


       /* Api to re-send activation code */
        function profileactivationresend_post()
        {
            $user_id=$this->post('user_id');
            $table='users';
            $condiction=array('user_id'=>$user_id,'status'=>0);
            $count=$this->api_model->count_all($table,$condiction);
            if($count>0){
                 if(appactivationEmail($user_id)){
                     $this->response(array('responsecode'=>200,'responsedetails'=>'Success','user_id'=>$user_id));
                 }else{
                     $this->response(array('error'=>'Something went wrong. Please try again later'),500);
                 }
             }else{
                 $this->response(array('error'=>'Your account is already activated'),500);
            }
        }

        /* Api to activate profile */
        function profileactivation_post()
        {
            $activation_code=$this->post('activation_code');
            $user_id=$this->post('user_id');
            $table='users';
            $condiction=array('user_id'=>$user_id,'status'=>0,'activation_code'=>$activation_code);
            $count=$this->api_model->count_all($table,$condiction);
            if($this->post('device_type')!='Android' && $this->post('device_type')!='IOS'){
                        $this->response(array("responsecode"=> 202,"error"=>"Invalid device type"));
            }
            if($count>0){
                $updata = array(
                    'status' => 1,
                    'activation_code' => ''
                );
                $condition = array(
                    'activation_code' => $activation_code,
                    'user_id'=>$user_id
                );
                $user_update = $this->user->update($updata, $condition);
                if($user_update){
                    $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$user_id;
                    $token= generateToken($token);
                    $final_result=array();
                    $session_token=array('responsecode'=>200,'responsedetails'=>'success','sessiontoken'=>$token);
                    $cond=array('user_id'=>$user_id);
                    $user_details=$this->api_model->get_user_details_by_param($cond);
                    if($user_details !='NULL'){
                    $user_details = array_map(function($tag) {
                       return array(
                       'userid' => $tag['userid'],
                       'remainsearch'=>remainContractorSearch($tag['userid']),
                       'name' => $tag['name'],
                       'email'=>$tag['email'],
                       'mobile'=>$tag['mobile'],
                       'usertype'=>$tag['usertype']
                       );
                   }, $user_details);
                    }else{
                       $this->response(array('error'=>'Something went wrong. Please try again later'),500);
                    }
                    if($user_details){
                        $device_details=array('user_id' =>$user_id,
                                              'device_token' =>$this->post('device_token'),
                                              'device_type' =>$this->post('device_type'),
                                              'device_id' =>$this->post('device_id'),
                                              'token' =>$token);
                        $this->api_model->add_delete_token($user_id,$device_details);
                    }
                    $final_result=($session_token + $user_details[0] + array('logintype'=>'app'));
                    $this->response($final_result,200); // 200 being the HTTP response code
                }else{
                    $this->response(array('error'=>'Something went wrong. Please try again later'),500);
                }
             }else{
                 $this->response(array('error'=>'Invalid activaton code'),500);
            }
        }

       /* Api for Login */
        function login_post()
        {
            //Validate User before Login
             $this->validation("Login");
             if(is_numeric(base64_decode($this->post('email_mobile')))){
                $cond=array('phone'=>base64_decode($this->post('email_mobile')),'password'=>md5(base64_decode($this->post('password'))));
             }else{
                $cond=array('email'=>base64_decode($this->post('email_mobile')),'password'=>md5(base64_decode($this->post('password'))));
             }
             $user_details=$this->api_model->get_user_details_by_param($cond);

             if($user_details !='NULL'){
             $user_details = array_map(function($tag) {
                return array(
                'userid' => $tag['userid'],
                'remainsearch'=>remainContractorSearch($tag['userid']),
                'name' => $tag['name'],
                'email'=>$tag['email'],
                'mobile'=>$tag['mobile'],
                'usertype'=>$tag['usertype'],
                'profileimage'=>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                'location'=>$tag['location']
                );
            }, $user_details);
             }else{
                $this->response(array("responsecode"=> 202,"error"=>"Invalid login details"));
             }
             $user_data=array('last_logged_in'=>date("Y-m-d H:i:s"));
             $this->api_model->update_user($user_data,$cond);
             $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$user_details[0]['userid'];
             $token= generateToken($token);
             $device_details=array( 'user_id' =>$user_details[0]['userid'],
                                    'device_token' =>$this->post('device_token'),
                                    'device_type' =>$this->post('device_type'),
                                    'device_id' =>$this->post('device_id'),
                                    'token' =>$token);
             $this->api_model->add_delete_token($user_details[0]['userid'], $device_details);
             $final_result=array();
             $sessiontoken=array('responsecode'=>200,'responsedetails'=>'Success','sessiontoken'=>$token);
             $final_result=($sessiontoken + $user_details[0] +array('logintype'=>'app'));
             $this->response($final_result,200); // 200 being the HTTP response code
        }

        /* Api for Contractor Login */
        function contractorlogin_post()
        {
            //Validate User before Login
             if(is_numeric(base64_decode($this->post('mobile')))){
                $cond=array('PrimaryMobileNo'=>base64_decode($this->post('mobile')),'status'=>1);
             }else{
                $this->response(array('responsedetails'=>'You are not a Fevicol Design Ideas registered Contractor. Please contact 18002670707 for registration.'));
             }
             if($this->post('device_type')!='Android' && $this->post('device_type')!='IOS'){
                $this->response(array("responsecode"=> 202,"error"=>"Invalid device type"));
                }
             $data=array('user_id');
             $table='user_contractors';
             $contractor_details=$this->api_model->get_data($table,$cond,$data);

             if(isset($contractor_details[0]['user_id'])){
                $this->config->load('sms');
                $this->load->library('SmsLib');
                $otpCode = rand(100000, 999999);
                $userid=$contractor_details[0]['user_id'];
                $To=base64_decode($this->post('mobile'));
                $text = $otpCode.' is your One Time Password for logged in to the Fevicol Design Ideas Application. Please use this password to confirm your login';
                $param_row=array('url'=>$this->config->item('url'),'request_type'=>'POST','response_type'=>'xml');
                $param=array('feedid'=>$this->config->item('feedid'),'username'=>$this->config->item('username'),'password'=>$this->config->item('password'),'To'=>$To,'Text'=>$text);
                $param=serialize($param);
                $response = $this->smslib->sendOTP($param_row,$param);
                if($response)
                {
                    if(is_array($response) && (array_key_exists('REQUEST-ERROR', $response)|| array_key_exists('RESPONSE-ERROR', $response)))
                    {
                        $this->response(array('error'=>'Something went wrong. Please try again later'));
                    }
                    else
                    {
                        $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$userid;
                        $token= generateToken($token);
                        $device_details=array('user_id' =>$userid,
                                              'device_token' =>$this->post('device_token'),
                                              'device_type' =>$this->post('device_type'),
                                              'device_id' =>$this->post('device_id'),
                                              'token' =>$token);
                        $this->api_model->add_delete_token($userid,$device_details);
                        $this->response(array('responsecode'=>200,'responsedetails'=>'success','OTP'=>$otpCode,'user_id'=>$userid,'sessiontoken'=>$token));
                    }
                }
                else
                {
                   $this->response(array('error'=>'Something went wrong. Please try again later'));
                }
             }
             else{
                $this->response(array('error'=>'Invalid mobile number'));
             }
        }

        /* Api to Validate Otp for contractor */
        function validateOTP_post()
        {
            $this->validation("sessiontoken");
            $user_id=$this->post('user_id');
            $table='user_contractors as uc';
            $cond=array('u.user_id'=>$user_id);
            $data=array('u.user_id','u.user_image','u.cover_image','uc.ContractorTypeID','uc.FirstName','uc.MiddleName','uc.LastName',
                        'uc.PrimaryMobileNo','uc.user_type_id','uc.MembershipNo','uc.RedeemedPoints','uc.BalancePoints',
                        'uc.NoOfCarpentors','uc.PriDealerName','uc.Alt1DealerName','uc.Alt2DealerName','uc.AdopterFirstName',
                        'uc.AdopterMiddleName','uc.AdopterLastName','uc.AdopterDesignation','uc.AdopterMobile','uc.ClubCode');
            $join=array('users as u'=>'uc.user_id=u.user_id');
            $contractor_details=$this->api_model->get_data($table,$cond,$data,$join);
            if(isset($contractor_details[0]['user_id'])){
                $table1='badges';
                $cond1=array('user_id'=>$user_id);
                $data1=array('badge_id','image');
                $award_images=$this->api_model->get_data($table1,$cond1,$data1);
                if(isset($award_images[0])){
                    $award_images= array_map(function($tag1) {
                           return array(
                           'imageid' => $tag1['badge_id'],
                           'imageurl'=>base_url().'uploads/badge/images/'.(($tag1['image']=='')?'dummy.jpg':$tag1['image']),
                           );
                       }, $award_images);
                    $contractor_details[0]['awardsimages']=$award_images;
                }else{
                    $contractor_details[0]['awardsimages']=array();
                }
                $lang_id=1;
                $projectphotos=$this->api_model->get_project_images($user_id,$lang_id);
                $noofprojects=$this->api_model->count_all('projects',array('user_id'=>$user_id));
                $contractor_details[0]['noofprojects']=$noofprojects;

                    if(count($projectphotos)>0){
                    $projectphotos= array_map(function($tag2) {
                    return array(
                        'projectimageid' => $tag2['project_image_id'],
                        'projectid' => $tag2['project_id'],
                        'projectcoverimageURL' => base_url().'uploads/portfolio/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                        'projectname'=>$tag2['title'],
                        'projectlikecount'=>$this->api_model->count_all('project_image_likes',array('image_id'=>$tag2['project_image_id'])),
                        'projectviewcount'=>$tag2['view_num']
                    );
                    }, $projectphotos);
                    $contractor_details[0]['projectphotos']=$projectphotos;
                    } else{
                    $contractor_details[0]['projectphotos']=array();
                    }
                $contractor_details= array_map(function($tag) {
                       return array(
                       'userid' => $tag['user_id'],
                       'name' => $tag['FirstName'].' '.$tag['MiddleName'].' '.$tag['LastName'],
                       'profileimage'=>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                       'coverimageurl'=>base_url().'uploads/users/cover_images/'.(($tag['cover_image']=='')?'dummy.jpg':$tag['cover_image']),
                       'contractortype'=>$tag['ContractorTypeID'],
                       'mobile'=>$tag['PrimaryMobileNo'],
                       'membershipnumber'=>$tag['MembershipNo'],
                       'redeemedpoints'=>$tag['RedeemedPoints'],
                       'balancepoints'=>$tag['BalancePoints'],
                       'awardsimages'=>$tag['awardsimages'],
                       'noofprojects'=>$tag['noofprojects'],
                       'projectphotos'=>$tag['projectphotos'],
                       'totalteammebers'=>$tag['NoOfCarpentors'],
                       'primarydealer'=>$tag['PriDealerName'],
                       'otherdealers'=>$tag['Alt1DealerName'].','.$tag['Alt2DealerName'],
                       'adoptername'=>$tag['AdopterFirstName'].' '.$tag['AdopterMiddleName'].' '.$tag['AdopterLastName'],
                       'adoptercompanyname'=>$tag['AdopterDesignation'],
                       'adopterphonenumber'=>$tag['AdopterMobile'],
                       'clubcode'=>$tag['ClubCode'],
                       'usertype'=>$tag['user_type_id']
                       );
                   }, $contractor_details);
                $final_result=(array('responsecode'=>200,'responsedetails'=>'Success') + $contractor_details[0] +array('logintype'=>'app'));
                $this->response($final_result,200);
            }else{
                $this->response(array('error'=>'Something went wrong. Please try again later'),500);
            }

        }

        /* Api for vaidate session token */
        function validatesessiontoken_post()
        {
            $data=array('user_id'=>$this->post('user_id'),
                        'token'=>$this->post('sessiontoken'),
                        'device_token'=>$this->post('device_token'),
                        'device_type'=>$this->post('device_type'),
                        'device_id'=>$this->post('device_id'));
            $validate=$this->api_model->validatetoken($data);
            if($validate=='NULL'){
                $this->response(array('error'=>'Session expired'));
            }else{
                $this->response(array('responsecode'=>200,'responsedetails'=>'Success'));
            }

        }

        /* Api for Forgot password */
        function forgotpassword_post()
        {
            //Validate User before Login
            $this->validation("forgotpassword");
            $condition = array('email' => base64_decode($this->post('email_id')),
                               'status' => 1
                                    );
            $check_fpass = $this->user->fieldValueCheck($condition);
            if($check_fpass){
                    $random_str = random_string('alnum', 16);
                    $forgot_password = md5($random_str);
                    $updata = array(
                            'forgot_pass' => $forgot_password
                    );
                    $condition = array(
                            'user_id' => $check_fpass
                    );
                    $user_update = $this->user->update($updata,$condition);

                    if($user_update){
                            $this->load->helper('user_email');
                            forgotPasswordEmail($check_fpass,'App');
                            $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
                    }else{
                            $this->response(array('error'=>'Something went wrong. Please try again later'),500);
                    }
            }
        }

         /* Api to logout */
        function logout_post()
        {

            $logout=$this->api_model->logout($this->post('userid'));
            if($logout=='true'){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api for myprofile */
        function myprofile_post()
        {
            $this->validation("sessiontoken");
            $userid=$this->post('user_id');
            $lang_id=$this->get_language_id();
            $userdetails=$this->api_model->designerprofile($userid,$lang_id);
            if(isset($userdetails[0])){
            $userdetails[0]['followers']=$this->api_model->count_all('user_followers',array('user_id'=>$userid));
            $userdetails[0]['comments']=$this->api_model->count_all('project_image_comments',array('user_id'=>$userid));
            $userdetails[0]['messages']=$this->api_model->count_all('user_messages',array('reciever_id'=>$userid));
            $userdetails[0]['reviews']=$this->api_model->count_all('user_review',array('user_id'=>$userid));
            $userdetails= array_map(function($tag) {
                return array(
                'userid' => $tag['user_id'],
                'name' => $tag['name'],
                'designation' => $tag['designation'],
                'location' => $tag['area'],
                'profileimageurl' =>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                'mobileno' => $tag['phone'],
                'followers' => $tag['followers'],
                'comments' => $tag['comments'],
                'reviews'=> $tag['reviews'],
                'messages'=>$tag['messages']
                );
            }, $userdetails);
            }else{
            $userdetails=array();
            }
            $projectphotos=$this->api_model->get_project_images($userid,$lang_id);
            if(count($projectphotos)>0){
            $projectphotos= array_map(function($tag2) {
            return array(
            'projectimageid' => $tag2['project_image_id'],
            'projectcoverimageURL' => base_url().'uploads/portfolio/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
            'projectname'=>$tag2['title'],
            'projectlikecount'=>$this->api_model->count_all('project_image_likes',array('image_id'=>$tag2['project_image_id'])),
            'projectviewcount'=>$tag2['view_num']
            );
            }, $projectphotos);
            } else{
            $projectphotos=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'userdetails'=>$userdetails,'portfolio'=>$projectphotos));

        }

            /* Api For Social Login */
        function sociallogin_post()
        {
             switch ($this->post('auth_provider')) {

                  case 'Fb':
                  $auth_token=$this->post('auth_token');
                  $fbdata=fb_api('me', $auth_token);
                  if(isset($fbdata['error'])){
                   $this->response(array('responsecode'=>202,'responsedetails'=>'Invalid ascess token'));
                  }
                  $auth_cond=array('auth_provider'=>'Fb','auth_id'=>$fbdata['id']);
                  $check_authorization=$this->api_model->check_user($auth_cond);
                  $lang_id=$this->get_language_id();
                  if($check_authorization=='TRUE'){
                     $user_data = array('user_type_id' => $this->post('usertype'),
                                'firstname' => $fbdata['first_name'],
                                'lastname' => $fbdata['last_name'],
                                'email' => (isset($fbdata['email'])?$fbdata['email']:''),
                                'username' => $fbdata['name'],
                                'phone' => (isset($fbdata['mobile'])?$fbdata['mobile']:''),
                                'area' => (isset($fbdata['area'])?$fbdata['area']:''),
                                'address' => (isset($fbdata['address'])?$fbdata['address']:''),
                                'postcode' => (isset($fbdata['postcode'])?$fbdata['postcode']:''),
                                'modified'=>date("Y-m-d H:i:s"),
                                'last_logged_in'=>date("Y-m-d H:i:s"),
                                'language_id' =>$lang_id
                                );
                     $user_id= $this->api_model->update_user($user_data,$auth_cond);
                  }else{
                     $email_cond=array('email'=>$fbdata['email']);
                     $check_email=$this->api_model->check_user($email_cond);
                        if($check_email=='TRUE'){
                           $this->response(array("responsecode"=> 201,"responsedetails"=>"This Email address already exists. Please use a different email or login through the registered email)"));
                        }
                     if(isset($fbdata['mobile'])){
                     $mobile_cond=array('phone'=>$fbdata['mobile']);
                     $check_mobile=$this->api_model->check_user($mobile_cond);
                        if($check_mobile=='TRUE'){
                          $this->response(array("responsecode"=> 202,"responsedetails"=>"This Mobile number already exists. Please use a different number or login through the registered mobile number"));
                       }
                    }
                    $user_data = array('user_type_id' => $this->post('usertype'),
                                'firstname' => $fbdata['first_name'],
                                'lastname' => $fbdata['last_name'],
                                'email' => (isset($fbdata['email'])?$fbdata['email']:''),
                                'username' => $fbdata['name'],
                                'phone' => (isset($fbdata['mobile'])?$fbdata['mobile']:''),
                                'area' => (isset($fbdata['area'])?$fbdata['area']:''),
                                'address' => (isset($fbdata['address'])?$fbdata['address']:''),
                                'postcode' => (isset($fbdata['postcode'])?$fbdata['postcode']:''),
                                'auth_id'=>$fbdata['id'],
                                'auth_provider'=>'Fb',
                                'created'=>date("Y-m-d H:i:s"),
                                'modified'=>date("Y-m-d H:i:s"),
                                'last_logged_in'=>date("Y-m-d H:i:s"),
                                'language_id' =>$lang_id
                                );
                    $user_id = $this->api_model->post_data($user_data,'users');
                   }
                    $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$user_id;
                    $token= generateToken($token);
                    $final_result=array();
                    $session_token=array('responsecode'=>200,'responsedetails'=>'success','sessiontoken'=>$token);
                    $cond=array('user_id'=>$user_id);
                    $user_details=$this->api_model->get_user_details_by_param($cond);
                    if($user_details !='NULL'){
                    $user_details = array_map(function($tag) {
                       return array(
                       'userid' => $tag['userid'],
                       'remainsearch'=>remainContractorSearch($tag['userid']),
                       'name' => $tag['name'],
                       'email'=>$tag['email'],
                       'mobile'=>$tag['mobile'],
                       'usertype'=>$tag['usertype'],
                       'profileimage'=>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                       'location'=>$tag['location']
                       );
                    }, $user_details);
                    }else{
                       $this->response(array("responsecode"=> 202,"error"=>"Invalid login details"));
                    }
                    if($user_details){
                       $device_details=array('user_id' =>$user_id,
                                              'device_token' =>$this->post('device_token'),
                                              'device_type' =>$this->post('device_type'),
                                              'device_id' =>$this->post('device_id'),
                                              'token' =>$token);
                       $this->api_model->add_delete_token($user_id,$device_details);
                    }
                    $final_result=($session_token + $user_details[0] + array('logintype'=>'Fb'));
                    $this->response($final_result,200);
                  break;

                  case 'Google':
                    $auth_token=$this->post('auth_token');
                    $gdata=google_api('userinfo', $auth_token);
                    if(isset($gdata['error'])){
                     $this->response(array('responsecode'=>202,'responsedetails'=>'Invalid ascess token'));
                        }
                    $auth_cond=array('auth_provider'=>'Google','auth_id'=>$gdata['id']);
                    $check_authorization=$this->api_model->check_user($auth_cond);
                    $lang_id=$this->get_language_id();
                    if($check_authorization=='TRUE'){
                    $user_data = array('user_type_id' => $this->post('usertype'),
                                'firstname' => $gdata['given_name'],
                                'lastname' => $gdata['family_name'],
                                'email' => (isset($gdata['email'])?$gdata['email']:''),
                                'username' => $gdata['name'],
                                'modified'=>date("Y-m-d H:i:s"),
                                'last_logged_in'=>date("Y-m-d H:i:s"),
                                'language_id' =>$lang_id
                                );
                     $user_id= $this->api_model->update_user($user_data,$auth_cond);
                    }else{
                     $user_data = array('user_type_id' => $this->post('usertype'),
                                'firstname' => $gdata['given_name'],
                                'lastname' => $gdata['family_name'],
                                'email' => (isset($gdata['email'])?$gdata['email']:''),
                                'username' => $gdata['name'],
                                'auth_id'=>$gdata['id'],
                                'auth_provider'=>'Google',
                                'created'=>date("Y-m-d H:i:s"),
                                'modified'=>date("Y-m-d H:i:s"),
                                'last_logged_in'=>date("Y-m-d H:i:s"),
                                'language_id' =>$lang_id
                                );
                     $user_id = $this->api_model->post_data($user_data,'users');
                   }
                    $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$user_id;
                    $token= generateToken($token);
                    $final_result=array();
                    $session_token=array('responsecode'=>200,'responsedetails'=>'success','sessiontoken'=>$token);
                    $cond=array('user_id'=>$user_id);
                    $user_details=$this->api_model->get_user_details_by_param($cond);
                    if($user_details !='NULL'){
                    $user_details = array_map(function($tag) {
                       return array(
                       'userid' => $tag['userid'],
                       'remainsearch'=>remainContractorSearch($tag['userid']),
                       'name' => $tag['name'],
                       'email'=>$tag['email'],
                       'mobile'=>$tag['mobile'],
                       'usertype'=>$tag['usertype'],
                       'profileimage'=>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                       'location'=>$tag['location']
                       );
                    }, $user_details);
                    }else{
                       $this->response(array("responsecode"=> 202,"error"=>"Invalid login details"));
                     }
                    if($user_details){
                       $device_details=array('user_id' =>$user_id,
                                              'device_token' =>$this->post('device_token'),
                                              'device_type' =>$this->post('device_type'),
                                              'device_id' =>$this->post('device_id'),
                                              'token' =>$token);
                       $this->api_model->add_delete_token($user_id,$device_details);
                    }
                    $final_result=($session_token + $user_details[0] + array('logintype'=>'Google'));
                    $this->response($final_result,200);
                  break;
                  case 'Twitter':
                   $this->load->library('twitteroauth');
                   $this->config->load('twitter');
                   $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'),$this->post('auth_token'),$this->post('token_secret'));
                   $access_token = array('oauth_token'=>$this->post('auth_token'),'oauth_token_secret'=>$this->post('token_secret'));
                   $twitteroauth = new TwitterOAuth($this->config->item('twitter_consumer_token'),$this->config->item('twitter_consumer_secret'), $access_token['oauth_token'], $access_token['oauth_token_secret']);
                   $tdata = $twitteroauth->get('account/verify_credentials',array(),$this->connection);

                   if (isset($tdata->errors)) {
                    $this->response(array('responsecode'=>500,'responsedetails'=>'Something went wrong. Please try again later.'));
                    }
                   else {
                        $auth_cond=array('auth_provider'=>'Twitter','auth_id'=>$tdata->id);
                        $check_authorization=$this->api_model->check_user($auth_cond);
                        $lang_id=$this->get_language_id();
                        $name=  explode(' ',$tdata->name);
                        $first_name=(isset($name[0])?$name[0]:'');
                        $last_name=(isset($name[1])?$name[1]:'');
                        if($check_authorization=='TRUE'){
                        $user_data = array('user_type_id' => $this->post('usertype'),
                                    'firstname' => $first_name,
                                    'lastname' => $last_name,
                                    'username' => $tdata->screen_name,
                                    'modified'=>date("Y-m-d H:i:s"),
                                    'last_logged_in'=>date("Y-m-d H:i:s"),
                                    'language_id' =>$lang_id
                                    );
                         $user_id= $this->api_model->update_user($user_data,$auth_cond);
                        }else{
                         $user_data = array('user_type_id' => $this->post('usertype'),
                                    'firstname' => $first_name,
                                    'lastname' => $last_name,
                                    'username' => $tdata->screen_name,
                                    'auth_id'=>$tdata->id,
                                    'auth_provider'=>'Twitter',
                                    'created'=>date("Y-m-d H:i:s"),
                                    'modified'=>date("Y-m-d H:i:s"),
                                    'last_logged_in'=>date("Y-m-d H:i:s"),
                                    'language_id' =>$lang_id
                                    );
                         $user_id = $this->api_model->post_data($user_data,'users');
                       }
                        $token=$this->post('device_token').$this->post('device_type').$this->post('device_id').$user_id;
                        $token= generateToken($token);
                        $final_result=array();
                        $session_token=array('responsecode'=>200,'responsedetails'=>'success','sessiontoken'=>$token);
                        $cond=array('user_id'=>$user_id);
                        $user_details=$this->api_model->get_user_details_by_param($cond);
                        if($user_details !='NULL'){
                        $user_details = array_map(function($tag) {
                           return array(
                           'userid' => $tag['userid'],
                           'remainsearch'=>remainContractorSearch($tag['userid']),
                           'name' => $tag['name'],
                           'email'=>$tag['email'],
                           'mobile'=>$tag['mobile'],
                           'usertype'=>$tag['usertype'],
                           'profileimage'=>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                           'location'=>$tag['location']
                           );
                       }, $user_details);
                        }else{
                           $this->response(array("responsecode"=> 202,"error"=>"Invalid login details"));
                        }
                        if($user_details){
                           $device_details=array('user_id' =>$user_id,
                                                  'device_token' =>$this->post('device_token'),
                                                  'device_type' =>$this->post('device_type'),
                                                  'device_id' =>$this->post('device_id'),
                                                  'token' =>$token);
                           $this->api_model->add_delete_token($user_id,$device_details);
                        }
                        $final_result=($session_token + $user_details[0] + array('logintype'=>'Twitter'));
                        $this->response($final_result,200);
                    }
                  break;


             }
        }

        /* Api to get interior designer  list */
        function interiordesignerdashboarddetails_post()
        {
            $this->validation("sessiontoken");
            $lang_id=$this->get_language_id();
            $cond=array('u.user_id'=>$this->post('user_id'));
            $user_details=$this->user->get_user_details($cond);
            $designername=$user_details['firstname'].' '.$user_details['lastname'];
            $designerimage=base_url().'uploads/users/images/'.(($user_details['user_image']=='')?'dummy.jpg':$user_details['user_image']);
            $designation=$user_details['designation'];
            $location=(($user_details['area']==0)?'':$user_details['area']).''.$user_details['city_name'];
            $phonenumber=$user_details['phone'];
            $tag['marketplace_count']=$this->api_model->count_all('project_posts',array('user_id'=>$this->post('user_id')));
            $tag['leads_count']=$this->api_model->count_all('project_posts_verification',array('designer_id'=>$this->post('user_id'),'lead_acceptence'=>'0'));
            $tag['mentorship_count']=$this->api_model->count_all('mentorship_posts',array('user_id'=>$this->post('user_id')));

            $this->response(array('responsecode'=>200,'responsedetails'=>'success','designer_name'=>$designername,'designer_profile_image'=>$designerimage,'designation'=>$designation,'location'=>$location,'phone_number'=>$phonenumber,'itemcount'=>$tag));
        }

          /* User Section Apis End */

        /* Api for Follow designers */
        function followdesigner_post()
        {
            $this->validation("sessiontoken");
            $designerid=$this->post('designer_id');
            $user_id=$this->post('user_id');
            $isfollow=$this->post('isfollow');
            if($isfollow =='1' || $isfollow =='0' ){
            $condiction=array('user_id'=>$designerid,'follower_id'=>$user_id);
            $data=array('user_id'=>$designerid,'follower_id'=>$user_id,'created'=> date("Y-m-d H:i:s"),'modified'=> date("Y-m-d H:i:s"));
            $follow_unfollow=$this->api_model->do_undo($condiction,'user_followers',$data);
            $total_follow=$this->api_model->count_all('user_followers',array('user_id'=>$designerid));
            if($follow_unfollow==1){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','nooffollowers'=>number_format($total_follow),'followstatus'=>1));
            }else{
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','nooffollowers'=>number_format($total_follow),'followstatus'=>0));
            }
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api to get all state list */
        function state_post()
        {
            $states = $this->state->get_rows(array('conditions'=>array('country_id'=>99,'status'=>1)));
            if(count($states)>1){
            $states = array_map(function($tag) {
                return array(
                'stateid' => $tag['state_id'],
                'state' => $tag['state_name']
                );
            }, $states);
            }else{
               $states=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>'en','states'=>$states));

        }

        /* Api to get city list */
        function city_post()
        {
            $cities = $this->city->get_rows(array('conditions'=>array('state_id'=>$this->post('stateid'),'status'=>1)));
            if(count($cities)>1){
            $cities = array_map(function($tag) {
                return array(
                'cityid' => $tag['city_id'],
                'city' => $tag['city_name']
                );
            }, $cities);
            }else{
                $cities=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','cities'=>$cities));
        }

        /* Api to get all image list from projects */
        function getallimage_post()
        {
            $pagination=$this->get_offset_size();
            $all_image=$this->api_model->get_project_details('',$pagination['offset'],$pagination['limit'],'');
            $count_image=$this->api_model->get_project_details('','','','','num_rows');
            $all_image = array_map(function($tag) {
                return array(
                'imageid' => $tag['project_image_id'],
                'projectid' => $tag['project_id'],
                'imageurl' => file_exists('uploads/portfolio/images/'.$tag['image'])? base_url().'uploads/portfolio/images/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                );
            }, $all_image);
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>  number_format($count_image),'images'=>$all_image));
        }

        /* Api to get all ideabook list */
        function getallideabook_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            $condiction=array('ideabook.user_id'=>$this->post('user_id'));
            }else{
            $condiction='';
            }
            $all_ideabook= $this->api_model->get_ideabook_rows($condiction);
            if(!empty($all_ideabook)){
            $all_ideabook = array_map(function($tag) {
                return array(
                'ideabook_id' => $tag['ideabook_id'],
                'ideabook_name' => $tag['ideabook_name'],
                'ideabook_coverimage' => base_url().'uploads/portfolio/images/'.(($tag['image']=='')?'dummy.jpg':$tag['image'])
                );
             }, $all_ideabook);
            }else{
             $all_ideabook=array();
            }
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','ideabook'=>$all_ideabook));
        }

        /* Api to create ideabook */
        function createIdeabook_post()
        {
            $this->validation("sessiontoken");
            $count_ideabook=$this->api_model->count_all('ideabook',array('ideabook_name'=>$this->post('ideabook_name'),'user_id'=>$this->post('user_id')));
            if($count_ideabook>0){
            $this->response(array('error'=>'Ideabook name already exist.Try diffrent name'));
            }else{
            $data=array('user_id'=>$this->post('user_id'),'ideabook_name'=>$this->post('ideabook_name'),'created'=>date("Y-m-d H:i:s"));
            $book_id=$this->api_model->post_data($data,'ideabook');
            $condiction=array('ideabook.ideabook_id'=>$book_id);
            $all_ideabook= $this->api_model->get_ideabook_rows($condiction);
            $all_ideabook = array_map(function($tag) {
                return array(
                'ideabook_id' => $tag['ideabook_id'],
                'ideabook_name' => $tag['ideabook_name'],
                'ideabook_coverimage'=>base_url().'uploads/portfolio/images/'.(($tag['image']=='')?'dummy.jpg':$tag['image'])
                );
             }, $all_ideabook);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','ideabook'=>$all_ideabook));
        }
        }

        /* Api to add image to ideabook */
        function addimagetoideabook_post()
        {
            $this->validation("sessiontoken");
            $data=array('ideabook_id'=>$this->post('ideabook_id'),'user_id'=>$this->post('user_id'),'image_id'=>$this->post('image_id'),'created'=>date("Y-m-d H:i:s"));
            $count_image=$this->api_model->count_all('ideabook_images',array('ideabook_id'=>$this->post('ideabook_id'),'image_id'=>$this->post('image_id')));
            if($count_image>0){
            $this->response(array('error'=>'Image already added to this Ideabook.Try with diffrent image'));
            }else{
            $insert_image=$this->api_model->post_data($data,'ideabook_images');
            if($insert_image){
            $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
            $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
            }
        }

        /* Api to get all ideabook detail list */
        function getideabookdetails_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $ideabook_details= $this->api_model->get_ideabook_images($this->post('ideabook_id'));
            if(count($ideabook_details)>0){
            $ideabook_details = array_map(function($tag) {
                return array(
                'imageid' => $tag['image_id'],
                'ideabookimageurl' => base_url().'uploads/portfolio/images/'.$tag['image']
                );
             }, $ideabook_details);
            }else{
            $ideabook_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','ideabookimages'=> $ideabook_details));
        }

        /* Api to get design idea deatils list */
        function designideadetails_post()
        {
           if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
           $lang_id=$this->get_language_id();
           $project_id=$this->api_model->get_project_id($this->post('image_id'));
           if($project_id=='null'){
            $this->response(array('error'=>'Something went wrong. Please try again later'));
           }else{
           $count_project=$this->api_model->count_all('projects',array('project_id'=>$project_id));
           if($count_project<1){
           $this->response(array('error'=>'Something went wrong. Please try again later'));
           }
           $condiction=array('lang_id'=>$lang_id,'project_id'=>$project_id);
           $details=$this->project->get_rows($condiction);
           $designer=array();
           $ProjectDetails=array();
           $image=array();
           $photoview=$this->project->get_related_view_projects($project_id);
           $otherprojects=array();
           $params = array('project_id'=>$project_id);
           $params['conditions'] = array('i.isDisplay'=>1,'i.wrong_tag'=>0,'i.isDelete'=>0,'i.status'=>1);
           $data['project_images'] = $this->project->get_image_rows($params);
           $product_image_path=base_url().'uploads/product/images/thumb/';
            if(!empty($data['project_images'][0]['product_id']))
            {
                $productId = explode(',', $data['project_images'][0]['product_id']);
                $recommendedproduct = $this->product->get_product_images($productId);
                if(isset($recommendedproduct[0])){
                $recommendedproduct = array_map(function($tag1) {
                return array(
                'productid' => $tag1['product_id'],
                'productimage'=>file_exists('uploads/product/images/thumb/'.$tag1['product_image'])? $product_image_path.$tag1['product_image']:$product_image_path.'dummy.jpg'
                );
             },  $recommendedproduct);
           }else{
               $recommendedproduct=array();
           }

            }else{
                $recommendedproduct=array();
            }
           $user_image_path=base_url().'uploads/users/images/';
           $project_image_path=base_url().'uploads/portfolio/images/';

           foreach($details as $key=>$value){
           if($key=='project'){
            $designer['id']= $value['user_id'];
            $designer['name']= $value['firstname'].' '.$value['lastname'];
            $designer['designation']= $value['designation'];
            $designer['company']= $value['company'];
            $designer['profileimageurl']=$user_image_path.((!empty($value['user_image']))?$value['user_image']:'dummy.jpg');
            $designer['followers']= $this->api_model->count_all('user_followers',array('user_id'=>$value['user_id']));
            $designer['likecount']= $this->api_model->count_all('project_image_likes',array('image_id'=>$this->post('image_id')));
            $designer['likestatus']=$this->status_check($this->post('image_id'),'project_image_likes','image_id');
            $ProjectDetails['id']=$value['project_id'];
            $ProjectDetails['name']=$value['title'];
            $ProjectDetails['details']=$value['description'];
            $image['location']=$value['location'];
            $image['interiortheme']=$value['interior_theme_style'];
           }
           if($key=='project_images'){
            foreach($value as $image_key=>$image_value){
            if($image_value['project_image_id']==$this->post('image_id')){
                $image['id']='';
                $image['roomareatype']=$image_value['room_area_type'];
                $tags =  $this->common_lib->getImageTags($this->post('image_id'));
                $image_tag = trim(array2string($tags),',');
                $image['tags']=strip_tags($image_tag);
                $ProjectDetails['imagename']=$image_value['image_title'];
                $nodesignideaparams = array('project_id'=>$project_id,'image_id'=>$this->post('image_id'));
                $totaldesignideas= $this->project->get_image_rows($nodesignideaparams);
                $image['noofdesignidea']=number_format(count($totaldesignideas));
                }
            else{
                $otherprojectphoto['imageid']=$image_value['project_image_id'];
                $otherprojectphoto['projectid']=$image_value['project_id'];
                $otherprojectphoto['imageurl']=file_exists('uploads/portfolio/images/'.$image_value['image'])? $project_image_path.$image_value['image']:$project_image_path.'dummy.jpg';
                $otherprojects[]=$otherprojectphoto;
           }
           }
           }

           }
           if(isset($photoview[0])){
           $photoview = array_map(function($tag) {
                return array(
                'imageid' => $tag['project_image_id'],
                'imageurl'=>file_exists('uploads/portfolio/images/'.$tag['image'])? base_url().'uploads/portfolio/images/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                'projectid' => $tag['project_id']
                );
             }, $photoview);
           }else{
               $photoview=array();
           }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','designer'=> $designer,'projectdetails'=>$ProjectDetails,'image'=>$image,'otherprojectphotos'=>$otherprojects,'photoviewedbyotheruser'=>$photoview,'recomendedproducts'=>$recommendedproduct));

           }
        }

        /* Api to get design idea deatils list using filter */
        function designideafiltercategory_post()
        {
           $lang_id=$this->get_language_id();
           $user_type=$this->post('tag');
           $param = array(
            'lang_id'=>$lang_id,
            'return_type'=>'count',
            'conditions'=>  array(
                'p.status'=>1
            )
            );
           $param['orderby_conditions'] = 'p.created DESC';
           if($user_type==1){
               $param['in_condition']['p.type_id']=array(93);
           }elseif($user_type==2){
               $param['in_condition']['p.type_id']=array(94,95,96,97,99,118);
           }
           if($user_type==5){
             $param['return_type'] = 'taggedFilters';
             $taggedFiletrs = $this->project->find_projects($param);

            if($taggedFiletrs['type_id']){
            $project_type_param = array('lang_id'=>1, 'conditions'=>array('status'=>1));
            $project_type_param['in_condition']['type_id'] = explode(',', $taggedFiletrs['type_id']);
            $data['project_type'] = $this->project_type->get_rows($project_type_param);
            }else{
            $data['project_type']='';
            }
            if($taggedFiletrs['interior_theme_style_id']){
            $interior_theme_styles_param = array('lang_id'=>1, 'conditions'=>array('t.status'=>1));
            $interior_theme_styles_param['in_condition']['t.interior_theme_style_id'] = explode(',', $taggedFiletrs['interior_theme_style_id']);
            $data['interior_theme_styles'] = $this->interior_theme_style->get_interior_theme_style_list($interior_theme_styles_param);
            }else{
            $data['interior_theme_styles']='';
            }
            if($taggedFiletrs['exterior_theme_style_id']){
            $exterior_theme_styles_param = array('lang_id'=>1, 'conditions'=>array('t.status'=>1));
            $exterior_theme_styles_param['in_condition']['t.exterior_theme_style_id'] = explode(',', $taggedFiletrs['exterior_theme_style_id']);
            $data['exterior_theme_styles'] = $this->exterior_theme_style->get_exterior_theme_styles_list($exterior_theme_styles_param);
           }else{
            $data['exterior_theme_styles']='';
           }
           if($taggedFiletrs['furniture_theme_style_id']){
            $fts_param = array('lang_id'=>1, 'conditions'=>array('f.status'=>1));
            $fts_param['in_condition']['f.furniture_theme_style_id'] = explode(',', $taggedFiletrs['furniture_theme_style_id']);
            $data['furniture_theme_styles']= $this->furniture_theme_style->get_furniture_theme_style_list($fts_param);
           }else{
            $data['furniture_theme_styles']='';
           }
           }else{
           $totalImg = $this->project->find_project_images($param);

           if($totalImg['room_area_type_id']){
            $room_area_type_param = array('lang_id'=>1, 'conditions'=>array('rt.status'=>1));
            $room_area_type_param['in_condition']['rt.room_area_type_id'] = explode(',', $totalImg['room_area_type_id']);
            $data['room_area_type'] = $this->room_area_type->get_unique_rat($room_area_type_param);
           }else{
            $data['room_area_type']='';
           }
           if($totalImg['interior_theme_style_id']){
            $interior_theme_styles_param = array('lang_id'=>1, 'conditions'=>array('t.status'=>1));
            $interior_theme_styles_param['in_condition']['t.interior_theme_style_id'] = explode(',', $totalImg['interior_theme_style_id']);
            $data['interior_theme_styles'] = $this->interior_theme_style->get_interior_theme_style_list($interior_theme_styles_param);
           }else{
            $data['interior_theme_styles']='';
           }
           if($totalImg['furniture_type_id']){
            $furniture_type_param = array('lang_id'=>1, 'conditions'=>array('f.status'=>1));
            $furniture_type_param['in_condition']['f.furniture_type_id'] = array_unique(explode(',', $totalImg['furniture_type_id']));
            $data['furniture_type'] = $this->furniture_type->get_unique_furniture($furniture_type_param);
            }else{
            $data['furniture_type']='';
            }
           if($totalImg['budget_id']){
            $budget_param = array('lang_id'=>1, 'conditions'=>array('b.status'=>1));
            $budget_param['in_condition']['b.budget_id'] = explode(',', $totalImg['budget_id']);
            $data['budgets'] = $this->project_budget->get_budget_list($budget_param);
            }else{
            $data['budgets']='';
            }
           if($totalImg['colour_id']){
            $colour_param = array('lang_id'=>1, 'conditions'=>array('c.status'=>1));
            $colour_param['in_condition']['c.colour_id'] = array_unique(explode(',', $totalImg['colour_id']));
            $data['colours'] = $this->colour->get_colour_list($colour_param);
            }else{
            $data['colours']='';
            }
           if($totalImg['material_id']){
            $material_param = array('lang_id'=>1, 'conditions'=>array('m.status'=>1));
            $material_param['in_condition']['m.material_id'] = array_unique(explode(',', $totalImg['material_id']));
            $data['materials']= $this->material->get_material_list($material_param);
           }else{
            $data['materials']='';
           }
           if($totalImg['exterior_theme_style_id']){
            $exterior_theme_styles_param = array('lang_id'=>1, 'conditions'=>array('t.status'=>1));
            $exterior_theme_styles_param['in_condition']['t.exterior_theme_style_id'] = explode(',', $totalImg['exterior_theme_style_id']);
            $data['exterior_theme_styles'] = $this->exterior_theme_style->get_exterior_theme_styles_list($exterior_theme_styles_param);
           }else{
            $data['exterior_theme_styles']='';
           }
           if($totalImg['furniture_theme_style_id']){
            $fts_param = array('lang_id'=>1, 'conditions'=>array('f.status'=>1));
            $fts_param['in_condition']['f.furniture_theme_style_id'] = explode(',', $totalImg['furniture_theme_style_id']);
            $data['furniture_theme_styles'] = $this->furniture_theme_style->get_furniture_theme_style_list($fts_param);
            }else{
            $data['furniture_theme_styles']='';
            }
           if($totalImg['furniture_theme_style_id']){
            $colour_theme_param = array('lang_id'=>1, 'conditions'=>array('t.status'=>1));
            $colour_theme_param['in_condition']['t.colour_theme_id'] = explode(',', $totalImg['colour_theme_id']);
            $data['colour_themes'] = $this->colour_theme->get_colour_theme_list($colour_theme_param);
            }else{
            $data['colour_themes']='';
            }
           if($totalImg['lighting_concept_id']){
            $lighting_concept_param = array('lang_id'=>1, 'conditions'=>array('l.status'=>1));
            $lighting_concept_param['in_condition']['l.lighting_concept_id'] = explode(',', $totalImg['lighting_concept_id']);
            $data['lighting_concepts'] = $this->lighting_concept->get_lighting_concept_list($lighting_concept_param);
           }else{
            $data['lighting_concepts']='';
           }
           if($totalImg['painting_id']){
            $painting_param = array('lang_id'=>1, 'conditions'=>array('p.status'=>1));
            $painting_param['in_condition']['p.painting_id'] = explode(',', $totalImg['painting_id']);
            $data['paintings']= $this->painting->get_painting_list($painting_param);
           }else{
            $data['paintings']='';
           }
           if($totalImg['unit_type_id']){
            $unit_type_param = array('lang_id'=>1, 'conditions'=>array('u.status'=>1));
            $unit_type_param['in_condition']['u.unit_type_id'] = explode(',', $totalImg['unit_type_id']);
            $data['unit_type'] = $this->unit_type->get_unit_type_list($unit_type_param);
            }else{
            $data['unit_type']='';
            }
           if($totalImg['room_size_id']){
            $room_size_param = array('lang_id'=>1, 'conditions'=>array('r.status'=>1));
            $room_size_param['in_condition']['r.room_size_id'] = explode(',', $totalImg['room_size_id']);
            $data['room_size'] = $this->room_size->get_room_size_list($room_size_param);
           }else{
            $data['room_size']='';
           }
           if($totalImg['ceiling_id']){
            $ceiling_param = array('lang_id'=>1, 'conditions'=>array('c.status'=>1));
            $ceiling_param['in_condition']['c.ceiling_id'] = explode(',', $totalImg['ceiling_id']);
            $data['ceiling'] = $this->ceiling->get_ceiling_list($ceiling_param);
            }else{
            $data['ceiling']='';
            }
           if($totalImg['floor_id']){
            $flooring_param = array('lang_id'=>1, 'conditions'=>array('f.status'=>1));
            $flooring_param['in_condition']['f.floor_id'] = explode(',', $totalImg['floor_id']);
            $data['flooring']= $this->floor->get_floor_list($flooring_param);
           }else{
            $data['flooring']='';
           }
           if($totalImg['paneling_partition_id']){
            $paneling_partition_param = array('lang_id'=>1, 'conditions'=>array('p.status'=>1));
            $paneling_partition_param['in_condition']['p.paneling_partition_id'] = explode(',', $totalImg['paneling_partition_id']);
            $data['paneling_partition'] = $this->paneling_partition->get_paneling_partition_list($paneling_partition_param);
           }else{
            $data['paneling_partition']='';
           }
           if($totalImg['door_window_id']){
            $door_window_param = array('lang_id'=>1, 'conditions'=>array('d.status'=>1));
            $door_window_param['in_condition']['d.door_window_id'] = explode(',', $totalImg['door_window_id']);
            $data['door_window'] = $this->door_window->get_door_window_list($door_window_param);
            }else{
            $data['door_window']='';
            }
           }
           $i=0;
           foreach($data as $key=>$category){
               $catname['category'][$i]['category_id']=$i;
               $catname['category'][$i]['category']=$key;
               if(is_array($category)){
               foreach($category as $keys=>$subcategory){
                 switch ($key) {
                    case 'project_type':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['type_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['type_name'];
                        break;
                    case 'interior_theme_styles':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['interior_theme_style_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['interior_theme_style'];
                        break;
                    case 'exterior_theme_styles':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['exterior_theme_style_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['exterior_theme_style'];
                        break;
                    case 'furniture_theme_styles':
                       $catname['category'][$i][$key][$keys]['id']=$subcategory['furniture_theme_style_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['furniture_theme_style'];
                        break;
                    case 'room_area_type':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['room_area_type_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['room_area_type'];
                        break;

                    case 'furniture_type':
                       $catname['category'][$i][$key][$keys]['id']=$subcategory['furniture_type_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['furniture_type'];
                        break;
                    case 'budgets':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['budget_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['budget'];
                        break;
                    case 'colours':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['colour_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['colour'];
                        break;
                    case 'materials':
                      $catname['category'][$i][$key][$keys]['id']=$subcategory['material_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['material'];
                        break;
                    case 'colour_themes':
                       $catname['category'][$i][$key][$keys]['id']=$subcategory['colour_theme_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['colour_theme'];
                        break;
                    case 'lighting_concepts':
                     $catname['category'][$i][$key][$keys]['id']=$subcategory['lighting_concept_id'];
                       $catname['category'][$i][$key][$keys]['name']=$subcategory['lighting_concept'];
                        break;
                    case 'paintings':
                       $catname['category'][$i][$key][$keys]['id']=$subcategory['painting_id'];
                       $catname['category'][$i][$key][$keys]['name']=$subcategory['painting'];
                        break;
                    case 'unit_type':
                      $catname['category'][$key][$keys]=$subcategory['furniture_theme_style_id'];
                       $catname['category'][$key][$keys]=$subcategory['furniture_theme_style'];
                        break;
                    case 'room_size':
                       $catname['category'][$i][$key][$keys]['id']=$subcategory['room_size_id'];
                       $catname['category'][$i][$key][$keys]['name']=$subcategory['room_size'];
                        break;
                    case 'ceiling':
                      $catname['category'][$i][$key][$keys]['id']=$subcategory['ceiling_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['ceiling'];
                        break;
                    case 'flooring':
                      $catname['category'][$i][$key][$keys]['id']=$subcategory['floor_id'];
                       $catname['category'][$i][$key][$keys]['name']=$subcategory['floor'];
                        break;
                    case 'paneling_partition':
                      $catname['category'][$i][$key][$keys]['id']=$subcategory['paneling_partition_id'];
                      $catname['category'][$i][$key][$keys]['name']=$subcategory['paneling_partition'];
                        break;
                    case 'door_window':
                        $catname['category'][$i][$key][$keys]['id']=$subcategory['door_window_id'];
                        $catname['category'][$i][$key][$keys]['name']=$subcategory['door_window'];
                        break;
                }

               }
           } $i++;
           }
           $this->response(array('responsecode'=>200,'responsedetails'=>'success','categories'=>$catname));


        }

        /* Api to get designer deatils list */
        function getalldesigner_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $pagination=$this->get_offset_size();
            if($this->post('search_text')!=''){
             $cond1=array("CONCAT(u.firstname,' ',u.lastname)" => $this->post('search_text'));
            }else{
             $cond1='null';
            }
            if($this->post('location')!=''){
             $location= explode(',', $this->post('location'));
             $cond2=array('area' => $location);
            }else{
             $cond2='null';
            }
           if($this->post('designer_type')!=''){
             $designer_type= explode(',', $this->post('designer_type'));
             $cond3=array('designation' => $designer_type);
            }else{
             $cond3='null';
            }
            $cond=array('search_text'=>$cond1,'location'=> $cond2,'designer_type'=>$cond3);
            $count_all_designers=$this->api_model->get_all_designers('','',$cond);
            $user_details=$this->api_model->get_all_designers($pagination['offset'],$pagination['limit'],$cond);
            if(isset($user_details[0])){
            $user_details= array_map(function($tag) {
                return array(
                'id' => $tag['user_id'],
                'name' => $tag['name'],
                'designation' => $tag['designation'],
                'company' => $tag['company'],
                'profilepic' =>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                'nooffollowers' =>$this->api_model->count_all('user_followers',array('user_id='=>$tag['user_id'])),
                'location' => $tag['city_name'],
                'avgrate' => ($tag['user_rating']==null)?'0.00':$tag['user_rating'],
                'followstatus' => $this->status_check($tag['user_id'],'user_followers','user_id','follower_id')
                );
             }, $user_details);
            }else{
            $user_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format(count($count_all_designers)),'designers'=>$user_details));
        }

        /* Api to get interior designer  list */
        function interiordesignerlist_post()
        {
            $lang_id=$this->get_language_id();
            $cond=array('user_type_id'=>3,'language_id'=>$lang_id);
            $data=array('firstname','lastname','user_id');
            $user_details=$this->api_model->get_data('users',$cond,$data);
            if(isset($user_details[0])){
            $user_details= array_map(function($tag) {
                return array(
                'designer_id' => $tag['user_id'],
                'designer_name' => $tag['firstname'].' '.$tag['lastname']
                );
             }, $user_details);
            // $user_details['all']='all';
            }else{
            $user_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','interiordesignerlist'=>$user_details));
        }

        /* Api to get design idea list */
        function designIdealist_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            $param = array(
            'lang_id'=>1,
            'return_type'=>'count',
            'conditions'=>  array(
                'p.status'=>1
            )
            );
            $user_type=$this->post('tag');
            if($user_type==1){
               $param['in_condition']['p.type_id']=array(93);
           }elseif($user_type==2){
               $param['in_condition']['p.type_id']=array(94,95,96,97,99,118);
           }
            $tag=$this->post('search_type_tag');
             switch ($tag) {
                    case 'project_type':
                        $param['conditions']['p.type_id']=$this->post('filterbysubcategoryid');
                        break;
                    case 'interior_theme_styles':
                        $param['in_condition']['p.interior_theme_style_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'exterior_theme_styles':
                        $param['in_condition']['p.exterior_theme_style_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'furniture_theme_styles':
                        $param['in_condition']['pi.furniture_theme_style_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'room_area_type':
                        $param['in_condition']['pi.room_area_type_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'furniture_type':
                        $param['multi_find_in_set']['pi.furniture_type_id'] = str_replace(',', '|', $this->post('filterbysubcategoryid'));
                        break;
                    case 'budgets':
                        $param['conditions']['p.budget_id'] = $this->post('filterbysubcategoryid');
                        break;
                    case 'colours':
                        $param['multi_find_in_set']['pi.colour_id'] = str_replace(',', '|', $this->post('filterbysubcategoryid'));
                        break;
                    case 'materials':
                        $param['multi_find_in_set']['pi.material_id'] = str_replace(',', '|', $this->post('filterbysubcategoryid'));
                        break;
                    case 'colour_themes':
                        $param['in_condition']['pi.colour_theme_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'lighting_concepts':
                        $param['in_condition']['pi.lighting_concept_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'paintings':
                        $param['in_condition']['pi.painting_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'unit_type':
                        $param['in_condition']['pi.unit_type_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'room_size':
                       $param['in_condition']['pi.room_size_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'ceiling':
                       $param['in_condition']['pi.ceiling_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'flooring':
                       $param['in_condition']['pi.floor_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'paneling_partition':
                       $param['in_condition']['pi.paneling_partition_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                    case 'door_window':
                       $param['in_condition']['pi.door_window_id'] = explode(',', $this->post('filterbysubcategoryid'));
                        break;
                }
               if($user_type==5){
                 $totalRec = $this->project->find_projects($param);
                 $param['offset'] = $pagination['offset'];
                 $param['limit'] = $pagination['limit'];
                 $param['return_type'] = 'array';
                 $project_details=$this->project->find_projects($param);
               }else{
                 $totalImg = $this->project->find_project_images($param);
                 $totalRec = $totalImg['imageCnt'];
                 $param['offset'] = $pagination['offset'];
                 $param['limit'] = $pagination['limit'];
                 $param['return_type'] = 'array';
                 $project_details= $this->project->find_project_images($param);
               }
            if(isset($project_details[0])){
            $project_details= array_map(function($tag) {
                return array(
                'image_id' => $tag['project_image_id'],
                'project_id' => $tag['project_id'],
                'imageurl' =>file_exists('uploads/portfolio/images/'.$tag['image'])? base_url().'uploads/portfolio/images/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                'image_name' => (isset($tag['image_title'])?$tag['image_title']:$tag['title']),
                'comment_count' => $tag['commentCount'],
                'likecount' => $tag['imageLike'],
                'viewcount' => $tag['view_num'],
                'likestatus' => $this->status_check($tag['project_image_id'],'project_image_likes','image_id')
                );
             }, $project_details);
            }else{
            $project_details=array();
            }
           $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'images'=>$project_details));
        }

        /* Api to get book list */
        function bookslist_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            if($this->post('filterbyyear')!=''){
             $filterbyyear=  explode(',', $this->post('filterbyyear'));
            }else{
             $filterbyyear='null';
            }
            if($this->post('filterbycategoryid')!=''){
             $filterbycategoryid=  explode(',', $this->post('filterbycategoryid'));
            }else{
             $filterbycategoryid='null';
            }
            if($this->post('filterbytypeid')!=''){
             $filterbytypeid=  explode(',', $this->post('filterbytypeid'));
            }else{
             $filterbytypeid='null';
            }
            if($this->post('filterbydesignerid')!=''){
             $filterbydesignerid=  explode(',', $this->post('filterbydesignerid'));
             foreach ($filterbydesignerid as $a => $b) {
                if(is_int($a)) {
                      $designer_id[]=$a;
                    }
                }
            }else{
             $designer_id='null';
            }
            $data=array('filterbyyear'=>$filterbyyear,'filterbycategoryid'=>$filterbycategoryid,'filterbytypeid'=>$filterbytypeid,'filterbydesignerid'=>$designer_id);
            $book_details=$this->api_model->get_book_details($data,$pagination['offset'],$pagination['limit'],$lang_id);
            $count_books=$this->api_model->get_book_details($data,'','',$lang_id,'num_row');
            if(isset($book_details[0])){
            $book_details= array_map(function($tag) {
                return array(
                'id' => $tag['book_id'],
                'issuedate' => TimeIntervalHelper($tag['issue_date']),
                'image' =>base_url().'uploads/book/images/'.(($tag['image_name']=='')?'dummy.jpg':$tag['image_name']),
                'price' => $tag['price'],
                'title' => $tag['title'],
                'description' => $tag['short_description'],
                'category' => $tag['book_cat_name'],
                'type' => getFevicolbookTypes($tag['book_type_id'],$tag['lang_id']),
                'previewURL' =>(($tag['pdf']=='')?'':base_url().'uploads/book/files/'.$tag['pdf']),
                'likecount'=>$this->api_model->count_all('book_likes',array('book_id='=>$tag['book_id'])),
                'viewcount'=>$tag['view_num'],
                'sharecount'=>$tag['share_count'],
                'likestatus'=>$this->status_check($tag['book_id'],'book_likes','book_id'),
                'additionalinfo'=>'',
                );
             }, $book_details);
            }else{
            $book_details=array();
            }

           $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($count_books),'images'=>$book_details));
        }

         /* Api to like book */
        function likebook_post()
        {
            $this->validation("sessiontoken");
            $bookid=$this->post('book_id');
            $user_id=$this->post('user_id');
            $islike=$this->post('islike');
            if($islike =='1' || $islike =='0' ){
            $condiction=array('book_id'=>$bookid,'user_id'=>$user_id);
            $data=array('user_id'=>$user_id,'book_id'=>$bookid,'created'=> date("Y-m-d H:i:s"),'modified'=> date("Y-m-d H:i:s"));
            $like_dislike=$this->api_model->do_undo($condiction,'book_likes',$data);
            $total_like=$this->api_model->count_all('book_likes',array('book_id'=>$bookid));
            if($like_dislike==1){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>number_format($total_like),'likestatus'=>1));
            }else{
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>($total_like),'likestatus'=>0));
            }
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

         /* Api to add book view count */
        function addbookviewcount_post()
        {
            $bookid=$this->post('book_id');
            $condiction=array('book_id'=>$bookid);
            $bookviewcount=$this->api_model->view_share_count($condiction,'view_num','books');
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','viewcount'=>number_format($bookviewcount)));
        }

         /* Api to add book share count */
        function addbooksharecount_post()
        {
            $this->validation("sessiontoken");
            $bookid=$this->post('book_id');
            $user_id=$this->post('user_id');
            $condiction=array('book_id'=>$bookid);
            $booksharecount=$this->api_model->view_share_count($condiction,'share_count','books');
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','sharecount'=>number_format($booksharecount)));
        }

         /* Api to get book category */
        function getbookcategory_post()
        {
            $lang_id=$this->get_language_id();
            $table='book_category_contents as bcc';
            $cond=array('bcc.lang_id'=>$lang_id);
            $data=array('bcc.book_cat_id as categoryid','bcc.book_cat_name as categorytitle');
            $book_category=$this->api_model->get_data($table,$cond,$data);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'categories'=>$book_category));
        }

         /* Api to delete image  from idea book */
        function deleteimagefromideabook_post()
        {
            $this->validation("sessiontoken");
            $delete_image=$this->api_model->delete_bookimage($this->post('imageid'),$this->post('ideabook_id'));
            if($delete_image=='true'){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

         /* Api to delete idea book  */
        function deleteideabook_post()
        {
            $this->validation("sessiontoken");
            $delete_image=$this->api_model->delete_ideabook($this->post('ideabook_id'));
            if($delete_image=='true'){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api to edit idea book  */
        function editIdeabook_post()
        {
            $this->validation("sessiontoken");
            $ideabook_id=$this->post('ideabook_id');
            $ideabook_name=$this->post('ideabook_name');
            $data=array('ideabook_name'=>$ideabook_name);
            $edit_ideabook=$this->api_model->edit('ideabook','ideabook_id',$ideabook_id,$data);
            if($edit_ideabook=='true'){
             $condiction=array('ideabook.ideabook_id'=>$ideabook_id);
             $all_ideabook= $this->api_model->get_ideabook_rows($condiction);
             $all_ideabook = array_map(function($tag) {
                return array(
                'ideabook_id' => $tag['ideabook_id'],
                'ideabook_name' => $tag['ideabook_name'],
                'ideabook_coverimage'=>base_url().'uploads/portfolio/images/'.(($tag['image']=='')?'dummy.jpg':$tag['image'])
                );
             }, $all_ideabook);
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','ideabook_id'=>$all_ideabook[0]['ideabook_id'],'ideabook_name'=>$all_ideabook[0]['ideabook_name'],'ideabook_coverimage'=>$all_ideabook[0]['ideabook_coverimage']));
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api to get book issue year */
        function getbookissueyear_post()
        {
            $book_issue_year=$this->api_model->book_issue_year();
            if(count($book_issue_year)>0){
            $book_issue_year= array_map(function($tag) {
                return array(
               // 'issue_date' => explode('-', $tag['issue_date'])[0]
                  'issue_date' => $tag->myformat
                );
             }, $book_issue_year);
            }else{
            $book_issue_year=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','Years'=>$book_issue_year));
        }

        /* Api to get book featured designer */
        function bookfeatureddesigners_post()
        {
            $pagination=$this->get_offset_size();
            $book_id=$this->post('book_id');
            $designer_details=$this->api_model->getfetureddesigners($book_id,$pagination['offset'],$pagination['limit']);
            $designer_count=$this->api_model->getfetureddesigners($book_id);
            if(isset($designer_details[0])){
            $designer_details= array_map(function($tag) {
                return array(
                'id' =>$tag['id'],
                'name' =>$tag['name'],
                'image' =>base_url().'uploads/users/images/'.(($tag['image']=='')?'dummy.jpg':$tag['image']),
                'designation' =>$tag['designation'],
                'location' =>$tag['location']
                );
             }, $designer_details);
            }else{
            $designer_details=array();
            }

            $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format(count($designer_count)),'designer'=>$designer_details));
        }

        /* Api to getbooktype */
        function getbooktype_post()
        {
            $lang_id=$this->get_language_id();
            $book_type=$this->book_type->get_rows(array('lang_id'=>$lang_id));
            if(isset($book_type[0])){
            $book_type= array_map(function($tag) {
                return array(
                'id' => $tag['book_type_id'],
                'type' => $tag['book_type_name']
                );
             }, $book_type);
            }else{
            $book_type=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'Types'=>$book_type));
        }

        /* Api to add image like */
        function imagelike_post()
        {
             $this->validation("sessiontoken");
             $imageid=$this->post('image_id');
             $user_id=$this->post('user_id');
             $likestatus=$this->post('likestatus');
             if($likestatus =='1' || $likestatus =='0' ){
             $condiction=array('image_id'=>$imageid,'user_id'=>$user_id);
             $data=array('user_id'=>$user_id,'image_id'=>$imageid,'created'=> date("Y-m-d H:i:s"));
             $like_unlike=$this->api_model->do_undo($condiction,'project_image_likes',$data);
             $total_like=$this->api_model->count_all('project_image_likes',array('image_id'=>$imageid));
             if($like_unlike==1){
              $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>1));
             }else{
              $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>0));
             }
             }else{
              $this->response(array('error'=>'Something went wrong. Please try again later'));
             }
         }

        /* Api to get all project images list */
        function globalsearch_post()
        {
         if($this->post('user_id')!=''){
         $this->validation("sessiontoken");
         }
         $lang_id=$this->get_language_id();
         $pagination=$this->get_offset_size();
         $keywords=$this->post('inputstring');
         $conditions['search']['keywords'] = filterString($keywords);
         $conditions['lang_id']=$lang_id;
         $conditions['return_type'] = 'count';
         switch ($this->post('api_differentiate_tag')) {
                 case 'Designidea':
                     $conditions['conditions']['p.status'] = 1;
                     $totalRec = $this->home_search->find_project_images($conditions);
                     $conditions['return_type'] = 'array';
                     $conditions['offset'] = $pagination['offset'];
                     $conditions['limit'] =$pagination['limit'];
                     $project_details= $this->home_search->find_project_images($conditions);
                     if(isset($project_details[0])){
                     $project_details= array_map(function($tag) {
                         return array(
                         'image_id' => $tag['project_image_id'],
                         'project_id' => $tag['project_id'],
                         'imageurl' =>file_exists('uploads/portfolio/images/'.$tag['image'])? base_url().'uploads/portfolio/images/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                         'image_name' => $tag['image_title'],
                         'comment_count' => $tag['commentCount'],
                         'likecount' =>  $tag['imageLike'],
                         'viewcount' => $tag['view_num'],
                         'likestatus' => $this->status_check($tag['project_image_id'],'project_image_likes','image_id')
                         );
                      }, $project_details);
                     }else{
                         $project_details=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'images'=>$project_details));
                     break;
                 case 'booklist':
                     $totalRec = $this->home_search->searchBookCount($conditions);
                     $conditions['start'] = $pagination['offset'];
                     $conditions['limit'] =$pagination['limit'];
                     $book_details= $this->home_search->searchBooks($conditions);
                     if(isset($book_details[0])){
                     $book_details= array_map(function($tag) {
                         return array(
                         'id' => $tag['book_id'],
                         'title' => $tag['title'],
                         'issuedate' =>displayFormatTime($tag['issue_date']),
                         'category' => $tag['book_cat_name'],
                         'price' => $tag['price'],
                         'image' => ($tag['image_name']!='')?base_url().'uploads/book/images/thumb/'.$tag['image_name']:base_url().'uploads/book/images/thumb/dummy.jpg',
                         'likecount' => $this->api_model->count_all('book_likes',$cond=array('book_id'=>$tag['book_id'])),
                         'viewcount' => $tag['view_num'],
                         'sharecount'=>'',
                         'likestatus'=>$this->status_check($tag['book_id'],'book_likes','book_id'),
                         'type'=>$tag['book_type_name'],
                         'description'=>$tag['short_description'],
                         'previewurl'=>($tag['pdf']!='')?base_url().'uploads/book/files/'.$tag['pdf']:base_url().'uploads/book/images/thumb/dummy.jpg',
                         'additionalinfo'=>''
                         );
                      }, $book_details);
                     }else{
                         $book_details=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'Books'=>$book_details));
                     break;
                 case 'interiopedia':
                     $totalRec = $this->home_search->searchInteriopedia($conditions);
                     $conditions['start'] = $pagination['offset'];
                     $conditions['limit'] =$pagination['limit'];
                     $conditions['return_type'] = 'array';
                     $interiopedia_details=$this->home_search->searchInteriopedia($conditions);

                     if(isset($interiopedia_details[0])){
                        foreach($interiopedia_details as $key=>$value){
                            if($value['type']=='art'){
                              $value['type']='Article';
                              $value['api_differentiate_tag']='article';
                              $path= base_url().'uploads/article/images/';
                            }elseif($value['type']=='diy'){
                              $value['type']='DIY';
                              $value['api_differentiate_tag']='diy';
                              $path= base_url().'uploads/doit/images/thumb/';
                            }elseif($value['type']=='dt'){
                              $value['type']='Design Trend';
                              $value['api_differentiate_tag']='designtrend';
                              $path= base_url().'uploads/design_trend/images/thumb/';
                            }elseif($value['type']=='dow'){
                              $value['type']='Designer of the Week';
                              $value['api_differentiate_tag']='dow';
                              $path= base_url().'/uploads/designer_week/images/';
                            }elseif($value['type']=='pro'){
                              $value['type']='Product';
                              $value['api_differentiate_tag']='product';
                              $path= base_url().'uploads/interiopediaproduct/images/thumb/';
                            }elseif($value['type']=='int'){
                              $value['type']='Interview';
                              $value['api_differentiate_tag']='interview';
                              $path= base_url().'uploads/interview/images/';
                            }
                            $interiopedia_details[$key]['type']=$value['type'];
                            $interiopedia_details[$key]['api_differentiate_tag']=$value['api_differentiate_tag'];
                            $interiopedia_details[$key]['image']=$path.$value['image'];
                        }
                     $interiopedia_details= array_map(function($tag) {
                         return array(
                         'type'=>$tag['type'],
                         'title'=>$tag['title'],
                         'api_differentiate_tag'=>$tag['api_differentiate_tag'],
                         'id' => $tag['id'],
                         'postedbyname' => $tag['firstname'].' '.$tag['lastname'],
                         'image_url' =>$tag['image'],
                         'description' =>RemoveHTMLtag($tag['description']),
                         'postedbydate' =>displayFormatTime($tag['created'])
                         );
                      }, $interiopedia_details);
                     }else{
                         $interiopedia_details=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'Interiopedia'=>$interiopedia_details));

                     break;
                 case 'designers':
                     $conditions['user_type_id'] = 3;
                     $totalRec = $this->home_search->searchUserCount($conditions);
                     $conditions['start'] = $pagination['offset'];
                     $conditions['limit'] =$pagination['limit'];
                     $designer_details= $this->home_search->searchUsers($conditions);;
                     if(isset($designer_details[0])){
                     $designer_details= array_map(function($tag) {
                         return array(
                         'id' => $tag['user_id'],
                         'name' => $tag['firstname'].$tag['lastname'],
                         'designation' =>$tag['designation'],
                         'company' => $tag['company'],
                         'profilepic' => ($tag['user_image']!='')?base_url().'uploads/users/images/'.$tag['user_image']:base_url().'uploads/users/images/dummy.jpg',
                         'nooffollowers' =>$this->api_model->count_all('user_followers',$cond=array('user_id'=>$tag['user_id'])),
                         'location' => $tag['state_name'].' '.$tag['city_name'],
                         'avgrate' => $tag['user_rating'],
                         'followstatus'=>$this->status_check($tag['user_id'],'user_followers','user_id','follower_id')
                         );
                      }, $designer_details);
                     }else{
                         $designer_details=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'Designers'=>$designer_details));
                     break;
                     }
         $totalcount=$this->api_model->searchProjects($condictions1);

     }

        /* Api to post image comment*/
        function addimageviewcount_post()
        {
         $imageid=$this->post('image_id');
         $condiction=array('project_image_id'=>$imageid);
         $imageviewcount=$this->api_model->view_share_count($condiction,'view_num','project_images');
         $this->response(array('responsecode'=>200,'responsedetails'=>'success','viewcount'=>$imageviewcount));
        }

         /* Api to post image comment*/
        function postcommentimage_post()
        {
                $this->validation("sessiontoken");
                // load common model
                $this->load->model('common_model');
                // getting data from user
                $data['user_id'] = $this->post('user_id');
                $data['project_image_id'] = $this->post('image_id');
                $data['comment'] = $this->post('comment');
                // add comment in comment table and get inserted id
                $last_comment_id = $this->common_model->add('project_image_comments',$data);
                if($last_comment_id){
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
                }else{
                   $this->response(array('responsecode'=>500,'error'=>'Something went wrong. Please try again later.'));
                }
         }

             /* Common Api to post  comment*/
        function commonpostcomment_post()
        {
                $this->validation("sessiontoken");
                // load common model
                $this->load->model('common_model');
                // getting data from
                switch ($this->post('api_differentiate_tag')) {
                    case 'article':
                        $table='article_comments';
                        $cond=array('article_id'=>$this->post('id'));
                        $data=array('article_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'product':
                        $table='interiopediaproduct_comments';
                        $cond=array('interproduct_id'=>$this->post('id'));
                        $data=array('interproduct_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'designtrend':
                        $table='design_trend_comments';
                        $cond=array('design_trend_id'=>$this->post('id'));
                        $data=array('design_trend_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'diy':
                        $table='do_it_comments';
                        $cond=array('doit_id'=>$this->post('id'));
                        $data=array('doit_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'dow':
                        $table='designer_week_comments';
                        $cond=array('designer_week_id'=>$this->post('id'));
                        $data=array('designer_week_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'interview':
                        $table='interview_comments';
                        $cond=array('interview_id'=>$this->post('id'));
                        $data=array('interview_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                    break;
                    case 'designer':
                        $table='interview_comments';
                        $data=array('designer_id'=>$this->post('id'), 'user_type'=>$this->post('usertype'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                        $cond=array('designer_id'=>$this->post('id'));
                    break;
                    case 'project':
                        $table='project_image_comments';
                        $data=array('project_image_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'comment'=>$this->post('comments'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                        $cond=array('project_image_id'=>$this->post('id'));
                    break;
                    default:
                        $this->response(array('responsecode'=>500,'error'=>'Something went wrong. Please try again later.'));
                    break;
                }

                // add comment in comment table and get inserted id
                $last_comment_id = $this->common_model->add($table,$data);
                if($last_comment_id){
                   $totalcomment=$this->api_model->count_all($table,$cond);
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','commentcount'=>number_format($totalcomment)));
                }else{
                   $this->response(array('responsecode'=>500,'error'=>'Something went wrong. Please try again later.'));
                }
         }

             /* Common api to like */
        function commonlike_post()
        {
                  $this->validation("sessiontoken");                                                                                                                                                                                                                                                                                                                                                                  $this->validation("sessiontoken");
                 // load common model
                 $this->load->model('common_model');

                 // getting data from
                 switch ($this->post('api_differentiate_tag')) {
                     case 'article':
                         $table='article_likes';
                         $cond=array('article_id'=>$this->post('id'));
                         $data=array('article_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'product':
                         $table='interiopediaproduct_likes';
                         $cond=array('interproduct_id'=>$this->post('id'));
                         $data=array('interproduct_id'=>$this->post('id'),'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'designtrend':
                         $table='design_trend_likes';
                         $cond=array('design_trend_id'=>$this->post('id'));
                         $data=array('design_trend_id'=>$this->post('id'),'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'diy':
                         $table='do_it_likes';
                         $cond=array('doit_id'=>$this->post('id'));
                         $data=array('doit_id'=>$this->post('id'),'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'dow':
                         $table='designer_week_likes';
                         $cond=array('designer_week_id'=>$this->post('id'));
                         $data=array('designer_week_id'=>$this->post('id'),'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'interview':
                         $table='interview_likes';
                         $cond=array('interview_id'=>$this->post('id'));
                         $data=array('interview_id'=>$this->post('id'),'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                     break;
                     case 'designer':
                         $table='user_likes';
                         $data=array('designer_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                         $cond=array('designer_id'=>$this->post('id'));
                     break;
                     case 'project':
                         $table='project_image_likes';
                         $data=array('image_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'));
                         $cond=array('image_id'=>$this->post('id'));
                     break;
                     case 'forum':
                         $table='forum_likes';
                         $data=array('forum_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                         $cond=array('forum_id'=>$this->post('id'));
                     break;
                     case 'book':
                         $table='book_likes';
                         $data=array('book_id'=>$this->post('id'), 'user_id'=>$this->post('user_id'),'created'=>date('Y-m-d H:i:s'),'modified'=>date('Y-m-d H:i:s'));
                         $cond=array('book_id'=>$this->post('id'));
                     break;
                     default:
                         $this->response(array('responsecode'=>500,'error'=>'Something went wrong. Please try again later.'));
                     break;
                 }
                 $cond['user_id']=$this->post('user_id');
                 $likestatus=$this->post('islike');
                 if($likestatus =='1' || $likestatus =='0' ){
                     $like_unlike=$this->api_model->do_undo($cond,$table,$data);
                     unset($cond['user_id']);
                     $total_like=$this->api_model->count_all($table,$cond);
                     if($like_unlike==1){
                      $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>1));
                     }else{
                      $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>0));
                     }
                     }else{
                      $this->response(array('error'=>'Something went wrong. Please try again later'));
                 }

          }

         /* Api to get image comment */
        function getcommentimage_post()
        {
                // load common model
                $this->load->model('common_model');
                // getting data from user
                $data['image_id'] = $this->post('image_id');
                $data['pageno'] = $this->post('pageno');
                $data['pagesize'] = $this->post('pagesize');
                // get all comment from comment table
                $all_comments = $this->common_model->get('project_image_comments',array('comment_id'),array('project_image_id'=>$data['image_id']));
                $total_comments = count($all_comments);
                // get record starts for that page
                $record_start_from = ($data['pageno']-1)*$data['pagesize'];
                $record_start_from = $record_start_from < 0 ? 0 : $record_start_from;
                $comments = $this->common_model->get('project_image_comments',array('comment_id AS commentid','comment AS comments','project_image_comments.user_id AS userid','CONCAT(users.firstname," ",users.lastname) AS username','users.user_image AS userprofilepic','project_image_comments.created AS date'),array('project_image_id'=>$data['image_id']),null,array('users'=>'project_image_comments.user_id = users.user_id'),array('users'=>'left'),$record_start_from,$data['pagesize'],array('project_image_comments.comment_id'=>'desc'));

                $image_path=base_url().'uploads/users/images/';
                for($i=0;$i< count($comments);$i++){
                    $time=explode(' ',$comments[$i]->date);
                    $comments[$i]->time= $time[1];
                    $comments[$i]->date=formatIntervalHelper($comments[$i]->date);
                    $comments[$i]->userprofilepic=($comments[$i]->userprofilepic=='')?$image_path.'dummy.jpg':$image_path.$comments[$i]->userprofilepic;
                }

                if(!empty($comments)){
                    $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($total_comments),'comments'=>$comments));
                }else{
                    $comments=array();
                    $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($total_comments),'comments'=>$comments));
                }
        }

         /* Api to get contractor type */
        function contractortype_post()
        {
            $lang_id=$this->get_language_id();
            $table='user_types as u';
            $cond=array('u.parent_type_id'=>6,'uc.lang_id'=>$lang_id);
            $data=array('u.user_type_id as id,uc.type_name as type');
            $join=array('user_type_contents as uc'=>'uc.user_type_id=u.user_type_id');
            $response=$this->api_model->get_data($table,$cond,$data,$join);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'contractors'=>$response));
        }

         /* Api to get worktype */
        function worktype_post()
        {
            $lang_id=$this->get_language_id();
            $work_type=$this->work_type->get_rows(array('conditions'=>array('status'=>1),'lang_id'=>$lang_id));
            if(isset($work_type[0])){
            $work_type= array_map(function($tag) {
            return array(
            'type_id' => $tag['type_id'],
            'type_name' => $tag['type_name']
            );
            }, $work_type);
            }else{
               $work_type=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'worktypes'=>$work_type));

        }

         /* Api to get Budget */
        function budget_post()
        {
            $lang_id=$this->get_language_id();
            $budget= $this->contractor_budget->get_rows(array('conditions'=>array('status'=>1),'order_by'=>'budget_id asc','lang_id'=>$lang_id));
            if(isset($budget[0])){
            $image_details= array_map(function($tag) {
            return array(
            'budget_id' => $tag['budget_id'],
            'budget' => $tag['budget']
            );
            }, $budget);
            }else{
               $budget=array();
            }

            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'budgets'=>$budget));

        }

        /* Api to get Specilization */
        function specialization_post()
        {
            $lang_id=$this->get_language_id();
            $table='specialization_contents';
            $cond=array('lang_id'=>$lang_id);
            $data=array('specialization_id as id,specialization');
            $response=$this->api_model->get_data($table,$cond,$data);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'specializations'=>$response));

        }

        /* Api for Interior designer profile */
        function interiordesignerprofile_post()
        {
            $userid=$this->post('designer_id');
            $lang_id=$this->get_language_id();
            $designerprofile=$this->api_model->designerprofile($userid,$lang_id);

            if(!isset($designerprofile[0])){
               $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
            $designerprofile[0]['followers']=$this->api_model->count_all('user_followers',array('user_id'=>$userid));
            $designerprofile[0]['comments']=$this->api_model->count_all('project_image_comments',array('user_id'=>$userid));
            $designerprofile[0]['messages']=$this->api_model->count_all('user_messages',array('reciever_id'=>$userid));
            $designerprofile[0]['reviews']=$this->api_model->count_all('user_review',array('user_id'=>$userid));

            if(count($designerprofile)>0){
            $designerprofile= array_map(function($tag) {
                return array(
                'id' => $tag['user_id'],
                'name' => $tag['name'],
                'designation' => $tag['designation'],
                'location' => $tag['area'],
                'profileimageurl' =>base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                'mobile' => $tag['phone'],
                'coverimageurl' =>base_url().'uploads/users/cover_images/'.(($tag['cover_image']=='')?'dummy.jpg':$tag['cover_image']),
                'followers' => $tag['followers'],
                'comments' => $tag['comments'],
                'reviews'=> $tag['reviews'],
                'sharecount'=>'0',
                'messages'=>$tag['messages'],
                'avgrate'=> ($tag['user_rating']==null)?'':$tag['user_rating'],
                'profiledownload_url'=>($tag['profile_pdf']!='')?base_url().'uploads/users/public_profile/'.$tag['profile_pdf']:''
                );
            }, $designerprofile);
            }else{
            $designerprofile=array();
            }
            $noofprojects=$this->api_model->count_all('projects',array('user_id'=>$userid));
            $projectphotos=$this->api_model->get_project_images($userid,$lang_id);

            if(isset($projectphotos[0])){
            $projectphotos= array_map(function($tag2) {
            return array(
            'projectid' => $tag2['project_id'],
            'projectimageid' => $tag2['project_image_id'],
            'projectcoverimageURL' => base_url().'uploads/portfolio/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
            'projectname'=>$tag2['title'],
            'projectlikecount'=>$this->api_model->count_all('project_image_likes',array('image_id'=>$tag2['project_image_id'])),
            'projectviewcount'=>$tag2['view_num']
            );
            }, $projectphotos);
            } else{
            $projectphotos=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'designer'=>$designerprofile[0],'noofprojects'=>number_format($noofprojects),'projectphotos'=>$projectphotos));
        }

        /* Api to send message to enterior designer */
        function sendmessagetointeriordesigner_post()
        {
            $this->validation("sessiontoken");
            $senderid=$this->post('user_id');
            $recieverid=$this->post('designer_id');
            $message_subject=$this->post('message_subject');
            $message_content=$this->post('message_description');
            $lang_id=$this->get_language_id();
            if($senderid==''||$recieverid==''||$message_subject==''|| $message_content==''|| $lang_id==''|| $senderid==$recieverid){
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
             $msgdata=array('sender_id'=>$senderid,'reciever_id'=>$recieverid,'subject'=>$message_subject,'contents'=>$message_content,'language_id'=>$lang_id,'senddate'=>date("Y-m-d H:i:s"));
            $send=$this->api_model->post_data($msgdata,'user_messages');
            if($send){
            $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
        }

         /* Api to like interiordesigner */
         function likeinteriordesigner_post()
        {
            $this->validation("sessiontoken");
            $designer_id=$this->post('designer_id');
            $user_id=$this->post('user_id');
            $likestatus=$this->post('islike');
            if($likestatus =='1' || $likestatus =='0' ){
            $condiction=array('designer_id'=>$designer_id,'user_id'=>$user_id);
            $data=array('user_id'=>$user_id,'designer_id'=>$designer_id,'created'=> date("Y-m-d H:i:s"));
            $like_unlike=$this->api_model->do_undo($condiction,'user_likes',$data);
            $total_like=$this->api_model->count_all('user_likes',array('designer_id'=>$designer_id));
            if($like_unlike==1){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>1));
            }else{
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>0));
            }
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

         /* Api to post Review */
         function postreview_post()
        {
            $this->validation("sessiontoken");
            $userid=$this->post('user_id');
            $designerid=$this->post('designer_id');
            $review=$this->post('review');
            $reviewstarcount=$this->post('reviewstarcount');
            if($designerid==''||$review==''||$reviewstarcount==''){
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            $reviewdata=array('user_id'=>$userid,'designer_id'=>$designerid,'review'=>$review,'reviewstarcount'=>$reviewstarcount,'reviewdate'=>date("Y-m-d H:i:s"));
            $post=$this->api_model->post_data($reviewdata,'user_review');
            if($post){
            $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
        }

        /* Api for Interior designer Review */
         function interiordesignerreviews_post()
        {
            $pagination=$this->get_offset_size();
            $designer_id=$this->post('designer_id');
            $total_count=$this->api_model->get_reviews($designer_id,'','');
            $review_details=$this->api_model->get_reviews($designer_id,$pagination['offset'],$pagination['limit']);

            if(isset($review_details[0])){
                $review_details= array_map(function($tag) {
                    return array(
                    'reviewid' => $tag['review_id'],
                    'reviews' => $tag['review'],
                    'reviewstarcount' => $tag['reviewstarcount'],
                    'userid' => $tag['user_id'],
                    'username' =>$tag['username'],
                    'userprofilepic' => base_url().'uploads/users/images/'.(($tag['user_image']=='')?'dummy.jpg':$tag['user_image']),
                    'reviewdate' => formatIntervalHelper($tag['reviewdate'])
                    );
                }, $review_details);
            }else{
               $review_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','total_count'=>number_format(count($total_count)),'reviews'=>$review_details));
        }

        /* Api for Enduser designer Activity */
        function enduserrecentactivities_post()
        {
            $this->validation("sessiontoken");
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            $designer_id=$this->post('user_id');
            $user_count=$this->api_model->count_all('users',array('user_id'=>$designer_id));
            if($user_count!=1){
                $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            $total_count=$this->api_model->count_all('user_activity',array('user_id'=>$designer_id,'associate_id !='=>0));
            $activities=$this->api_model->get_activity_log($designer_id,$pagination['offset'],$pagination['limit']);
            foreach($activities as $key=>$activity){
              switch ($activity['activity_type']) {
                case 'followers':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Followed  designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/user/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_like':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Liked  designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/user/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_review':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Give review on user ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'project_image_like':
                    $cond=array('project_image_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('image_title','image_description');
                    $table='project_image_contents';
                    $cond2=array('project_image_id'=>$activity['associate_id']);
                    $data2=array('image');
                    $table2='project_images';
                    $project_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $project_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']=base_url().'uploads/portfolio/images/'.(isset($project_image[0]['image'])?$project_image[0]['image']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Liked image ".(isset($project_image_name[0]['image_title'])?$project_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'book_like':
                    $cond=array('book_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('title','short_description');
                    $table='book_contents';
                    $cond2=array('book_id'=>$activity['associate_id']);
                    $data2=array('image_name');
                    $table2='book_images';
                    $book_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $book_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']=base_url().'uploads/book/images/'.(isset($book_image[0]['image_name'])?$book_image[0]['image_name']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Liked image ".(isset($book_image_name[0]['image_title'])?$book_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'project_image_comment':
                    $cond=array('project_image_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('image_title','image_description');
                    $table='project_image_contents';
                    $cond2=array('project_image_id'=>$activity['associate_id']);
                    $data2=array('image');
                    $table2='project_images';
                    $project_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $project_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']=base_url().'uploads/portfolio/images/'.(isset($project_image[0]['image'])?$project_image[0]['image']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Give comment on project image ".(isset($project_image_name[0]['image_title'])?$project_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_invite_mentorship':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Invite for mentorship the designer ".@$user_details[0]['firstname']." ".@$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_apply_mentorship':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Apply for mentorship the designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'create_ideabook':
                    $cond=array('user_id'=>$activity['user_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']=@$user_details[0]['firstname']." ".@$user_details[0]['lastname']." has created ideabbok ".$activity['activity'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                }

            }
            if(isset($activities[0])){
                $activities= array_map(function($tag) {
                    return array(
                    'activityid' => $tag['activityid'],
                    'activitytitle' => $tag['activity_title'],
                    'activitydate' =>formatIntervalHelper($tag['activity_date']),
                    'activitypic' => $tag['activitypic'],
                    'activitydescription' =>$tag['activitydescription']
                    );
                }, $activities);
            }else{
               $activities=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','total_count'=>number_format($total_count),'recentactivities'=>$activities));
        }

          /* Api for Interior designer Activity */
        function interiordesigneractivity_post()
        {
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            $designer_id=$this->post('designer_id');
            $user_count=$this->api_model->count_all('users',array('user_id'=>$designer_id));
            if($user_count!=1){
                $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            $total_count=$this->api_model->count_all('user_activity',array('user_id'=>$designer_id,'associate_id !='=>0));
            $activities=$this->api_model->get_activity_log($designer_id,$pagination['offset'],$pagination['limit']);
            foreach($activities as $key=>$activity){
              switch ($activity['activity_type']) {
                case 'followers':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Followed  designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_like':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Liked  designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_review':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Give review on user ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'project_image_like':
                    $cond=array('project_image_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('image_title','image_description');
                    $table='project_image_contents';
                    $cond2=array('project_image_id'=>$activity['associate_id']);
                    $data2=array('image');
                    $table2='project_images';
                    $project_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $project_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']=base_url().'uploads/portfolio/images/'.(isset($project_image[0]['image'])?$project_image[0]['image']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Liked image ".(isset($project_image_name[0]['image_title'])?$project_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'book_like':
                    $cond=array('book_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('title','short_description');
                    $table='book_contents';
                    $cond2=array('book_id'=>$activity['associate_id']);
                    $data2=array('image_name');
                    $table2='book_images';
                    $book_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $book_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']=base_url().'uploads/book/images/'.(isset($book_image[0]['image_name'])?$book_image[0]['image_name']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Liked book ".(isset($book_image_name[0]['image_title'])?$book_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'project_image_comment':
                    $cond=array('project_image_id'=>$activity['associate_id'],'lang_id'=>$lang_id);
                    $data=array('image_title','image_description');
                    $table='project_image_contents';
                    $cond2=array('project_image_id'=>$activity['associate_id']);
                    $data2=array('image');
                    $table2='project_images';
                    $project_image=$this->api_model->get_data($table2,$cond2,$data2);
                    $project_image_name=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitypic']= base_url().'uploads/portfolio/images/'.(isset($project_image[0]['image'])?$project_image[0]['image']:'dummy.jpg');
                    $activities[$key]['activitydescription']="Give comment on project image ".(isset($project_image_name[0]['image_title'])?$project_image_name[0]['image_title']:'');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_invite_mentorship':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Invite for mentorship the designer ".@$user_details[0]['firstname']." ".@$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'user_apply_mentorship':
                    $cond=array('user_id'=>$activity['associate_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']="Apply for mentorship the designer ".$user_details[0]['firstname']." ".$user_details[0]['lastname'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                case 'create_ideabook':
                    $cond=array('user_id'=>$activity['user_id']);
                    $data=array('firstname','lastname','user_image');
                    $table='users';
                    $user_details=$this->api_model->get_data($table,$cond,$data);
                    $activities[$key]['activitydescription']=@$user_details[0]['firstname']." ".@$user_details[0]['lastname']." has created ideabbok ".$activity['activity'];
                    $activities[$key]['activitypic']=base_url().'uploads/users/images/'.(isset($user_details[0]['user_image'])?$user_details[0]['user_image']:'dummy.jpg');
                    $activities[$key]['activityid']=($key+1);
                    break;
                }
            }

            if(isset($activities[0])){
                $activities= array_map(function($tag) {
                    return array(
                    'activityid' =>$tag['activityid'],
                    'activitytitle' => $tag['activity_title'],
                    'activitydate' =>formatIntervalHelper($tag['activity_date']),
                    'activitypic' =>$tag['activitypic'],
                    'activitydescription' =>$tag['activitydescription']
                    );
                }, $activities);
            }else{
               $activities=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','total_count'=>number_format($total_count),'recentactivities'=>$activities));
        }

        /* Api to find Contractor */
        function findcontractor_post()
        {
                $this->validation("sessiontoken");
                $state_id = $this->post('state_id');
                $city_id = $this->post('city_id');
                $area = $this->post('area');
                $postcode = $this->post('pin');
                if(!empty($postcode)){
                        $searchPostcode = $postcode;
                  }else{
                        $AddressStr = $area.' '.getCityName($city_id).' '.getStateName($state_id).' india';
                        $searchPostcode = getPinCode($AddressStr);
                        $searchPostcode = !empty($searchPostcode)?$searchPostcode :0;
                 }
                $searchData = array('user_id' => $this->post('user_id'),
                'contractor_type_id' => $this->post('contractortypeid'),
                'firstname' => $this->post('fname'),
                'lastname' => $this->post('lname'),
                'user_type_id' => $this->post('usertype'),
                'email' => base64_decode($this->post('email')),
                'phone' => base64_decode($this->post('mobile')),
                'state_id' => $this->post('state_id'),
                'city_id' => $this->post('city_id'),
                'area' => $this->post('area'),
                'postcode' =>$searchPostcode,
                'budget_id' => $this->post('budgetid'),
                'type_id' => $this->post('worktypeid'),
                'specialization_id' => $this->post('specializationid'),
                'mobile_otp' => $this->post('otp')
                );
                $remainSearch =remainContractorSearch($this->post('user_id'));
                if($remainSearch > 0){
                    $locationAddr = $searchData['postcode'].' '.$searchData['area'].' '.getCityName($searchData['city_id']).' '.getStateName($searchData['state_id']).' india';
                    $searchLatLon = getLatLon($locationAddr);
                    $searchData['latitude'] = $searchLatLon['latitude'];
                    $searchData['longitude'] = $searchLatLon['longitude'];
                    $id = $this->contractor->search_insert($searchData);
                    if($id)
                     {
                        $searchRow = $this->contractor->search_rows(array('contractor_search_id'=>$id));
                         if($this->post('user_id'))
                          {
                            $searchedIds = searchedUserIds($this->post('user_id'))?searchedUserIds($this->post('user_id')):0;
                          }
                          else
                          {
                            $this->response(array("error"=>"Something went wrong. Please try again later."));
                          }
                        $searchData = $this->contractor->search_contractors(array('latitude'=>$searchRow['latitude'],'longitude'=>$searchRow['longitude'],'limit'=>4,'where_not_in'=>$searchedIds));
                        $this->contractor->search_update(array('result_user_id'=>$searchData['contractorIds']),array('contractor_search_id'=>$id));
                        $searchContractors= $searchData['conData'];
                        $searchContractors= array_map(function($tag2) {
                            return array(
                            'id' => $tag2['ContractorID'],
                            'name' => $tag2['FirstName'].' '.$tag2['MiddleName'].' '.$tag2['LastName'],
                            'phno' => ($tag2['PrimaryMobileNo']=='')?'': $tag2['PrimaryMobileNo'],
                            );
                         }, $searchContractors);
                        $this->response(array('responsecode'=>200,'responsedetails'=>'success','leftsearchedcount'=>($remainSearch-1),'contractors'=>$searchContractors));
                     }
                    else{
                        $this->response(array("error"=>"Something went wrong. Please try again later."));
                    }
                 }
                else{
                    $this->response(array('responsecode'=>201,'responsedetails'=>'Search exceed for this week','leftsearchedcount'=>0));
                 }


        }

        /* Api for upload Image */
        function uploadprofilepicinmyprofile_post()
        {
              $this->validation("sessiontoken");
              $userid=$this->post('user_id');
              $image=$_FILES['image'];
              $uploads_dir = 'uploads/users/images';
              $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
              if(in_array($image['type'], $allowed_mime_type_arr)){
              $name= time().$image['name'];
              if(move_uploaded_file($image['tmp_name'], $uploads_dir."/".$name)){
                  $data=array('user_image'=>$name);
                  $update_image=$this->api_model->edit('users','user_id',$userid,$data);
                    if($update_image=='true'){
                      $cond=array('user_id'=>$userid);
                      $user_details=$this->api_model->get_user_details_by_param($cond);
                      $user_pic=base_url().'uploads/users/images/'.$user_details[0]['user_image'];
                      $this->response(array('responsecode'=>200,'responsedetails'=>'success','profilepicurl'=>$user_pic));
                    }else{
                      $this->response(array("error"=>"Something went wrong. Please try again later."));
                    }
              }else{
                 $this->response(array("error"=>"Something went wrong. Please try again later."));
              }
              }else{
                 $this->response(array("error"=>"Something went wrong. Please try again later."));
              }

        }

        /* Api to create OTP */
        function createOTP_post()
        {
           //$this->validation("sessiontoken");
           $this->config->load('sms');
           $this->load->library('SmsLib');
           $otpCode = rand(100000, 999999);
           $userid=$this->post('user_id');
           $To=$this->post('mobile');
           $mob_cond=array('phone'=>$To);
           $mobile_email=$this->api_model->check_user($mob_cond);
            if($mobile_email=='TRUE'){
                $this->response(array("responsecode"=> 201,"responsedetails"=>'Mobile number already exist.Try with diffrent number'));
            }
           $text = 'Your One Time Password for registering on the Fevicol Design Ideas website is '.$otpCode.'. Please use this password to confirm the registration';
           $param_row=array('url'=>$this->config->item('url'),'request_type'=>'POST','response_type'=>'xml');
           $param=array('feedid'=>$this->config->item('feedid'),'username'=>$this->config->item('username'),'password'=>$this->config->item('password'),'To'=>$To,'Text'=>$text);
           $param=serialize($param);
           $response = $this->smslib->sendOTP($param_row,$param);
           if($response)
           {
               if(is_array($response) && (array_key_exists('REQUEST-ERROR', $response)|| array_key_exists('RESPONSE-ERROR', $response)))
               {
                   $this->response(array('error'=>'Something went wrong. Please try again later'));
               }
               else
               {
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','OTP'=>$otpCode));
               }
           }
           else
           {
              $this->response(array('error'=>'Something went wrong. Please try again later'));
           }
        }

        /* Api to filter designer type OTP */
        function filterdesignertype_post()
        {
          $table='users as u';
          $cond=array('u.user_type_id'=>3,'u.designation !='=>'');
          $data=array('u.designation as typename');
          $response=$this->api_model->get_data($table,$cond,$data,'',$distinct='true');
          $this->response(array('responsecode'=>200,'responsedetails'=>'success','designerType'=>$response));
        }

         /* Api to filter designer location type */
        function filterdesignerlocation_post()
        {
            $table='users';
            $cond=array('user_type_id'=>3,'area !='=>'');
            $data=array('area as locationname');
            $response=$this->api_model->get_location($table,$cond,$data);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','location'=>$response));
        }

          /* Api to get market place */
        function marketplacelist_post()
        {
            $this->validation("sessiontoken");
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            if($this->post('filterbytypeofproperty')){$param['in_condition']['ps.property_type_id'] = explode(',',$this->post('filterbytypeofproperty'));}
            if($this->post('filterbylocation')){$locations = $this->post('filterbylocation');}else{$locations='';}
            if($this->post('filterbytypeofwork')){$param['in_condition']['ps.type_id'] = explode(',', $this->post('filterbytypeofwork'));}
            if($this->post('filterbybudget')){$param['in_condition']['ps.budget_id'] = explode(',', $this->post('filterbybudget'));}
            if($this->post('filterbyprojectstartdate')){$project_date_arr= explode(',', $this->post('filterbyprojectstartdate'));}else{ $project_date_arr = array();}
            $param = array(
              'orderby_conditions'=>'ps.created Desc',
              'conditions'=>  array('ps.status'=>1),
            );
            $param['offset'] = $pagination['offset'];
            $param['limit'] = $pagination['limit'];

             $i = 0;


            if(!empty($project_start_arr)){

                 foreach($project_start_arr as $key=>$val){

                     $tmp_arr = array();
                     $tmp_arr = explode('-', $val);

                     $project_date_arr[$i]['start'] = date('Y-m-d', strtotime('-'.$tmp_arr[0].' days'));
                     $project_date_arr[$i]['end']   = date('Y-m-d', strtotime('+'.$tmp_arr[1].' days'));

                     $i++;
                  }
             }
            $allRecords = $this->project_posts->find_project_posts($param, $project_date_arr, $locations);
            unset($param['offset']);
            unset($param['limit']);
            $param['return_type'] = 'count';
            $totalcount = $this->project_posts->find_project_posts($param, $project_date_arr, $locations);
            if(count($allRecords)>0){
            foreach($allRecords as $key=>$value){
                $image=$this->project_posts->getProjectImage(1, $value['post_id']);
                if($value['furniture_type_id']!='' || $value['furniture_type_id']!=0){
                $cond=array('furniture_type_id'=>$value['furniture_type_id'],'lang_id'=>$lang_id);
                $data=array('furniture_type');
                $table='furniture_type_contents';
                $furniture_type=$this->api_model->get_data($table,$cond,$data);
                $allRecords[$key]['furniture_type']=$furniture_type[0]['furniture_type'];
                }else{
                $allRecords[$key]['furniture_type']='';
                }
                if($value['room_area_type_id']!='' || $value['room_area_type_id']!=0){
                $cond1=array('room_area_type_id'=>$value['room_area_type_id'],'lang_id'=>$lang_id);
                $data1=array('room_area_type');
                $table1='room_area_type_contents';
                $room_area_type=$this->api_model->get_data($table1,$cond1,$data1);
                $allRecords[$key]['room_area_type']=$room_area_type[0]['room_area_type'];
                }else{
                $allRecords[$key]['room_area_type']='';
                }
                if(postProjectAppliedstatus($value['post_id'],$this->post('user_id'))){
                    $allRecords[$key]['apply_status']='1';
                }else{
                    $allRecords[$key]['apply_status']='0';
                }
                $allRecords[$key]['image']=(isset($image[0])? base_url().'uploads/project_posts/thumb/'.$image[0]['post_image']:base_url().'uploads/portfolio/images/dummy.jpg');
            }

            $allRecords= array_map(function($tag2) {
                          return array(
                          'marketplace_id' => $tag2['post_id'],
                          'project_title' => ucwords(trim($tag2['site_address'])).', '.ucwords(trim($tag2['city_name'])).' - '.ucwords(trim($tag2['pincode'])).', '.ucwords(trim($tag2['state_name'])),
                          'image' => $tag2['image'],
                          'user_name' => $tag2['name'],
                          'location'=>$tag2['site_address'].' '.$tag2['city_name'],
                          'property_category'=>'',
                          'type_of_property_id'=>$tag2['property_type_id'],
                          'type_of_property'=>($tag2['property_type_name']==null)?'':$tag2['property_type_name'],
                          'scope_of_work'=> $tag2['entire_area']." ".$tag2['room_area_type']." ".$tag2['furniture_type'],
                          'type_of_work_id'=>$tag2['type_id'],
                          'type_of_work'=>($tag2['type_work']==null)?'':$tag2['type_work'],
                          'budget'=>($tag2['budget']==null)?'':$tag2['budget'],
                          'project_start_date'=>((floor((strtotime($tag2['project_start_dt'])-time())/(60*60*24))>0)?floor((strtotime($tag2['project_start_dt'])-time())/(60*60*24)):'NA'),
                          'no_of_bids_received'=>$tag2['bid_count'],
                          'no_of_shares'=>'',
                          'apply_status'=>$tag2['apply_status']
                        );
                       }, $allRecords);
            }else{
            $allRecords=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcount),'marketplacelist'=>$allRecords));
        }

        function marketplacedetails_post()
        {
            $projectId=$this->post('marketplace_id');
            $allRecords = $this->project_posts->get_single_post($projectId);
            if(empty($allRecords)){$this->response(array('error'=>'Something went wrong. Please try again later'));}
            $cn = 0;
            foreach( $allRecords as $key1=>$val1){
                $allRecords[$cn]['interior_theme_style_id']  = $this->project_posts->interior_theme_style($val1['interior_theme_style_id']);
                $allRecords[$cn]['furniture_theme_style_id'] = $this->project_posts->furniture_theme_style($val1['furniture_theme_style_id']);
                $cn++;
            }
//                echo "<pre>";
//                print_r($allRecords);
//                die;
            $tag2=$allRecords[0];
            $get_image=$this->project_posts->getProjectImage(1, $tag2['post_id']);
            $image=(isset($get_image[0])? base_url().'uploads/project_posts/'.$get_image[0]['post_image']:base_url().'uploads/portfolio/images/dummy.jpg');
            $numberofprojects = $this->api_model->count_all('project_posts','');
            $user_name = $tag2['name'];
            $location=$tag2['site_address'].' '.$tag2['city_name'];
            $project_title=ucwords(trim($tag2['site_address'])).', '.ucwords(trim($tag2['city_name'])).' - '.ucwords(trim($tag2['pincode'])).', '.ucwords(trim($tag2['state_name']));
            $property_category='';
            $type_of_property=($tag2['property_type_name']==null)?'':$tag2['property_type_name'];
            $scope_of_work= $tag2['entire_area']." ".$tag2['room_area_type']." ".$tag2['furniture_type'];
            $type_of_work=($tag2['property_type_name']==null)?'':$tag2['property_type_name'];
            $budget=($tag2['budget']==null)?'':$tag2['budget'];
            $project_start_date=((floor((strtotime($tag2['project_start_dt'])-time())/(60*60*24))>0)?floor((strtotime($tag2['project_start_dt'])-time())/(60*60*24)):'NA');
            $no_of_bids_received=$tag2['bid_count'];
            $marital_status=$tag2['marital_status'];
            $members_in_the_house=$tag2['family_members'];
            $property_owner_preferable_interior_img=(isset($tag2['interior_theme_style_id'][0]['image'])? upload_url().'interior_theme_styles/images/thumb/'.$tag2['interior_theme_style_id'][0]['image']:'');
            $property_owner_preferable_exterior_img='';
            $property_owner_furniture_preference=(isset($tag2['furniture_theme_style_id'][0]['furniture_theme_style'])?$tag2['furniture_theme_style_id'][0]['furniture_theme_style']:'');
            $property_owner_furniture_preference_preferred_colour_theme='';
            $property_owners_preferred_colour_for_interior=$tag2['interior_color'];
            $sample_picture= $this->project_posts->getProjectImage(0, $tag2['post_id']);
            $room_size= $this->project_posts->room_size($tag2['room_size_id']);
            if(isset($sample_picture[0])){
            $sample_picture= array_map(function($tag) {
                return array(
                'current_picture_id' => $tag['image_id'],
                'current_picture_url' => base_url().'uploads/project_posts/thumb/thumb_'.$tag['post_image']
                );
             }, $sample_picture);
            }else{
               $sample_picture=array();
            }

            $lat = 0;
            $lon = 0;

            $lat = $tag2['latitude'];
            $lon = $tag2['longitude'];
            $otherproject=$this->project_posts->otherNearByProject($lat, $lon);
            if(isset($otherproject[0])){
            $otherproject= array_map(function($tag) {
                return array(
                'project_title' => ucwords(trim($tag['site_address'])).', '.ucwords(trim($tag['city_name'])).' - '.ucwords(trim($tag['pincode'])).', '.ucwords(trim($tag['state_name'])),
                'no_of_bids_received' => $tag['bid_count'],
                'no_of_shares' => '',
                );
             }, $otherproject);
            }else{
               $otherproject=array();
            }
            $similarproject=$this->project_posts->getSimilarTypeProject($tag2['property_type_id']);
            if(isset($similarproject[0])){
            $similarproject= array_map(function($tag1) {
                    return array(
                    'project_title' =>  ucwords(trim($tag1['site_address'])).', '.ucwords(trim($tag1['city_name'])).' - '.ucwords(trim($tag1['pincode'])).', '.ucwords(trim($tag1['state_name'])),
                    'no_of_bids_received' => $tag1['bid_count'],
                    'no_of_shares' => '',
                    );
                 }, $similarproject);
            }else{
              $similarproject=array();
            }

            $this->response(array('responsecode'=>200,'responsedetails'=>'success',
                                'number_of_projects'=>$numberofprojects
                                ,'project_title'=>$project_title
                                ,'image'=>$image
                                ,'user_name'=>$user_name,'location'=>$location,'no_of_bids_received'=>$no_of_bids_received
                                ,'no_of_shares'=>''
                                ,'apply_status'=>$this->status_check($projectId,"project_posts_apply","post_id")
                                ,'property_category'=>''
                                ,'type_of_property'=>$type_of_property,'scope_of_work'=> $scope_of_work
                                ,'type_of_work'=>$type_of_work,'budget'=>$budget
                                ,'project_start_date'=>$project_start_date,'marital_status'=>$marital_status
                                ,'room_size'=>$room_size
                                ,'current_picture_of_area'=>$sample_picture
                                ,'members_in_the_house'=>$members_in_the_house,'property_owner_preferable_interior_img'=>$property_owner_preferable_interior_img
                                ,'property_owner_preferable_exterior_img'=>'','property_owner_furniture_preference'=>$property_owner_furniture_preference
                                ,'property_owner_furniture_preference_preferred_colour_theme'=>'','property_owners_preferred_colour_for_interior'=>$property_owners_preferred_colour_for_interior
                                ,'others_projects_near_the_location'=>$otherproject,'similar_types_of_projects'=>$similarproject));
        }

         /* Api for Interview  listing */
        function commondetails_post()
        {
            if($this->post('user_id')!=''){
                $this->validation("sessiontoken");
                }
            $lang_id=$this->get_language_id();
            $param['lang_id']=$lang_id;
            //array('conditions'=>array('status'=>1),'in_condition'=>array('section'=>array(1,3)))
            $param['conditions']=array('status'=>1);
            $param['in_condition']=array('section'=>array(1,3));
            switch ($this->post('api_differentiate_tag')) {
                case 'interview':
                        $interviewlist=$this->interview_Model->get_rows($param);
                        if(count($interviewlist)>0){
                        $interviewlist= array_map(function($tag2) {
                              return array(
                              'id' => $tag2['interview_id'],
                              'name' => $tag2['title'],
                              'publishedon' =>displayFormatTime($tag2['published_on']),
                              'category' => $tag2['type_name'],
                              'postedbyimage'=>base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                              'postedbyname'=>$tag2['firstname'].' '.$tag2['lastname'],
                              'description'=>$tag2['description'],
                              'short_description'=>$tag2['short_description'],
                              'imageurl'=>  base_url().'uploads/interview/images/thumb/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                              'likecount'=>$this->api_model->count_all('interview_likes',array('interview_id'=> $tag2['interview_id'])),
                              'sharecount'=>'0',
                              'likestatus'=>$this->status_check($tag2['interview_id'],"interview_likes","interview_id")
                              );
                           }, $interviewlist);
                         }else{
                         $interviewlist=array();
                         }
                         $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'interview'=>$interviewlist));
                         break;
                default:
                     $this->response(array('error'=>'Something went wrong. Please try again later'));
                 break;
            }
          }

         /* Api to get designer of the week */
        function designeroftheweek_post()
        {
            if($this->post('user_id')!=''){
                $this->validation("sessiontoken");
                }
            $lang_id=$this->get_language_id();
            $param['lang_id']=$lang_id;
            $param['conditions']=array('is_displayed'=>1);
            $designer_of_the_week=$this->designer_Week->get_rows($param);
            $tag2=$designer_of_the_week;
            $designer_week_id=$tag2[0]['designer_week_id'];
            $designer_id= $tag2[0]['user_id'];
            $designer_name= $tag2[0]['firstname'].' '.$tag2[0]['lastname'];
            $imageurl=  base_url().'uploads/users/images/'.(($tag2[0]['user_image']=='')?'dummy.jpg':$tag2[0]['user_image']);
            $description=$tag2[0]['short_description'];
            $published_date=TimeIntervalHelper($tag2[0]['published_on']);
            $category=$tag2[0]['type_name'];
            $like_count=$this->api_model->count_all('designer_week_likes',array('designer_week_id'=> $tag2[0]['designer_week_id']));
            $share_count='';
            $likestatus=$this->status_check($tag2[0]['designer_week_id'],"designer_week_likes","designer_week_id");
            $images=base_url().'uploads/designer_week/images/thumb/'.(($tag2[0]['image']=='')?'dummy.jpg':$tag2[0]['image']);
            $designer_desc=RemoveHTMLtag($tag2[0]['description']);
            $param1['lang_id']=$lang_id;
            $param1['conditions']=array('status'=>1,'designer_week_id !='=>$tag2[0]['designer_week_id']);
            $param1['wherein_conditions']=array('year'=>date('Y'),'month'=>date('m'));
            $param1['in_condition']=array('section'=>array(1,3));
            $recent_designer_of_the_week=$this->designer_Week->get_rows($param1);
            $people_who_also_liked=$this->designer_Week->get_likes(array('conditions'=>array('designer_week_id'=>$tag2[0]['designer_week_id'])));
            if(isset($people_who_also_liked[0])){
                $people_who_also_liked= array_map(function($tag1) {
                       return array(
                       'designer_name' => $tag1['title'],
                       'date' =>displayFormatTime($tag1['published_on']),
                       'image_url'=>base_url().'uploads/designer_week/images/thumb/'.(($tag1['image']=='')?'dummy.jpg':$tag1['image'])
                       );
                    }, $people_who_also_liked);
                  }else{
                  $people_who_also_liked=array();
                  }

            if(isset($recent_designer_of_the_week[0])){
                $recent_img_url=base_url().'uploads/designer_week/images/thumb/'.(($recent_designer_of_the_week[0]['image']=='')?'dummy.jpg':$recent_designer_of_the_week[0]['image']);
                $recent_designer_name=$recent_designer_of_the_week[0]['firstname'].' '.$recent_designer_of_the_week[0]['lastname'];
                $recent_publish_date=displayFormatTime($recent_designer_of_the_week[0]['published_on']);
                $recent_designer_profile_pic=base_url().'uploads/users/images/'.(($recent_designer_of_the_week[0]['user_image']=='')?'dummy.jpg':$recent_designer_of_the_week[0]['user_image']);
                $recent_designer_desc=  RemoveHTMLtag($recent_designer_of_the_week[0]['description']);
                }else{
                 $recent_img_url='';
                 $recent_designer_name='';
                 $recent_publish_date='';
                 $recent_designer_profile_pic='';
                 $recent_designer_desc='';
                }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'designer_week_id'=>$designer_week_id,'designer_id'=>$designer_id,'designer_name'=>$designer_name,'publish_date'=>$published_date,'category'=>$category,'like_count'=>$like_count,'share_count'=>$share_count,'likestatus'=>$likestatus,'images'=>$images,'designer_desc'=>$designer_desc,'designer_profile_pic'=>$imageurl,'recent_img_url'=>$recent_img_url,'recent_designer_name'=>$recent_designer_name,'recent_designer_profile_pic'=>$recent_designer_profile_pic,'recent_designer_desc'=>$recent_designer_desc,'recent_publish_date'=>$recent_publish_date,'people_who_also_liked'=>$people_who_also_liked));
        }

         /* Api to get designer of the week */
        function tasteofdesignlist_post()
        {
            $lang_id=$this->get_language_id();
            $table='furniture_theme_style_contents';
            $table1='colour_contents';
            $table2='colour_theme_contents';
            $table3='interior_theme_style_contents';
            $table4='room_size_contents';
            $cond=array('lang_id'=>$lang_id);
            $data=array('furniture_theme_style_id as furniture_style_id,furniture_theme_style as furniture_style_type');
            $data1=array('colour_id as favourite_colour_id,colour as favourite_colour');
            $data2=array('colour_theme_id as colour_theme_id,colour_theme as colour_theme_type');
            $data3=array('interior_theme_style_id as interiors_look_id,interior_theme_style as interiors_look_type');
            $data4=array('room_size_id as room_size_id,room_size as room_size');
            $furniture_theme=$this->api_model->get_data($table,$cond,$data);
            $color=$this->api_model->get_data($table1,$cond,$data1);
            $color_theme=$this->api_model->get_data($table2,$cond,$data2);
            $interior_theme=$this->api_model->get_data($table3,$cond,$data3);
            $room_size=$this->api_model->get_data($table4,$cond,$data4);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','interiors_look_list'=>$interior_theme,'furniture_style_list'=>$furniture_theme,'colour_theme_list'=>$color_theme,'favourite_colour_list'=>$color,'room_size_list'=>$room_size));
        }

        /* Api to Post Mentorship  */
        function rateinteriordesigner_post()
        {
            $this->validation("sessiontoken");
            $designer_id=$this->post('designer_id');
            $user_id=$this->post('user_id');
            $rating=$this->post('rate');
            if($rating>5){
               $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
            $rating_exists = $this->user->isAlreadyRating($user_id,$designer_id);
            if ($rating_exists) {
                $save_data = array('user_id' => $designer_id, 'rating_user_id' =>$user_id, 'rating' => $rating, 'modified' => date('Y-m-d H:i:s'));
                $return = $this->user->give_user_rating($save_data, $rating_exists);
            } else {
                $save_data = array('user_id' => $designer_id, 'rating_user_id' => $user_id, 'rating' => $rating, 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'), 'status' => 1);
                $return = $this->user->give_user_rating($save_data);
            }
            if($return){
                $this->response(array('responsecode'=>200,'responsedetails'=>'success','rate'=>ceil($return)));
            }else{
                $this->response(array('error'=>'Something went wrong. Please try again later'));
            }

        }

           /* Api for Article listing */
        function articledetails_post()
        {
          if($this->post('user_id')!=''){
              $this->validation("sessiontoken");
              }
          $lang_id=$this->get_language_id();
          $param['lang_id']=$lang_id;
          switch ($this->post('api_differentiate_tag')) {
              case 'article':
                  $param['article_id']=$this->post('id');
                  $articledetails=$this->article->get_rows($param);
                  $reletedarticle=$this->article->get_rows(array('conditions'=>array('type_id'=>$articledetails['article_details']['type_id'],'article_id !='=>$this->post('id'))));
                  $likedarticles=$this->article->get_likes(array('conditions'=>array('article_id'=>$this->post('id'))));
                   if(isset($articledetails['article_details']['article_id'])){
                       $tag2=$articledetails['article_details'];
                   $details= array(
                        'id'=>$tag2['article_id'],
                        'posteddate'=>displayFormatTime($tag2['published_on']),
                        'likecount'=>$this->api_model->count_all('article_likes',array('article_id'=> $tag2['article_id'])),
                        'sharecount'=>'',
                        'likestatus'=>$this->status_check($tag2['article_id'],"article_likes","article_id"),
                        'postedby'=>$articledetails['article_details']['firstname']." ".$tag2['lastname'],
                        'postedbyimage' => base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                        'aboutarticleimage' => preg_replace("/<img[^>]+\>/i", "",$tag2['description']),
                        'articleimage'=> base_url().'uploads/article/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                        'articletitle'=>$tag2['title'],
                        'aboutarticlevideo'=>'',
                        'articlevideo'=>''
                        );
                   }else{
                   $details=array();
                   }
                   if(isset($reletedarticle[0])){
                      $reletedarticle= array_map(function($tag3) {
                            return array(
                            'articleid' => $tag3['article_id'],
                            'imageurl' =>base_url().'uploads/article/images/'.(($tag3['image']=='')?'dummy.jpg':$tag3['image']),
                            'imagedescription' => $tag3['short_description'],
                            'date' => formatIntervalHelper($tag3['published_on'])
                            );
                         }, $reletedarticle);
                   }else{
                       $reletedarticle=array();
                       }
                   if(isset($likedarticles[0])){
                      $likedarticles= array_map(function($tag4) {
                            return array(
                            'articleid' => $tag4['article_id'],
                            'imageurl' => base_url().'uploads/article/images/'.(($tag4['image']=='')?'dummy.jpg':$tag4['image']),
                            'imagedescription' => $tag4['short_description'],
                            'date' => formatIntervalHelper($tag4['published_on'])
                            );
                         }, $likedarticles);
                   }else{
                       $likedarticles=array();
                       }
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'articledetails'=>$details,'relatedarticles'=>$reletedarticle,'alsoliked'=>$likedarticles));
                   break;
              case 'designtrend':
                  $id=$this->post('id');
                  $design_trend = $this->design_trend->get_rows(array('design_trend_id'=>$id,'conditions'=>array('status'=>1),'comment_conditions'=>array('status'=>1),'in_condition'=>array('section'=>array(1,3))));
                  $users_like_this_design_trend=$this->design_trend->get_likes(array('conditions'=>array('design_trend_id'=>$id)));
                  $type_id=$design_trend['design_trend_details']['type_id'];
                  $related_design_trends=$this->design_trend->get_rows(array('conditions'=>array('type_id'=>$type_id,'design_trend_id !='=>$id,'status'=>1),'in_condition'=>array('section'=>array(1,3))));
                  if(isset($design_trend['design_trend_details']['design_trend_id'])){
                   $tag2=$design_trend['design_trend_details'];
                   $details= array(
                        'id'=>$tag2['design_trend_id'],
                        'posteddate'=>displayFormatTime($tag2['published_on']),
                        'likecount'=>$this->api_model->count_all('design_trend_likes',array('design_trend_id'=> $tag2['design_trend_id'])),
                        'sharecount'=>'',
                        'likestatus'=>$this->status_check($tag2['design_trend_id'],"design_trend_likes","design_trend_id"),
                        'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                        'postedbyimage' => base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                        'aboutarticleimage' => preg_replace("/<img[^>]+\>/i", "",$tag2['description']),
                        'articleimage'=> base_url().'uploads/design_trend/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                        'articletitle'=>$tag2['title'],
                        'aboutarticlevideo'=>'',
                        'articledvideo'=>''
                        );
                   }else{
                   $details=array();
                   }
                   if(isset($related_design_trends[0])){
                      $related_design_trends= array_map(function($tag3) {
                            return array(
                            'articleid' => $tag3['design_trend_id'],
                            'imageurl' =>base_url().'uploads/design_trend/images/'.(($tag3['image']=='')?'dummy.jpg':$tag3['image']),
                            'imagedescription' => $tag3['short_description'],
                            'date' => formatIntervalHelper($tag3['published_on'])
                            );
                         }, $related_design_trends);
                   }else{
                       $related_design_trends=array();
                       }
                   if(isset($users_like_this_design_trend[0])){
                      $users_like_this_design_trend= array_map(function($tag4) {
                            return array(
                            'articleid' => $tag4['design_trend_id'],
                            'imageurl' => base_url().'uploads/design_trend/images/'.(($tag4['image']=='')?'dummy.jpg':$tag4['image']),
                            'imagedescription' => $tag4['title'],
                            'date' => formatIntervalHelper($tag4['published_on'])
                            );
                         }, $users_like_this_design_trend);
                   }else{
                       $users_like_this_design_trend=array();
                       }
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'articledetails'=>$details,'relatedarticles'=>$related_design_trends,'alsoliked'=>$users_like_this_design_trend));
                   break;
              case 'doit':
                  $id=$this->post('id');
                  $doit = $this->doit->get_rows(array('doit_id'=>$id,'conditions'=>array('status'=>1),'comment_conditions'=>array('status'=>1),'in_condition'=>array('section'=>array(1,3))));
                  $users_like_this_doit=$this->doit->get_likes(array('conditions'=>array('doit_id'=>$id)));
                  $type_id=$doit['doit_details']['type_id'];
                  $related_doits =$this->doit->get_rows(array('conditions'=>array('type_id'=>$type_id,'status'=>1,'doit_id !='=>$id),'in_condition'=>array('section'=>array(1,3))));
                  if(isset($doit['doit_details']['doit_id'])){
                       $tag2=$doit['doit_details'];
                   $details= array(
                        'id'=>$tag2['doit_id'],
                        'posteddate'=>displayFormatTime($tag2['published_on']),
                        'likecount'=>$this->api_model->count_all('do_it_likes',array('doit_id'=> $tag2['doit_id'])),
                        'sharecount'=>'',
                        'likestatus'=>$this->status_check($tag2['published_on'],"do_it_likes","doit_id"),
                        'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                        'postedbyimage' => base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                        'aboutarticleimage' => preg_replace("/<img[^>]+\>/i", "",$tag2['description']),
                        'articleimage'=> base_url().'uploads/doit/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                        'articletitle'=>$tag2['title'],
                        'aboutarticlevideo'=>'',
                        'articlevideo'=>''
                        );
                   }else{
                   $details=array();
                   }
                   if(isset($related_doits[0])){
                      $related_doits= array_map(function($tag3) {
                            return array(
                            'articleid' => $tag3['doit_id'],
                            'imageurl' =>base_url().'uploads/doit/images/'.(($tag3['image']=='')?'dummy.jpg':$tag3['image']),
                            'imagedescription' => $tag3['short_description'],
                            'date' => formatIntervalHelper($tag3['published_on'])
                            );
                         }, $related_doits);
                   }else{
                       $related_doits=array();
                       }
                   if(isset($users_like_this_doit[0])){
                      $users_like_this_doit= array_map(function($tag4) {
                            return array(
                            'articleid' => $tag4['doit_id'],
                            'imageurl' => base_url().'uploads/doit/images/'.(($tag4['image']=='')?'dummy.jpg':$tag4['image']),
                            'imagedescription' => $tag4['title'],
                            'date' => formatIntervalHelper($tag4['published_on'])
                            );
                         }, $users_like_this_doit);
                   }else{
                       $users_like_this_doit=array();
                       }
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'articledetails'=>$details,'relatedarticles'=>$related_doits,'alsoliked'=>$users_like_this_doit));
                   break;
               default:
                   $this->response(array('error'=>'Something went wrong. Please try again later'));
               break;

           }


        }

        /* Api to Get Article Recent Comments */
        function getrecentcommentsarticle_post()
        {
             $pagination=$this->get_offset_size();
             if($this->post('article_id')!=''){
             $param=array('conditions'=>array('article_id'=>$this->post('article_id')),'start'=>$pagination['offset'],'limit'=>$pagination['limit']);
             $param1=array('article_id'=> $this->post('article_id'));
             }else{
             $param=array('start'=>$pagination['offset'],'limit'=>$pagination['limit']);
             $param1='';
             }
             $commentlist=$this->article->get_comment_rows($param);
             $totalcomments=$this->api_model->count_all('article_comments',$param1);
             if($totalcomments>0){
                 $commentlist= array_map(function($tag2) {
                  return array(
                  'commentid' => $tag2['comment_id'],
                  'comments' => $tag2['comment'],
                  'userid' => $tag2['user_id'],
                  'username'=>$tag2['firstname'].' '.$tag2['lastname'],
                  'userprofilepic'=>base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                  'date'=>formatIntervalHelper($tag2['created'])
                  );
                }, $commentlist);
             }else{
              $commentlist=array();
             }
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcomments),'comments'=>$commentlist));
        }

        /* Api to get common comments for diffrent tags */
        function getcommonrecentcomments_post()
        {
             $pagination=$this->get_offset_size();
             switch ($this->post('api_differentiate_tag')) {
                 case 'article':
                     $table='article_comments as a';
                     $cond=array('a.article_id'=>$this->post('id'));
                     $data=array('a.comment_id', 'a.comment as comments', 'a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'product':
                     $table='interiopediaproduct_comments as a';
                     $cond=array('a.interproduct_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'designtrend':
                     $table='design_trend_comments as a';
                     $cond=array('a.design_trend_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'diy':
                     $table='do_it_comments as a';
                     $cond=array('a.doit_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'dow':
                     $table='designer_week_comments as a';
                     $cond=array('a.designer_week_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'interview':
                     $table='interview_comments as a';
                     $cond=array('a.interview_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'designer':
                     $table='designer_comments as a';
                     $cond=array('a.designer_id'=>$this->post('id'));
                     $data=array('a.id as comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                 case 'project':
                     $table='project_image_comments as a';
                     $cond=array('a.project_image_id'=>$this->post('id'));
                     $data=array('a.comment_id','a.comment as comments','a.created as created','u.user_id','u.lastname','u.firstname','u.user_image');
                     $join=array('users as u'=>'u.user_id=a.user_id');
                 break;
                default:
                     $this->response(array('error'=>'Something went wrong. Please try again later'));
                 break;
             }
             if($this->post('id')==''){
                 $cond='';
             }
             $gruupby='comment_id';
             $orderby='comment_id';
             $commentlist=$this->api_model->get_data($table,$cond,$data,$join,'',$pagination,$gruupby,$orderby);
            // $commentlist=$this->article->get_comment_rows($param);
             $totalcomments=$this->api_model->count_all($table,$cond);
             if($totalcomments>0){
                 $commentlist= array_map(function($tag2) {
                  return array(
                  'commentid' => $tag2['comment_id'],
                  'comments' => $tag2['comments'],
                  'userid' => $tag2['user_id'],
                  'username'=>$tag2['firstname'].' '.$tag2['lastname'],
                  'userprofilepic'=>base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                  'date'=>formatIntervalHelper($tag2['created'])
                  );
                }, $commentlist);
             }else{
              $commentlist=array();
             }
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcomments),'comments'=> $commentlist));
        }

        /* Api to like Article */
        function likearticle_post()
        {
            $this->validation("sessiontoken");
            $articleid=$this->post('article_id');
            $user_id=$this->post('user_id');
            $islike=$this->post('islike');
            if($islike =='1' || $islike =='0' ){
            $condiction=array('article_id'=>$articleid,'user_id'=>$user_id);
            $data=array('user_id'=>$user_id,'article_id'=>$articleid,'created'=> date("Y-m-d H:i:s"),'modified'=> date("Y-m-d H:i:s"));
            $like_dislike=$this->api_model->do_undo($condiction,'article_likes',$data);
            $total_like=$this->api_model->count_all('article_likes',array('article_id'=>$articleid));
            if($like_dislike==1){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>1));
            }else{
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>0));
            }
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api to Add Article Share Count */
        function addarticlesharecount_post()
        {
            $this->validation("sessiontoken");
            $articleid=$this->post('article_id');
            $user_id=$this->post('user_id');
            $data=array('user_id'=>$user_id,'article_id'=>$articleid,'created'=> date("Y-m-d H:i:s"));
            $this->api_model->post_data($data,'article_share');
            $total_share=$this->api_model->count_all('article_share',array('article_id'=>$articleid));
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','sharecount'=>$total_share));
        }

        /* Common Api for article, product, designtrend, diy listing */
        function commonlist_post()
        {
            if($this->post('user_id')!=''){
                $this->validation("sessiontoken");
                }
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            $param['offset']=$pagination['offset'];
            $param['limit']=$pagination['limit'];
            $param['lang_id']=$lang_id;
            $param['orderby_conditions']='a.published_on DESC';
            switch ($this->post('api_differentiate_tag')) {

                case 'article':
                    $articlelist=$this->article->find_articles($param);
                    $param1=array('return_type'=>'count','lang_id'=>$lang_id);
                    $totalcount=$this->article->find_articles($param1);
                     if($totalcount>0){
                    $articlelist= array_map(function($tag2) {
                          return array(
                          'id' => $tag2['article_id'],
                          'title' => $tag2['title'],
                          'description' => $tag2['short_description'],
                          'posteddate'=>TimeIntervalHelper($tag2['published_on']),
                          'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                          'postedbyimage'=> base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                          'long_description'=>$tag2['description'],
                          'image'=>  base_url().'uploads/article/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'large_image'=>  base_url().'uploads/article/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'likecount'=>$this->api_model->count_all('article_likes',array('article_id'=> $tag2['article_id'])),
                          'commentcount'=>$this->api_model->count_all('article_comments',array('article_id'=>$tag2['article_id'])),
                          'sharecount'=>'',
                          'likestatus'=>$this->status_check($tag2['article_id'],"article_likes","article_id")
                          );
                       }, $articlelist);
                     }else{
                     $articlelist=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'totalcount'=>number_format($totalcount),'article'=>$articlelist));
                break;

                case 'product':
                    $param1=array('return_type'=>'count','lang_id'=>$lang_id);
                    $interopedialist=$this->interiopediaproduct->find_interiopediaproducts($param);
                    $totalcount=$this->interiopediaproduct->find_interiopediaproducts($param1);
                    if(count($interopedialist)>0){
                    $interopedialist= array_map(function($tag2) {
                          return array(
                          'id' => $tag2['interproduct_id'],
                          'title' => $tag2['title'],
                          'description' => $tag2['short_description'],
                          'posteddate'=>TimeIntervalHelper($tag2['published_on']),
                          'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                          'postedbyimage'=> base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                          'long_description'=>$tag2['description'],
                          'image'=>  base_url().'uploads/interiopediaproduct/images/thumb/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'large_image'=>  base_url().'uploads/interiopediaproduct/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'likecount'=>$this->api_model->count_all('interiopediaproduct_likes',array('interproduct_id'=> $tag2['interproduct_id'])),
                          'commentcount'=>$this->api_model->count_all('interiopediaproduct_comments',array('interproduct_id'=>$tag2['interproduct_id'])),
                          'sharecount'=>'',
                          'likestatus'=>$this->status_check($tag2['interproduct_id'],"interiopediaproduct_likes","interproduct_id")
                          );
                       }, $interopedialist);
                     }else{
                     $interopedialist=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'totalcount'=>number_format($totalcount),'product'=>$interopedialist));

                break;

                case 'designtrend':
                    if($this->post('design_trend_category')=='1' || $this->post('design_trend_category')=='2'){
                    $param['conditions']=array('a.design_trend_category'=>$this->post('design_trend_category'));
                    $param1['conditions']=array('a.design_trend_category'=>$this->post('design_trend_category'));
                    }
                    $param1=array('lang_id'=>$lang_id,'return_type'=>'count');
                    $totalcount = $this->design_trend->find_design_trends($param1);
                    $design_trends = $this->design_trend->find_design_trends($param);

                    if(count($design_trends>0)){
                          $design_trends= array_map(function($tag2) {
                          return array(
                          'id' => $tag2['design_trend_id'],
                          'image' => base_url().'uploads/design_trend/images/thumb/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'title' => $tag2['title'],
                          'posteddate'=>TimeIntervalHelper($tag2['published_on']),
                          'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                          'postedbyimage'=> base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                          'long_description'=>$tag2['description'],
                          'large_image' => base_url().'uploads/design_trend/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'likecount'=>$this->api_model->count_all('design_trend_likes',array('design_trend_id'=>$tag2['design_trend_id'])),
                          'commentcount'=>$this->api_model->count_all('design_trend_comments',array('design_trend_id'=>$tag2['design_trend_id'])),
                          'sharecount'=>'',
                          'description'=>$tag2['short_description'],
                          'likestatus'=>$this->status_check($tag2['design_trend_id'],"design_trend_likes","design_trend_id")
                          );
                        },  $design_trends);
                     }else{
                       $design_trends=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'totalcount'=>number_format($totalcount),'designtrend'=> $design_trends));
                break;

                case 'diy':
                    $param1=array('lang_id'=>$lang_id,'return_type'=>'count');
                    $totalcount = $this->doit->find_doits($param1);
                    $doitlist = $this->doit->find_doits($param);
                    if(count($doitlist>0)){
                          $doitlist= array_map(function($tag2) {
                          return array(
                          'id' => $tag2['doit_id'],
                          'image' => base_url().'uploads/doit/images/thumb/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'title' => $tag2['title'],
                          'posteddate'=>TimeIntervalHelper($tag2['published_on']),
                          'postedby'=>$tag2['firstname']." ".$tag2['lastname'],
                          'postedbyimage'=> base_url().'uploads/users/images/'.(($tag2['user_image']=='')?'dummy.jpg':$tag2['user_image']),
                          'long_description'=>$tag2['description'],
                          'large_image' => base_url().'uploads/doit/images/'.(($tag2['image']=='')?'dummy.jpg':$tag2['image']),
                          'likecount'=>$this->api_model->count_all('do_it_likes',array('doit_id'=>$tag2['doit_id'])),
                          'commentcount'=>$this->api_model->count_all('do_it_comments',array('doit_id'=>$tag2['doit_id'])),
                          'sharecount'=>'',
                          'description'=>$tag2['short_description'],
                          'likestatus'=>$this->status_check($tag2['doit_id'],"do_it_likes","doit_id")
                          );
                        },  $doitlist);
                     }else{
                       $doitlist=array();
                     }
                     $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'totalcount'=>number_format($totalcount),'diy'=> $doitlist));
                break;
                default :
                 $this->response(array('error'=>'Something went wrong. Please try again later'));
                break;


            }

        }

        /* Api to Like Design Trend  */
       function likedesigntrend_post()
        {
            $this->validation("sessiontoken");
            $designtrend_id=$this->post('designtrend_id');
            $user_id=$this->post('user_id');
            $islike=$this->post('islike');
            if($islike =='1' || $islike =='0' ){
            $condiction=array('design_trend_id'=>$designtrend_id,'user_id'=>$user_id);
            $data=array('user_id'=>$user_id,'design_trend_id'=>$designtrend_id,'created'=> date("Y-m-d H:i:s"),'modified'=> date("Y-m-d H:i:s"));
            $like_dislike=$this->api_model->do_undo($condiction,'design_trend_likes',$data);
            $total_like=$this->api_model->count_all('design_trend_likes',array('design_trend_id'=>$designtrend_id));
            if($like_dislike==1){
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>1));
            }else{
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','likecount'=>$total_like,'likestatus'=>0));
            }
            }else{
             $this->response(array('error'=>'Something went wrong. Please try again later'));
            }
        }

        /* Api to Post Comment Design Trend  */
        function postcommentdesigntrend_post()
        {
            $this->validation("sessiontoken");
            $data=array('design_trend_id'=>$this->post('designtrend_id'),'comment'=>trim($this->post('comments')),'user_id'=>$this->post('user_id'),'user_type'=>$this->post('usertype'),'created'=> date("Y-m-d H:i:s"),'modified'=> date("Y-m-d H:i:s"));
            $this->api_model->post_data($data,'design_trend_comments');
            $total_comments=$this->api_model->count_all('design_trend_comments',array('design_trend_id'=>$this->post('designtrend_id')));
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','commentcount'=>number_format($total_comments)));
        }

//        /* Api to Add Design Trend Share Count   */
//        function adddesigntrendsharecount_post()
//        {
//            $this->validation("sessiontoken");
//            $designtrend_id=$this->post('designtrend_id');
//            $user_id=$this->post('user_id');
//            $data=array('user_id'=>$user_id,'design_trend_id'=>$designtrend_id,'created'=> date("Y-m-d H:i:s"));
//            $this->api_model->post_data($data,'design_trend_share');
//            $total_share=$this->api_model->count_all('design_trend_share',array('design_trend_id'=>$designtrend_id));
//            $this->response(array('responsecode'=>200,'responsedetails'=>'success','sharecount'=>number_format($total_share)));
//        }

        /* Api to Post a project */
        function postproject_post()
        {
            $this->validation("sessiontoken");
            $data=array('user_id'=>$this->post('user_id'),
                        'state_id'=>$this->post('state'),
                        'city_id'=>$this->post('city'),
                        'site_address'=>$this->post('site_address'),
                        'pincode'=>$this->post('pincode'),
                        'gender'=>$this->post('gender'),
                        'created'=>date('Y-m-d H:i:s'),
                        'modified'=>date('Y-m-d H:i:s'));
            if($this->post('interior_designer')=='all'){
             $data['assigned_to_all_designer']='1';
            }else{
             $data['assigned_to_all_designer']='0';
            }
            $post_id=$this->api_model->post_data($data,'project_posts');
            if($post_id ){
                if($this->post('interior_designer')!='all'){
                    $interior_designer_ids=  explode(',', $this->post('interior_designer'));
                    foreach($interior_designer_ids as $id){
                    $designer=array('post_id'=>$post_id,
                                    'designer_id'=>$id,
                                    'designer_set_no'=>1);
                    $this->api_model->post_data($designer,"project_post_designer_mapping");
                    }
                }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','post_id'=>$post_id));
            }else{
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }

         }

         /* Api to Post a project */
        function postprojectsecond_post()
        {
            $this->validation("sessiontoken");
            $scope_of_work=$this->post('scope_of_work');
            $data=array('property_type_id'=>$this->post('type_of_property_id'),
                        'type_id'=>$this->post('type_of_work_id'),
                        'budget_id'=>$this->post('budget_id'),
                        'modified'=>date('Y-m-d H:i:s'));
            if($scope_of_work!=''){
            $scope_of_work= json_decode($scope_of_work, TRUE);
            if(count($scope_of_work)>0){
            foreach($scope_of_work as $type1=>$work){
                if($type1=='entire_area'){
                    $data['entire_area']=$work;
                } elseif($type1=='room'){
                    $data['room_area_type_id']=$work;
                } elseif($type1=='furniture'){
                    $data['furniture_type_id']=$work;
                }
            }
            }
            }
            $result=$this->api_model->edit('project_posts','post_id',$this->post('post_id'),$data);
            if($result=='true'){
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','post_id'=>$this->post('post_id')));
            }else{
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            }

         /* Api to Post a project */
        function postprojectthird_post()
        {
            $this->validation("sessiontoken");
            $data=array('marital_status'=>$this->post('marital_status'),
                        'family_members'=>$this->post('no_of_members'),
                        'interior_theme_style_id'=>$this->post('interiors_look_id'),
                        'furniture_theme_style_id'=>$this->post('furniture_style_id'),
                        'colour_theme_id'=>$this->post('color_theme_id'),
                        'interior_color'=>$this->post('favourite_color_id'),
                        'room_size_id'=>$this->post('room_size_id'),
                        'modified'=>date('Y-m-d H:i:s'));
            $childrens=$this->post('children_details');
            if($childrens!=''){
            $childrens= json_decode($childrens, TRUE);
            if(isset($childrens[0])){
            foreach($childrens as $type=>$child){
                  if($type!=3){
                  $data['child'.($type+1).'_gender']=$child['gender'];
                  $data['child'.($type+1).'_age']=$child['age'];
                  }
            }
            }
            }
            $result=$this->api_model->edit('project_posts','post_id',$this->post('post_id'),$data);
            if($result=='true'){
            $image_code_64 = $this->post('sample_picture');
            if($image_code_64!=''){
            $image_name='proj.jpg';
            $image_name = rand() . time() . $image_name;
            $img = imagecreatefromstring(base64_decode($image_code_64));
            if($img != false)
            {
            imagejpeg($img, 'uploads/project_posts/'.$image_name);
            }
            $sample_image=array('post_id'=>$this->post('post_id'),
                            'post_image'=>$image_name,
                            'created'=>date('Y-m-d H:i:s'),
                            'modified'=>date('Y-m-d H:i:s'));
            $this->api_model->post_data($sample_image,'project_posts_image');
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
            $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
        }

         /* Api to post a project image */
        function addimagesinpostaproject_post()
        {
              $this->validation("sessiontoken");
              $userid=$this->post('user_id');
              $check_post_id=$this->api_model->count_all('project_posts',array('post_id'=>$this->post('post_id')));
              if($check_post_id==0){
                  $this->response(array("error"=>"Post id is incorrect. Please try again later."));
              }
              $sample_picture1=$_FILES['sample_picture1'];

              $uploads_dir = 'uploads/project_posts/';
              $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
              $return_id1='';
              if($sample_picture1!='' && $sample_picture1['name']!='' && in_array($sample_picture1['type'], $allowed_mime_type_arr)){
              $name1= time().$sample_picture1['name'];
              if(move_uploaded_file($sample_picture1['tmp_name'], $uploads_dir."/".$name1)){
                  $image_data1=array('post_id'=>$this->post('post_id'),
                            'post_image'=>$name1,
                            'created'=>date('Y-m-d H:i:s'),
                            'modified'=>date('Y-m-d H:i:s'));
                 $return_id1= $this->api_model->post_data($image_data1,'project_posts_image');
              }
              }

              if($return_id1==''){
                  $this->response(array("error"=>"Something went wrong. Please try again later."));
              }
              else{
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
              }

        }

        /* Api to Post Testimonials  */
        function posttestimonial_post()
        {
            $this->validation("sessiontoken");
            $lang_id=$this->get_language_id();
            $testimonial_data = array(
				'lang_id' =>$lang_id,
				'user_id' =>$this->post('user_id'),
				'testimonial' => $this->input->post('testimonials'),
				'status' => 0
			);
            $insert = $this->testimonial->insert($testimonial_data);
            if($insert){
                    $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
                }else{
                     $this->response(array("error"=>"Something went wrong. Please try again later."));
                }
        }

         /* Api to Post Testimonials  */
        function postnewthread_post()
        {
            $this->validation("sessiontoken");
            $lang_id=$this->get_language_id();
            $forum_data = array('forum_cat_id' => $this->post('category_id'),
				'user_id' => $this->post('user_id'),
				'user_type' =>1,
				'created' => date("Y-m-d H:i:s"),
                                'modified' => date("Y-m-d H:i:s")
				);

            $forum_id = $this->api_model->post_data($forum_data,'forums');
            $forum_cont_data = array('forum_id' => $forum_id,
                                     'lang_id' => $lang_id,
                                     'title' =>$this->post('title'),
                                     'description' => $this->post('descriptions')
                                    );
            $this->api_model->post_data($forum_cont_data,'forum_contents');
            forumActivityAdd($forum_id,'P',$this->post('user_id'));
            if($forum_id){
                    $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
                }else{
                     $this->response(array("error"=>"Something went wrong. Please try again later."));
                }
        }

        /* Api to get interior designer  list */
        function typeofpropertylist_post()
        {
            $lang_id=$this->get_language_id();
            $cond=array('lang_id'=>$lang_id);
            $data=array('property_type_id','property_type_content');
            $property_details=$this->api_model->get_data('property_type_contents',$cond,$data);
            if(count($property_details)>0){
            $property_details= array_map(function($tag) {
                return array(
                'type_of_property_id' => $tag['property_type_id'],
                'type_of_property_name' => $tag['property_type_content']
                );
             }, $property_details);
            }else{
            $property_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','typeofpropertylist'=>$property_details));
        }

        /* Api to get worklist   list */
        function typeofworklist_post()
        {
                $lang_id=$this->get_language_id();
                $work_type=$this->work_type->get_rows(array('conditions'=>array('status'=>1),'lang_id'=>$lang_id));
                if(isset($work_type[0])){
                $work_type= array_map(function($tag) {
                return array(
                'type_of_work_id' => $tag['type_id'],
                'type_of_work_name' => $tag['type_name']
                );
                }, $work_type);
                }else{
                   $work_type=array();
                }
                $this->response(array('responsecode'=>200,'responsedetails'=>'success','language'=>$this->post('language'),'typeofworklist'=>$work_type));
        }

         /* Api to get bodherlist   list */
        function scopeofworklist_post()
        {
            $lang_id=$this->get_language_id();
            $table='room_area_type_contents';
            $table1='furniture_type_contents';
            $table2='entire_areas';
            $cond=array('lang_id'=>$lang_id);
            $data=array('room_area_type_id as room_id,room_area_type as room_type');
            $data1=array('furniture_type_id as furniture_id,furniture_type');
            $data2=array('entire_area_id as entire_area_id,entire_area as entire_area_name');
            $room=$this->api_model->get_data($table,$cond,$data);
            $furniture=$this->api_model->get_data($table1,$cond,$data1);
            //$area=$this->api_model->get_data($table2,'',$data2);
            $area=array(array('entire_area_id'=>1,'entire_area_name'=>'0-500'),array('entire_area_id'=>2,'entire_area_name'=>'500-1000'),array('entire_area_id'=>3,'entire_area_name'=>'1000-1500'),array('entire_area_id'=>4,'entire_area_name'=>'1500-2000'));
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','entire_area_list'=>$area,'room_list'=>$room,'furniture_list'=>$furniture));
        }

        /* Api to get bodherlist   list */
        function budgetlist_post()
        {
            $lang_id=$this->get_language_id();
            $table='project_budget_contents';
            $cond=array('lang_id'=>$lang_id);
            $data=array('budget_id as budget_id,budget as budget_type');
            $response=$this->api_model->get_data($table,$cond,$data);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','budgetlist'=>$response));
        }

        /* Api to get bodherlist   list */
        function specificskilllist_post()
        {
            $lang_id=$this->get_language_id();
            $table='mentorship_skills as m';
            $data=array('m.skill_id ,mc.skill as skill_name');
            $join=array('mentorship_skill_contents as mc'=>'mc.mentorship_skill_id=m.skill_id');
            $params=array('mc.lang_id'=>$lang_id);
            $response=$this->api_model->get_data($table,$params,$data,$join);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','specificskilllist'=>$response));
        }

        /* Api to get all All Testimonials list  */
        function getalltestimonials_post()
        {
            $pagination=$this->get_offset_size();
            $lang_id=$this->get_language_id();
            $params['lang_id']=$lang_id;
            $totalcount=$this->testimonial->rows_count($params);
            $params['start']=$pagination['offset'];
            $params['limit']=$pagination['limit'];
            $alltestonomial=$this->testimonial->get_rows($params);
            $alltestonomial = array_map(function($tag) {
                return array(
                'testimonial_id' => $tag['testimonial_id'],
                'testimonial_text' => $tag['testimonial'],
                'user_profile_pic' => file_exists('uploads/users/images/'.$tag['user_image'])? base_url().'uploads/users/images/'.$tag['user_image']:base_url().'uploads/users/images/dummy.jpg',
                'user_name' => $tag['firstname'].' '.$tag['lastname'],
                'user_designation' => $tag['type_name']
            );
            }, $alltestonomial);
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcount),'testimonials'=>$alltestonomial));
        }

        /* Api to get all All Testimonials list  */
        function getallforums_post()
        {
            $pagination=$this->get_offset_size();
            $lang_id=$this->get_language_id();
            $params['start']=$pagination['offset'];
            $params['limit']=$pagination['limit'];
            $params['lang_id']=$lang_id;
            $allforum=$this->forum->get_rows($params);

            if(count($allforum)>0){
            foreach($allforum as $key=>$tag){
                    $cond=array('forum_id'=>$tag['forum_id']);
                    $table='forum_replies';
                    $data=array('reply');
                    $recent_comment=$this->api_model->get_data($table,$cond,$data);
                    $last_text=end($recent_comment);
                    if($tag['forum_cat_id']!=0){
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['name']=$tag['forum_cat_name'];
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['id']=$tag['forum_cat_id'];
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['data'][]= array(
                    'forum_cat_id' => $tag['forum_cat_id'],
                    'forum_id' => $tag['forum_id'],
                    'forum_title' =>urldecode(html_entity_decode(strip_tags($tag['title']))),
                    'forum_desc' => urldecode(html_entity_decode(strip_tags($tag['description']))),
                    'posted_by' => $tag['firstname'].' '.$tag['lastname'],
                    'like_count' => $this->api_model->count_all('forum_likes',array('forum_id'=>$tag['forum_id'])),
                    'comment_count' => $this->api_model->count_all('forum_replies',array('forum_id'=>$tag['forum_id'])),
                    'postedtimeduration'=> formatInterval($tag['created']),
                    'view_count' => $tag['view_num'],
                    'last_updated_on' => formatIntervalHelper($tag['modified']),
                    'last_updated_text' =>RemoveHTMLtag($last_text['reply'])
                    );
                    }
            }
            } else{
               $forumdetails=array();
            }

           $forumdetails = array_values($forumdetails);

            $this->response(array('responsecode'=>200,'responsedetails'=>'success','Alldetails'=>$forumdetails));
        }

        /* Api to get all All Forum topic category wise  */
        function getalltopicsforumcatewise_post()
        {
            $pagination=$this->get_offset_size();
            $lang_id=$this->get_language_id();
            $params['start']=$pagination['offset'];
            $params['limit']=$pagination['limit'];
            $params['lang_id']=$lang_id;
            $params['conditions']=array('forum_cat_id'=>$this->post('category_id'));
            $allforum=$this->forum->get_rows($params);
            if(isset($allforum[0]['forum_id'])){
            foreach($allforum as $key=>$tag){
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['name']=$tag['forum_cat_name'];
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['id']=$tag['forum_cat_id'];
                    $forumdetails[str_replace(' ', '', $tag['forum_cat_name'])]['data'][]= array(
                    'forum_id' => $tag['forum_id'],
                    'forum_title' =>urldecode(html_entity_decode(strip_tags($tag['title']))),
                    'forum_desc' => urldecode(html_entity_decode(strip_tags($tag['description']))),
                    'posted_by' => $tag['firstname'].' '.$tag['lastname'],
                    'like_count' => $this->api_model->count_all('forum_likes',array('forum_id'=>$tag['forum_id'])),
                    'comment_count' => $this->api_model->count_all('forum_replies',array('forum_id'=>$tag['forum_id'])),
                    'view_count' => $tag['view_num'],
                    'last_updated_on' => formatIntervalHelper($tag['modified']),
                    'last_updated_text' => ''
                    );
            }
            }else{
                    $forumdetails=array();
            }
            $forumdetails = array_values($forumdetails);
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','Alldetails'=>$forumdetails));
        }

        /* Api to get all All Testimonials list  */
        function mentormenteelist_post()
        {
            //$this->validation("sessiontoken");
            $pagination=$this->get_offset_size();
            $lang_id=$this->get_language_id();
            $start=$pagination['offset'];
            $limit=$pagination['limit'];
            $mentorship_type = $this->post('mentor_mentee_type');
            $displayMenType = ($mentorship_type == 1)?2:1;
            $designerName = $this->post('filterbyldesignername');
            $designerType = $this->post('filterbydesignertype');
           // $specSkill = $this->post('sortSkill');
            $mentorLocation = $this->input->post('filterbylocation');
           // $mentorLocation = $this->post('sort_city_id');
            $sortConditions = array('mentorship_type'=>$displayMenType,'m.status'=>1);

            if(!empty($designerName)){
                    $cArr = array('sortDesignerName'=>$designerName);
                    $sortConditions = array_merge($sortConditions, $cArr);
            }
            if(!empty($mentorLocation)){
                    $tArr = array('m.location'=>$mentorLocation);
                    $sortConditions = array_merge($sortConditions, $tArr);
            }
            if(!empty($designerType)){
                    $dtArr = array('u.user_type_id'=>$designerType);
                    $sortConditions = array_merge($sortConditions, $dtArr);
            }
//            if(!empty($specSkill) && $specSkill != 'Specific Skills'){
//                    $dtArr = array('m.skill_id'=>$specSkill);
//                    $sortConditions = array_merge($sortConditions, $dtArr);
//            }
            $totalRec = $this->mentorship->rows_count(array('expiryCheck'=>1,'conditions'=>$sortConditions));
            $mentorships = $this->mentorship->get_rows(array('expiryCheck'=>1,'conditions'=>$sortConditions,'start'=>$start,'limit'=>$limit));

            if(isset($mentorships[0])){
            $mentorships = array_map(function($tag) {
                return array(
                'mentor_mentee_id' => $tag['mentorship_id'],
                'designer_name' => $tag['firstname'].' '.$tag['lastname'],
                'designer_profile_image' => file_exists('uploads/users/images/'.$tag['user_image'])? base_url().'uploads/users/images/'.$tag['user_image']:base_url().'uploads/users/images/dummy.jpg',
                'designer_id' => $tag['user_id'],
                'no_of_projects' => $tag['projects_num'],
                'no_of_stars' => ($tag['user_rating']==null)?'':$tag['user_rating'],
                'details' => RemoveHTMLtag(($tag['about_company']=='')?$tag['sheeking_for']:$tag['about_company']),
                'skills' => $tag['skill'],
                'posted_on' => TimeIntervalHelper($tag['created']),
                'location' => $tag['type_name'],
                'duration' => $tag['duration'] ,
                'expiry_date' => TimeIntervalHelper($tag['expiry_date']),
                'apply_status'=>$this->status_check($tag['mentorship_id'],"mentorship_posts","mentorship_id")
            );
            }, $mentorships);
        }else{
            $mentorships=array();
        }
             $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'mentormenteelist'=>$mentorships));
        }

        /* Api to Post Mentorship  */
        function postdetailsformentorshipseeking_post()
        {
            $this->validation("sessiontoken");
            $postDate = date("Y-m-d H:i:s");
            $dateCreate = date_create($postDate);
            date_add($dateCreate,date_interval_create_from_date_string("90 days"));
            $expiry_date = date_format($dateCreate,"Y-m-d H:i:s");
//            if($this->post('api_differentiation_tag')!=1 || $this->post('api_differentiation_tag')!=2){
//              $this->response(array("error"=>"Something went wrong. Please try again later."));
//            }
            $mdata = array(
                    'user_id' => $this->post('user_id'),
                    'about_company' => $this->post('about_company'),
                    'sheeking_for' => $this->post('seeking_mentorship_for'),
                    'academic_background' => $this->post('academic_background'),
                    'mentorship_type' => $this->post('api_differentiation_tag'),
                    'skill_id' => $this->post('specific_skills'),
          //          'location' => $this->post('location'),
                    'duration' => $this->post('duration'),
                    'state_id' => $this->post('state_id'),
		    'city_id' => $this->post('city_id'),
                    'expiry_date' => $expiry_date,
                    'created' => $postDate
            );
            $insert = $this->mentorship->insert($mdata);
                if($insert){
                    $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
                }else{
                     $this->response(array("error"=>"Something went wrong. Please try again later."));
                }
        }

        /* Api to Post Mentorship  */
        function apply_post()
        {
            $this->validation("sessiontoken");
            if($this->post('api_differentiation_tag')=='Mentor'|| $this->post('api_differentiation_tag')=='Mentee'){
            $mdata = array('user_id' => $this->post('user_id'),
                           'mentorship_id' =>$this->post('id'));
            $insert = $this->mentorship->insertPost($mdata);
            if($insert){
                    emailToMentor($insert);
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success','apply_status'=>1));
            }else{
                   $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            }elseif($this->post('api_differentiation_tag')=='Marketplace'){

                if($this->post('id') && !postProjectApplied($this->post('id')))
                    {
                        $data = array(
                                'user_id' => $this->post('user_id'),
                                'post_id' => $this->post('id'),
                                'lead_acceptence' => 1,
                                'designer_set_no' => getDesignerSetNo($this->post('id')),
                                'accepted_on' => date("Y-m-d H:i:s")
                        );
                        $insert = $this->project_posts->insertPostApply($data);
                        if($insert){
                                emailToPostProject($insert);
                                $this->response(array('responsecode'=>200,'responsedetails'=>'success','apply_status'=>1));
                        }else{
                               $this->response(array("error"=>"Something went wrong. Please try again later."));
                        }
                    }

            }else{
                 $this->response(array("error"=>"Something went wrong. Please try again later."));
            }


        }

        /* Api to Post Mentorship  */
        function reject_post()
        {
            $this->validation("sessiontoken");
            $api_diff_tag=$this->post('api_differentiation_tag');
            if($api_diff_tag=='lead'){
                $cond=array('designer_id'=>$this->post('user_id'),'project_post_id'=>$this->post('id'));
                $update=$this->api_model->update_leads($cond);
            if($update=='true'){
                   $this->response(array('responsecode'=>200,'responsedetails'=>'success'));
            }else{
                   $this->response(array("error"=>"Something went wrong. Please try again later."));
            }
            }else{
                   $this->response(array("error"=>"Something went wrong. Please try again later."));
            }

        }

        /* Api to Get Inspired image list  */
        function getinspiredimagelist_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $pagination=$this->get_offset_size();
            $param['offset']=$pagination['offset'];
            $param['limit']=$pagination['limit'];
            $lang_id=$this->get_language_id();
            $designer = $this->input->post('filterbydesignername');
            $location = $this->input->post('filterbylocation');
            $sort_by = $this->input->post('sortby');

            if($this->post('type')==1){
                $param = array(
                        'lang_id'=>1,
                        'return_type'=>'count',
                        'conditions'=>  array(
                         'p.status'=>1)
                        );

                if($designer){$param['conditions']['p.user_id'] = $designer;}
                if($location){$param['like_conditions']['pc.location'] = $location;}
                if($sort_by){
			  switch ($sort_by)
                                {
				  case 'most_rated':
					  //$param['orderby_conditions'] = 'user_rating DESC';
					  break;
				  case 'most_viewed':
					  $param['orderby_conditions'] = 'pi.view_num DESC';
					  break;
				  case 'most_commented':
					  $param['orderby_conditions'] = 'commentCount DESC';
					  break;
				  case 'latest':
					  $param['orderby_conditions'] = 'p.created DESC';
					  break;
                                }
                            }
                $totalcount= $this->project->find_project_images($param);
                $totalcount=$totalcount['imageCnt'];
                $param['offset']=$pagination['offset'];
                $param['limit']=$pagination['limit'];
                $param['return_type'] = 'array';
                $alldata = $this->project->find_project_images($param);
                if(isset($alldata[0])){
                $alldata = array_map(function($tag) {
                return array(
                    'image_id' => $tag['project_image_id'],
                    'project_id' => $tag['project_id'],
                    'imageURL' => file_exists('uploads/portfolio/images/thumb/'.$tag['image'])? base_url().'uploads/portfolio/images/thumb/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                    'image_name' => $tag['image_title'],
                    'location' => $tag['city_name'].' '.$tag['state_name'],
                    'likecount' => $tag['imageLike'],
                    'viewcount' => $tag['view_num'],
                    'likestatus' => $this->status_check($tag['project_image_id'],"project_image_likes","image_id"));
                }, $alldata);
                }else{
                    $alldata=array();
                }
                $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcount),'Images'=>$alldata));
            }
            if($this->post('type')==2){
                $param = array('lang_id'=>1,
                               'return_type'=>'count',
                               'conditions'=>  array('a.status'=>1)
                              );
                if($designer){$param['conditions']['a.user_id'] = $designer;}
                if($sort_by){
			   switch ($sort_by)
                                {
                                case 'most_rated':
                                    $param['orderby_conditions'] = 'a.design_trend_rating DESC';
                                    break;
                                case 'most_commented':
                                    $param['orderby_conditions'] = 'COUNT(ac.comment_id) DESC';
                                    break;
                                case 'most_viewed':
                                    $param['orderby_conditions'] = 'a.view_num DESC';
                                    break;
                                case 'latest':
                                    $param['orderby_conditions'] = 'a.published_on DESC';
                                    break;
                                }
                            }
                $totalcount= $this->design_trend->find_design_trends($param);
                $param['offset']=$pagination['offset'];
                $param['limit']=$pagination['limit'];
                $param['return_type'] = 'array';
                $alldata = $this->design_trend->find_design_trends($param);
                if(isset($alldata[0])){
                $alldata = array_map(function($tag) {
                return array(
                    'design_trend_id' => $tag['design_trend_id'],
                    'imageURL' => file_exists('uploads/design_trend/images/thumb/'.$tag['image'])? base_url().'uploads/design_trend/images/thumb/'.$tag['image']:base_url().'uploads/design_trend/images/thumb/dummy.jpg',
                    'image_name' => $tag['title'],
                    'likecount' =>$this->api_model->count_all('design_trend_likes',array('design_trend_id'=>$tag['design_trend_id'])),
                    'viewcount' => $tag['view_num'],
                    'likestatus' =>$this->status_check($tag['design_trend_id'],"design_trend_likes","design_trend_id"));
                }, $alldata);
                }else{
                    $alldata=array();
                }
                $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalcount),'Images'=>$alldata));
            }


        }

        /* Api to get all All leadlist*/
        function leadlist_post()
        {
            $this->validation("sessiontoken");
            $pagination=$this->get_offset_size();
            $table = '`project_posts_verification` as ppv';
            $select = '`ppv`.`project_post_verification_id`,`ppv`.`lead_acceptence`,`u`.`user_image`, `ps`.*, CONCAT_WS(" ", `u`.`firstname`, `u`.`lastname` ) as name, `tc`.`type_name` as type_work, `ptc`.`property_type_content` as property_type_name, `pbc`.`budget` as budget, `s`.`state_name`, `c`.`city_name`';
            $select .= ', (SELECT COUNT(*) FROM project_posts_apply WHERE post_id = ps.post_id AND lead_acceptence = 1 AND status = 1) as bid_count';

            $joinArr['`project_posts` as ps']['cond'] = 'ppv.project_post_id = ps.post_id';
            $joinArr['`project_posts` as ps']['type'] = 'LEFT';

            $joinArr['`users` as u']['cond'] = 'ps.user_id = u.user_id';
            $joinArr['`users` as u']['type'] = 'LEFT';

            $joinArr['`type_contents` as tc']['cond'] = 'ps.type_id = tc.type_id';
            $joinArr['`type_contents` as tc']['type'] = 'LEFT';

            $joinArr['`property_type_contents` as ptc']['cond'] = 'ps.property_type_id = ptc.property_type_id';
            $joinArr['`property_type_contents` as ptc']['type'] = 'LEFT';

            $joinArr['`project_budget_contents` as pbc']['cond'] = 'ps.budget_id = pbc.budget_id';
            $joinArr['`project_budget_contents` as pbc']['type'] = 'LEFT';

            $joinArr['`states` as s']['cond'] = 's.state_id = ps.state_id';
            $joinArr['`states` as s']['type'] = 'LEFT';

            $joinArr['`cities` as c']['cond'] = 'c.city_id = ps.city_id';
            $joinArr['`cities` as c']['type'] = 'LEFT';

            $whereArr['`ppv`.`designer_id`'] = $this->post('user_id');
            $group_by = '`ppv`.`project_post_verification_id`';
            $order_by = '`ppv`.`project_post_verification_id` DESC';

            $limitArr['limit'] = $pagination['limit'];
            $limitArr['offset'] = $pagination['offset'];
            $resArr = $this->basic_model->get_data($table, $select, $whereArr, $joinArr, '', '', $group_by);
            $totalRec = count($resArr);
            $allleads = $this->basic_model->get_pagination_list($table, $select, $whereArr, $joinArr, $order_by, $group_by, $limitArr);
            if(isset($allleads[0])){
            foreach($allleads as $keys=>$values){
                $image=$this->project_posts->getProjectImage(1, $values['post_id']);
                $allleads[$keys]['image']=(isset($image[0])? base_url().'uploads/project_posts/thumb/'.$image[0]['post_image']:base_url().'uploads/portfolio/images/dummy.jpg');
            }

            $allleads = array_map(function($tag) {
                return array(
                'project_id' => $tag['post_id'],
                'project_title' => 'Project at '.ucwords(trim($tag['site_address'])) . ', ' . ucwords(trim($tag['city_name'])) . ' - ' . ucwords(trim($tag['pincode'])) . ', ' . ucwords(trim($tag['state_name'])),
                'image' => $tag['image'],
                'user_name' => $tag['name'],
                'location' => $tag['site_address'].' '.$tag['city_name'],
                'type_of_property_id'=>$tag['property_type_id'],
                'type_of_property'=>($tag['property_type_name']==null)?'':$tag['property_type_name'],
                'type_of_work_id'=>$tag['type_id'],
                'type_of_work'=>($tag['type_work']==null)?'':$tag['type_work'],
                'budget'=>$tag['budget'],
                'project_start_date'=>((floor((strtotime($tag['project_start_dt'])-time())/(60*60*24))>0)?floor((strtotime($tag['project_start_dt'])-time())/(60*60*24)):'NA'),
                'no_of_bids_received'=>$tag['bid_count'],
                'apply_status'=>$tag['lead_acceptence'],
                'no_of_shares'=>''
            );
            }, $allleads);
            }else{
            $allleads=array();
            }

            $this->response(array('responsecode'=>200,'responsedetails'=>'success','totalcount'=>number_format($totalRec),'leadlist'=>$allleads));
        }

        /* Api to get all project images using project id*/
        function getprojectdetails_post()
        {
            if($this->post('user_id')!=''){
            $this->validation("sessiontoken");
            }
            $lang_id=$this->get_language_id();
            $pagination=$this->get_offset_size();
            $cond=array('project_id'=>$this->post('project_id'),'lang_id'=>$lang_id,'start_image'=>$pagination['offset'],'limit_image'=>$pagination['limit']);
            $project_image_details=$this->project->get_image_rows($cond);
            $table='projects as p';
            $params=array('p.project_id'=>$this->post('project_id'),'pc.lang_id'=>$lang_id);
            $data=array('p.project_id', 'pc.title');
            $join=array('project_contents as pc'=>'pc.project_id=p.project_id');
            $project_details=$this->api_model->get_data($table,$params,$data,$join);
            $total_count=$this->api_model->count_all('project_images',array('isDelete'=>0,'status'=>1,'project_id'=>$this->post('project_id')));
            if(!isset($project_details[0])){
               $this->response(array("error"=>"Something went wrong. Please try again later."));
            }

            if(isset($project_image_details[0])){
            $project_image_details= array_map(function($tag) {
                return array(
                'imageid' => $tag['project_image_id'],
                'imageurl' => file_exists('uploads/portfolio/images/'.$tag['image'])? base_url().'uploads/portfolio/images/'.$tag['image']:base_url().'uploads/portfolio/images/dummy.jpg',
                'image_desc'=>$tag['image_description'],
                'likecount'=>$tag['imageLike'],
                'commentcount'=>$this->api_model->count_all('project_image_comments',array('project_image_id'=>$tag['project_image_id'])),
                'viewcount'=>$tag['view_num'],
                'likestatus'=>$this->status_check($tag['project_image_id'],'project_image_likes','image_id')
                );
             }, $project_image_details);
            }else{
            $project_image_details=array();
            }
            $this->response(array('responsecode'=>200,'responsedetails'=>'success','projectname'=>$project_details[0]['title'],'project_image_count'=>number_format($total_count),'projectimages'=>$project_image_details));
        }

       /* Common function for various Apis */
        function status_check($id,$table,$field,$field2='')
        {
            if($this->post('user_id')==''){
                return '0';
             }else{
                if($field2!=''){
                $count_status=$this->api_model->count_all($table,array($field=>$id,$field2=>$this->post('user_id')));
                }else{
                $count_status=$this->api_model->count_all($table,array($field=>$id,'user_id'=>$this->post('user_id')));
                }
                if($count_status>0){
                       return '1';
                    }else{
                        return '0';
                    }
                }
        }

        function get_offset_size()
        {
           if($this->post('pageno')!='' && $this->post('pageno')!=''){
               $pageno=($this->post('pageno')-1);
               $pagesize=$this->post('pagesize');
               $offset=($pageno*$pagesize);
               $offset=($offset<0?0:$offset);
             }else{
               $offset='';
               $pagesize='';
             }
             return array('offset'=>$offset,'limit'=>$pagesize);
        }

        function get_language_id()
        {
          $language_id=$this->api_model->get_lang($this->post('language'));
         if($language_id=='NULL')
             $this->response(array("error"=>"Something went wrong. Please try again later."));
         else
            return $lang_id=$language_id['lang_id'];
        }

        /* End */
        /* Validation for Apis */
        function validation($type)
        {
            switch ($type) {

                case 'Registration':
                    if(strlen(base64_decode($this->post('password')))< 5 || strlen(base64_decode($this->post('password')))>15){
                        $this->response(array("responsecode"=> 202,"error"=>"Password length should be between 5 and 15"));
                    }
                    if(!is_int(base64_decode($this->post('mobile'))) && strlen(base64_decode($this->post('mobile')))!=10){
                        $this->response(array("responsecode"=> 202,"error"=>"Please enter correct mobile number"));
                    }
                    if (!filter_var(base64_decode($this->post('email_id')), FILTER_VALIDATE_EMAIL)) {
                      $this->response(array("responsecode"=> 202,"error"=>"Invalid email format."));
                    }
                    if($this->post('device_type')!='Android' && $this->post('device_type')!='IOS'){
                        $this->response(array("responsecode"=> 202,"error"=>"Invalid device type"));
                    }
                    $email_cond=array('email'=>base64_decode($this->post('email_id')));
                    $check_email=$this->api_model->check_user($email_cond);
                        if($check_email=='TRUE'){
                           $this->response(array("responsecode"=> 201,"responsedetails"=>"This Email address already exists. Please use a different email or login through the registered email)"));
                        }
                    $mobile_cond=array('phone'=>base64_decode($this->post('mobile')));
                    $check_mobile=$this->api_model->check_user($mobile_cond);
                        if($check_mobile=='TRUE'){
                          $this->response(array("responsecode"=> 202,"responsedetails"=>"This Mobile number already exists. Please use a different number or login through the registered mobile number"));
                       }
                    break;
                case 'Login':
                    if(base64_decode($this->post('email_mobile')=='')){
                        $this->response(array("responsecode"=> 202,"error"=>"PLease give your email or mobile number"));
                    }
                    if(base64_decode($this->post('password'))==''){
                        $this->response(array("responsecode"=> 202,"error"=>"Please enter your Password"));
                    }
                    if($this->post('device_token')==''){
                        $this->response(array("responsecode"=> 202,"error"=>"Device token invalid"));
                    }
                    if($this->post('device_type')!='Android' && $this->post('device_type')!='IOS'){
                        $this->response(array("responsecode"=> 202,"error"=>"Invalid device type"));
                    }
                    if($this->post('device_id')==''){
                        $this->response(array("responsecode"=> 202,"error"=>"Invalid device id"));
                    }

                    $var=(is_numeric(base64_decode($this->post('email_mobile')))?'phone':'email');
                    $mob_email_cond=array($var=>base64_decode($this->post('email_mobile')));
                    $mobile_email=$this->api_model->check_user($mob_email_cond);
                    if($mobile_email=='NULL'){
                        $this->response(array("responsecode"=> 201,"responsedetails"=>'Invalid login details'));
                    }
                    break;
                case 'forgotpassword':
                    if (!filter_var(base64_decode($this->post('email_id')), FILTER_VALIDATE_EMAIL)) {
                        $this->response(array("responsecode"=> 202,"error"=>"Invalid email format."));
                    }
                    $email_cond=array('email'=>base64_decode($this->post('email_id')));
                    $check_email=$this->api_model->check_user($email_cond);
                    if($check_email=='NULL'){
                       $this->response(array("responsecode"=> 201,"responsedetails"=>"Email id not registered"));
                    }
                    break;
                case 'sessiontoken':
                    $data=array('user_id'=>$this->post('user_id'),'token'=>$this->post('session_token'));
                    $validate=$this->api_model->validatetoken($data);
                    if($validate=='NULL'){
                        $this->response(array('error'=>'Session expired'));
                    }
                    break;
            }

        }
        /* End */

    }
