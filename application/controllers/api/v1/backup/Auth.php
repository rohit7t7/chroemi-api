<?php

//defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,apikey");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS,apikey");

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        print_r($this->input->request_headers('apikey'));
        if(!$this->input->get_request_header('apikey',TRUE)){
            $this->response(array('responsecode'=> 405,'error'=>'API key is missing.'));
        }elseif($this->input->get_request_header('apikey',TRUE) != 'uthjhi787ybg88fgh788'){
            $this->response(array('responsecode'=> 406,'error' => 'Invalid API key'));
        }
    }

    public function login_post(){
      $email = $this->input->post('email');
      $password = $this->input->post('password');
      if($email == 'abhra@gmail.com' && $password == '12345'){
        $userdata = array('username'=>'Abhra','firstname'=>'Abhranil','lastname'=>'Majumdar','email'=>'abhra@gmail.com');
        $this->response(array('responsecode'=> 200,'status' => 'true','data' =>$userdata));
      }else{
        $this->response(array('responsecode'=> 201,'status' => 'false','data' =>'Invalid credential'));
      }
    }

    public function signup_post(){
      $email = $this->input->post('email');
      $password = $this->input->post('password');
      $username = $this->input->post('username');
      $firstname = $this->input->post('firstname');
      $lastname = $this->input->post('lastname');
      if(!$email || !$password || !$username || !$firstname || !$lastname){
        $this->response(array('responsecode'=> 201,'status' => 'false','data' =>'Please provide all credentials'));
      }else{
        $userdata = array('username'=>$username,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email);
        $this->response(array('responsecode'=> 200,'status' => 'true','data' =>$userdata));
      }
    }
}
