<?php
function generateToken($length = 20) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString.time();
}

function sendOtp($email,$otp)
	{
		$ci =& get_instance();
		$ci->load->library('email');
		$mailContent = 'Dear User,
			<br/>Please use this code '.$otp.' to activate your email.';
		$mailContent .= '<br/>We are excited to have you onboard..
			<br/><br/>Cheers!';
		$ci->email->to($email);
                $ci->email->from('info@Chroemi');
                $ci->email->subject('Chroemi Team');
                $ci->email->message($mailContent);
                if($ci->email->send()){
                    return TRUE;
                }else{
                    //echo $ci->email->print_debugger(); die;
                    return TRUE;
                }

	}
