<?php
function generateToken($length = 20) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString.time();
}

function sendOtp($email,$otp)
	{
		$ci =& get_instance();
		$ci->load->library('email');
		$mailContent = 'Dear User,
			<br/>Please use this code '.$otp.' to activate your email.';
		$mailContent .= '<br/>We are excited to have you onboard..
			<br/><br/>Cheers!';
		$ci->email->to($email);
                $ci->email->from('info@Chroemi');
                $ci->email->subject('Chroemi Team');
                $ci->email->message($mailContent);
                if($ci->email->send()){
                    return TRUE;
                }else{
                    //echo $ci->email->print_debugger(); die;
                    return TRUE;
                }

	}

  function getOffsetSize($pageno,$pagesize)
  {
     if($pageno !=''){
         $pageno=($pageno-1);
         $offset=($pageno*$pagesize);
         $offset=($offset<0?0:$offset);
       }else{
         $offset=null;
         $pagesize=null;
       }
       return array('offset'=>$offset,'limit'=>$pagesize);
  }

  function successErrorCode($code)
  {
    switch($code){
      case 200:
        return array('responseCode'=>200,'status'=>true);
        break;
      case 201:
        return array('responseCode'=>201,'status'=>false);
        break;
      case 405:
        return array('responseCode'=> 405,'error'=>'API key is missing.');
        break;
      case 406:
        return array('responseCode'=> 406,'error' => 'Invalid API key');
        break;
      case 407:
        return array('responseCode'=> 407,'error' => 'Please provide valid accesstoken');
        break;
    }
  }
