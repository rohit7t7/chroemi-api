<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template
{
    private $data;
    private $js_file;
    private $css_file;
    private $CI;

    public function __construct()
    {

        $this->CI =& get_instance();
        $this->CI->load->helper('url');

        // default CSS and JS that they must be load in any pages
    
    $this->addJS( 'assets/admin/js/jquery.min.js') ;
    $this->addJS( 'assets/admin/js/jquery-ui.js') ;
	$this->addJS( 'assets/admin/js/bootstrap.min.js') ;
	$this->addJS( 'assets/admin/js/metisMenu.min.js') ;
	//$this->addJS( 'assets/admin/js/raphael-min.js') ;
	//$this->addJS( 'assets/admin/js/morris.min.js') ;
	//$this->addJS( 'assets/admin/js/morris-data.js') ;
	$this->addJS( 'assets/admin/js/sb-admin-2.js') ;
	$this->addJS( 'assets/admin/js/flot-data.js') ;
    $this->addJS( 'assets/admin/js/jquery.dataTables.min.js') ;
    $this->addJS( 'assets/admin/js/dataTables.bootstrap.min.js') ;
    $this->addJS( 'assets/admin/js/canvasjs.min.js') ;

    $this->addJS( 'assets/admin/js/jquery.datetimepicker.js');
   
     $this->addJS( 'assets/admin/js/jquery.datetimepicker.full.js');
   
    

     //$this->addJS( 'assets/admin/js/excanvas.min.js');
//$this->addJS( 'assets/admin/js/jquery.flot.js');
    // $this->addJS( 'assets/admin/js/jquery.flot.pie.js');
    //$this->addJS( 'assets/admin/js/jquery.flot.resize.js');
    //$this->addJS( 'assets/admin/js/jquery.flot.time.js');
    //$this->addJS( 'assets/admin/js/jquery.flot.tooltip.min.js');
    //$this->addJS( 'assets/admin/js/flot-data.js');



    $this->addCSS(  'assets/admin/css/bootstrap.min.css') ;
    $this->addCSS( 'assets/admin/css/font-awesome.min.css' );
	$this->addCSS( 'assets/admin/css/metisMenu.min.css') ;
	$this->addCSS( 'assets/admin/css/timeline.css') ;
	$this->addCSS( 'assets/admin/css/sb-admin-2.css');
	//$this->addCSS( 'assets/admin/css/morris.css') ;
	$this->addCSS( 'assets/admin/css/jquery-ui.min.css' );
    $this->addCSS( 'assets/admin/css/dataTables.bootstrap.css' );
    $this->addCSS( 'assets/admin/css/jquery.datetimepicker.css' );


 
    }

    public function show( $folder, $page, $data )
    {
        if ( ! file_exists('application/views/'.$folder.'/'.$page.'.php' ) )
        {

            show_404();
        }
        else
        {
            
            $this->data['page_var'] = $data;
           


            $this->load_JS_and_css();               
        $this->data['header'] = $this->CI->load->view('layouts/header.php', $this->data, true);

        $this->data['noaction']='0';
        if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']['type']=='2') { 
            $CI =& get_instance();
            $CI->load->model('permissionmodel');
            $CI->load->model('rolemodel');
            $data['supervisorrole'] = $CI->permissionmodel->getById(null,$_SESSION['logged_in']['id']);
            echo "<pre>";
            print_r($data['supervisorrole']); die;
             
            if(count($data['supervisorrole'])>0)
            {

                $access = explode(", " , $data['supervisorrole'][0]['assignedaccess']);


                if(in_array($CI->router->fetch_class().'/delete',$access)==false)
                {   
                   $this->data['noaction'] = '1';
                }
                
                
                $accessrole = $CI->router->fetch_class()."/".$CI->router->fetch_method();
                if(in_array($accessrole,$access)==false)
                {

                    
                    if($accessrole != "dashboard/logout" || $accessrole != "login/index" )
                    {
                        
                        $this->data['content'] = $this->CI->load->view('dashboard/forbidden.php', $this->data, true); 
                    }
                    
                }
                else
                {

                    
                    $this->data['content'] = $this->CI->load->view($folder.'/'.$page.'.php', $this->data, true);
                }
                
            }
            
        }
else {

$this->data['content'] = $this->CI->load->view($folder.'/'.$page.'.php', $this->data, true);

            
        
        
        
      }
	    $this->data['footer'] = $this->CI->load->view('layouts/footer.php', $this->data, true);

            $this->CI->load->view('template/template.php', $this->data);
        }
    }

    public function addJS( $name )
    {
        $js = new stdClass();
        $js->file = $name;
        $this->js_file[] = $js;
    }

    public function addCSS( $name )
    {
        $css = new stdClass();
        $css->file = $name;
        $this->css_file[] = $css;
    }

    private function load_JS_and_css()
    {
        $this->data['html_head'] = '';
	    $this->data['html_footer'] = '';

        if ( $this->css_file )
        {

            foreach( $this->css_file as $css )
            {

                $this->data['html_head'] .= "<link rel='stylesheet' type='text/css' href='".base_url().$css->file."'>". "\n";
            }
        }

        if ( $this->js_file )
        {
            foreach( $this->js_file as $js )
            {
                $this->data['html_footer'] .= "<script type='text/javascript' src='".base_url().$js->file."'></script>". "\n";
            }
        }
    }

    private function init_menu()
    {        
      // your code to init menus
      // it's a sample code you can init some other part of your page
    }
}
