<?php
class ApiModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get($table,$what,$condition1 = null, $condition2 = null, $join = null, $join_type = null, $start = null, $limit = null, $order = null)
    {
        //$this->db->_escape_char='';
        foreach($what as $value)
        {
            $this->db->select($value);
        }
        $this->db->from($this->db->dbprefix.'_'.$table);
        if($condition1 != null)
        {
            foreach($condition1 as $key => $value)
            {
                $this->db->where($key,$value);
            }
        }
        if($condition2 != null)
        {
            $this->db->where($condition2);
        }
        if($join != null && $join_type != null)
        {
            foreach($join as $key => $value)
            {
                $this->db->join($key, $value, $join_type[$key]);
            }
        }
        if($order != null)
        {
            foreach($order as $key => $value)
            {
                $this->db->order_by($key, $value);
            }
        }
        if($limit != null){
           $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        //echo '<pre>';
        //echo $this->db->last_query();//exit;
        $result = $query->result();
        return $result;
    }

    function add($table,$data)
    {
      $insert =  $this->db->insert($this->db->dbprefix.'_'.$table, $data);
      //  $this->exceptions->checkForError();
      //echo '<pre>';
      //echo $this->db->last_query();//exit;
        if(!$insert){
          return false;
        }
        $last_insert_id = $this->db->insert_id();
        return $last_insert_id;
    }

    function update($table,$data,$condition)
    {
        foreach($condition as $key => $value)
        {
            $this->db->where($key, $value);
        }
        $result = $this->db->update($this->db->dbprefix.'_'.$table, $data);
        return $result;
    }

    function delete($table,$condiction)
    {
      foreach($condiction as $key => $value)
      {
          $this->db->where($key,$value);
      }
      $result = $this->db->delete($this->db->dbprefix.'_'.$table);
      return $result;
    }
    function count()
    {

    }
}
?>
