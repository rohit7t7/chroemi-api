<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class AttendeeModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_users($lang)
{

	$query = $this->db->select('*');
	$query = $this->db->where('deletestatus', '0');

	if($lang != "")
	$query = $this->db->where('language_id', $lang);


	$query = $this->db->get($this->db->dbprefix.'_attendee');
	$allusers= $query->result_array();

	return $allusers;
}

public function get_allusers()
{

	$query = $this->db->select('*');
	$query = $this->db->get($this->db->dbprefix.'_attendee');
	$allusers= $query->result_array();

	return $allusers;
}

 public function getuseroption($lang="")
	{
		$usrarr = $this->get_users($lang);

		if(!empty($usrarr))
		{
			$useroptions[''] ="Select an Attendee";	
			foreach($usrarr as $index => $usroption)
			{
				$useroptions[$usroption['id']] = $usroption['name'].'('.$usroption['email'].')';
			}
		}
		else
			$useroptions = array();
		

		return $useroptions;
	}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		//$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_attendee');
		$num = $query->num_rows();


     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }




public function savedata($data)
{
	return $this->db->insert($this->db->dbprefix.'_attendee', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_attendee.id',$id);
	return $this->db->update($this->db->dbprefix.'_attendee', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_attendee.id',$id);
	 return $this->db->update($this->db->dbprefix.'_attendee',$data);
}

}
?>
