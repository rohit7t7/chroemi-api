<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class BaseModel extends CI_Model {

public function __construct()
{
	$this->load->database();


if($this->session->userdata('site_lang') == 'english')
	$this->config->set_item('language', 'english');
 elseif($this->session->userdata('site_lang') == 'indonesia')		
	 $this->config->set_item('language', 'indonesia');
}
 
 
	public function getDeleteStatus($table,$fieldname,$attribute)
	{

		$query_sub = $this->db->select('max(id) as maxid');
		$query_sub = $this->db->where($fieldname, $attribute); 
		$query_sub = $this->db->get($this->db->dbprefix.'_'.$table);
		$resultarray_sub= $query_sub->result_array();
		
		$query = $this->db->select('deletestatus');
		$query = $this->db->where($fieldname, $attribute);
		$query = $this->db->where('id=', $resultarray_sub[0]['maxid']);

		$query = $this->db->get($this->db->dbprefix.'_'.$table);
		//echo $this->db->last_query(); die;
		$resultarray= $query->result_array();
	
		return $resultarray;
	}

}
?>
