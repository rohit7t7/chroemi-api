<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class CommissionModel extends CI_Model {

public function __construct()
{
	$this->load->database();
	
}
 
 
public function get_commissions()
{

	$query = $this->db->select('c.*');
	$query = $this->db->from($this->db->dbprefix.'_commission as c');
	$query = $this->db->get();
	$allcommissions= $query->result_array();

	return $allcommissions;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->get($this->db->dbprefix.'_commission');
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }





public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_commission.id',$id);
	return $this->db->update($this->db->dbprefix.'_commission', $data);

}


}
?>
