<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class ContentModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function getContent($lang="")
{

	$query = $this->db->select('*');

	if($lang != "")
	$query = $this->db->where('language_id', $lang);

	$query = $this->db->get($this->db->dbprefix.'_cms');
	$allusers= $query->result_array();

	return $allusers;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->get($this->db->dbprefix.'_cms');
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }




public function savedata($data)
{
	return $this->db->insert($this->db->dbprefix.'_cms', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_cms.id',$id);
	return $this->db->update($this->db->dbprefix.'_cms', $data);

}

public function delete_info($id)
{
	 return $this->db->delete($this->db->dbprefix.'_cms', array('id' => $id));
	 
}

}
?>
