<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class CurrencyModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_currencylist()
{

	$query = $this->db->select('*');
	//$query = $this->db->where('id','65');
	$query = $this->db->get($this->db->dbprefix.'_currency');
	$currencylist= $query->result_array();

	return $currencylist;
}

public function getcurrencyoption()
{
	$currencyarr = $this->get_currencylist();

	if(!empty($currencyarr))
	{
		foreach($currencyarr as $index => $currval)
		{
			$curroptions[$currval['id']] = $currval['currency_code'];
		}
	}
	else
		$curroptions = array();
	

	return $curroptions;
}

	public function getdefaultcurrency()
	{
		$query = $this->db->select('*');
		$query = $this->db->where('default','1');
		$query = $this->db->get($this->db->dbprefix.'_currency');
		$defaultcurrency= $query->row_array();

		return $defaultcurrency;
	}

 


}
?>
