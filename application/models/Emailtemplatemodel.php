<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class EmailtemplateModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_emailtemplate($lang)
{

	$query = $this->db->select('*');
	$query = $this->db->where('languageid', $lang);

	$query = $this->db->get($this->db->dbprefix.'_email_template');
	$alltype= $query->result_array();


	return $alltype;
}

public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		
		$query = $this->db->get($this->db->dbprefix.'_email_template');
		
		$num = $query->num_rows();

		
     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }

    public function getdata($typeid)
	{

		$query = $this->db->select('*');
		//$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_email_template');


		$num = $query->num_rows();

		return $num;
     	
    }




public function savedata($data)
{
	
	return $this->db->insert($this->db->dbprefix.'_email_template', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_email_template.id',$id);
	return $this->db->update($this->db->dbprefix.'_email_template', $data);

}



}
?>
