<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class EventcategoryModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_categories($lang="",$parent_id="")
{

	$query = $this->db->select('*');
	$query = $this->db->where('deletestatus', '0');

	//if($parent_id !='')
	//{
		//if($parent_id ==2)
			//$query = $this->db->where('parent_id!=', null);
		//else
			//$query = $this->db->where('parent_id', null);
	//}
	
		

	if($lang !='')
	$query = $this->db->where('language_id', $lang);

	$query = $this->db->get($this->db->dbprefix.'_category');
	//echo $this->db->last_query();
	$allcategory= $query->result_array();

	return $allcategory;
}

//get all events
public function getcategoryoption($lang="",$parent_id="")
{
	$catarr = $this->get_categories($lang,$parent_id);

	if(!empty($catarr))
	{
	    $catoptions[""]="";		
		foreach($catarr as $index => $srvcategory)
		{
			$catoptions[$srvcategory['id']] = $srvcategory['category'];
		}
	}
	else
		$catoptions = array();
	

	return $catoptions;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_category');
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }

   

public function savedata($data)
{
	return $this->db->insert($this->db->dbprefix.'_category', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_category.id',$id);
	return $this->db->update($this->db->dbprefix.'_category', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );

	 $this->db->where($this->db->dbprefix.'_category.id',$id);
	 return $this->db->update($this->db->dbprefix.'_category',$data);
}

}
?>
