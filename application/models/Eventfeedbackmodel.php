<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class ServicefeedbackModel extends CI_Model {

public function __construct()
{
	$this->load->database();
	
}
 
 
public function get_feedback()
{

	$query = $this->db->select('f.*');
	$query = $this->db->from($this->db->dbprefix.'_feedback as f');
	$query = $this->db->join($this->db->dbprefix.'_services as s', 'f.service_id = s.id');
	$query =$this->db->join($this->db->dbprefix.'_users as u', 'f.user_id = u.id');
	$query = $this->db->where('f.deletestatus', '0');
	
	$query = $this->db->get();
	
	$allfeedback= $query->result_array();

	return $allfeedback;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_feedback');
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }




public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_feedback.id',$id);
	return $this->db->update($this->db->dbprefix.'_feedback', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_feedback.id',$id);
	 return $this->db->update($this->db->dbprefix.'_feedback',$data);
}

}
?>
