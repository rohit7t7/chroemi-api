<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Eventfundsmodel extends CI_Model {

public function __construct()
{
	$this->load->database();
	
}

public function get_funds($id="")
{
    
	//$query = $this->db->select('s.*,t.servicetype,c.category');
	$query = $this->db->select('ef.* , e.name, curr.currency_code');
      
        //$query = $this->db->from($this->db->dbprefix.'_events as e');
         $query = $this->db->from($this->db->dbprefix.'_event_funds as ef');
	$query = $this->db->join($this->db->dbprefix.'_events as e', 'ef.event_id = e.id');
        $query = $this->db->join($this->db->dbprefix.'_currency as curr', 'ef.currency_id = curr.id');
	//$query =$this->db->join($this->db->dbprefix.'_servicetype as t', 's.typeid = t.id');
	

	$query = $this->db->where('e.deletestatus', '0');
        $query = $this->db->where('ef.event_id', $id);
	//$query = $this->db->where('c.deletestatus', '0');
	//$query = $this->db->where('t.deletestatus', '0');
	$query = $this->db->get();
      // echo $this->db->last_query(); die;
	$allfunds= $query->result_array();

	return $allfunds;
}
public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		//$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_events');
		$num = $query->num_rows();

		// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }
    
    public function savedata($data)
{
	$this->db->insert($this->db->dbprefix.'_event_funds', $data);
	$insert_id = $this->db->insert_id();

   return  $insert_id;
}
}
?>
