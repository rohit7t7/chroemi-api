<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class ServiceorderModel extends CI_Model {

public function __construct()
{
	$this->load->database();
	
}
 
 
public function get_servicerequest($param='',$type="")
{

	$query = $this->db->select('o.*,st.servicetype,st.id as typeid,MID(order_date,1,10) as orderdate');
	$query = $this->db->from($this->db->dbprefix.'_orders as o');
	$query = $this->db->join($this->db->dbprefix.'_services as s', 's.id = o.service_id');
	$query = $this->db->join($this->db->dbprefix.'_servicetype as st', 'st.id = s.typeid');
	
	if($param == 'latest')
	{
		//$query = $this->db->where('o.order_status', '0');
		$d2 = date('Y-m-d', strtotime('today - 30 days'));

		$query = $this->db->where('mid(o.order_date,1,10) >=', $d2);
		$query = $this->db->where('mid(o.order_date,1,10) <=', date('Y-m-d'));
	}

	if($type == 'pending')
	{
		$query = $this->db->where('o.order_status', '0');
	}
	elseif($type == 'confirm')
	{
		$query = $this->db->where('o.order_status', '1');
	}
	elseif($type == 'cancel')
	{
		$query = $this->db->where('o.order_status', '2');
	}
	
	$query = $this->db->where('o.deletestatus', '0');
	//$query = $this->db->order_by('orderdate', 'DESC');
	$query = $this->db->get();
	//echo $this->db->last_query();
		//die;
	

	$allorders= $query->result_array();

	return $allorders;
}


public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_orders.id',$id);
	 return $this->db->update($this->db->dbprefix.'_orders',$data);
}

public function confirm_info($id)
{
	 $data = array(
               'order_status' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_orders.id',$id);
	 return $this->db->update($this->db->dbprefix.'_orders',$data);
}

public function cancel_info($id)
{
	 $data = array(
               'order_status' => '2'
            );
	 $this->db->where($this->db->dbprefix.'_orders.id',$id);
	 return $this->db->update($this->db->dbprefix.'_orders',$data);
}

	public function searchdata($postdata)
	{

		$query = $this->db->select('o.*,t.servicetype,t.id as typeid');
		$query = $this->db->from($this->db->dbprefix.'_orders as o');
		$query = $this->db->join($this->db->dbprefix.'_services as s', 's.id = o.service_id');
		$query = $this->db->join($this->db->dbprefix.'_servicecategory as c', 'c.id = s.categoryid');
		$query = $this->db->join($this->db->dbprefix.'_servicetype as t', 't.id = s.typeid');
		$query = $this->db->where('o.deletestatus', '0');

		//if( $postdata['searchtype']=='latestorder')
			//$query = $this->db->where('o.order_status', '0');
		

		if($postdata['categoryid']!='')
		$query = $this->db->where('c.id', $postdata['categoryid']);

		if($postdata['typeid']!='')
		$query = $this->db->where('t.id', $postdata['typeid']);

	
		if($postdata['startdate']!='' && $postdata['enddate'] != '' )
		{
			
			$query = $this->db->where('mid(o.order_date,1,10) >=', $postdata['startdate']);
			$query = $this->db->where('mid(o.order_date,1,10) <=', $postdata['enddate']);
		}
		
		
		if($postdata['startdate']=='' && $postdata['enddate'] != '')
		{
			
			$query = $this->db->where('mid(o.order_date,1,10) >=', date('Y-m-d'));	
			$query = $this->db->where('mid(o.order_date,1,10) <=', $postdata['enddate']);
		}
		
		

		if($postdata['startdate']!='' && $postdata['enddate'] == '')
		{
		
			$query = $this->db->where('mid(o.order_date,1,10) >=', $postdata['startdate']);	
			$query = $this->db->where('mid(o.order_date,1,10) <=', date('Y-m-d'));	
		}

		if($postdata['startdate']=='' && $postdata['enddate'] == '')
		{
			$d2 = date('Y-m-d', strtotime('today - 30 days'));

			$query = $this->db->where('mid(o.order_date,1,10) >=', $d2);
			$query = $this->db->where('mid(o.order_date,1,10) <=', date('Y-m-d'));
		}
		
		
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;

		$allorders= $query->result_array();

		return $allorders;
		
	}


	public function get_refunddetails()
	{

		$query = $this->db->select('o.*');
		$query = $this->db->from($this->db->dbprefix.'_orders as o');
		$query = $this->db->join($this->db->dbprefix.'_services as s', 's.id = o.service_id');
		$query = $this->db->where('o.refund_status', '1');
		$query = $this->db->get();
		
		$allrefunddet= $query->result_array();

		return $allrefunddet;
		//$query = $this->db->join($this->db->dbprefix.'_servicetype as st', 'st.id = s.typeid');
	}

	public function get_refundrequest()
	{

	$query = $this->db->select('o.*');
	$query = $this->db->from($this->db->dbprefix.'_orders as o');
	$query = $this->db->join($this->db->dbprefix.'_services as s', 's.id = o.service_id');
	$query = $this->db->where('o.refund_status', '2');
	$query = $this->db->get();

	$allrefunddet= $query->result_array();

	return $allrefunddet;
	//$query = $this->db->join($this->db->dbprefix.'_servicetype as st', 'st.id = s.typeid');
	}

}
?>
