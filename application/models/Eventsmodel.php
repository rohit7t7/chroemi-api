<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class EventsModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_services($lang="")
{
    $query = $this->db->select('s.id as id , s.name as name , s.address as address , c.category as category');
   
	//$query = $this->db->select('s.*,t.servicetype,c.category');
	$query = $this->db->from($this->db->dbprefix.'_events as s');
        
	$query = $this->db->join($this->db->dbprefix.'_category as c', 's.categoryid = c.id');
	//$query =$this->db->join($this->db->dbprefix.'_servicetype as t', 's.typeid = t.id');
	
	if($lang !='')
	$query = $this->db->where('s.language_id', $lang);

	$query = $this->db->where('s.deletestatus', '0');
	//$query = $this->db->where('c.deletestatus', '0');
	//$query = $this->db->where('t.deletestatus', '0');
	$query = $this->db->get();
       // echo$this->db->last_query(); 
	$allservices= $query->result_array();

	return $allservices;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		//$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_events');
		$num = $query->num_rows();

		// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }

    public function getserviceoption($lang="")
	{
		$srvarr = $this->get_services($lang);

		if(!empty($srvarr))
		{	
			$srvoptions[''] ="Select an Event";	
			foreach($srvarr as $index => $srvoption)
			{
				$srvoptions[$srvoption['id']] = $srvoption['name'];
			}
		}
		else
			$srvoptions = array();
		

		return $srvoptions;
	}

    public function getimageinfo($id)
	{

		$query = $this->db->select('*');
		$query = $this->db->where('event_id', $id);
		$query = $this->db->get($this->db->dbprefix.'_media');
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num !=0){
    		return $query->result_array();

    	}
    	// Then, return the value from Model to the calling controller
    }




public function savedata($data)
{
	$this->db->insert($this->db->dbprefix.'_events', $data);
	$insert_id = $this->db->insert_id();

   return  $insert_id;
}

public function saveimgdata($imgdata,$serviceid)
{

	$data['serviceid'] = $serviceid;
	$data['image'] = $imgdata;


	if($serviceid != '')
		$this->db->delete($this->db->dbprefix.'_media', array('id' => $serviceid));
	
	return $this->db->insert($this->db->dbprefix.'_media', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_events',$id);
	return $this->db->update($this->db->dbprefix.'_events', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_events.id',$id);
	 return $this->db->update($this->db->dbprefix.'_events',$data);
}

public function searchdata($postdata)
	{

		if($postdata['categoryid']!='')
		{
			$qrcat =  $this->db->select('psc.*');
			$qrcat = $this->db->where('psc.parent_id', $postdata['categoryid']);
			$qrcat = $this->db->where('psc.deletestatus', '0');
			$qrcat = $this->db->get($this->db->dbprefix.'_category as psc');
			$num = $qrcat->num_rows();

			$cond = "";
		if($num ==0)
		{
			$catid = $postdata['categoryid'];
			$cond = "c.id = '$catid'";
				
		}
			else
			{
				$parentcatarr =  $this->db->select('psc.id');
				$parentcatarr = $this->db->from($this->db->dbprefix.'_category as psc');
				$parentcatarr = $this->db->where('psc.parent_id', $postdata['categoryid']);
				$parentcatarr = $this->db->where('psc.deletestatus', '0');
				$parentcatarr = $this->db->get();
				$parentcatarrdet = $parentcatarr->result_array();	

				$pids = "";
				foreach($parentcatarrdet as $pid)
				{
					$pids = $pid['id'];
					$cond .= "c.id = '$pids' or ";
				}

				$cond = substr($cond,0,-3);
				//echo $cond;

				
			}

		}

	
		$query = $this->db->select('s.*,c.category');
		$query = $this->db->from($this->db->dbprefix.'_events as s');
		$query = $this->db->join($this->db->dbprefix.'_category as c', 'c.id = s.categoryid');
		//$query = $this->db->join($this->db->dbprefix.'_servicetype as t', 't.id = s.typeid');
		$query = $this->db->where('s.deletestatus', '0');

		if($postdata['categoryid']!='')
			$query = $this->db->where($cond);
		
		

		//if($postdata['typeid']!='')
		//$query = $this->db->where('t.id', $postdata['typeid']);

		$query = $this->db->get();

			//echo $this->db->last_query();

		$allservics= $query->result_array();

		return $allservics;
		
	}

	public function get_svcreatereports($postdata="")
	{
		
		$currdate = date("Y-m-d");

		if(!empty($postdata))
		{

			$startdate = $postdata['startdate'];
			$enddate = $postdata['enddate'];
			$userid = $postdata['userid'];

			if($postdata['startdate']!='' && $postdata['enddate'] != '' )
			$cond = "where createddate >= '$startdate' and createddate <= '$enddate'";
		
	
			if($postdata['startdate']=='' && $postdata['enddate'] != '')
				$cond = "where createddate >= '$currdate' and createddate <= '$enddate'";
				
		
			if($postdata['startdate']!='' && $postdata['enddate'] == '')
				$cond = "where createddate >= '$startdate' and createddate <= '$currdate'";
			
			if($postdata['startdate']=='' && $postdata['enddate'] == '')
				$cond = "";	

			if($postdata['userid']!='' && $cond =="")
				$conduser = "where createdby = '$userid'";	
			elseif($postdata['userid']!='' && $cond !="")
				$conduser = "and createdby = '$userid'";

			if($postdata['userid']=='')
				$conduser = "";


		}
		else
		{
			$cond = "";
			$conduser = "";	

		}


		$query = $this->db->query("SELECT count(*) as srvcount, createdby FROM `ch_events` $cond $conduser GROUP BY createdby");
				
		$allsrvs= $query->result_array();

		return $allsrvs;
		
	}

}
?>
