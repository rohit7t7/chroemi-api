<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class LanguageModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_languages()
{

	$this->db->select('id,language');
	$this->db->from($this->db->dbprefix.'_language');
	$this->db->where('status','1');
	$query = $this->db->get();

	$alllanguage= $query->result_array();

	foreach($alllanguage as $index => $lang)
	{
		$langoptions[$lang['id']] = $lang['language'];
	}

	return $langoptions;
}


}
?>
