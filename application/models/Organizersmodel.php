<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class OrganizersModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_supervisors($lang="")
{

	$query = $this->db->select('*');
	$query = $this->db->where('deletestatus', '0');
	$query = $this->db->where('type', '2');
	$query = $this->db->where('id!=', $_SESSION['logged_in']['id']);
	$query = $this->db->get($this->db->dbprefix.'_organizers');
	
	$allsupervisors= $query->result_array();

	return $allsupervisors;
}


public function getSupervisors($lang)
{

	$query = $this->db->select('*');
	$query = $this->db->where('deletestatus', '0');
	$query = $this->db->where('language_id', $lang);
	
	$query = $this->db->get($this->db->dbprefix.'_organizers');
	$allsupervisors= $query->result_array();

	return $allsupervisors;
}

//get all users
public function getsupervisoroption($lang="")
{
	$suparr = $this->getSupervisors($lang);

	if(!empty($suparr))
	{
	    $superoptions[""]="";		
		foreach($suparr as $index => $srvcategory)
		{
			$superoptions[$srvcategory['id']] = $srvcategory['name'];
		}
	}
	else
		$superoptions = array();
	

	return $superoptions;
}


public function get_emailtemplate()
{

	$query = $this->db->select('*');
	$query = $this->db->where('id', '1');
	$query = $this->db->get($this->db->dbprefix.'_email_template');
	$etemplate= $query->result_array();

	return $etemplate;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		//$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_organizers');
		//echo  $this->db->last_query();
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }


public function getallsupId($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->get($this->db->dbprefix.'_organizers');
		//echo  $this->db->last_query();
		$num = $query->num_rows();

     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }




public function savedata($data)
{
	$this->db->insert($this->db->dbprefix.'_organizers', $data);
	$getsuperid = $this->db->insert_id();
	$data1['supervisor_id'] = $getsuperid;
	$data1['role_id'] = 3;
	return $this->db->insert($this->db->dbprefix.'_permission', $data1);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_organizers.id',$id);
	return $this->db->update($this->db->dbprefix.'_organizers', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_organizers.id',$id);
	 return $this->db->update($this->db->dbprefix.'_organizers',$data);
}

}
?>
