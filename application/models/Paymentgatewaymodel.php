<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class PaymentgatewayModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_paymentgateway()
{

	$query = $this->db->select('*');
	$query = $this->db->where('deletestatus', '0');

	$query = $this->db->get($this->db->dbprefix.'_paymentgateway');
	$alltype= $query->result_array();


	return $alltype;
}

public function gettypeoption()
{
	$typearr = $this->get_paymentgateway();

	if(!empty($typearr))
	{
		$typeoptions['']='';	
		foreach($typearr as $index => $srvtype)
		{
			$typeoptions[$srvtype['id']] = $srvtype['name'];
		}
	}
	else
		$typeoptions = array();
	

	return $typeoptions;
}


public function getById($id)
{

		$query = $this->db->select('*');
		$query = $this->db->where('id', $id);
		$query = $this->db->where('deletestatus', '0');
		$query = $this->db->get($this->db->dbprefix.'_paymentgateway');
		
		$num = $query->num_rows();

		
     	// Check if the query was successful
    	if($num ==0){
    		return array();

    	}else{
    		return $query->row_array();
    	}
    	// Then, return the value from Model to the calling controller
    }




public function savedata($data)
{
	return $this->db->insert($this->db->dbprefix.'_paymentgateway', $data);
}

public function update_info($data,$id)
 {

	$this->db->where($this->db->dbprefix.'_paymentgateway.id',$id);
	return $this->db->update($this->db->dbprefix.'_paymentgateway', $data);

}

public function delete_info($id)
{
	 $data = array(
               'deletestatus' => '1'
            );
	 $this->db->where($this->db->dbprefix.'_paymentgateway.id',$id);
	 return $this->db->update($this->db->dbprefix.'_paymentgateway',$data);
}

}
?>
