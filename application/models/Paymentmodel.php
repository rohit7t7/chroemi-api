<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class PaymentModel extends CI_Model {

public function __construct()
{
	$this->load->database();
	
}
 
 
	public function searchdata($postdata)
	{

		$query = $this->db->select('o.*,t.servicetype,t.id as typeid');
		$query = $this->db->from($this->db->dbprefix.'_orders as o');
		$query = $this->db->join($this->db->dbprefix.'_services as s', 's.id = o.service_id');
		$query = $this->db->join($this->db->dbprefix.'_servicecategory as c', 'c.id = s.categoryid');
		$query = $this->db->join($this->db->dbprefix.'_servicetype as t', 't.id = s.typeid');
		$query = $this->db->where('o.deletestatus', '0');
		
		
		if($postdata['startdate']!='' && $postdata['enddate'] != '' )
		{
			
			$query = $this->db->where('mid(o.order_date,1,10) >=', $postdata['startdate']);
			$query = $this->db->where('mid(o.order_date,1,10) <=', $postdata['enddate']);
		}
		
		
		if($postdata['startdate']=='' && $postdata['enddate'] != '')
		{
			
			$query = $this->db->where('mid(o.order_date,1,10) >=', date('Y-m-d'));	
			$query = $this->db->where('mid(o.order_date,1,10) <=', $postdata['enddate']);
		}
		
		

		if($postdata['startdate']!='' && $postdata['enddate'] == '')
		{
		
			$query = $this->db->where('mid(o.order_date,1,10) >=', $postdata['startdate']);	
			$query = $this->db->where('mid(o.order_date,1,10) <=', date('Y-m-d'));	
		}
		
		
		$query = $this->db->get();
		
		$allorders= $query->result_array();

		return $allorders;
		
	}

}
?>
