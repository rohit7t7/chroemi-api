<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class PermissionModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function getPermission($lang)
{

	//$query = $this->db->select('*');
	//$query = $this->db->group_by('supervisor_id');

	$query = $this->db->select('p.id,p.supervisor_id, s.name , GROUP_CONCAT(r.role SEPARATOR ", ") as assignedroles, r.id as rid');
    $query = $this->db->from($this->db->dbprefix.'_permission p');
    $query = $this->db->join($this->db->dbprefix.'_roles r', 'p.role_id=r.id', 'join'); 
    $query = $this->db->join($this->db->dbprefix.'_supervisors s', 's.id=p.supervisor_id', 'left');
    $query = $this->db->where('s.type = 2');
    $query = $this->db->where("s.deletestatus = '0'");
    $query = $this->db->where("s.language_id = $lang");
    $query = $this->db->where('s.id != '.$_SESSION['logged_in']['id']);
    $query = $this->db->group_by('p.supervisor_id,p.id');
    //$query = $this->db->join($this->db->dbprefix.'_roles r', 'r.id=p.role_id', 'left');
    //
    
	//$query = $this->db->get($this->db->dbprefix.'_permission');
	$query = $this->db->get();
	echo $this->db->last_query(); die;
	$allpermission= $query->result_array();

	return $allpermission;
}

public function getById($id,$supervisoerId)
{

		//$query = $this->db->select('*');
	//$query = $this->db->group_by('supervisor_id');

	$query = $this->db->select('ANY_VALUE(p.id) as id,p.supervisor_id , s.name , GROUP_CONCAT(r.role SEPARATOR ",") as assignedroles, 
	GROUP_CONCAT(r.access SEPARATOR ",") as assignedaccess , GROUP_CONCAT(r.id SEPARATOR ",") as assignedrolesid, ANY_VALUE(r.id) as rid, s.all_permission as allpermission');
    $query = $this->db->from($this->db->dbprefix.'_permission p'); 
    $query = $this->db->join($this->db->dbprefix.'_supervisors s', 's.id=p.supervisor_id', 'left');
    $query = $this->db->join($this->db->dbprefix.'_roles r', 'p.role_id=r.id', 'right');
    $query = $this->db->where('p.supervisor_id = '.$supervisoerId);
    $query = $this->db->where('s.type = 2');
    // $query = $this->db->order_by("p.id");
    
    $query = $this->db->group_by('p.supervisor_id');
    //$query = $this->db->join($this->db->dbprefix.'_roles r', 'r.id=p.role_id', 'left');
	//$query = $this->db->get($this->db->dbprefix.'_permission');
	$query = $this->db->get();

	//echo $this->db->last_query();
	//die;
	$num = $query->num_rows();

	if($num ==0){
    		return array();

    	}else{
    		return $query->result_array();
    	}
	
	
    }




public function savedata($data)
{
	return $this->db->insert($this->db->dbprefix.'_supervisors', $data);
}

public function update_info($roleIdArray,$allpermission,$supervisor_id)
 {

 	
 	if($allpermission ==1)
 		$permdata['all_permission'] = $allpermission;
 	else
 		$permdata['all_permission'] = 0;

 	

 	$this->db->where($this->db->dbprefix.'_supervisors.id',$supervisor_id);
	$this->db->update($this->db->dbprefix.'_supervisors', $permdata);

 	$this->db->delete($this->db->dbprefix.'_permission', array('supervisor_id' => $supervisor_id));
 	for($i=0;$i<count($roleIdArray);$i++)
         	{
         		//echo $this->input->post('role_id')[$i];
         		$cdata['supervisor_id'] = $supervisor_id;
		   		$cdata['role_id'] = $roleIdArray[$i];
		   		$this->db->insert($this->db->dbprefix.'_permission', $cdata);
         	}

         	if($this->session->userdata('site_lang') == 'english')
	        	$this->session->set_flashdata('msg', '<div class="alert alert-success">Permission updated successfully</div>');
	        elseif($this->session->userdata('site_lang') == 'indonesia')
	        	$this->session->set_flashdata('msg', '<div class="alert alert-success">Izin berhasil diperbarui</div>');
 	
 	redirect('permission');   

}

public function delete_info($id,$supervisorid)
{
	 
	 return $this->db->delete($this->db->dbprefix.'_permission', array('supervisor_id' => $supervisorid));
}

	public function checkpermission($userid,$roleid)
	{
		$query = $this->db->select('p.*');
		$query = $this->db->from($this->db->dbprefix.'_permission as p');
		$query = $this->db->where('p.supervisor_id', $userid);
		$query = $this->db->where('p.role_id', $roleid);
		
		$query = $this->db->get();
		$num = $query->num_rows();
		
		return $num;
	}

}
?>
