<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class RoleModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
public function get_role()
{

	$query = $this->db->select('*');
	$query = $this->db->where('status', '1');
	$query = $this->db->get($this->db->dbprefix.'_roles');
	$allusers= $query->result_array();

	return $allusers;
}




}
?>
