<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class SearchModel extends CI_Model {

public function __construct()
{
	$this->load->database();
}
 
 
	public function getsupervisorsearchresult($keywords)
	{

		$query = $this->db->select('*');
		$query = $this->db->where("(name LIKE '%$keywords%' OR email LIKE '%$keywords%' OR phone LIKE '%$keywords%' OR createddate LIKE '%$keywords%')");
		$query = $this->db->where('deletestatus','0');
		$query = $this->db->get($this->db->dbprefix.'_organizers');

		$allsupervisors= $query->result_array();

		return $allsupervisors;
	}

	public function getusersearchresult($keywords)
	{

		$query = $this->db->select('*');
		$query = $this->db->where("(name LIKE '%$keywords%' OR email LIKE '%$keywords%' OR phone LIKE '%$keywords%' OR createddate LIKE '%$keywords%')");
		$query = $this->db->where('deletestatus','0');
		$query = $this->db->get($this->db->dbprefix.'_attendee');

		$allusers= $query->result_array();

		return $allusers;
	}

	public function getcategorysearchresult($keywords)
	{

		$query = $this->db->select('*');
		$query = $this->db->where("(category LIKE '%$keywords%')");
		$query = $this->db->where('deletestatus','0');
		$query = $this->db->get($this->db->dbprefix.'_category');

		$allcategory= $query->result_array();

		return $allcategory;
	}

	public function getservicesearchresult($keywords)
	{

		$query = $this->db->select('*');
		$query = $this->db->where("(servicename LIKE '%$keywords%' OR description LIKE '%$keywords%' OR cost LIKE '%$keywords%' OR availabilitystartdate LIKE '%$keywords%' OR availabilityenddate LIKE '%$keywords%' OR features LIKE '%$keywords%' OR discount LIKE '%$keywords%' OR createddate LIKE '%$keywords%')");
        $query = $this->db->where('deletestatus','0');
		$query = $this->db->get($this->db->dbprefix.'_events');

		$allcategory= $query->result_array();

		return $allcategory;
	}

	/*public function getordersearchresult($keywords)
	{

		$query = $this->db->select('*');
		$query = $this->db->where("(invoiceid LIKE '%$keywords%' OR billing_name LIKE '%$keywords%' OR billing_email_address LIKE '%$keywords%' OR billing_street_address LIKE '%$keywords%' OR billing_city LIKE '%$keywords%' OR billing_postcode LIKE '%$keywords%' OR billing_state LIKE '%$keywords%' OR billing_country LIKE '%$keywords%' OR billing_phone LIKE '%$keywords%' OR billing_fax LIKE '%$keywords%' OR order_amount LIKE '%$keywords%' OR order_date LIKE '%$keywords%')");
		$query = $this->db->where('deletestatus','0');
		$query = $this->db->get($this->db->dbprefix.'_orders');

		$allorders= $query->result_array();

		return $allorders;
	}*/

}
?>