<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('attendee'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 
<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

              <?php echo form_open_multipart('attendee/create'); 

              echo "<div class='error_msg'>";
              if (isset($error_message)) {
              echo $error_message;
              } ?>

               <p><span class="error_msg">*</span> 

                <?php  if($this->session->userdata('site_lang') == 'english') {?>
                 marks are mandatory fields.

                <?php  } if($this->session->userdata('site_lang') == 'indonesia') {?>
                    tanda adalah bidang wajib.
                <?php } ?>
               </p>
             <?php echo validation_errors();

              echo "</div>";
              ?>
<div class="form-group">
<input style="display:none">
 <input type="password" style="display:none">
<?php
echo form_label($this->lang->line('name')); ?> <span class="error_msg">*</span>
<?php echo form_hidden(array('name' => 'id')); 
 echo form_input(array('name' => 'name','class'=>'form-control','value'=>set_value('name')));
 ?>  
</div>

<!--div class="form-group">
<?php
//echo form_label($this->lang->line('name').' ('.$this->lang->line('in').' '.$this->lang->line('indonesian') .')'); ?> <span class="error_msg">*</span>
 <?php //echo form_input(array('name' => 'name_in_indonesian','class'=>'form-control','value'=>set_value('name_in_indonesian')));
 ?>  
</div-->

<div class="form-group">
<?php
echo form_label($this->lang->line('email')); ?> <span class="error_msg">*</span>
 <?php echo form_input(array('name' => 'email','class'=>'form-control','value'=>set_value('email')));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('phone')); ?> <span class="error_msg">*</span>
 <?php echo form_input(array('name' => 'phone','class'=>'form-control','value'=>set_value('phone')));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('password')); ?> <span class="error_msg">*</span>
<?php echo form_password(array('name' => 'password','class'=>'form-control','value'=>set_value('password')));
 ?>  
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('gender')); 
$genderoptions = array('f'=>'Female','m'=>'Male');
 echo form_dropdown('gender', $genderoptions, set_value('gender'),'class="form-control"'); ?>

</div>

<div class="form-group correct-height width-4">
<?php echo form_label($this->lang->line('dob')); 
echo form_input(array('name' => 'dob','id'=>'dob','class'=>'form-control','value'=>set_value('dob'), 'readonly'=>'true'));  ?>
<input type="button" value="select" id="dobdatepicker" style="display:none">
</div>


<div class="form-group">
 <?php
echo form_label($this->lang->line('address')); 
 echo form_textarea(array('name' => 'address', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>set_value('address')));  ?>

 </div>

 <div class="form-group">
<?php echo form_label($this->lang->line('upload').' '. $this->lang->line('image')); 
echo form_upload(array('name' => 'image'));  ?>

</div>


<!--div class="form-group">
 
<?php //echo form_label($this->lang->line('language')); 
//echo form_dropdown('language_id', $page_var['languageData'], set_value('language_id'),'class="form-control"'); ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

 <script >
 $(function() 
 {   $( "#dobdatepicker" ).datepicker({
          showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#dob').val(dateText) },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd",
         maxDate: '0' });
 });


 </script>