
<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('attendee'); ?> <?php echo $this->lang->line('details'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">


<div class="form-group">
<?php
echo form_label($this->lang->line('name') .':'); 
 echo form_hidden(array('name' => 'id','value'=>htmlentities($page_var['modelData']['id'],ENT_QUOTES))); 
 echo htmlentities($page_var['modelData']['name'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('email') .':'); 
 echo htmlentities($page_var['modelData']['email'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('phone') .':'); 
 echo htmlentities($page_var['modelData']['phone'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('password').':'); 
 echo "******";//$page_var['modelData']['password'];
 ?>  
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('gender').':'); 

 if(htmlentities($page_var['modelData']['gender'],ENT_QUOTES) == 'f') { echo 'Female';}else if(htmlentities($page_var['modelData']['gender'],ENT_QUOTES) == 'm'){ echo 'Male';}?>

</div>

<div class="form-group">
<?php echo form_label($this->lang->line('dob').':'); 
echo htmlentities($page_var['modelData']['dob'],ENT_QUOTES);  ?>

</div>


<div class="form-group">
 <?php
echo form_label($this->lang->line('address').':'); 
 echo htmlentities($page_var['modelData']['address'],ENT_QUOTES);  ?>

 </div>

 <div class="form-group">
<?php echo form_label($this->lang->line('view').' '.$this->lang->line('image').':'); 
?>

<?php 
if(isset($page_var['modelData']['image']))
{ ?>
<img src="<?php echo base_url() ?>upload/thumbnail/attendee/<?php echo htmlentities($page_var['modelData']['image'],ENT_QUOTES)?>" border="0" >
<?php }else{ ?>

<img src="<?php echo base_url() ?>assets/admin/images/default.jpg" class="width-45" border="0">
<?php } ?>

</div>


<!--div class="form-group">
 
<?php /*echo form_label($this->lang->line('language').':'); 
if(htmlentities($page_var['modelData']['language_id'],ENT_QUOTES) == '1')
  { 
    echo 'English';
   }elseif(htmlentities($page_var['modelData']['language_id'],ENT_QUOTES) == '2'){
    echo 'Indonesia';
   };*/
 ?>

</div-->

<div class="form-group">
<?php echo form_label($this->lang->line('status').':'); 
 if(htmlentities($page_var['modelData']['status'],ENT_QUOTES) == '1')
  { 
    echo 'Active';
   }elseif(htmlentities($page_var['modelData']['status'],ENT_QUOTES) == '0'){
    echo 'Inactive';
   };
   ?>

</div>

            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>

  </div>
<?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>