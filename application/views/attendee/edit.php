<?php if(count($page_var['modelData']) > 0) {?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('attendee'); ?>: <?php echo $page_var['modelData']['name']; ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 
<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

               <?php $hidden = array('id' => $page_var['modelData']['id']); 
                  echo form_open_multipart('attendee/update','',$hidden); ?>


                <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
                }
                ?>
                 <p><span class="error_msg">*</span> 
                <?php  if($this->session->userdata('site_lang') == 'english') {?>
                 marks are mandatory fields.

                <?php  } if($this->session->userdata('site_lang') == 'indonesia') {?>
                    tanda adalah bidang wajib.
                <?php } ?>

                 </p>
               <?php echo validation_errors();

                echo "</div>";
                ?>
<div class="form-group">
<?php
echo form_label($this->lang->line('name')); ?> <span class="error_msg">*</span>
<?php echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo form_input(array('name' => 'name','class'=>'form-control','value'=>$page_var['modelData']['name']));
 ?>  
</div>

<!--div class="form-group">
<?php
//echo form_label($this->lang->line('name').' ('.$this->lang->line('in').' '.$this->lang->line('indonesian') .')'); ?> <span class="error_msg">*</span>
<?php 
 //echo form_input(array('name' => 'name_in_indonesian','class'=>'form-control','value'=>$page_var['modelData']['name_in_indonesian']));
 ?>  
</div-->

<div class="form-group">
<?php
echo form_label($this->lang->line('email')); ?> <span class="error_msg">*</span>
<?php echo form_input(array('name' => 'email','class'=>'form-control','value'=>$page_var['modelData']['email']));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('phone')); ?> <span class="error_msg">*</span>
<?php echo form_input(array('name' => 'phone','class'=>'form-control','value'=>$page_var['modelData']['phone']));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('password')); ?> <span class="error_msg">*</span>
<?php echo form_password(array('name' => 'password','class'=>'form-control','value'=>$page_var['modelData']['password']));
 ?>  
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('gender')); 
$genderoptions = array('f'=>'Female','m'=>'Male');
 echo form_dropdown('gender', $genderoptions, $page_var['modelData']['gender'],'class="form-control"'); ?>

</div>

<div class="form-group correct-height width-4">
<?php echo form_label($this->lang->line('dob')); 

if($page_var['modelData']['dob']=='0000-00-00'){
  echo form_input(array('name' => 'dob','id'=>'dob','class'=>'form-control','value'=>'', 'readonly'=>'true'));
}else{
  echo form_input(array('name' => 'dob','id'=>'dob','class'=>'form-control','value'=>$page_var['modelData']['dob'], 'readonly'=>'true'));
}

  ?>
<input type="button" value="select" id="dobdatepicker" style="display:none">
</div>


<div class="form-group">
 <?php
echo form_label($this->lang->line('address')); 
 echo form_textarea(array('name' => 'address', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>$page_var['modelData']['address']));  ?>

 </div>

 <div class="form-group">
<?php echo form_label($this->lang->line('upload').' '. $this->lang->line('image')); 
echo form_upload(array('name' => 'image'));  ?>

<?php 
if(isset($page_var['modelData']['image']))
{ ?>
<img src="<?php echo base_url() ?>upload/thumbnail/users/<?php echo $page_var['modelData']['image']?>" >
<?php } ?>

</div>


<!--div class="form-group">
 
<?php //echo form_label($this->lang->line('language')); 
//echo form_dropdown('language_id', $page_var['languageData'], $page_var['modelData']['language_id'],'class="form-control"'); ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status'));  
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

 <?php } else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>   

 <?php } ?>

 <script >
 $(function() 
 {   $( "#dobdatepicker" ).datepicker({
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#dob').val(dateText) },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd",
         maxDate: '0' });
 });


 </script>