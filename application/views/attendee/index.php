<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('attendee'); ?><a href="<?php echo base_url(); ?>attendee/create" class="btn btn-default pull-right move-up"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('attendee'); ?></a></h1>
   
                </div>
                <!-- /.col-lg-12 -->
              
            </div>



          <?php 

          if($this->session->flashdata('msg'))  
                echo $this->session->flashdata('msg');

              if($this->session->flashdata('updatemsg'))  
                echo $this->session->flashdata('updatemsg');

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

            if($this->session->flashdata('uploadmsg'))  
                echo $this->session->flashdata('uploadmsg');
          ?>           
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          
                        </div>
                       
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('name'); ?></th>
                                              <th><?php echo $this->lang->line('email'); ?></th>
                                               <th><?php echo $this->lang->line('phone'); ?></th>
                                                <th><?php echo $this->lang->line('registered'); ?> <?php echo $this->lang->line('on'); ?></th>
                                            <th><?php echo $this->lang->line('action'); ?></th>
                                           
                                            
                                        </tr>
                                    </thead>
<!--echo date_format(date_create(htmlentities($usrdetails['createddate'],ENT_QUOTES)),'jS \of F Y'); --> 
                                    
                                    <tbody>
                                    <?php foreach($page_var as $usrdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td><a href="<?php echo base_url(); ?>attendee/details/<?php echo htmlentities($usrdetails['id'],ENT_QUOTES) ?>"><?php echo htmlentities($usrdetails['name'],ENT_QUOTES); ?></a></td>
                                            <td><?php echo htmlentities($usrdetails['email'],ENT_QUOTES); ?></td>
                                            <td><?php echo htmlentities($usrdetails['phone'],ENT_QUOTES); ?></td>
                                            <td><?php echo substr($usrdetails['createddate'],0,10) ; ?></td>
                                            <td><a href="<?php echo base_url(); ?>attendee/edit/<?php echo htmlentities($usrdetails['id'],ENT_QUOTES) ?>"><i class="fa fa-edit"></i></a>
                                            &nbsp;<a href="#" 
                                            <?php if($noaction=='0') { ?>
                                            onClick="show_confirm(<?php echo htmlentities($usrdetails['id'],ENT_QUOTES);?>)" 
                                            <?php } else { ?>
                                            
                                            onClick="javascript: alert('<?php echo $this->lang->line("you")?> <?php echo $this->lang->line("are"); ?> <?php echo $this->lang->line("not"); ?> <?php echo $this->lang->line("allowed"); ?> <?php echo $this->lang->line("to"); ?> <?php echo $this->lang->line("perform"); ?> <?php echo $this->lang->line("the"); ?> <?php echo $this->lang->line("operation"); ?>');"
                                            <?php } ?> ><i class="fa fa-trash"></i></a></td>
                                            
                                           
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div> 

<script type="text/javascript">

        var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
               "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
             "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                  "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

        var currentLang = english;


    $(document).ready(function() {
        var dtable = $('#dataTables-example').DataTable({
                "oLanguage": english,
                responsive: true,
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 4 ] } ],
                 "order": [[ 3, "desc" ]]
        });

         
          $('#langchange').click(function(){
            dtable.fnDestroy();
            dtable = null;
            currentLang = (currentLang == english) ? indonesian : english;
            dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
          });


    });


    function show_confirm(gotoid)
    {
        var r=confirm("<?php echo $this->lang->line('do')?> <?php echo $this->lang->line('you')?> <?php echo $this->lang->line('really')?> <?php echo $this->lang->line('want')?> <?php echo $this->lang->line('to')?> <?php echo $this->lang->line('delete')?>?");

        if(r==true)
        {
          // window.location="<?php echo base_url();?>users/delete/"+gotoid;

          var delpath="<?php echo base_url();?>users/delete";
             $.ajax({

              
              url: delpath, 
              type:'POST',
              data: {'userid': gotoid} ,
              success: function(result){
                if(result ==0)
                {
                    alert('Attendee deleted successfully.');
                    window.location = "<?php echo base_url();?>users";
                }
                 else if(result ==1)
                {
                    alert('Layanan dihapus diperbarui.');
                    window.location = "<?php echo base_url();?>users";
                }
                else if(result ==2)
                {
                  alert("Attendee should not be deleted.User is already ordered a service.");
                  return false;
                }
                else if(result ==3)
                {
                  alert("Layanan seharusnya tidak dihapus.Layanan aku s sudah dipesan sebuah layanan.");
                  return false;
                }
              }});

        }
    }

 </script>