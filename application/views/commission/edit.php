<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('commission'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open('commission/update','',$hidden); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();
            echo "</div>";

            $perentagetype = "";
            $fixedtype = "";
            if($page_var['modelData']['type'] ==1)
            {
                $perentagetype = "checked";
                $fixedtype="";
            }
             elseif($page_var['modelData']['type'] ==2)
             {
                 $perentagetype = "";
                $fixedtype="checked";
             }
            ?>

            <div class="form-group">
                <label><?php
echo form_label($this->lang->line('type')); ?></label>&nbsp;&nbsp;

                <label class="radio-inline">
                    <input type="radio" name="type" id="optionspercentage" value="1" <?php echo $perentagetype ?> ><?php
echo form_label($this->lang->line('Percentage')); ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="type" id="optionsfixed" value="2" <?php echo $fixedtype ?> ><?php
echo form_label($this->lang->line('Fixed')); ?>
                </label>
                                            
            </div>

 <div class="form-group inline-ech"> 
<?php echo form_label($this->lang->line('commission')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id']));
 echo form_input(array('name' => 'commission','value'=> htmlentities($page_var['modelData']['commission'],ENT_QUOTES),'class'=>'form-control box-leftech', 'style'=>'width:150px'));?>
 
 <div class="text-left2">

 </div>
</div>
    <div class="clearfix"></div>
   <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 

 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>  

    <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>    


 <script>

 function checkcommissiontype()
 {
    $('.text-left2').empty();

  if($("input[name='type']:checked").val() ==1)
        $('.text-left2').append('%');
    else if($("input[name='type']:checked").val() ==2)
        $('.text-left2').append("<?php echo $page_var['currencyData']['currency_code']; ?>");
 }

  $(document).ready(function(){
     checkcommissiontype();
  
 });

 
 $("input[type='radio']").click(function(){

    checkcommissiontype();
 });

 </script>   