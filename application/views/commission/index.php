<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('commission'); ?> <?php echo $this->lang->line('management'); ?> </h1>

                    
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

          if($this->session->flashdata('msg'))  
                echo $this->session->flashdata('msg');

              if($this->session->flashdata('updatemsg'))  
                echo $this->session->flashdata('updatemsg');

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');
          ?>           
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                       
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('commission'); ?> </th>
                                             <th><?php echo $this->lang->line('type'); ?></th>
                                            <th><?php echo $this->lang->line('action'); ?></th>
                                           
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php foreach($page_var as $commissiondetails) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo htmlentities($commissiondetails['commission'],ENT_QUOTES); ?></td>
                                            <td><?php if($commissiondetails['type']==1){ echo form_label($this->lang->line('Percentage'));}elseif($commissiondetails['type']==2){ echo form_label($this->lang->line('Fixed'));} ?></td>
                                            <td><a href="<?php echo base_url(); ?>commission/edit/<?php echo $commissiondetails['id'] ?>"><i class="fa fa-edit"></i></a></td>
                                            
                                           
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div> 

<script type="text/javascript">
     
     function show_confirm(gotoid)
    {
        var r=confirm("<?php echo $this->lang->line('do')?> <?php echo $this->lang->line('you')?> <?php echo $this->lang->line('really')?> <?php echo $this->lang->line('want')?> <?php echo $this->lang->line('to')?> <?php echo $this->lang->line('delete')?>?");

        if(r==true)
        {
           window.location="<?php echo base_url();?>servicetype/delete/"+gotoid;

        }
    }

 </script>