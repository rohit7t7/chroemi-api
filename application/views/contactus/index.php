<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('contact'); ?> <?php echo $this->lang->line('us'); ?></h1>

                    
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

          if($this->session->flashdata('msg'))  
                echo $this->session->flashdata('msg');

              if($this->session->flashdata('updatemsg'))  
                echo $this->session->flashdata('updatemsg');

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');
          ?>           
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                       
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th> <?php echo $this->lang->line('name'); ?></th>
                                             <th><?php echo $this->lang->line('email'); ?> </th>
                                              <th><?php echo $this->lang->line('phone'); ?> </th>
                                              <th style="width:70px"><?php echo $this->lang->line('contact'); ?> <?php echo $this->lang->line('on'); ?> </th>
                                              <th><?php echo $this->lang->line('message'); ?> </th>
                                            
                                          
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                        <?php foreach($page_var as $contactdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo htmlentities($contactdetails['name'],ENT_QUOTES); ?></td>
                                             <td><?php echo htmlentities($contactdetails['email'],ENT_QUOTES); ?></td>
                                             <td><?php echo htmlentities($contactdetails['phone'],ENT_QUOTES); ?></td>
                                             <td><?php echo $contactdetails['contactdate']; ?></td>
                                            <td><?php echo htmlentities($contactdetails['message'],ENT_QUOTES); ?></td>
                                            
                                           
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div> 

<script type="text/javascript">
        
    var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
               "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                  "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;
                    
               $(document).ready(function() {
                    $('#dataTables-example').DataTable({
                             "oLanguage": english,
                            responsive: true,
                            "aoColumnDefs": [ { "bSortable": false } ],
                             "order": [[ 3, "desc" ]]
                    });

                     $('#langchange').click(function(){
                        dtable.fnDestroy();
                        dtable = null;
                        currentLang = (currentLang == english) ? indonesian : english;
                        dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
                      });
                });

   
 </script>