<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">

                    <h1 class="page-header"> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('content'); ?></h1>

                  

                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
       

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">


                <?php echo form_open_multipart('content/create'); ?>
                <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
                }
                echo validation_errors();

                echo "</div>";
              ?>

<div class="form-group">
<?php
echo form_label($this->lang->line('page').' '.$this->lang->line('name')); 
 echo form_hidden(array('page_name' => 'id')); 
 echo form_input(array('name' => 'page_name','class'=>'form-control','value'=>set_value('page_name')));
 ?>  
</div>
<div class="form-group">
<?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('title')); 

 echo form_input(array('name' => 'meta_title','class'=>'form-control','value'=>set_value('meta_title')));
 ?>  
</div>





<div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('keywords')); 
 echo form_textarea(array('name' => 'meta_keywords', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','id'=>'meta_keywords','value'=>set_value('meta_keywords')));  ?>

 </div>

 

 <div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('description')); 
 echo form_textarea(array('name' => 'meta_description', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>set_value('meta_description')));  ?>

 </div>
 <div class="form-group">
 <?php
echo form_label($this->lang->line('content')); 
 echo form_textarea(array('name' => 'content', 'rows'=> '10',
      'cols'=> '10','class'=>'form-control','id'=>'contenttext','value'=>set_value('content')));  ?>

 </div>

<!--div class="form-group">
 
<?php /*echo form_label($this->lang->line('language')); 
echo form_dropdown('language_id', $page_var['languageData'], set_value('language_id'),'class="form-control"'); */?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','0'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>
 <script type='text/javascript' src='<?php echo base_url(); ?>assets/admin/js/nicEdit-latest.js'></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function() {
   // new nicEditor().panelInstance('area1');
    new nicEditor({fullPanel : true}).panelInstance('contenttext');
    //new nicEditor({iconsPath : '../js/nicEditorIcons.gif'}).panelInstance('contentmanagementsystem-description');
    //new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('contentmanagementsystem-description');
    //new nicEditor({maxHeight : 100}).panelInstance('area5');
    new nicEditor({uploadURI:"http://yourdomain.com/nicUpload.php"});

    function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // only relatively recently standardized and is not supported in
            // some browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}

});

    /*submit : function(e) {
    var url = this.inputs['href'].value;
    if(url == "http://" || url == "") {
        alert("Introduce una URL valida para crear el Link.");
        return false;
    }
    this.removePane();

    if(!this.ln) {
        //**************** YOUR CHANGE WITH A BIT OF VARIATION **************
        var selected = this.getSelected();
        var tmp = 'javascript:void(0)';
        if (selected == '') {
            tmp = url;
            this.pasteHtmlAtCaret(this.inputs['title'].value || tmp, true);
        }
        //**************** END OF YOUR CHANGE WITH A BIT OF VARIATION **************

        this.ne.nicCommand("createlink",tmp);
        this.ln = this.findElm('A','href',tmp);

        // set the link text to the title or the url if there is no text selected
        if (this.ln.innerHTML == tmp) {
            this.ln.innerHTML = this.inputs['title'].value || url;
        };
    }

    if(this.ln) {
        var oldTitle = this.ln.title;
        this.ln.setAttributes({
            href: this.inputs['href'].value,
            title: this.inputs['title'].value,
            target: '_blank'
        });
        // set the link text to the title or the url if the old text was the old title
        if (this.ln.innerHTML == oldTitle) {
            this.ln.innerHTML = this.inputs['title'].value || this.inputs['href'].value;
        };
    }
}*/

function getSelected()
{
    if (document.selection)
        return document.selection.createRange().text;
    else
        return window.getSelection();
}

    function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            
            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}
</script>

 