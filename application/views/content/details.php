<?php if(count($page_var['modelData']) > 0) {?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">

                    <h1 class="page-header"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('content'); ?> <?php echo $this->lang->line('details'); ?></h1>

                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">


<div class="form-group">
  <?php
echo form_label($this->lang->line('page').' '.$this->lang->line('name').':');
 echo form_hidden(array('page_name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo htmlentities($page_var['modelData']['page_name'],ENT_QUOTES);
 ?> 
 
</div>
<div class="form-group">
<?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('title').':');

 echo htmlentities($page_var['modelData']['meta_title'],ENT_QUOTES);
 ?>  
</div>





<div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('keywords').':'); 
 echo htmlentities($page_var['modelData']['meta_keywords'],ENT_QUOTES);  ?>

 </div>

 

 <div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('description').':'); 
 echo htmlentities($page_var['modelData']['meta_description'],ENT_QUOTES);  ?>

 </div>
 <div class="form-group">
 <?php
echo form_label($this->lang->line('content').':'); ; 
 echo $page_var['modelData']['content'];  ?>

 </div>
 <!--div class="form-group">
 
<?php /*echo form_label($this->lang->line('language').':'); 
if($page_var['modelData']['language_id'] == '1')
  { 
    echo 'English';
   }else{
    echo 'Indonesia';
   };*/
 ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status').':'); 
 if($page_var['modelData']['status'] == '1')
  { 
    echo 'Active';
   }else{
    echo 'Inactive';
   };
   ?>

</div>

            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>