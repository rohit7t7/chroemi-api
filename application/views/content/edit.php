<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('content'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  
<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
              <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open_multipart('content/edit/'.$page_var['modelData']['id'],'',$hidden); ?>


              <?php
              echo "<div class='error_msg'>";
              if (isset($error_message)) {
              echo $error_message;
              }
              echo validation_errors();

              echo "</div>";
              ?>

<div class="form-group">
  <?php
echo form_label($this->lang->line('page').' '.$this->lang->line('name')); 
 echo form_hidden(array('page_name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo form_input(array('name' => 'page_name','class'=>'form-control','value'=>$page_var['modelData']['page_name']));
 ?> 
 
</div>
<div class="form-group">
<?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('title')); 

 echo form_input(array('name' => 'meta_title','class'=>'form-control','value'=>htmlentities($page_var['modelData']['meta_title'],ENT_QUOTES),'readonly'=>true));
 ?>  
</div>





<div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('keywords')); 
 echo form_textarea(array('name' => 'meta_keywords', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>htmlentities($page_var['modelData']['meta_keywords'],ENT_QUOTES)));  ?>

 </div>

 

 <div class="form-group">
 <?php
echo form_label($this->lang->line('meta').' '.$this->lang->line('description')); 
 echo form_textarea(array('name' => 'meta_description', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>htmlentities($page_var['modelData']['meta_description'],ENT_QUOTES)));  ?>

 </div>
 <div class="form-group">
 <?php
echo form_label($this->lang->line('content')); 
 echo form_textarea(array('name' => 'content', 'rows'=> '10',
      'cols'=> '10','class'=>'form-control','id'=>'contenttext','value'=> html_entity_decode($page_var['modelData']['content'])));  ?>

 </div>

<!--div class="form-group">
 
<?php /*echo form_label($this->lang->line('language')); 
echo form_dropdown('language_id', $page_var['languageData'], $page_var['modelData']['language_id'],'class="form-control"');*/ ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','0'=>'Inactive');
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

    <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

 <script type='text/javascript' src='<?php echo base_url(); ?>/assets/admin/js/nicEdit-latest.js'></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function() {
   
    new nicEditor({fullPanel : true}).panelInstance('contenttext');
    
});
</script>