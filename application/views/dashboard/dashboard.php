 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('dashboard'); ?> <?//php echo $content; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $page_var['usercount']; ?></div>
                                    <div><?php echo $this->lang->line('number'); ?> <?php echo $this->lang->line('of'); ?> <?php echo $this->lang->line('attendee'); ?></div>
                                </div>
                            </div>
                        </div>
                        <a href="users">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('details'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $page_var['servicecount']; ?></div>
                                    <div><?php echo $this->lang->line('number'); ?> <?php echo $this->lang->line('of'); ?> <?php echo $this->lang->line('event'); ?></div>
                                </div>
                            </div>
                        </div>
                        <a href="services">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('details'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                

                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $page_var['categorycount']; ?></div>
                                    <div><?php echo $this->lang->line('number'); ?> <?php echo $this->lang->line('of'); ?> <?php echo $this->lang->line('category'); ?></div>
                                </div>
                            </div>
                        </div>
                        <a href="servicecategory">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('details'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $page_var['supervisordetcount']; ?></div>
                                    <div><?php echo $this->lang->line('number'); ?> <?php echo $this->lang->line('of'); ?> <?php echo $this->lang->line('organizer'); ?></div>
                                </div>
                            </div>
                        </div>
                        <a href="supervisors">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('details'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>  
            </div>
    </div>

