<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('credentials'); ?> </h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php echo form_open('emailcredentials/create',array('id' => 'credentialform')); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($page_var['error_message'])) {
            echo $page_var['error_message'];
            }
            echo validation_errors();

            echo "</div>";
            ?>

 <div class="form-group">
<?php
echo form_label($this->lang->line('user').' '. $this->lang->line('name')); 
 echo form_input(array('name' => 'username','class'=>'form-control', 'id'=>'username', 'value'=>set_value('username'))); ?>
</div>

 <div class="form-group">
<?php
echo form_label($this->lang->line('password')); 
 echo form_password(array('name' => 'password','class'=>'form-control', 'id'=>'password', 'value'=>set_value('password'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('host')); 
 echo form_input(array('name' => 'host','class'=>'form-control', 'id'=>'host','value'=>set_value('host'))); ?>
</div>

 <div class="form-group">
<?php
echo form_label($this->lang->line('port')); 
 echo form_input(array('name' => 'port','class'=>'form-control',  'id'=>'port','value'=>set_value('port'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('smtpsecure')); 
 echo form_input(array('name' => 'smtpsecure','class'=>'form-control', 'id'=>'smtpsecure', 'value'=>set_value('smtpsecure'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('from').' '.$this->lang->line('user')); 
 echo form_input(array('name' => 'fromuser','class'=>'form-control',  'id'=>'fromuser',  'value'=>set_value('fromuser'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('from').' '.$this->lang->line('user') .' '.$this->lang->line('name')); 
 echo form_input(array('name' => 'fromname','class'=>'form-control',  'id'=>'fromname',  'value'=>set_value('fromname'))); ?>
</div>



<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div> 
 </div>  

 <script>

 $( "#credentialform" ).submit(function( event ) {

    
  
  
 
});

 </script>     


