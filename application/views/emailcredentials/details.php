<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('credentials'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

 <div class="form-group"> 
<?php echo form_label($this->lang->line('user').' '. $this->lang->line('name').' : ');
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
echo $page_var['modelData']['username']; ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('password').' : ');
 //echo htmlentities($page_var['modelData']['password'],ENT_QUOTES); 
  echo "******";
?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('host').' : ');
 echo htmlentities($page_var['modelData']['host'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('port').' : ');
 echo htmlentities($page_var['modelData']['port'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('smtpsecure').' : ');

 echo htmlentities($page_var['modelData']['smtpsecure'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('from').' '.$this->lang->line('user').' : ');
 echo htmlentities($page_var['modelData']['fromuser'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('from').' '.$this->lang->line('name').' : ');
 echo htmlentities($page_var['modelData']['fromname'],ENT_QUOTES); ?>
</div>

 
<div class="form-group">
<?php echo form_label($this->lang->line('status').' : '); 
 if($page_var['modelData']['status'] == '1')
  { 
    echo 'Active';
   }else{
    echo 'Inactive';
   };
   ?>

</div>
 
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>    
      
  <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>