<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('credentials'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open('emailcredentials/update',array('id' => 'credentialform'),$hidden); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();
            echo "</div>";
            ?>

 <div class="form-group"> 

 <?php 
echo form_label($this->lang->line('name')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
  echo form_input(array('name' => 'username','class'=>'form-control', 'id'=>'username', 'value'=>htmlentities($page_var['modelData']['username'],ENT_QUOTES))); 

  ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('password'));
 echo form_password(array('name' => 'password','value'=>htmlentities($page_var['modelData']['password'],ENT_QUOTES),'class'=>'form-control','id'=>'password'));
 // htmlentities($page_var['modelData']['serverkey'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('host'));
 echo form_input(array('name' => 'host','value'=>htmlentities($page_var['modelData']['host'],ENT_QUOTES),'class'=>'form-control','id'=>'host')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('port'));
 echo form_input(array('name' => 'port','value'=>htmlentities($page_var['modelData']['port'],ENT_QUOTES),'class'=>'form-control','id'=>'port')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('smtpsecure'));

 echo form_input(array('name' => 'smtpsecure','value'=>htmlentities($page_var['modelData']['smtpsecure'],ENT_QUOTES),'class'=>'form-control','id'=>'smtpsecure')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('from').' '.$this->lang->line('user'));
 echo form_input(array('name' => 'fromuser','value'=>htmlentities($page_var['modelData']['fromuser'],ENT_QUOTES),'class'=>'form-control','id'=>'fromuser')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('from').' '.$this->lang->line('name'));
 echo form_input(array('name' => 'fromname','value'=>htmlentities($page_var['modelData']['fromname'],ENT_QUOTES),'class'=>'form-control','id'=>'fromname')); ?>
</div>

 
 <div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
    
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>
 </div>
  <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 

 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>         

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

<script>

 $( "#credentialform" ).submit(function( event ) {

    if($('#paymenttype').val() == 1 || $('#paymenttype').val() == 3)
    {
        if($('#serverkey').val()=='')
        {
            alert("Please enter Serverkey");
            return false;
        }
        else if($('#clientkey').val()=='')
        {
            alert("Please enter Clientkey");
            return false;
        }
        else
            $('form').submit();
    }

     if($('#paymenttype').val() == 2)
    {
        if($('#apiusername').val()=='')
        {
            alert("Please enter Api User Name");
            return false;
        }
        else if($('#apipassword').val()=='')
        {
            alert("Please enter Api Password");
            return false;
        }
         else if($('#apisignature').val()=='')
        {
            alert("Please enter Api Signature");
            return false;
        }
        else
             $('form').submit();
    }
  
  
 
});

 </script> 