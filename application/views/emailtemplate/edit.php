<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('Template'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open('emailtemplate/update',$hidden); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();
            echo "</div>";
            ?>
<div class="form-group"> 
<input type="hidden" name="id" value="<?php echo $page_var['modelData']['id']; ?>" /> 
 <?php 
echo form_label($this->lang->line('section')).'<br>'; 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
  //echo form_input(array('name' => 'subject','class'=>'form-control', 'id'=>'section', 'value'=>htmlentities($page_var['modelData']['section'],ENT_QUOTES))); 
echo $page_var['modelData']['section'];
  ?>
</div>
 <div class="form-group"> 

 <?php 
echo form_label($this->lang->line('subject')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
  echo form_input(array('name' => 'subject','class'=>'form-control', 'id'=>'subject', 'value'=>htmlentities($page_var['modelData']['subject'],ENT_QUOTES))); 

  ?>
</div>
<div class="form-group">
 <?php
echo form_label($this->lang->line('body')); 
 echo form_textarea(array('name' => 'body', 'rows'=> '10',
      'cols'=> '10','class'=>'form-control','id'=>'contenttext','value'=>html_entity_decode($page_var['modelData']['body'])));  ?>

 </div>


 
 
  <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 

 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>         

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>
<script type='text/javascript' src='/gomandor/assets/admin/js/nicEdit-latest.js'></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function() {
   
    new nicEditor({fullPanel : true}).panelInstance('contenttext');
    
});
</script>

