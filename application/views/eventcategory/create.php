<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('event'); ?> <?php echo $this->lang->line('category'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php echo form_open_multipart('eventcategory/create'); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();

            echo "</div>";
            ?>

 <div class="form-group">           
<?php echo form_label($this->lang->line('category').' '. $this->lang->line('name')); 
 echo form_hidden(array('name' => 'id')); 
 echo form_input(array('name' => 'category','class'=>'form-control','value'=>set_value('category'))); ?>
 </div> 

 <!--<div class="form-group">           
<?php //echo form_label($this->lang->line('parent').' '. $this->lang->line('category')); 
 //echo form_dropdown('parent_id',  $page_var['servicecategoryData'], set_value('parent_id'),'class="form-control"'); ?>

 </div> --> 
 <div class="form-group">
<?php echo form_label($this->lang->line('upload').' '.$this->lang->line('service').' '.$this->lang->line('category').' '.$this->lang->line('image')); 
echo form_upload(array('name' => 'imageavatar'));  ?>

</div>
 <!--div class="form-group">
 <?php
 //echo form_label($this->lang->line('language')); 
//echo form_dropdown('language_id', $page_var['languageData'], set_value('language_id'),'class="form-control"'); ?>
</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

 </div>
 <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div> 
 </div>     