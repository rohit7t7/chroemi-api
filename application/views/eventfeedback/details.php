<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('feedback'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

<div class="form-group">
<?php
echo form_label($this->lang->line('view').' '. $this->lang->line('service').':'); 
 $allservicelist = $this->servicesmodel->getById($page_var['modelData']['service_id']);
echo $allservicelist['servicename'];
   ?>
</div>


<div class="form-group">
<?php 

echo form_label($this->lang->line('user').' '. $this->lang->line('name').':'); 
$alluserdetails = $this->usersmodel->getById($page_var['modelData']['user_id']);
echo $alluserdetails['name']; 

?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('rating').':'); 
 echo htmlentities($page_var['modelData']['rating'],ENT_QUOTES) ;
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('feedback').':'); 
 echo htmlentities($page_var['modelData']['feedback'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('created').' '. $this->lang->line('date').':'); 
 echo substr(htmlentities($page_var['modelData']['date'],ENT_QUOTES) ,0,10);
 ?>  
</div>

            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>