<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('feedback'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
              echo form_open('servicefeedback/update','',$hidden); ?>
              <?php
              echo "<div class='error_msg'>";
              if (isset($error_message)) {
              echo $error_message;
              }
              echo validation_errors();

              echo "</div>";
              ?>

<div class="form-group">
<?php
echo form_label($this->lang->line('service').' '.$this->lang->line('name')); 
if(array_key_exists($page_var['modelData']['service_id'],$page_var['serviceData']))
echo form_dropdown('service_id', $page_var['serviceData'], $page_var['modelData']['service_id'],'class="form-control"'); 
else
{
  $allservicedetails = array_merge(array(""=>""),$page_var['serviceData']);
  echo form_dropdown('service_id', $allservicedetails, "",'class="form-control"'); 
}

?>
</div>


<div class="form-group">
<?php 

echo form_label($this->lang->line('user').' '.$this->lang->line('name')); 

if(array_key_exists($page_var['modelData']['user_id'],$page_var['userData']))
  echo form_dropdown('user_id', $page_var['userData'], $page_var['modelData']['user_id'],'class="form-control"'); 
else
{
  $alluserdetails = array_merge(array(""=>""),$page_var['userData']);
  echo form_dropdown('user_id', $alluserdetails, "",'class="form-control"'); 
}
?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('rating')); 
 echo form_input(array('name' => 'rating','class'=>'form-control','value'=>htmlentities($page_var['modelData']['rating'],ENT_QUOTES)));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('feedback')); 
 echo form_input(array('name' => 'feedback','class'=>'form-control','value'=>htmlentities($page_var['modelData']['feedback'],ENT_QUOTES)));
 ?>  
</div>


<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>