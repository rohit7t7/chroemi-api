<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('service'); ?> <?php echo $this->lang->line('orders'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

               if($this->session->flashdata('confirmmsg'))  
                echo $this->session->flashdata('confirmmsg');

               if($this->session->flashdata('cancelmsg'))  
                echo $this->session->flashdata('cancelmsg');
          ?>   
           <div class="row">
              <div class="col-lg-12">
                  <?php if($this->uri->segment(2)=='latestorder') { echo form_open('serviceorder/latestorder',array('id' => 'serviceorderform'));}elseif($this->uri->segment(2)=='allorder') { echo form_open('serviceorder/allorder',array('id' => 'serviceorderform'));}?>
                 
                 <input type="hidden" name="searchtype" id="searchtype" value="<?php echo $this->uri->segment(2)?>">

                
                  <div class="correct-height width-3"><?php $allcat =  $page_var['servicecategoryData']; echo form_label($this->lang->line('service') .' '. $this->lang->line('category')); echo form_dropdown('categoryid', $allcat, set_value('categoryid'),'class="form-control" style="width:100%" id="categoryid" onchange="this.form.submit()"'); ?></div>
                  <div class="correct-height width-3"><?php $alltype = $page_var['servicetypeData'];echo form_label($this->lang->line('service').' '. $this->lang->line('type')); echo form_dropdown('typeid', $alltype, set_value('typeid'),'class="form-control" style="width:100%" id="typeid" onchange="this.form.submit()"'); ?></div>
                  <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('from')); echo form_input(array('name' => 'startdate','id'=>'startdate','class'=>'form-control','value'=>set_value('startdate')));  ?>
                    <input type="button" value="select" id="startdatepicker" style="display:none"></div>
                  <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('to')); echo form_input(array('name' => 'enddate','id'=>'enddate','class'=>'form-control','value'=>set_value('enddate')));  ?>
                    <input type="button" value="select" id="enddatepicker" style="display:none"></div>
                 <div class="correct-height width-3 last-btn"><input type="button" value="<?php echo $this->lang->line('reset')?>" onclick="resetvalue()" style="position:relative;left:0px;"></div>
                 <?php echo form_close(); ?> 
              </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                     <div class="panel-heading">
                          
                        </div>
                                                            
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive chg-scroll">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('invoice'); ?> <?php echo $this->lang->line('id'); ?></th>
                                            
                                            <th><?php echo $this->lang->line('service'); ?></th>
                                            <th><?php echo $this->lang->line('service'); ?> <?php echo $this->lang->line('type'); ?></th>
                                            <th><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('info'); ?></th>
                                            <th><?php echo $this->lang->line('order'); ?> <?php echo $this->lang->line('date'); ?></th>
                                             <th><?php echo $this->lang->line('status'); ?></th>
                                             <th><?php echo $this->lang->line('action'); ?></th>
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php foreach($page_var['serviceorder'] as $orderdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td>#<?php echo htmlentities($orderdetails['invoiceid'],ENT_QUOTES); ?></td>
                                             <td><?php $srvarr = $this->servicesmodel->getById($orderdetails['service_id']); echo htmlentities($srvarr['servicename'],ENT_QUOTES); ?></td>
                                             <td><?php echo htmlentities($orderdetails['servicetype'],ENT_QUOTES); ?></td>
                                             <td><button type="button" class="btn btn-info" data-toggle="modal"  style="width: 80px; height: 20px;font-size:10px;text-align:center;padding: 0px;"  data-target="#myModal_<?php echo $orderdetails['id'] ?>"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('info'); ?></button></td>
                                              <td><?php echo $orderdetails['order_date'] ?></td>

                                             <td><?php 
                                                if($orderdetails['order_status'] ==0)
                                                 { 
                                                    echo "<font color='#F1C40F'><b>Pending</b></font>";
                                                 } 
                                                 elseif($orderdetails['order_status'] ==1)
                                                 { 
                                                    echo "<font color='#1E8449'><b>Confirmed</b></font>";
                                                 } 
                                                 elseif($orderdetails['order_status'] ==2)
                                                 { 
                                                    echo "<font color='#CD6155'><b>Cancelled</b></font>";
                                                 }   

                                              ?>

                                              </td>


                                            <td>
                                             <?php if($orderdetails['order_status'] ==0){ ?>
                                              <a href="#" onClick="show_confirm(<?php echo $orderdetails['id'];?>)"><b><?php echo $this->lang->line('confirm'); ?></b></a>&nbsp;&nbsp;
                                              <?php } if($orderdetails['order_status'] ==0 || $orderdetails['order_status'] ==1){?>
                                              <a href="#" onClick="show_cancel(<?php echo $orderdetails['id'];?>)"><b><?php echo $this->lang->line('cancel'); ?></b></a>
                                              <?php } ?>
                                              </td>
                                            
                                           
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>

                            
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
  

 <?php foreach($page_var['serviceorder'] as $orderdetails) { ?>
  <!-- Modal -->
  <div class="modal fade" id="myModal_<?php echo $orderdetails['id'] ?>" role="dialog">
    <div class="modal-dialog">

     <!-- Modal content-->
      <div class="modal-content">
        
    
    <?php if($orderdetails['typeid']=='2' || $orderdetails['typeid']=='4') {?>
     
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('details'); ?></h4>
        </div>
        
        <div class="modal-body">
          <table border="0" style="width:100%">
              <tr><td></td><td></td><td></td><td></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('user'); ?> <?php echo $this->lang->line('name'); ?></td><td colspan="2"><?php $usrarr = $this->usersmodel->getById($orderdetails['user_id']); echo htmlentities($usrarr['name'],ENT_QUOTES);?></td></td>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('name'); ?></td> <td colspan="2"><?php echo htmlentities($orderdetails['billing_name'],ENT_QUOTES) ; ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('address'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_email_address'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('address'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_street_address'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('city'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_city'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('state'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_state'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('postcode'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_postcode'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('country'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_country'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('phone'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_phone'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('fax'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_fax'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('type'); ?></td><td colspan="2"><?php 
                        
                       if($orderdetails['paymenttype']=='M')
                          { echo "Mandiri ClickPay" ; }
                        elseif($orderdetails['paymenttype']=='B')
                          { echo "BCA bank transfer" ;}
                        elseif($orderdetails['paymenttype']=='C')
                          {echo "Credit Card" ;}

                        ?></td></tr>
          </table>

        </div>
        
      

    <?php } else {?>

          <div class="modal-body">
            <b><?php echo $this->lang->line('billing')?> <?php echo $this->lang->line('details')?> <?php echo $this->lang->line('not') ?> <?php echo $this->lang->line('available') ?> <?php echo $this->lang->line('for') ?> <?php echo $this->lang->line('this')?> <?php echo $this->lang->line('service'); ?></b>
        </div>
    <?php } ?>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('close'); ?></button>
        </div>
      
      </div>
    </div>
  </div>

  <?php } ?>

</div>

<script type="text/javascript">
     
     var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
              "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                 "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + "0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

           $(document).ready(function() {
                $('#dataTables-example').DataTable({
                         "oLanguage": english,
                        responsive: true,
                        "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 6 ] } ],
                        "order": [[ 4, "desc" ]]
                });

                 $('#langchange').click(function(){
                  dtable.fnDestroy();
                  dtable = null;
                  currentLang = (currentLang == english) ? indonesian : english;
                  dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
                });

                
            });

    function show_confirm(gotoid)
    {

       var r=confirm("<?php echo $this->lang->line('do')?> <?php echo $this->lang->line('you')?> <?php echo $this->lang->line('really')?> <?php echo $this->lang->line('want')?> <?php echo $this->lang->line('to')?> <?php echo $this->lang->line('confirm')?> <?php echo $this->lang->line('the')?> <?php echo $this->lang->line('order')?>?");
       //var searchtype = $('#searchtype').val();
       
        if(r==true)
        {
           window.location="<?php echo base_url();?>serviceorder/confirm/"+gotoid;

        }
    }

     function show_cancel(gotoid)
    {
        var r=confirm("<?php echo $this->lang->line('do')?> <?php echo $this->lang->line('you')?> <?php echo $this->lang->line('really')?> <?php echo $this->lang->line('want')?> <?php echo $this->lang->line('to')?> <?php echo $this->lang->line('cancel')?> <?php echo $this->lang->line('the')?> <?php echo $this->lang->line('order')?>?");
        var searchtype = $('#searchtype').val();

        if(r==true)
        {
           window.location="<?php echo base_url();?>serviceorder/cancel/"+gotoid;

        }
    }

    function resetvalue()
    {
      $('#categoryid').val('');
      $('#typeid').val('');
      $('#startdate').val('');
      $('#enddate').val('');
      $('#serviceorderform').submit();

    }

    $(function() 
   {   $( "#startdatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','maxDate', jQuery('#enddate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#startdate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });

  $(function() 
 {   $( "#enddatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','minDate', jQuery('#startdate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#enddate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });



   
 </script>