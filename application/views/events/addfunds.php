<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('fund'); ?>&nbsp;<?php echo $this->lang->line('of'); ?>&nbsp;<?php echo $this->lang->line('event'); ?> : <?php echo $page_var['funds'][0]['name']; ?></h1>
                     
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php echo form_open_multipart('events/addfund/'.$page_var['id']); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();

            echo "</div>";
            ?>

 <div class="form-group">           
<?php echo form_label($this->lang->line('fund').' '. $this->lang->line('amount')); 
 
 echo form_input(array('name' => 'fund_amount','class'=>'form-control')); ?>
 </div> 
<div class="form-group">
<?php
echo form_label($this->lang->line('currency')); 
echo form_dropdown('currency_id', $page_var['currency'], set_value('currency_id'),'class="form-control"'); ?>
</div>
 <!--<div class="form-group">           
<?php //echo form_label($this->lang->line('parent').' '. $this->lang->line('category')); 
 //echo form_dropdown('parent_id',  $page_var['servicecategoryData'], set_value('parent_id'),'class="form-control"'); ?>

 </div> --> 

 <!--div class="form-group">
 <?php
 //echo form_label($this->lang->line('language')); 
//echo form_dropdown('language_id', $page_var['languageData'], set_value('language_id'),'class="form-control"'); ?>
</div-->

 <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
  echo form_hidden('fundadd','1');
 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div> 
 </div>     