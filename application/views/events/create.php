<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('event'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php echo form_open_multipart('events/create',array('id' => 'serviceform')); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();

            echo "</div>";
            ?>

<div class="form-group">
<?php
echo form_label($this->lang->line('event').' '. $this->lang->line('category')); 
echo form_dropdown('categoryid', $page_var['servicecategoryData'], set_value('categoryid'),'class="form-control"'); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('event').' '. $this->lang->line('name')); 
 echo form_hidden(array('name' => 'id')); 
 echo form_input(array('name' => 'servicename','class'=>'form-control','value'=>set_value('servicename')));
 ?>  
</div>
<div class="form-group">
 <?php
echo form_label($this->lang->line('description')); 
 echo form_textarea(array('name' => 'description', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>set_value('description')));  ?>

 </div>

 <div class="form-group">
 <?php
echo form_label($this->lang->line('address')); 
 echo form_textarea(array('name' => 'address', 'rows'=> '2',
      'cols'=> '7','class'=>'form-control','value'=>set_value('address')));  ?>

 </div>

<div class="form-group other-date">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('start').' '.$this->lang->line('date')); 
echo form_input(array('name' => 'availabilitystartdate','id'=>'availabilitystartdate','class'=>'form-control','value'=>set_value('availabilitystartdate')));  ?>
<input type="button" value="select" id="startdatepicker" style="display:none">
</div>
<div class="form-group other-date">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('end').' '.$this->lang->line('date')); 
echo form_input(array('name' => 'availabilityenddate','id'=>'availabilityenddate','class'=>'form-control','value'=>set_value('availabilityenddate')));  ?>
<input type="button" value="select" id="enddatepicker" style="display:none">
</div>
<div class="form-group">
<?php echo form_label($this->lang->line('features')); 
echo form_textarea(array('name' => 'features','class'=>'form-control','value'=>set_value('features')));  ?>
</div>

 <!--<div class="form-group">
<?php //echo form_label($this->lang->line('cost')); 
//echo form_input(array('name' => 'cost','class'=>'form-control','value'=>set_value('cost')));  ?>
</div>-->

<!-- <div class="form-group">
<?php 

//echo form_label($this->lang->line('currency')); 
//echo form_dropdown('currency_id', $page_var['currencyData'], $page_var['defaultcurrency']['id'],'class="form-control" id="currency_id"'); 
?>
</div>-->

<!--<div class="form-group">
<?php //echo form_label($this->lang->line('has').' '.$this->lang->line('discount')); ?>  &nbsp;&nbsp;

<?php //echo form_checkbox('hasdiscount', 'hasdiscount', FALSE);?>
</div>-->

<!--<div class="form-group" id="discountdiv" >

<?php //echo form_label($this->lang->line('discount')); ?> <b>(%)</b>
<?php //echo form_input(array('name' => 'discount','class'=>'form-control','value'=>set_value('discount'),'id'=>'discount')); ?>

</div> -->

<!--<div class="form-group" id="discountexpirydiv" >

<?php //echo form_label($this->lang->line('discount').' '.$this->lang->line('expiry').' '.$this->lang->line('date')); 
//echo form_input(array('name' => 'discountex_date','id'=>'discountex_date','class'=>'form-control','value'=>set_value('discountex_date'))); ?>

</div> --> 

<div class="form-group">
<?php echo form_label($this->lang->line('upload').' '.$this->lang->line('event').' '.$this->lang->line('icon')); 
echo form_upload(array('name' => 'imageavatar'));  ?>

</div>

<!--div class="form-group">
<?php //echo form_label($this->lang->line('upload').' '.$this->lang->line('image')); 
 //echo form_upload('image[]','','multiple'); ?>

</div-->
<!--div class="form-group">
 
<?php /*echo form_label($this->lang->line('language')); 
echo form_dropdown('language_id', $page_var['languageData'], set_value('language_id'),'class="form-control"');*/ ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

 <script >

 var dateToday = <?php date('Y-m-d'); ?>

  $(document).ready(function(){

     $("[name=hasdiscount]").attr('disabled',true);
    $('#discountdiv').hide();
    $('#discountexpirydiv').hide();
  
 });

 $('[name=hasdiscount]').click(function(){

   if (this.checked) 
   {
    $('#discount').attr('disabled',false);
     $('#discountdiv').show();
     $('#discountexpirydiv').show();
   }
   else
   {
      $('#discount').attr('disabled',true);
      $('#discountdiv').hide();
      $('#discountexpirydiv').hide();
      $('#discountex_date').val('');
   }
         
 });

 $("#typeid").change(function() {

         if( $(this).val() ==1 || $(this).val() ==3)
       {
          $("[name=hasdiscount]").attr('disabled',true);
           $('#discount').val('0.00') ;
       }
        else if( $(this).val() ==2 || $(this).val() ==4)
        {
           $("[name=hasdiscount]").attr('disabled',false);
        }
    });
       

 

$(function() 
 {  
    $.datetimepicker.setLocale('en');


  $('#availabilitystartdate').datetimepicker({
  dayOfWeekStart : 1,
  lang:'en',
    minDate: dateToday,
  //disabledDates:['2016/11/08'],
  startDate:  dateToday,
   format:'Y-m-d H:i:s'
  });
//$('#bookingdate').datetimepicker({value:'',step:10});

   $('#availabilityenddate').datetimepicker({
  dayOfWeekStart : 1,
  lang:'en',
    minDate: dateToday,
  //disabledDates:['2016/11/08'],
  startDate:  dateToday,
   format:'Y-m-d H:i:s'
  });

    $('#discountex_date').datetimepicker({
  dayOfWeekStart : 1,
  lang:'en',
    minDate: dateToday,
  //disabledDates:['2016/11/08'],
  startDate:  dateToday,
   format:'Y-m-d H:i:s'
  });

 });


 
 var submitflg = "";
$( "#serviceform" ).submit(function( event ) { 
  
//  var submitflg = checkcurrency();
 

       
      //if(submitflg == true)
      //{
       // var submitflg = checkcost();

       // if(submitflg == true)
         // var submitflg = chkdiscount();
       // else
         // var submitflg = false;

       
         // if(submitflg == true)
              var submitflg = checkavaildate(); 
         // else
              //var submitflg = false;

           if(submitflg == true)
               $( "form" ).submit();
             else
               return false;

      //}
     // else
        //return false;
 
});


function chkdiscount()
{
   if($("[name=hasdiscount]").prop('checked') == true)
  {
    if($('#discount').val() == "" || $('#discount').val() == "0.00" || $('#discount').val() == "0")
    {
      alert("Please enter discount");
      return false;
        //event.preventDefault();
    }
    else if($('#discountex_date').val() == "" )
    {
      alert("Please enter discount expiry date");
      return false;
        //event.preventDefault();
    }
   else
     return true;

  }
  else
    return true;

}

   //check currency
  function checkcurrency()
  {
   
     if($('#currency_id option:selected').text() != 'USD' && $('#currency_id option:selected').text() !='IDR')
    {
        alert("Please enter currency USD or IDR");
          //event.preventDefault();
          return false;

    }
    else
      return true;

  }

  function checkavaildate()
  {
     var availstartdate =$('#availabilitystartdate').val().substr(0,10);
    var availenddate =$('#availabilityenddate').val().substr(0,10);
    var serviceexpirydate = $('#discountex_date').val().substr(0,10);
     var curdate = '<?php echo date("Y-m-d h:i:s") ?>';

     if(availstartdate != '')
     {
        if(curdate > availstartdate)
        {
           alert("Availability Start Date should not be previous date.");
           return false;
           //event.preventDefault();
        }
        else
        {
          if(availenddate !='')
          {
             if(curdate > availenddate)
             {
               alert("Availability End Date should not be previous date.");
               return false;
               //event.preventDefault();
            }
            else
             {
               if(availstartdate > availenddate)
              {
               alert("Availability Start Date should not be greater than Availability End Date.");
               return false;
               //event.preventDefault();
              }
             else
              {


                if(serviceexpirydate !='' && $("[name=hasdiscount]").prop('checked') == true)
                {
                   if(curdate > serviceexpirydate)
                   {
                     alert("Discount Expiry Date should not be previous date.");
                     return false;
                     //event.preventDefault();
                  }
                  else if(serviceexpirydate > availenddate)
                    {
                       alert("Discount Expiry Date should be within Availability Start Date and Availability End Date.");
                       return false;
                      // event.preventDefault();
                    }
                     else if(serviceexpirydate < availstartdate)
                    {
                       alert("Discount Expiry Date should be within Availability Start Date and Availability End Date.");
                       return false;
                      // event.preventDefault();
                    }
                    else
                      return true;
                }
                else
                   return true;
                
               

                  } 
                }
          }
          else
          {
             alert("Please enter Availability End Date.");
             return false;
            //event.preventDefault();
          }
        }
     }
     else
     {
       alert("Please enter Availability Start Date.");
       return false;
         // event.preventDefault();
     }

  }

   function checkcost()
    {
       if($("#typeid").val() == 2)
        {
          if($('[name=cost]').val() == "" || $('[name=cost]').val() == "0.00" || $('[name=cost]').val() == "0")
          {
            alert("Please enter cost");
             return false;
              //event.preventDefault();
          }
          else
            return true;
        }
        else
          return true;
    }

 </script>