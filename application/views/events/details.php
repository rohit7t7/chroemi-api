<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('event'); ?> <?php echo $this->lang->line('details'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

<div class="form-group">
<?php
echo form_label($this->lang->line('event').' '. $this->lang->line('category').':'); 
 $allcatlist = $this->eventcategorymodel->getcategoryoption();

 if(array_key_exists($page_var['modelData']['categoryid'],$allcatlist))
 echo $allcatlist[$page_var['modelData']['categoryid']]; 
else
  echo " -------  ";
 ?>
</div>



<div class="form-group">
<?php
echo form_label($this->lang->line('event').' '. $this->lang->line('name').':'); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo htmlentities($page_var['modelData']['name'],ENT_QUOTES);
 ?>  
</div>
<div class="form-group">
 <?php
echo form_label($this->lang->line('description').':');
 echo htmlentities($page_var['modelData']['description'],ENT_QUOTES);  ?>

 </div>

  <div class="form-group">
<?php echo form_label($this->lang->line('address').':'); 
echo htmlentities($page_var['modelData']['address'],ENT_QUOTES);  ?>
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('start').' '.$this->lang->line('date').':');
echo htmlentities($page_var['modelData']['availabilitystartdate'],ENT_QUOTES);  ?>

</div>
<div class="form-group">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('end').' '.$this->lang->line('date').':');
echo htmlentities($page_var['modelData']['availabilityenddate'],ENT_QUOTES);  ?>

</div>
<div class="form-group">
<?php echo form_label($this->lang->line('features').':'); 
echo htmlentities($page_var['modelData']['features'],ENT_QUOTES);  ?>
</div>

 <!-- <div class="form-group">
<?php //echo form_label($this->lang->line('cost').':'); 

/*if($page_var['modelData']['currency_id'] == '144')
              $amtinidr = $page_var['modelData']['cost'] * 13530; 
else if($srvdetails['currency_id'] == '65')
              $amtinidr =  $page_var['modelData']['cost'];*/
		  //$amtinidr =  $page_var['modelData']['cost'];

  //echo htmlentities($amtinidr,ENT_QUOTES);?> IDR
<?php 

 //$currencycode = $this->currencymodel->getcurrencyoption(); echo $currencycode[$page_var['modelData']['currency_id']]; 

?>
</div> -->

<!-- <div class="form-group" id="discountdiv">

<?php //echo form_label($this->lang->line('discount').':'); ?><b> (%)</b>
<?php //echo htmlentities($page_var['modelData']['discount'],ENT_QUOTES); ?>

</div> 
-->
<!-- <div class="form-group" id="discountexpirydiv" >

<?php //echo form_label($this->lang->line('discount').' '.$this->lang->line('expiry').' '.$this->lang->line('date').': '); 
//echo form_input(array('name' => 'discountex_date','id'=>'discountex_date','class'=>'form-control','value'=>htmlentities($page_var['modelData']['discountex_date'],ENT_QUOTES),'readonly'=>'true')); 
//echo htmlentities($page_var['modelData']['discountex_date'],ENT_QUOTES);  ?>


</div> -->


<div class="form-group">
<?php echo form_label($this->lang->line('image').':'); 

if(isset($page_var['modelData']['imageavatar']))
{ ?>
<img src="<?php echo base_url() ?>upload/thumbnail/event/<?php echo $page_var['modelData']['imageavatar']?>" >
<?php } ?>
</div>
<!--div class="form-group">
 
<?php/* echo form_label($this->lang->line('language').':'); 
if($page_var['modelData']['language_id'] == '1')
  { 
    echo 'English';
   }else{
    echo 'Indonesia';
   };*/
 ?>

</div-->
<div class="form-group">
<?php echo form_label($this->lang->line('status').':'); 
 if($page_var['modelData']['status'] == '1')
  { 
    echo 'Active';
   }else{
    echo 'Inactive';
   };
   ?>

</div>

            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

 <script>

  $(document).ready(function(){
  
     //chksrtvicetype(<?php echo $page_var['modelData']['typeid'] ?>);

 });

  function chksrtvicetype(typeid)
   {
      if( typeid ==1)
      {
          $('#costdiv').hide();
          $('#discountdiv').hide();
      }
       else if( typeid ==2)
       {
           $('#costdiv').show();
           $('#discountdiv').show();
       }
          
   }

 $(function() 
 {   $( "#startdatepicker" ).datepicker({
        beforeShow : function(){
            jQuery( this ).datepicker('option','maxDate', jQuery('#availabilityenddate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#availabilitystartdate').val(dateText) },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });

  $(function() 
 {   $( "#enddatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','minDate', jQuery('#availabilitystartdate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#availabilityenddate').val(dateText) },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });
 </script>