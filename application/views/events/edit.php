<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('event'); ?> : <?php echo $page_var['modelData']['name']; ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
              echo form_open_multipart('events/update',array('id' => 'serviceform'),$hidden); ?>
              <?php
              echo "<div class='error_msg'>";
              if (isset($error_message)) {
              echo $error_message;
              }
              echo validation_errors();

              echo "</div>";
              ?>

<div class="form-group">
<?php
echo form_label($this->lang->line('event') .' '. $this->lang->line('category')); 
if(array_key_exists($page_var['modelData']['categoryid'],$page_var['eventcategoryData']))
echo form_dropdown('categoryid', $page_var['eventcategoryData'], $page_var['modelData']['categoryid'],'class="form-control"'); 
else
{
  $allcatdetails = array_merge(array(""=>""),$page_var['eventcategoryData']);
  echo form_dropdown('categoryid', $allcatdetails, "",'class="form-control"'); 
}

?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('event') .' '. $this->lang->line('name')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo form_input(array('name' => 'servicename','class'=>'form-control','value'=>htmlentities($page_var['modelData']['name'],ENT_QUOTES)));
 ?>  
</div>
<div class="form-group">
 <?php
echo form_label($this->lang->line('description')); 
 echo form_textarea(array('name' => 'description', 'rows'=> '2',
      'cols'=> '10','class'=>'form-control','value'=>htmlentities($page_var['modelData']['description'],ENT_QUOTES)));  ?>

 </div>



<div class="form-group other-date">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('start').' '.$this->lang->line('date')); 
echo form_input(array('name' => 'availabilitystartdate','class'=>'form-control','id'=>'availabilitystartdate','value'=>htmlentities($page_var['modelData']['availabilitystartdate'],ENT_QUOTES),'readonly'=>'true'));  ?>
<!--input type="button" value="select" id="startdatepicker" style="display:none"-->
</div>
<div class="form-group other-date">
<?php echo form_label($this->lang->line('availability').' '.$this->lang->line('end').' '.$this->lang->line('date')); 
echo form_input(array('name' => 'availabilityenddate','class'=>'form-control','id'=>'availabilityenddate','value'=>htmlentities($page_var['modelData']['availabilityenddate'],ENT_QUOTES)));  ?>
<!--input type="button" value="select" id="enddatepicker" style="display:none"-->
</div>
<div class="form-group">
<?php echo form_label($this->lang->line('features')); 
echo form_input(array('name' => 'features','class'=>'form-control','value'=>htmlentities($page_var['modelData']['features'],ENT_QUOTES)));  ?>
</div>

 <!-- <div class="form-group">
<?php //echo form_label($this->lang->line('cost')); 
//echo form_input(array('name' => 'cost','class'=>'form-control','value'=>htmlentities($page_var['modelData']['cost'],ENT_QUOTES)));  ?>
</div> -->

<!-- <div class="form-group">
<?php 

//echo form_label($this->lang->line('currency')); 
//echo form_dropdown('currency_id', $page_var['currencyData'], $page_var['modelData']['currency_id'],'class="form-control" id="currency_id"'); 
?>
</div> -->

<!-- <div class="form-group">
<?php // echo form_label($this->lang->line('has').' '.$this->lang->line('discount')); ?>  &nbsp;&nbsp;

<?php // echo form_checkbox('hasdiscount', 'hasdiscount', FALSE);?>
</div> -->

<!-- <div class="form-group" id="discountdiv">

<?php //echo form_label($this->lang->line('discount')); ?> <b>(%)</b>
<?php //echo form_input(array('name' => 'discount','class'=>'form-control','value'=>htmlentities($page_var['modelData']['discount'],ENT_QUOTES),'id'=>'discount')); ?>

</div> --> 

<!-- <div class="form-group" id="discountexpirydiv" >

<?php //echo form_label($this->lang->line('discount').' '.$this->lang->line('expiry').' '.$this->lang->line('date')); 
//echo form_input(array('name' => 'discountex_date','id'=>'discountex_date','class'=>'form-control','value'=>htmlentities($page_var['modelData']['discountex_date'],ENT_QUOTES),'readonly'=>'true')); 
//echo form_input(array('name' => 'discountex_date','class'=>'form-control','id'=>'discountex_date','value'=>htmlentities($page_var['modelData']['discountex_date'],ENT_QUOTES)));  ?>


</div> -->

<div class="form-group">
<?php echo form_label($this->lang->line('upload').' '.$this->lang->line('event').' '.$this->lang->line('icon')); 
echo form_upload(array('name' => 'imageavatar'));  ?>

<?php 
if(isset($page_var['modelData']['imageavatar']))
{ ?>
<img src="<?php echo base_url() ?>upload/thumbnail/event/<?php echo $page_var['modelData']['imageavatar']?>" >
<?php } ?>
</div>

<!--div class="form-group">
<?php //echo form_label($this->lang->line('upload').' '.$this->lang->line('image')); 
 echo form_upload('image[]','','multiple'); ?>

<?php 
if(isset($page_var['modelimageData']))
{ 
  foreach($page_var['modelimageData'] as $index =>$imgdetails) {?>
<img src="<?php echo base_url() ?>upload/service/thumbnail/<?php echo $imgdetails['image']?>" >
<?php }} ?>
</div-->

<!--div class="form-group">
 
<?php/* echo form_label($this->lang->line('language')); 
echo form_dropdown('language_id', $page_var['languageData'], $page_var['modelData']['language_id'],'class="form-control"'); */?>

</div-->

<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

  </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

 <script>

 var dateToday = <?php date('Y-m-d'); ?>
 $(document).ready(function(){
  
    chkhasdiscount();
    chksrtvicetype(<?php echo $page_var['modelData']['typeid'] ?>);

 });

 function chkhasdiscount()
 {
  if($('#discount').val()=='0.00' || $('#discount').val()=='')
    {
      $('[name=hasdiscount]').prop('checked', false);
      $('#discountdiv').hide();
      $('#discountexpirydiv').hide();
    } 
  else
  {
    $('[name=hasdiscount]').prop('checked', true);
    $('#discountdiv').show();
     $('#discountexpirydiv').show();
  }

 }

 $('[name=hasdiscount]').click(function(){

   if (this.checked) 
   {
      $('#discount').attr('disabled',false);
     $('#discountdiv').show();
     $('#discountexpirydiv').show();

   }
   else
   {
     $('#discount').attr('disabled',true);
     $('#discountdiv').hide();
     $('#discountexpirydiv').hide();
     $('#discountex_date').val('');
   }
         
 });

   $("#typeid").change(function() {

      chksrtvicetype($(this).val());

    });

  function chksrtvicetype(typeid)
   {

      if( typeid ==1 || typeid ==3)
      {
          //$('#costdiv').hide();
          //$('#hasdiscountdiv').hide();
          $("[name=hasdiscount]").attr('disabled',true);
          $('#discount').val('0.00') ;
          $('#discountex_date').val('0000-00-00');
      }
       else if( typeid ==2 || typeid ==4)
       {
          // $('#costdiv').show();
          // $('#hasdiscountdiv').show();
          $("[name=hasdiscount]").attr('disabled',false);

       }
          
   }

    $(function() 
 {  
    $.datetimepicker.setLocale('en');


    $('#availabilityenddate').datetimepicker({
  dayOfWeekStart : 1,
  lang:'en',
    minDate: dateToday,
  //disabledDates:['2016/11/08'],
  startDate:  dateToday,
   format:'Y-m-d H:i:s'
  });

    $('#discountex_date').datetimepicker({
  dayOfWeekStart : 1,
  lang:'en',
    minDate: dateToday,
  //disabledDates:['2016/11/08'],
  startDate:  dateToday,
   format:'Y-m-d H:i:s'
  });

 });
 
 
    var submitflg = "";
  $( "#serviceform" ).submit(function( event ) {

     var submitflg = checkcurrency();

       
      if(submitflg == true)
      {
        var submitflg = checkcost();

        if(submitflg == true)
          var submitflg = chkdiscount();
        else
           var submitflg = false;

          if(submitflg == true)
              var submitflg = checkavaildate(); 
            else
               var submitflg = false;

            if(submitflg == true)
               $( "form" ).submit();
            else
               return false;

      }
      else
        return false;

    });
  
  function chkdiscount()
  {
    if($("[name=hasdiscount]").prop('checked') == true)
    {
      if($('#discount').val() == "" || $('#discount').val() == "0.00" || $('#discount').val() == "0")
      {
        alert("Please enter discount");
         return false;
          //event.preventDefault();
      }
       else if($('#discountex_date').val() == "" )
      {
        alert("Please enter discount expiry date");
         return false;
         // event.preventDefault();
      }
      else
      return true;
    }
    else
    {
      return true;
    }
   
  }

   //check currency
  function checkcurrency()
  {
   
     if($('#currency_id option:selected').text() != 'USD' && $('#currency_id option:selected').text() !='IDR')
    {
        alert("Please enter currency USD or IDR");
          //event.preventDefault();
          return false;

    }
    else
      return true;

  }
 
 function checkavaildate()
 {
    var availstartdate =$('#availabilitystartdate').val().substr(0,10);
    var availenddate =$('#availabilityenddate').val().substr(0,10);
    var serviceexpirydate = $('#discountex_date').val().substr(0,10);
       var curdate = '<?php echo date("Y-m-d") ?>';

        
          if(availenddate !='')
          {
              if(curdate > availenddate)
             {
               alert("Availability End Date should not be previous date.");
                return false;
               //event.preventDefault();
            }
            else
             {
              if(availstartdate > availenddate)
              {
               alert("Availability Start Date should not be greater than Availability End Date.");
                return false;
              // event.preventDefault();
              }
              else
              {

               
                if( (serviceexpirydate !='0000-00-00' || serviceexpirydate !='') && $("[name=hasdiscount]").prop('checked') == true)
                {
                     if(curdate > serviceexpirydate)
                     {
                       alert("Discount Expiry Date should not be previous date.");
                        return false;
                       //event.preventDefault();
                    }
                    else if(serviceexpirydate > availenddate)
                    {
                       alert("Discount Expiry Date should not be greater than Availability End Date.");
                        return false;
                       //event.preventDefault();
                    }
                  else if(serviceexpirydate < availstartdate)
                    {
                       alert("Discount Expiry Date should be within Availability Start Date and Availability End Date.");
                        return false;
                       //event.preventDefault();
                    }
                    else
                    {
                      return true;
                    }
                }
                else
                  return true;

                
              }

             } 
          }
          else
          {
             alert("Please enter Availability End Date.");
            //event.preventDefault();
          }
      
 }
  
      
    function checkcost()
    {
       if($("#typeid").val() == 2)
        {
          if($('[name=cost]').val() == "" || $('[name=cost]').val() == "0.00" || $('[name=cost]').val() == "0")
          {
            alert("Please enter cost");
             return false;
              //event.preventDefault();
          }
          else
            return true;
        }
        else
          return true;
    }
  
 

 </script>