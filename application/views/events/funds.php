<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('fund'); ?>&nbsp;<?php echo $this->lang->line('of'); ?>&nbsp;<?php echo $this->lang->line('event'); ?> : <?php echo $page_var['funds'][0]['name']; ?><a href="<?php echo base_url(); ?>events/addfund/<?php echo $page_var['id']; ?>" class="btn btn-default move-up pull-right"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('fund'); ?></a></h1>
                     
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

          if($this->session->flashdata('msg'))  
                echo $this->session->flashdata('msg');

              if($this->session->flashdata('updatemsg'))  
                echo $this->session->flashdata('updatemsg');

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

            if($this->session->flashdata('uploadmsg'))  
                echo $this->session->flashdata('uploadmsg');

              if($this->session->flashdata('errmsg'))  
                echo $this->session->flashdata('errmsg');
          ?>  

          
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                       
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('amount'); ?></th>
                                             
                                            <th><?php echo $this->lang->line('currency'); ?></th>
                                             
                                             <th><?php echo $this->lang->line('date'); ?></th>
                                              
                                           
                                          
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php foreach($page_var['funds'] as $srvdetails) { 
                                   
                                   //echo $currencycode[$srvdetails['currency_id']];
                                        //  if($srvdetails['currency_id'] == '144')
                                             // $costinidr = $srvdetails['cost'] * 13530; 
                                        //  else if($srvdetails['currency_id'] == '65')
                                             // $costinidr =  $srvdetails['cost'];

                                    ?>
                                        <tr class="odd gradeX">
                                            
                                            <td><?php echo htmlentities($srvdetails['fund_amount'],ENT_QUOTES); ?></td>
                                            <td><?php echo htmlentities($srvdetails['currency_code'],ENT_QUOTES); ?></td>
                                             <td><?php echo htmlentities($srvdetails['date'],ENT_QUOTES); ?></td>
                                          
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div> 

<script type="text/javascript">
     
     var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
               "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
               "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                  "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                  "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

          $(document).ready(function() { 
              $('#dataTables-example').DataTable({
                      "oLanguage": english,
                      responsive: true,
                      "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 2 ] } ],
                      "order": [[ 2, "asc" ]]
              });

               $('#langchange').click(function(){
                dtable.fnDestroy();
                dtable = null;
                currentLang = (currentLang == english) ? indonesian : english;
                dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
              });
          });



  

 </script>