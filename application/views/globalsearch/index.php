  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('search'); ?> <?php echo $this->lang->line('result');; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

             <!-- /.row -->
            <div class="row">

                <ul class="tab tab-line">

                
                      <li id="supervisor1" class="tablinks" ><a href="#" onclick="getresult('organizer')"><?php echo $this->lang->line('organizer'); ?></a></li>
                 
                      <li id="user1" class="tablinks" ><a href="#"  onclick="getresult('attendee')"><?php echo $this->lang->line('attendee'); ?></a></li>
                
                      <li id="category1" class="tablinks" ><a href="#" onclick="getresult('category')"><?php echo $this->lang->line('category'); ?></a></li>
                
                      <li id="service1" class="tablinks" ><a href="#" onclick="getresult('event')"><?php echo $this->lang->line('event'); ?></a></li>
                 
                  </ul>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                       
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="supervisorinfo">
                                    

                                </table>
                              

                                
                            </div>

                        </div>
                        


                    </div>
                </div>            
            </div>

 </div>

    <script>

        var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
              "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                 "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + "0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

          $(document).ready(function(){

           $('#supervisor1').addClass('active');
          
           resltarr = <?php print_r(json_encode($page_var)); ?>;

            
                 $('#supervisorinfo').DataTable({
                         "oLanguage": english,
                        responsive: true,
                         "aaData":  resltarr ,
                        "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ],
                        "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('name');?>","mData": "name" }, // <-- which values to use inside object
                                { "sTitle": "<?php echo $this->lang->line('email');?>" ,"mData": "email" },
                                { "sTitle": "<?php echo $this->lang->line('phone');?>","mData": "phone" },
                                { "sTitle": "<?php echo $this->lang->line('create');?> <?php echo $this->lang->line('date');?>","mData": "createddate" }
                                
                            ]
                });
                     
         
      });

     function getresult(getslid)
      {


            $('.tablinks').removeClass('active');

            if(getslid == 'supervisor')
                $('#supervisor1').addClass('active');
             else if(getslid == 'user')
                $('#user1').addClass('active');
             else if(getslid == 'category')
                $('#category1').addClass('active');

             $('#supervisorinfo').dataTable().fnDestroy();
           $('#supervisorinfo').dataTable();


            var searchdata = $('#globalsearchval').val();
             $.ajax({
                type:"POST",
                url: "<?php echo base_url(); ?>globalsearch/gensearch", 
                data:{'searchdata':searchdata,'tabvalue':getslid},
                success: function(result){


                    if(getslid == 'supervisor')
                    {

                            resltarr = $.parseJSON(result);                   

                                table = $('#supervisorinfo').DataTable( {
                                 "oLanguage": english,
                                responsive: true,   
                                bDestroy: true,
                                "sPaginationType": "full_numbers",
                               
                               "aaData":  resltarr ,

                              "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('name');?>","mData": "name" }, // <-- which values to use inside object
                                { "sTitle": "<?php echo $this->lang->line('email');?>" ,"mData": "email" },
                                { "sTitle": "<?php echo $this->lang->line('phone');?>","mData": "phone" },
                                { "sTitle": "<?php echo $this->lang->line('create');?> <?php echo $this->lang->line('date');?>","mData": "createddate" }
                                
                            ]
                               
                            } );
                    
                    }
                     else if(getslid == 'user')
                    {

                               resltarr = $.parseJSON(result);                   

                                $('#supervisorinfo').DataTable( {
                                    "oLanguage": english,
                                    responsive: true,
                                bDestroy: true,
                                "sPaginationType": "full_numbers",
                               
                               "aaData":  resltarr ,
                              "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('name');?>","mData": "name" }, 
                                { "sTitle": "<?php echo $this->lang->line('email');?>" ,"mData": "email" },
                                { "sTitle": "<?php echo $this->lang->line('phone');?>","mData": "phone" },
                                { "sTitle": "<?php echo $this->lang->line('create');?> <?php echo $this->lang->line('date');?>","mData": "createddate" }
                                
                            ]
                               
                            } );
                    
                    }
                    
                   else if(getslid == 'category')
                    {


                            catresltarr = $.parseJSON(result);                   
                               $('#supervisorinfo').DataTable( {
                                "oLanguage": english,
                                responsive: true,
                                bDestroy: true,
                                "sPaginationType": "full_numbers",
                               
                               "aaData":  catresltarr ,
                               "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1,2,3 ], "visible": false } ],
                              "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('category');?>","mData": "category" },
                                 { "sTitle": "<?php echo $this->lang->line('category');?>","mData": "category" },
                               { "sTitle": "<?php echo $this->lang->line('category');?>","mData": "category" },
                               { "sTitle": "<?php echo $this->lang->line('category');?>","mData": "category" } // <-- which values to use inside object
                                                                
                            ]
                               
                            } );
                    
                    }
                    else if(getslid == 'service')
                    {
                            srvresltarr = $.parseJSON(result);                   
                               $('#supervisorinfo').DataTable( {
                                "oLanguage": english,
                                responsive: true,
                                bDestroy: true,
                               
                               "aaData":  srvresltarr ,
                              "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('service');?> <?php echo $this->lang->line('name');?>","mData": "servicename" },
                                 { "sTitle": "<?php echo $this->lang->line('description');?>","mData": "description" },
                                  { "sTitle": "<?php echo $this->lang->line('cost');?>","mData": "cost" },
                                  { "sTitle": "<?php echo $this->lang->line('create');?> <?php echo $this->lang->line('date');?>","mData": "createddate" }  // <-- which values to use inside object
                                                                
                            ]
                               
                            } );
                    
                    }
                    else if(getslid == 'order')
                    {
                            ordresltarr = $.parseJSON(result);                   
                               $('#supervisorinfo').DataTable( {
                                "oLanguage": english,
                                responsive: true,
                                bDestroy: true,
                               
                               "aaData":  ordresltarr ,
                              "aoColumns": [
                                
                                { "sTitle": "<?php echo $this->lang->line('invoice');?> <?php echo $this->lang->line('id');?>","mData": "invoiceid" },
                                 { "sTitle": "<?php echo $this->lang->line('billing');?> <?php echo $this->lang->line('details');?>","mData": "billing_name" },
                                  { "sTitle": "<?php echo $this->lang->line('order');?> <?php echo $this->lang->line('amount');?>","mData": "order_amount" },
                                  { "sTitle": "<?php echo $this->lang->line('order');?> <?php echo $this->lang->line('date');?>","mData": "order_date" } // <-- which values to use inside object
                                                                
                            ]
                               
                            } );
                    
                    }
            }
        });
       
        
      }



    </script>

