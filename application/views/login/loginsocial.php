<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Choreomi</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>/assets/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>/assets/admin/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>/assets/admin/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>/assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="background-img">
<?php
if (isset($logout_message)) {
echo "<div class='message'>";
echo $logout_message;
echo "</div>";
}
?>
<?php
if (isset($message_display)) {
echo "<div class='message'>";
echo $message_display;
echo "</div>";
}
?>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                <div class="login-panel panel-default">
                    <div class="logo"><img src="<?php echo base_url()?>/assets/admin/images/logo.png" class="logo-img"/></div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url(); ?>login">
                        <?php
                            echo "<div class='error_msg'>";
                            if (isset($error_message)) {
                            echo $error_message;
                            }
                            echo validation_errors();
                            echo "</div>";
                            ?>
                            <fieldset class="bg-white">
                                <input style="display:none">
                                <input type="password" style="display:none">
                                <div class="form-group remove-pad">
                                    <input class="form-control fname" placeholder="User Name" type="text" name="email" id="name" value="" autofocus>
                                </div>
                                <div class="form-group remove-pad">
                                    <input class="form-control pwd" placeholder="Password" type="password" name="password" id="password" placeholder="password" value="">
                                </div>
                               
                               <input type="submit" class="btn btn-lg btn-success btn-block org-btn" value=" Login " name="submit"/><br />
                                <input type="hidden" name="submit" id="submit" value="1" />

                            </fieldset>
                            <div class="button">
                                <!--a href="#" class="btn btn-off pull-left">Forgot password?</a-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>/assets/admin/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>/assets/admin/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>/assets/admin/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>/assets/admin/js/sb-admin-2.js"></script>

</body>

<style>
.error_msg{
color:red;
font-size: 16px;
}

.message{
position: absolute;
font-weight: bold;
font-size: 28px;
color: #6495ED;
left: 262px;
width: 500px;
text-align: center;
}


</style>

</html>
