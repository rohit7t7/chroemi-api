
<?php if(count($page_var['modelData']) > 0) {?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($page_var['modelData']['type'] == '2') {echo $this->lang->line('view');?> <?php echo $this->lang->line('supervisor');?> <?php echo $this->lang->line('details');} if($page_var['modelData']['type'] == '1') { echo $this->lang->line('view');?> <?php echo $this->lang->line('admin');?> <?php echo $this->lang->line('details');}?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">


<div class="form-group">
<?php
echo form_label($this->lang->line('name').':'); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo htmlentities($page_var['modelData']['name'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('email').':'); 
 echo htmlentities($page_var['modelData']['email'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('phone').':'); 
 echo htmlentities($page_var['modelData']['phone'],ENT_QUOTES);
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('password').':'); 
 echo "******";//$page_var['modelData']['password'];
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('type').':'); 
 echo (htmlentities($page_var['modelData']['type'],ENT_QUOTES) == 1 ? 'Admin' : 'Supervisor'); ?>
 
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('status').':'); 
 if(htmlentities($page_var['modelData']['status'],ENT_QUOTES) == '1')
  { 
    echo 'Active';
   }else{
    echo 'Inactive';
   };
   ?>

</div>

            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

<?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>