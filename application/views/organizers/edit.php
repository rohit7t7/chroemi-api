
<?php if(count($page_var['modelData']) > 0) {?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($page_var['modelData']['type'] == '2') {echo $this->lang->line('edit');?> <?php echo $this->lang->line('supervisor');?>: <?php echo $page_var['modelData']['name'];}elseif($page_var['modelData']['type'] == '1'){echo $this->lang->line('edit');?> <?php echo $this->lang->line('admin');?>: <?php echo $page_var['modelData']['name'];} ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
 
<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
      
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

                 <?php $hidden = array('id' => $page_var['modelData']['id']); 
                  echo form_open_multipart('supervisors/update','',$hidden); ?>


                <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
                }
                echo validation_errors();

                echo "</div>";
                ?>
<div class="form-group">
<?php
echo form_label($this->lang->line('name')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo form_input(array('name' => 'name','class'=>'form-control','value'=>htmlentities($page_var['modelData']['name'],ENT_QUOTES)));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('email')); 
 echo form_input(array('name' => 'email','class'=>'form-control','value'=>htmlentities($page_var['modelData']['email'],ENT_QUOTES)));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('phone')); 
 echo form_input(array('name' => 'phone','class'=>'form-control','value'=>htmlentities($page_var['modelData']['phone'],ENT_QUOTES)));
 ?>  
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('password')); 
 echo form_password(array('name' => 'password','class'=>'form-control','value'=>htmlentities($page_var['modelData']['password'],ENT_QUOTES)));
 ?>  
</div>

<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>

</div>

<?php if($_SESSION['logged_in']['type']==1) { ?>
<div class="form-group">
<?php echo form_label($this->lang->line('type')); 
$typeoptions = array('1'=>'Admin','2'=>'Supervisor');
 echo form_dropdown('type', $typeoptions, $page_var['modelData']['type'],'class="form-control"'); ?>

</div> <?php } ?>

<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

 <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

