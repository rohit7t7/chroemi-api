<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('gateways'); ?> <?php echo $this->lang->line('credentials'); ?></h1>
                </div>
 
               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php echo form_open('paymentcredentials/create',array('id' => 'credentialform')); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($page_var['error_message'])) {
            echo $page_var['error_message'];
            }
            echo validation_errors();

            echo "</div>";
            ?>

 <div class="form-group">
<?php
echo form_label($this->lang->line('payment').' '. $this->lang->line('type')); 
echo form_dropdown('paymenttype', $page_var['paymenttypeData'], set_value('paymenttype'),'class="form-control" id="paymenttype"'); ?>
</div>

 <div class="form-group">
<?php
echo form_label($this->lang->line('serverkey')); 
 echo form_input(array('name' => 'serverkey','class'=>'form-control', 'id'=>'serverkey', 'value'=>set_value('serverkey'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('clientkey')); 
 echo form_input(array('name' => 'clientkey','class'=>'form-control', 'id'=>'clientkey','value'=>set_value('clientkey'))); ?>
</div>

 <div class="form-group">
<?php
echo form_label($this->lang->line('api').' '.$this->lang->line('user').' '.$this->lang->line('name')); 
 echo form_input(array('name' => 'apiusername','class'=>'form-control',  'id'=>'apiusername','value'=>set_value('apiusername'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('api').' '.$this->lang->line('password')); 
 echo form_password(array('name' => 'apipassword','class'=>'form-control', 'id'=>'apipassword', 'value'=>set_value('apipassword'))); ?>
</div>

<div class="form-group">
<?php
echo form_label($this->lang->line('api').' '.$this->lang->line('signature')); 
 echo form_input(array('name' => 'apisignature','class'=>'form-control',  'id'=>'apisignature',  'value'=>set_value('apisignature'))); ?>
</div>




<div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
 echo form_dropdown('status', $statusoptions, set_value('status'),'class="form-control"'); ?>

</div>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div> 
 </div>  

 <script>

 $( "#credentialform" ).submit(function( event ) {

    if($('#paymenttype').val() == 1 || $('#paymenttype').val() == 3)
    {
        if($('#serverkey').val()=='')
        {
            alert("Please enter Serverkey");
            return false;
        }
        else if($('#clientkey').val()=='')
        {
            alert("Please enter Clientkey");
            return false;
        }
        else
            $('form').submit();
    }

     if($('#paymenttype').val() == 2)
    {
        if($('#apiusername').val()=='')
        {
            alert("Please enter Api User Name");
            return false;
        }
        else if($('#apipassword').val()=='')
        {
            alert("Please enter Api Password");
            return false;
        }
         else if($('#apisignature').val()=='')
        {
            alert("Please enter Api Signature");
            return false;
        }
        else
             $('form').submit();
    }
  
  
 
});

 </script>     


