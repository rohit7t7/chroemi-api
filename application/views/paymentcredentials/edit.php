<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('gateways'); ?></h1>
                </div>
 
               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open('paymentcredentials/update',array('id' => 'credentialform'),$hidden); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();
            echo "</div>";
            ?>

 <div class="form-group"> 

 <?php 
echo form_label($this->lang->line('name')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 if(array_key_exists($page_var['modelData']['paymenttype'],$page_var['paymenttypeData']))
  echo form_dropdown('paymenttype', $page_var['paymenttypeData'], $page_var['modelData']['paymenttype'],'class="form-control" id="paymenttype"'); 
else
{
  $alltypedetails = array_merge(array(""=>""),$page_var['paymenttypeData']);
  echo form_dropdown('paymenttype', $alltypedetails, "",'class="form-control" id="paymenttype"'); 
}
  ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('serverkey'));
 echo form_input(array('name' => 'serverkey','value'=>htmlentities($page_var['modelData']['serverkey'],ENT_QUOTES),'class'=>'form-control','id'=>'serverkey'));
 // htmlentities($page_var['modelData']['serverkey'],ENT_QUOTES); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('clientkey'));
 echo form_input(array('name' => 'clientkey','value'=>htmlentities($page_var['modelData']['clientkey'],ENT_QUOTES),'class'=>'form-control','id'=>'clientkey')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('api').' '.$this->lang->line('user').' '. $this->lang->line('name'));
 echo form_input(array('name' => 'apiusername','value'=>htmlentities($page_var['modelData']['apiusername'],ENT_QUOTES),'class'=>'form-control','id'=>'apiusername')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('api').' '.$this->lang->line('password'));

 echo form_input(array('name' => 'apipassword','value'=>htmlentities($page_var['modelData']['apipassword'],ENT_QUOTES),'class'=>'form-control','id'=>'apipassword')); ?>
</div>

<div class="form-group"> 
<?php echo form_label($this->lang->line('api').' '.$this->lang->line('signature'));
 echo form_input(array('name' => 'apisignature','value'=>htmlentities($page_var['modelData']['apisignature'],ENT_QUOTES),'class'=>'form-control','id'=>'apisignature')); ?>
</div>

 
 <div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
    
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>
 </div>
  <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 

 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>         

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

<script>

 $( "#credentialform" ).submit(function( event ) {

    if($('#paymenttype').val() == 1 || $('#paymenttype').val() == 3)
    {
        if($('#serverkey').val()=='')
        {
            alert("Please enter Serverkey");
            return false;
        }
        else if($('#clientkey').val()=='')
        {
            alert("Please enter Clientkey");
            return false;
        }
        else
            $('form').submit();
    }

     if($('#paymenttype').val() == 2)
    {
        if($('#apiusername').val()=='')
        {
            alert("Please enter Api User Name");
            return false;
        }
        else if($('#apipassword').val()=='')
        {
            alert("Please enter Api Password");
            return false;
        }
         else if($('#apisignature').val()=='')
        {
            alert("Please enter Api Signature");
            return false;
        }
        else
             $('form').submit();
    }
  
  
 
});

 </script> 