<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('gateways'); ?>: <?php echo $page_var['modelData']['name']; ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

            <?php $hidden = array('id' => $page_var['modelData']['id']); 
                echo form_open('paymentgateway/update','',$hidden); ?>
            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();
            echo "</div>";
            ?>

 <div class="form-group"> 
<?php echo form_label($this->lang->line('name')); 
 echo form_hidden(array('name' => 'id','value'=>$page_var['modelData']['id'])); 
 echo form_input(array('name' => 'name','value'=>htmlentities($page_var['modelData']['name'],ENT_QUOTES),'class'=>'form-control')); ?>
</div>

 
 <div class="form-group">
<?php echo form_label($this->lang->line('status')); 
$statusoptions = array('1'=>'Active','2'=>'Inactive');
    
 echo form_dropdown('status', $statusoptions, $page_var['modelData']['status'],'class="form-control"'); ?>
 </div>
  <div class="form-group">
 <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 

 echo form_close(); ?>   
            
</div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>   
 </div>         

   <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>