<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('payment'); ?> <?php echo $this->lang->line('history'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

               if($this->session->flashdata('confirmmsg'))  
                echo $this->session->flashdata('confirmmsg');

               if($this->session->flashdata('cancelmsg'))  
                echo $this->session->flashdata('cancelmsg');
          ?>   
           <div class="row">
              <div class="col-lg-12">
                 <?php echo form_open('paymenthistory/index',array('id' => 'paymenthistoryform'));?>
                 
                  <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('from')); echo form_input(array('name' => 'startdate','id'=>'startdate','class'=>'form-control','value'=>set_value('startdate')));  ?>
                    <input type="button" value="select" id="startdatepicker" style="display:none"></div>
                  <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('to')); echo form_input(array('name' => 'enddate','id'=>'enddate','class'=>'form-control','value'=>set_value('enddate')));  ?>
                    <input type="button" value="select" id="enddatepicker" style="display:none"></div>
                 <div class="correct-height width-3 last-btn"><input type="button" value="<?php echo $this->lang->line('reset')?>" onclick="resetvalue()" style="position:relative;left:0px;"></div>
                  <div class="correct-height width-3" style="line-height: 45px;width:190px;" id="displaytotordcost"><B><?php echo $this->lang->line('total') ?> <?php echo $this->lang->line('amount') ?> : </B></div>
                <?php echo form_close(); ?> 
              </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                     <div class="panel-heading">
                          
                        </div>
                                                            
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive chg-scroll">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('invoice'); ?> <?php echo $this->lang->line('id'); ?></th>
                                            <th><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('info'); ?></th>
                                             <th style="width:130px"><?php echo $this->lang->line('order'); ?> <?php echo $this->lang->line('amount'); ?> ( In IDR )</th>
                                             <th><?php echo $this->lang->line('order'); ?> <?php echo $this->lang->line('date'); ?></th>
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php $totamount = 0;
                                    $amtinidr =0;

                                      if(!empty($page_var['paymenthistory']))
                                      {
                                        foreach($page_var['paymenthistory'] as $orderdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td>#<?php echo htmlentities($orderdetails['invoiceid'],ENT_QUOTES); ?></td>
                                            <td><button type="button" class="btn btn-info" data-toggle="modal"  style="width: 80px; height: 20px;font-size:10px;text-align:center;padding: 0px;"  data-target="#myModal_<?php echo $orderdetails['id'] ?>"><?php echo $this->lang->line('view'); ?> <?php echo $this->lang->line('info'); ?></button></td>
                                             <td><?php 

                                            // $currencycode = $this->currencymodel->getcurrencyoption(); 
                                              if($orderdetails['currency_id'] == '144')
                                                  $amtinidr = $orderdetails['order_amount'] * 13530; 
                                              else if($orderdetails['currency_id'] == '65')
                                                  $amtinidr =  $orderdetails['order_amount'];

                                             echo htmlentities($amtinidr,ENT_QUOTES); ?> 

                                             <?php  //$ordercurrencycode = $currencycode[$orderdetails['currency_id']]; echo $ordercurrencycode;?></td>
                                            <td> <?php echo date_format(date_create( htmlentities($orderdetails['order_date'],ENT_QUOTES)),'jS \of F Y h:i:s A'); ?> </td>
                                            <?php  $totamount = $totamount + $amtinidr;
                                             ?>
                                        </tr>
                                        <?php } ?>
                                        <div id="finalordamt" style="display:none;"><?php echo $totamount; ?> IDR <?php //echo $ordercurrencycode; ?></div>
                                    
                                      <?php } ?>  
                                      
                                
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
  

 <?php foreach($page_var['paymenthistory'] as $orderdetails) { ?>
  <!-- Modal -->
  <div class="modal fade" id="myModal_<?php echo $orderdetails['id'] ?>" role="dialog">
    <div class="modal-dialog">

     <!-- Modal content-->
      <div class="modal-content">
        
    
    <?php if($orderdetails['typeid']=='2' || $orderdetails['typeid']=='4') {?>
     
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('details'); ?></h4>
        </div>
        
        <div class="modal-body">
          <table border="0" style="width:100%">
              <tr><td></td><td></td><td></td><td></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('name'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_name'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('email'); ?> <?php echo $this->lang->line('address'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_email_address'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('address'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_street_address'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('city'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_city'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('state'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_state'],ENT_QUOTES); ?></td></tr>
              <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('postcode'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_postcode'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('country'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_country'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('phone'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_phone'],ENT_QUOTES); ?></td></tr>
               <tr><td colspan="2"><?php echo $this->lang->line('billing'); ?> <?php echo $this->lang->line('fax'); ?></td><td colspan="2"><?php echo htmlentities($orderdetails['billing_fax'],ENT_QUOTES); ?></td></tr>
          </table>

        </div>
        
      

    <?php } else {?>

          <div class="modal-body">
            <b><?php echo $this->lang->line('billing')?> <?php echo $this->lang->line('details')?> <?php echo $this->lang->line('not') ?> <?php echo $this->lang->line('available') ?> <?php echo $this->lang->line('for') ?> <?php echo $this->lang->line('this')?> <?php echo $this->lang->line('service'); ?></b>
        </div>
    <?php } ?>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('close'); ?></button>
        </div>
      
      </div>
    </div>
  </div>

  <?php } ?>

</div>

<script type="text/javascript">
     
      var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
              "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                  "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

   $(document).ready(function() {
        $('#dataTables-example').DataTable({
                 "oLanguage": english,
                responsive: true,
                //"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
        });

        $('#langchange').click(function(){
            dtable.fnDestroy();
            dtable = null;
            currentLang = (currentLang == english) ? indonesian : english;
            dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
          });

        $('#displaytotordcost').append($('#finalordamt').text());
        $('#displaytotordcost').css('font-weight','bold');

    });

    
    function resetvalue()
    {
      $('#startdate').val('');
      $('#enddate').val('');
      $('#paymenthistoryform').submit();

    }

    $(function() 
   {   $( "#startdatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','maxDate', jQuery('#enddate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#startdate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });

  $(function() 
 {   $( "#enddatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','minDate', jQuery('#startdate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#enddate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });



   
 </script>