<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('performance'); ?> <?php echo $this->lang->line('reports'); ?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

               if($this->session->flashdata('confirmmsg'))  
                echo $this->session->flashdata('confirmmsg');

               if($this->session->flashdata('cancelmsg'))  
                echo $this->session->flashdata('cancelmsg');
          ?>   
           <div class="row">
              <div class="col-lg-12">
                      <?php echo form_open('performancereports',array('id' => 'reportform'));  ?>         
                
                 <div class="correct-height width-3"><?php $alluser =  $page_var['usersData']; echo form_label($this->lang->line('user') .' '. $this->lang->line('name')); echo form_dropdown('userid', $alluser, set_value('userid'),'class="form-control" style="width:100%" id="userid" onchange="this.form.submit()"'); ?></div>
                 <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('from')); echo form_input(array('name' => 'startdate','id'=>'startdate','class'=>'form-control','value'=>set_value('startdate')));  ?>
                    <input type="button" value="select" id="startdatepicker" style="display:none"></div>
                  <div class="correct-height date width-3"><?php echo form_label($this->lang->line('date').' '. $this->lang->line('to')); echo form_input(array('name' => 'enddate','id'=>'enddate','class'=>'form-control','value'=>set_value('enddate')));  ?>
                    <input type="button" value="select" id="enddatepicker" style="display:none"></div>
                 <div class="correct-height width-3 last-btn"><input type="button" value="<?php echo $this->lang->line('reset')?>" onclick="resetvalue()" style="position:relative;left:0px;"></div>
                 <?php echo form_close(); ?> 
              </div>
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                     <div class="panel-heading">
                          
                        </div>
                                                            
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive chg-scroll">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('user'); ?> <?php echo $this->lang->line('name'); ?></th>
                                            
                                            <th><?php echo $this->lang->line('no'); ?>  <?php echo $this->lang->line('of'); ?> <?php echo $this->lang->line('services'); ?> <?php echo $this->lang->line('created'); ?></th>
                      
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php foreach($page_var['performresults'] as $srvdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php $usrarr = $this->supervisorsmodel->getById($srvdetails['createdby']); echo htmlentities($usrarr['name'],ENT_QUOTES); ?></td>
                                            <td><?php echo $srvdetails['srvcount'] ?></td>

                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>

                            
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
  

</div>

<script type="text/javascript">
     
     var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
              "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": showing + " 0 to 0 of 0 " + records,
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                 "sZeroRecords": "<?php echo $this->lang->line('nothing');?> <?php echo $this->lang->line('found');?> - <?php echo $this->lang->line('sorry');?>",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": showing + "0 to 0 of 0 " + records,
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

           $(document).ready(function() {
                $('#dataTables-example').DataTable({
                         "oLanguage": english,
                        responsive: true,
                        "aoColumnDefs": [ { "bSortable": false } ]
                });

                 $('#langchange').click(function(){
                  dtable.fnDestroy();
                  dtable = null;
                  currentLang = (currentLang == english) ? indonesian : english;
                  dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
                });

                
            });

  
    function resetvalue()
    {
       $('#userid').val(''); 
      $('#startdate').val('');
      $('#enddate').val('');
      $('#reportform').submit();

    }

    $(function() 
   {   $( "#startdatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','maxDate', jQuery('#enddate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#startdate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });

  $(function() 
 {   $( "#enddatepicker" ).datepicker({
       beforeShow : function(){
            jQuery( this ).datepicker('option','minDate', jQuery('#startdate').val() );
        },
      showOn: "both",
          buttonImage: "<?php echo base_url(); ?>assets/admin/images/calendar.png",
          buttonImageOnly: true,
          onSelect: function (dateText, inst) { $('#enddate').val(dateText);this.form.submit(); },  
         changeMonth:true,
         changeYear:true,
         yearRange:"-100:+0",
         dateFormat:"yy-mm-dd" });
 });



   
 </script>