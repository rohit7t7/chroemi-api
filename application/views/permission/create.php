<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('add');?> <?php echo $this->lang->line('permission');?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>


<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">

              <?php if(isset($page_var['modelData']))
              { echo form_open_multipart('permission/update/'.$page_var['id']); } ?>
              <?php
              echo "<div class='error_msg'>";
              if (isset($error_message)) {
              echo $error_message;
              }
              echo validation_errors();

              echo "</div>";
              ?>


<div class="form-group">
<?php

echo form_label($this->lang->line('supervisor'));
//print_r($page_var['supervisorData']); 
 echo form_input(array('name' => 'supervisor_id','id'=>'supervisor_id','type'=>'hidden')); 
 if(isset($page_var['modelData']) )
 {
  if(count($page_var['modelData'])>0)
  {
    echo form_input(array('name' => 'supervisor','class'=>'form-control','id'=>'supervisor','value'=>$page_var['modelData']['0']['name']));
  }
  else
  {
    echo form_input(array('name' => 'supervisor','class'=>'form-control','id'=>'supervisor','value'=>$page_var['supervisorData']['name']));
  }
  
 }
 else
 {
  echo form_input(array('name' => 'supervisor','class'=>'form-control','id'=>'supervisor',set_value('supervisor')));
 }
 
 ?>  
</div>
<?php if(!isset($page_var['modelData']) ||  count($page_var['modelData'])==0) { ?>
<div class="form-group">
<?php echo form_label($this->lang->line('all').' '.$this->lang->line('permission')); 

 /*$data = array(
    'name'        => 'allpermission',
    'id'          => 'allpermission',
    'value'       => '0',
    'checked'     => FALSE,
    'style'       => 'margin:10px',
    );
 echo form_checkbox($data);*/

for ($row = 0; $row < count($page_var['roleData']); $row++) {

    
    ?>
    <div class="checkbox">
        <label>
    <?php
    //echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], FALSE);
    //echo $page_var['roleData'][$row]['role'];
    if($page_var['roleData'][$row]['id']==3)
    {
        $inputpreferences   = array( 'name'        => 'role_id[]',
                             
                             'value'       => $page_var['roleData'][$row]['id'], 
                             'checked'     => False,                                                    
                             'disabled'    => ''
                            );
        echo form_checkbox($inputpreferences).$page_var['roleData'][$row]['role'];
    }
    else
    {
        echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], False,"class=checkbox_permission");
        echo $page_var['roleData'][$row]['role'];
    }
    ?>
    </label>
    </div>
    <?php
    
    
} ?>
<input type="hidden" name="role_id[]" value="3" >
</div>
<?php } else { ?>
<div class="form-group">
<?php
//echo $page_var['modelData'][0]['assignedrolesid'];
$roleid =explode(",",$page_var['modelData'][0]['assignedrolesid']);
 echo form_label('All Permission:');

  $data = array(
    'name'        => 'allpermission',
    'id'          => 'allpermission',
    'value'       => '0',
    'checked'     => FALSE,
    'style'       => 'margin:10px',
    );
 echo form_checkbox($data);

for ($row = 0; $row < count($page_var['roleData']); $row++) {

    
    if(in_array($page_var['roleData'][$row]['id'], $roleid ))
    {
        ?>
    <div class="checkbox">
        <label>
    <?php
        //echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], TRUE);
        //echo $page_var['roleData'][$row]['role'];
    if($page_var['roleData'][$row]['id']==3)
    {
        $inputpreferences   = array( 'name'        => 'role_id[]',
                             
                             'value'       => $page_var['roleData'][$row]['id'], 
                             'checked'     => true,                                                    
                             'disabled'    => ''
                            );
        echo form_checkbox($inputpreferences).$page_var['roleData'][$row]['role'];
    }
    else
    {
        echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], true,"class=checkbox_permission");
        echo $page_var['roleData'][$row]['role'];
    }
        ?>
    </label>
    </div>
    <?php
    }
    else
    {
      ?>
    <div class="checkbox">
        <label>
    <?php
        //echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], FALSE);
        //echo $page_var['roleData'][$row]['role'];
    if($page_var['roleData'][$row]['id']==3)
    {
        $inputpreferences   = array( 'name'        => 'role_id[]',
                             
                             'value'       => $page_var['roleData'][$row]['id'], 
                             'checked'     => False,                                                    
                             'disabled'    => ''
                            );
        echo form_checkbox($inputpreferences).$page_var['roleData'][$row]['role'];
    }
    else
    {
        echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], False,"class=checkbox_permission");
        echo $page_var['roleData'][$row]['role'];
    }
        ?>
    </label>
    </div>
    <?php
    }
    
}

 ?>
 <input type="hidden" name="role_id[]" value="3" >
</div>
<?php }  ?>
<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 


 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>
 <script>
  $(document).ready(function() {
    $('#allpermission').click(function() {
       if(this.checked){
            $('#allpermission').val('1');
            $('.checkbox_permission').each(function(){
                this.checked = true;
            });
        }else{
              $('#allpermission').val('0');
             $('.checkbox_permission').each(function(){
                this.checked = false;
            });
        }
    });


    });

  $('.checkbox_permission').click(function(){ 
  
       /* if(this.checked == false)
        { 
            $("#allpermission").val('0'); 
            $("#allpermission").attr('checked',false);
        }*/

     if ($('.checkbox_permission:checked').length == $('.checkbox_permission').length )
       {
             
            $("#allpermission").val('1'); 
            $("#allpermission")[0].checked =true;
                    
               
        }
        else {
          $("#allpermission").val('0'); 
           $("#allpermission")[0].checked =false;
        }

    });

  $(function() {


    var availableTags = [ 
    <?php 

    if(count($page_var['superData'])>0)
          {
            $i=1;
            $val="";
            foreach ($page_var['superData'] as $key => $value) {
              //echo '{"'.$value['name'].'"}';
              echo '{"label":"'.$value['name'].'('.$value['email'] .')", "value":"'.$value['name'].'", "id": '.$value['id'].'}';
              if(count($page_var['superData'])!=$i)
              echo ',';
              $i++;
            }
          }?>
      
    ];
    $( "#supervisor" ).autocomplete({
      source: availableTags,
     select: function (event, ui) {
      window.location.href = "<?php echo base_url(); ?>permission/update/"+ui.item.id;
      //alert(ui.item.id); 
      $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {ui:{value:$(this).val()}});
    } 
    });
  });
  $('#superid').on('blur', function(){ 
});
  </script>

