<?php if(count($page_var['modelData']) > 0) {?>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('edit');?> <?php echo $this->lang->line('permission');?></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  <?php $hidden = array('id' => $page_var['modelData'][0]['id']); 
  echo form_open_multipart('permission/edit/'.$page_var['modelData'][0]['id'].'/'.$page_var['modelData'][0]['supervisor_id'],'',$hidden); ?>

<div class = "row">
<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">



            <?php
            echo "<div class='error_msg'>";
            if (isset($error_message)) {
            echo $error_message;
            }
            echo validation_errors();

            echo "</div>";
            ?>


<div class="form-group">
<?php
echo form_label($this->lang->line('supervisor').': '); 
 echo $page_var['modelData']['0']['name'];
 ?>  
</div>

<!--<div class="form-group">
 
<?php echo form_label('Role :'); 
echo form_dropdown('role_id', $page_var['roleData'], $page_var['modelData']['role_id'],'class="form-control"'); ?>

</div>-->
<div class="form-group">
<?php
//echo $page_var['modelData'][0]['assignedrolesid'];
$roleid =explode(",",$page_var['modelData'][0]['assignedrolesid']);

$allpermissionval = $page_var['modelData'][0]['allpermission'];


if($allpermissionval == 1)
{
    $checkedstatus = TRUE;
    $chkval =1;
}
else
{
    $checkedstatus = FALSE;
    $chkval = 0;
}

?>

<?php echo form_label($this->lang->line('all').' '.$this->lang->line('permission'));
 $data = array(
    'name'        => 'allpermission',
    'id'          => 'allpermission',
    'value'       => $chkval,
    'checked'     => $checkedstatus,
    'style'       => 'margin:10px',
    );
 echo form_checkbox($data);

for ($row = 0; $row < count($page_var['roleData']); $row++) {

    if(in_array($page_var['roleData'][$row]['id'], $roleid ))
    {
        ?>
    <div class="checkbox">
        <label>
    <?php
        
    if($page_var['roleData'][$row]['id']==3)
    {
        $inputpreferences   = array( 'name'        => 'role_id[]',
                             
                             'value'       => $page_var['roleData'][$row]['id'], 
                             'checked'     => true,                                                    
                             'disabled'    => ''
                            );
        echo form_checkbox($inputpreferences).$page_var['roleData'][$row]['role'];
    }
    else
    {
        echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], TRUE,"class=checkbox_permission");
        echo $page_var['roleData'][$row]['role'];
    }
    
        ?>
    </label>
    </div>
    <?php
    }
    else
    {
      ?>
    <div class="checkbox">
        <label>
    <?php
        if($page_var['roleData'][$row]['id']==3)
    {
        $inputpreferences   = array( 'name'        => 'role_id[]',
                             
                             'value'       => $page_var['roleData'][$row]['id'], 
                             'checked'     => False,                                                    
                             'disabled'    => ''
                            );
        echo form_checkbox($inputpreferences).$page_var['roleData'][$row]['role'];
    }
    else
    {
        echo form_checkbox('role_id[]', $page_var['roleData'][$row]['id'], False,"class=checkbox_permission");
        echo $page_var['roleData'][$row]['role'];
    }
        ?>
    </label>
    </div>
    <?php
    }
    
}


 ?>
 <input type="hidden" name="role_id[]" value="3" >
</div>

<div class="form-group">
<?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('submit'))); 
 echo form_close(); ?> 

 </div>  
            
 </div>        

 </div>

 </div>  
            
 </div>        

 </div>
 </div>
 </div>

 <?php }else{ ?>
  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><div class='alert alert-danger info-error'><img src="<?php echo base_url()?>assets/admin/images/back-icon.png" onclick="window.history.back();">Failed to find the entry</div></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>
  </div>          

<?php } ?>

<script>

   $(document).ready(function() {
    $('#allpermission').click(function() {
       if(this.checked){
            $('.checkbox_permission').each(function(){
                $('#allpermission').val('1');    
                this.checked = true;
            });
        }else{
             $('.checkbox_permission').each(function(){
                $('#allpermission').val('0');  
                this.checked = false;
            });
        }
    });


      });

    $('.checkbox_permission').click(function(){ 
  
       /* if(this.checked == false)
        { 
            $("#allpermission").val('0'); 
            $("#allpermission").attr('checked',false);
        }*/

        if ($('.checkbox_permission:checked').length == $('.checkbox_permission').length )
         {

         
            $("#allpermission").val('1'); 
            $("#allpermission")[0].checked =true;
                
           
    }
    else {
      $("#allpermission").val('0'); 
       $("#allpermission")[0].checked =false;
    }

    });

</script>