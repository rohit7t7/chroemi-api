<style>
.roleclass {
    width: 30em;
    border: 1px solid #000000;
    word-wrap: break-word;
}
</style>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $this->lang->line('permission'); ?><a href="<?php echo base_url(); ?>permission/create" class="btn btn-default move-up pull-right"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('permission'); ?></a></h1>
                </div>
                <!-- /.col-lg-12 -->

               
            </div>

          <?php 

          if($this->session->flashdata('msg'))  
                echo $this->session->flashdata('msg');

              if($this->session->flashdata('updatemsg'))  
                echo $this->session->flashdata('updatemsg');

             if($this->session->flashdata('deletemsg'))  
                echo $this->session->flashdata('deletemsg');

            if($this->session->flashdata('uploadmsg'))  
                echo $this->session->flashdata('uploadmsg');
          ?>           
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                         
                        </div>
                       
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                             <th><?php echo $this->lang->line('supervisor'); ?></th>
                                             <th><?php echo $this->lang->line('role'); ?></th>
                                            <th><?php echo $this->lang->line('action'); ?></th>
                                           
                                            
                                        </tr>
                                    </thead>

                                    
                                    <tbody>
                                    <?php foreach($page_var as $usrdetails) { ?>
                                        <tr class="odd gradeX">
                                            <td><a href="<?php echo base_url(); ?>supervisors/details/<?php echo $usrdetails['supervisor_id'] ?>"><?php echo $usrdetails['name']; ?></a></td>
                                            <td class="roleclass"><?php echo $usrdetails['assignedroles']; ?></td>
                                            <td><a href="<?php echo base_url(); ?>permission/edit/<?php echo $usrdetails['id'] ?>/<?php echo $usrdetails['supervisor_id'] ?>"><i class="fa fa-edit"></i></a>&nbsp;
                                            <a href="#" 
                                             <?php if($noaction=='0') { ?>
                                            onClick="show_confirm(<?php echo $usrdetails['id'];?>,<?php echo $usrdetails['supervisor_id'];?>)"

                                            <?php } else { ?>
                                                    onClick="javascript: alert('<?php echo $this->lang->line("you")?> <?php echo $this->lang->line("are"); ?> <?php echo $this->lang->line("not"); ?> <?php echo $this->lang->line("allowed"); ?> <?php echo $this->lang->line("to"); ?> <?php echo $this->lang->line("perform"); ?> <?php echo $this->lang->line("the"); ?> <?php echo $this->lang->line("operation"); ?>');"
                                                    <?php } ?> ><i class="fa fa-trash"></i></a></td>
                                            
                                           
                                        </tr>
                                    
                                      <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div> 

<script type="text/javascript">
     
     var show = "<?php echo $this->lang->line('show');?>";
        var entries = "<?php echo $this->lang->line('entries');?>";
         var showing = "<?php echo $this->lang->line('showing');?>";
         var records = "<?php echo $this->lang->line('records');?>";
         var oflang = "<?php echo $this->lang->line('of');?>";
         var tolang = "<?php echo $this->lang->line('to');?>";
          var searchlang = "<?php echo $this->lang->line('search');?>";
     
     var english = {"sLengthMenu": show + " _MENU_ " + entries,
              "sZeroRecords": "Nothing found - sorry",
              "sInfo": showing + " _START_ " + tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
              "sInfoEmpty": "Showing 0 to 0 of 0 records",
              "sInfoFiltered": "(filtered from _MAX_ total records)",
               "sSearch": searchlang,
               "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
          };

          var indonesian = {"sProcessing": "Procesando...",
                 "sLengthMenu": show + " _MENU_ " + entries,
                 "sZeroRecords": "No se encontraron resultados",
                 "sInfo": showing + "_START_ "+ tolang +" _END_ "+ oflang +" _TOTAL_ " + records,
                 "sInfoEmpty": "No existen registros",
                 "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
                 "sInfoPostFix": "",
                 "sSearch":  searchlang,
                 "sUrl": "",
                  "oPaginate": {
                "sFirst":    "<?php echo $this->lang->line('first');?>",
                "sPrevious": "<?php echo $this->lang->line('previous');?>",
                "sNext":     "<?php echo $this->lang->line('next');?>",
                "sLast":     "<?php echo $this->lang->line('last');?>"
                }
                };

                var currentLang = english;

                $(document).ready(function() {
                    $('#dataTables-example').DataTable({
                            "oLanguage": english,
                            responsive: true,
                            "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 2 ] } ]
                    });

                         $('#langchange').click(function(){
                        dtable.fnDestroy();
                        dtable = null;
                        currentLang = (currentLang == english) ? indonesian : english;
                        dtable = $('#dataTables-example').dataTable( {"oLanguage": currentLang} );
                      });
                });


    function show_confirm(gotoid,supervisorid)
    {

        var r=confirm("<?php echo $this->lang->line('do')?> <?php echo $this->lang->line('you')?> <?php echo $this->lang->line('really')?> <?php echo $this->lang->line('want')?> <?php echo $this->lang->line('to')?> <?php echo $this->lang->line('delete')?>?");

        if(r==true)
        {
           window.location="<?php echo base_url();?>permission/delete/"+gotoid+"/"+supervisorid;

        }
    }

 </script>