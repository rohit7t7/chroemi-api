<!DOCTYPE html>
<html lang="en">
<?php echo $header; ?>
<body>
     <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top border-bt" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/admin/images/logo.png" class="admin-logo"/></a>
            </div>
            <!-- /.navbar-header -->

              <ul class="nav navbar-top-links navbar-right">

                  <li class="dropdown correct-line">

                    <select id="langchange" onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;">
                    <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?> >English</option>
                    <option value="indonesia" <?php if($this->session->userdata('site_lang') == 'indonesia') echo 'selected="selected"'; ?>>Indonesian</option>
                    </select>
                    <?php $loggedinusername = $this->session->userdata('logged_in'); ?>
                    <p> <?php echo $this->lang->line('welcome_message')?> <?php echo $loggedinusername['name']; ?></p>

                    <a class="dropdown-toggle user" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!--li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li-->
                        <li><a href="<?php echo base_url(); ?>dashboard/logout"><i class="fa fa-sign-out fa-fw"></i> <?php echo $this->lang->line('logout'); ?></a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                          <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" id="globalsearchval" value="<?php echo $this->session->userdata('search_data'); ?>" placeholder="<?php echo $this->lang->line('search'); ?>...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"  onclick="javascript:window.location.href='<?php echo base_url(); ?>globalsearch/supervisorsearch/'+ $('#globalsearchval').val();">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard fa-fw"></i> <?php echo $this->lang->line('dashboard'); ?> </a>
                        </li>

                        
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('attendee'); ?> <?php echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>attendee"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('attendee'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>allattendee/create"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('attendee'); ?></a>
                                </li>
                               
                            </ul>

                        </li>

                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('organizer'); ?> <?php echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>organizers"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('organizer'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>allorganizers/create"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('organizer'); ?></a>
                                </li>
                               
                            </ul>

                        </li>

                         <!-- <li>
                            <a href="<?php //echo base_url(); ?>permission"> <i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('permission'); ?> <?php //echo $this->lang->line('management'); ?></a>
                        </li> -->

                          <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> <?php echo $this->lang->line('category'); ?> <?php echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>eventcategory"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('category'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>alleventcategory/create"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('category'); ?></a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                         <!-- <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> <?php //echo $this->lang->line('type'); ?> <?php //echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php //echo base_url(); ?>servicetype"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('all'); ?> <?php //echo $this->lang->line('service'); ?> <?php //echo $this->lang->line('type'); ?></a>
                                </li>
                                
                               
                            </ul>
                            <!-- /.nav-second-level 
                        </li>-->

                       <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> <?php echo $this->lang->line('event'); ?> <?php echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>events"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('event'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>allevents/create"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('event'); ?></a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                         

                      <!--   <li>
                            <a href="<?php //echo base_url(); ?>contactus"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('contact'); ?> <?php //echo $this->lang->line('us'); ?> <?php //echo $this->lang->line('management'); ?></a>
                        </li> -->

                        

                         

                        <!--<li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('service'); ?> <?php //echo $this->lang->line('orders'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php //echo base_url(); ?>serviceorder/latestorder"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('latest'); ?> <?php //echo $this->lang->line('orders'); ?></a>    
                                </li>
                                 <li>
                                    <a href="<?php //echo base_url(); ?>serviceorder/allorder"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('all'); ?> <?php //echo $this->lang->line('orders'); ?></a>    
                                </li>
                            </ul>

                        </li>-->

                         <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('content'); ?> <?php echo $this->lang->line('management'); ?><span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url(); ?>content"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('content'); ?></a>    
                                </li>
                                 <li>
                                    <a href="<?php echo base_url(); ?>allcontent/create"><i class="fa fa-table fa-fw"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('content'); ?></a>    
                                </li>
                            </ul>
                        </li>

                        <!-- <li>
                            <a href="<?php //echo base_url(); ?>paymenthistory"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('payment'); ?> <?php //echo $this->lang->line('history'); ?></a>
                        </li> -->

                        <!--<li>
                           <a href="#"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('refund'); ?> <?php //echo $this->lang->line('info'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                   <a href="<?php //echo base_url(); ?>refundhistory"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('refund'); ?> <?php //echo $this->lang->line('history'); ?></a>
                               </li>
                                <li>
                                   <a href="<?php //echo base_url(); ?>refundrequest"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('refund'); ?> <?php //echo $this->lang->line('request'); ?></a>
                               </li>
                           </ul>
                       </li> -->
                       
                    <!--     <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('settings'); ?> <?php //echo $this->lang->line('management'); ?><span class="fa arrow"> </span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php //echo base_url(); ?>commission"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('commission'); ?> </a>    
                                </li>
                               
                            </ul>
                        </li> -->

                        <!-- <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('payment'); ?> <?php //echo $this->lang->line('management'); ?><span class="fa arrow"> </span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php //echo base_url(); ?>paymentgateway"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('payment'); ?> <?php //echo  $this->lang->line('gateways'); ?> </a>    
                                </li>

                                <li>
                                    <a href="<?php //echo base_url(); ?>paymentcredentials"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('payment'); ?> <?php //echo  $this->lang->line('credentials'); ?></a>    
                                </li>

                                                             
                            </ul>
                        </li> -->

                        

                         

                         <!--li>
                            <a href="<?php //echo base_url(); ?>reports"><i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('reports'); ?> </a>
                        </li-->

                        
			<!--<li>
                            <a href="<?php //echo base_url(); ?>emailtemplate"> <i class="fa fa-table fa-fw"></i> <?php //echo $this->lang->line('email'); ?> <?php //echo $this->lang->line('Template'); ?> <?php //echo $this->lang->line('management'); ?></a>
                        </li>-->
              
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

       
<div id="content">
<?php echo $content; ?>
</div>

<div id="footer">
<?php echo $footer; ?>
</div>
</body>
</html>

