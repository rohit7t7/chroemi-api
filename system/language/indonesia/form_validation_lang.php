<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Itu {field} bidang aku s wajib..';
$lang['form_validation_isset']			= 'Itu {field} bidang harus memiliki Sebuah nilai.';
$lang['form_validation_valid_email']		= 'Itu {field} bidang harus berisi Sebuah sah e-mail alamat.';
$lang['form_validation_valid_emails']		= 'Itu {field} bidang harus berisi semua sah e-mail alamat.';
$lang['form_validation_valid_url']		= 'Itu {field} bidang harus berisi Sebuah sah URL.';
$lang['form_validation_valid_ip']		= 'Itu {field} bidang harus berisi Sebuah sah AKU P.';
$lang['form_validation_min_length']		= 'Itu {field} bidang harus menjadi di paling sedikit {param} karakter di panjangnya.';
$lang['form_validation_max_length']		= 'Itu {field} bidang tidak bisa melebihi {param} karakter di panjangnya.';
$lang['form_validation_exact_length']		= 'Itu {field} bidang harus menjadi persis {param} karakter di panjangnya.';
$lang['form_validation_alpha']			= 'Itu {field} bidang mungkin hanya berisi alfabetis karakter.';


$lang['form_validation_alpha_numeric']		= 'Itu {field} bidang mungkin hanya berisi alpha - numerik karakter.';
$lang['form_validation_alpha_numeric_spaces']	= 'Itu {field} bidang mungkin hanya berisi alpha - numerik karakter dan spasi.';
$lang['form_validation_alpha_dash']		= 'Itu {field} bidang mungkin hanya berisi alpha - numerik karakter, garis bawah, dan strip.';
$lang['form_validation_numeric']		= 'Itu {field} bidang harus berisi hanya nomor.';
$lang['form_validation_is_numeric']		= 'Itu {field} bidang harus berisi hanya numerik karakter.';

$lang['form_validation_integer']		= 'Itu {field} bidang harus berisi sebuah bilangan bulat.';
$lang['form_validation_regex_match']		= 'Itu {field} bidang aku s tidak di itu benar format.';
$lang['form_validation_matches']		= 'Itu {field} bidang tidak tidak pertandingan itu {param} bidang.';
$lang['form_validation_differs']		= 'Itu {field} bidang harus berbeda dari itu {param} bidang.';
$lang['form_validation_is_unique'] 		= 'Itu {field} bidang harus berisi Sebuah unik nilai.';

$lang['form_validation_is_natural']		= 'Itu {field} bidang harus hanya berisi digit.';
$lang['form_validation_is_natural_no_zero']	= 'Itu {field} bidang harus hanya berisi digit dan harus menjadi 
lebih besar dari nol.';

$lang['form_validation_decimal']		= 'Itu {field} bidang harus berisi Sebuah desimal jumlah.';

$lang['form_validation_less_than']		= 'Itu {field} bidang harus berisi Sebuah jumlah kurang dari {param}.';

$lang['form_validation_less_than_equal_to']	= 'Itu {field} bidang harus berisi Sebuah jumlah kurang dari atau sama untuk {param}.';

$lang['form_validation_greater_than']		= 'Itu {field} bidang harus berisi Sebuah jumlah lebih besar dari {param}.';

$lang['form_validation_greater_than_equal_to']	= 'Itu {field} bidang harus berisi Sebuah jumlah lebih besar dari atau sama untuk {param}.';

$lang['form_validation_error_message_not_set']	= 'tidak untuk mengakses sebuah kesalahan pesan sesuai untuk anda bidang nama {field}.';

$lang['form_validation_in_list']		= 'Itu {field} bidang harus menjadi satu dari: {param}.';
